<!doctype html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Recibo de condominio #{{ $invoice_id }}</title>
        
            <style type="text/css">
                @page {
                    margin: 0px;
                }
        
                body {
                    margin: 0px;
                }
        
                * {
                    font-family: Verdana, Arial, sans-serif;
                }
        
                a {
                    color: #fff;
                    text-decoration: none;
                }
        
                table {
                    font-size: x-small;
                }
        
                tfoot tr td {
                    font-weight: bold;
                    font-size: x-small;
                }
        
                .invoice table {
                    margin: 0px;
                }
        
                .invoice h4 {
                    margin-left: 15px;
                }
        
                .user_information {
                    background-color: #fff;
                    padding:5px;
                }

                .information {
                    border-bottom: solid 1px #93273d;
                    
                }
        
                .information .logo {
                    margin: 5px;
                }
        
                .information table {
                    padding: 10px;
                    padding-bottom:-10px;
                }
            </style>
        
        </head>
        <body>
        
        <div class="information">
            <table width="100%">
                <tr>
                    <td align="left" style="width: 50%;">
                    <img 
                        src="logo.jpg"
                        alt="Logo"
                        width="120"
                        class="logo"
                    />
                    <h3>Inmobiliaria Terepaima, C.A.
                        <br/><span style="font-size:9px;">RIF J-284892934</span>
                    </h3>
                    <p>
                        Telefono: 0212 245 12 11
                        <br />
                        Email: inmobiliariaterepaima@gmail.com
                        <br />
                        www.inmobiliariaterepaima.com
                    </p>
                    </td>
                    <td align="right" style="width: 50%;">
                        <h2 style="text-size:16px;">Recibo de condominio {{ $invoice_month_date }} {{  $invoice_emit_date }}</h2>
                        <p style="font-size:16px;">
                            {{ $property_name }}
                            <br />
                            {{ $residence_name }} - {{ $property_number }}
                            <br />
                            RIF: {{ $residence_rif }}                            
                        </p>                     
                        <p>
                            Alicuota: {{ $property_aliquot }}%
                        </p>
                    </td>
                </tr>        
            </table>
        </div>
        <div class="account_information">
            <table width="100%" style="height:95px;">
                <tr>
                    <td align="left" style="width: 50%;">
                        <table style="float:left;">
                            <tbody>
                                <tr>
                                    <td colspan="1" style="text-align:center;"> Datos para acceder a tu cuenta </td>
                                </tr>
                                <tr>
                                    <td colspan="1" style="text-align:center;"><strong>http://condominio.inmobiliariaterepaima.com/</strong></td>
                                </tr>
                                <tr>
                                    <td> Email: {{ $user_email }}</td>
                                    <td>  </td>
                                </tr>
                                <tr>
                                    <td> Clave: {{ $user_password }}</td>
                                    <td>  </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td align="right" style="width: 50%;">
                        <table style="float:right; font-size:11px; width:68%;">
                            <tbody>
                                <tr>
                                    <td> Datos para realizar tu pago MERCANTIL </td>
                                </tr>
                                <tr>
                                    <td>0115-5478-5521-7441-2544 </td>
                                </tr>
                                <tr>
                                    <td>Titular: RESIDENCIAS MONT BLANC</td>
                                </tr>
                                <tr>
                                    <td>Pago movil: 0416 473 91 33</td>
                                </tr>
                                <tr>
                                    <td>Rif: J30852654</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>        
            </table>
        </div>
        <div class="user_information">
            <table width="100%" style="height:65px;">
                <tr>
                    <td align="left" style="width: 50%;">
                        <table style="float:left;">
                            <tbody>
                                <tr>
                                    <td> Tasa </td>
                                    <td> {{ $exchange_provider }} </td>
                                </tr>
                                <tr>
                                    <td> Cambio del dia </td>
                                    <td> {{ $exchange_rate }} </td>
                                </tr>
                                <tr>
                                    <td> Monto de la tasa </td>
                                    <td> {{ $exchange_amount }} Bs. </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td align="right" style="width: 50%; ">
                        <table style="float:right; font-size:12px; background-color:#e2e2e2; padding:5px;">
                            <tbody>
                                <tr>
                                    <td> Monto del recibo </td>
                                    <td align="right"><strong>{{ $invoice_total_amount_ves }}  Bs.</strong></td>
                                    <td align="right"><strong>{{ $invoice_total_amount_usd }} $</strong> </td>
                                </tr>
                                <tr>
                                    <td>  Abonos </td>
                                    <td align="right"> <strong>{{ $invoice_total_balance_usd }} Bs.</strong>  </td>
                                    <td align="right"> <strong>{{ $invoice_total_balance_ves }} $</strong>  </td>
                                </tr>
                                <tr>
                                    <td> Total (Deuda)</td>
                                    <td align="right"> <strong>{{ $invoice_total_usd }} Bs.</strong>  </td>
                                    <td align="right"> <strong>{{ $invoice_total_ves }} $</strong> </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>        
            </table>
        </div>
        <div class="invoice">
            <table width="100%" style="font-size:10px;">
                <thead>
                <tr>
                    <th width="250">GASTOS COMUNES</th>
                    <th width="70">Fecha</th>
                    <th width="85">Total</th>
                    <th width="85">Tu aporte</th>
                </tr>
                </thead>
                <tbody>                
                    @foreach($invoice_items_expenses as $invoice_item_expense)
                    <tr>
                        <td width='250'>{{ $invoice_item_expense['expense'] }}</td>
                        <td width='70'>{{ $invoice_item_expense['date_expense'] }}</td>
                        <td width='85' align='right'>{{ $invoice_item_expense['amount_expense'] }} Bs.</td>
                        <td width='85' align='right'>{{ $invoice_item_expense['invoice_expense'] }} Bs.</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td width='250'>APORTE AL FONDO DE RESERVA</td>
                        <td width='70'>07/2021</td>
                        <td width='85' align='right'>300.000,00 Bs.</td>
                        <td width='85' align='right'>18.900,00 Bs.</td>
                    </tr>
                </tbody>
        
                <tfoot>
                    <tr>
                        <td colspan="2" width="320" align="right"  style="font-size:10px;">Total gastos comunes</td>
                        <td width="85" align="right"  style="font-size:10px;">{{ $invoice_total_expenses_usd }} Bs.</td>
                        <td width="85" align="right"  style="font-size:10px;">{{ $invoice_total_expenses_ves }} Bs.</td>
                    </tr>
                </tfoot>
            </table>
            <table width="100%" style="font-size:10px;">
                <tbody>  
                    <tr>
                        <td width='250'></td>
                        <td width='70'>07/2021</td>
                        <td width='85' align='right'>00,00 Bs.</td>
                        <td width='85' align='right'>00,00 Bs.</td>
                    </tr>              
                    <tr>
                        <td width='250'>-</td>
                        <td width='70'>07/2021</td>
                        <td width='85' align='right'>00,00 Bs.</td>
                        <td width='85' align='right'>00,00 Bs.</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2" width="320" align="right"  style="font-size:10px;">Total gastos no comunes</td>
                        <td width="85" align="right"  style="font-size:10px;">00,00 Bs.</td>
                        <td width="85" align="right"  style="font-size:10px;">00,00 Bs.</td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <hr/>
        <div class="invoice" >
            <table width="100%" style="font-size:10px;">
                <thead>
                <tr>
                    <th width="250">Descripcion</th>
                    <th width="60">Saldo Anterior</th>
                    <th width="60">Debe</th>
                    <th width="60">Haber</th>
                    <th width="60">Saldo</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($invoice_items_funds as $invoice_item_fund)
                    <tr>
                        <td width='250'>{{ $invoice_item_fund['concept'] }}</td>
                        <td width='60' align='right'>{{ $invoice_item_fund['last_balance'] }} Bs.</td>
                        <td width='60' align='right'>{{ $invoice_item_fund['expense'] }} Bs.</td>
                        <td width='60' align='right'>{{ $invoice_item_fund['income'] }} Bs.</td>
                        <td width='60' align='right'>{{ $invoice_item_fund['balance'] }} Bs.</td>
                    </tr> 
                    @endforeach
                </tbody>        
                <tfoot >
                    <tr>
                        <td width="250" align="right" style="font-size:10px;">Total</td>
                        <td width="60" align="right" style="font-size:10px;">22.246.326,00 Bs.</td>
                        <td width="60" align="right" style="font-size:10px;">00,00 Bs.</td>
                        <td width="60" align="right" style="font-size:10px;">600.000,00 Bs.</td>
                        <td width="60" align="right" style="font-size:10px;">22.846.326,00 Bs.</td>
                    </tr>
                </tfoot>
            </table>
        </div>
        
        <div class="information" style="position: absolute; bottom: 46px;">
            <table width="100%" style="height:95px;">
                <tr>
                    <td align="left" style="width: 50%;">
                        <table style="float:left;">
                            <tbody>
                                <tr>
                                    <td style="text-align:center;"> 
                                        Al acumular 3 recibo(s) sin cancelar su cuenta sera pasada a Departamento Legal sin previo aviso. Evite cargos adicionales
                                        <br/>
                                        El pago del recibo, no implica la cancelacion de anteriores y es nulo son firma autorizada.                                       
                                        </td> 
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td align="right" style="width: 50%;">
                        <table style="float:right; font-size:13px;">
                            <tbody>
                                <tr>
                                    <td style="text-align:center;"> 
                                    NOTA PERSONALIZADA DE LA RESIDENCIA 
                                    <br/>
                                    De no cancelar antes del final de cada mes se generara intereses de mora del 1% mas gastos de cobranza
                                </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>        
            </table>
        </div>
        <div class="information" style="position: absolute; bottom: 0;">
            <table width="100%">
                <tr>
                    <td align="left" style="width: 50%;">
                        www.inmobiliariaterepaima.com
                    </td>
                    <td align="right" style="width: 50%;">
                        Siguenos en Instagram &nbsp; 
                        <img src="instagram.png" style="width:20px;" />
                        <strong>@inmobiliariaterepaima</strong>
                    </td>
                </tr>        
            </table>
        </div>
        </body>
        </html>