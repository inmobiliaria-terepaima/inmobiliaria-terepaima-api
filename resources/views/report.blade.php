<!doctype html>
    <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Recibos Pendientes </title>
        
            <style type="text/css">
                @page {
                    margin: 0px;
                }
        
                body {
                    margin: 0px;
                }
        
                * {
                    font-family: Verdana, Arial, sans-serif;
                }
        
                a {
                    color: #fff;
                    text-decoration: none;
                }
        
                table {
                    font-size: x-small;
                }
        
                tfoot tr td {
                    font-weight: bold;
                    font-size: x-small;
                }
        
                .invoice table {
                    margin-top: 50px;
                    padding:5px;
                    border-bottom: solid 1px #93273d;
                }
        
                .invoice h4 {
                    margin-left: 15px;
                }
        
                .user_information {
                    background-color: #fff;
                    padding:5px;
                }

                .information {
                    border-bottom: solid 1px #93273d;
                    
                }
        
                .information .logo {
                    margin: 5px;
                }
        
                .information table {
                    padding: 10px;
                    padding-bottom:-10px;
                }

                .header th {
                   height: 40px;
                   line-height: 2px;
                }
            </style>
        
        </head>
        <body>
        
        <div class="information">
            <table width="100%">
                <tr>
                    <td align="left" style="width: 50%;">
                    <img 
                        src="logo.jpg"
                        alt="Logo"
                        width="120"
                        class="logo"
                    />
                    <h3>Inmobiliaria Terepaima, C.A.
                        <br/><span style="font-size:9px;">RIF J-284892934</span>
                    </h3>
                    <p>
                        Telefono: 0212 245 12 11
                        <br />
                        Email: inmobiliariaterepaima@gmail.com
                        <br />
                        www.inmobiliariaterepaima.com
                    </p>
                    </td>
                    <td align="right" style="width: 50%;">
                        <h2 style="text-size:16px;">RECIBOS PENDIENTES</h2>
                        <h3 style="text-size:16px;">Fecha {{ date('d/m/Y H:i:s') }}  </h3>
                        <p style="font-size:16px;">                             
                            <br />
                            @foreach ($data as  $drug => $d)
                                @if ($loop->first)
                                    {{ $d['property']['residence']['name'] }}
                                    <br />
                                    RIF: {{ $d['property']['residence']['rif'] }}
                                @endif
                            @endforeach                                            
                        </p>   
                    </td>
                </tr>        
            </table>
        </div>
        <div class="invoice">
            <table width="100%" class="header">
                <thead >
                    <tr>
                        <th width="40" align='left'># Recibo</th>
                        <th width="90" align='left'>Nro apto / casa</th>
                        <th width="150" align='left'>Propietario</th>
                        <th width="50" align='left'>Emision</th>
                        <th width="50" align='left'>Deuda</th>
                        <th width="50" align='left'>Tasa</th>
                        <th width="50" align='left'>USD</th>
                    </tr>
                </thead>
                <tbody>    
                    @foreach($data as  $data_invoice)
                        <tr>
                          <td width='40' align='left'>#{{ $data_invoice->id }}</td>
                          <td width='90' align='left'>{{ $data_invoice->property->number_property }}</td>
                          <td width='150' align='left'> {{ $data_invoice->property->user->name }} </td>
                          <td width='50' align='left'> @dateFormat($data_invoice->created_at) </td>
                          <td width='50' align='left'> @moneyFormat($data_invoice->amount) </td>
                           
                          <td width='50' align='left'> 0.00 </td>
                           
                          <td width='50' align='left'> 0.00 </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>        
        <div class="information" style="position: absolute; bottom: 46px;">
            <table width="100%" style="height:95px;">
                <tr>
                    <td align="left" style="width: 50%;">
                        <table style="float:left;">
                            <tbody>
                                <tr>
                                    <td style="text-align:center;"> 
                                        Al acumular 3 recibo(s) sin cancelar su cuenta sera pasada a Departamento Legal sin previo aviso. Evite cargos adicionales
                                        <br/>
                                        El pago del recibo, no implica la cancelacion de anteriores y es nulo son firma autorizada.                                       
                                        </td> 
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td align="right" style="width: 50%;">
                        <table style="float:right; font-size:13px;">
                            <tbody>
                                <tr>
                                    <td style="text-align:center;"> 

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>        
            </table>
        </div>
        <div class="information" style="position: absolute; bottom: 0;">
            <table width="100%">
                <tr>
                    <td align="left" style="width: 50%;">
                        www.inmobiliariaterepaima.com
                    </td>
                    <td align="right" style="width: 50%;">
                        Siguenos en Instagram &nbsp; 
                        <img src="instagram.png" style="width:20px;" />
                        <strong>@inmobiliariaterepaima</strong>
                    </td>
                </tr>        
            </table>
        </div>
        </body>
    </html>