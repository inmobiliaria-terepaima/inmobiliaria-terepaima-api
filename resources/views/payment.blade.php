<!doctype html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Comprobante de pago #{{{ $data['id'] }}}</title>
        
            <style type="text/css">
                @page {
                    margin: 0px;
                }
        
                body {
                    margin: 0px;
                }
        
                * {
                    font-family: Verdana, Arial, sans-serif;
                }
        
                a {
                    color: #fff;
                    text-decoration: none;
                }
        
                table {
                    font-size: x-small;
                }
        
                tfoot tr td {
                    font-weight: bold;
                    font-size: x-small;
                }
        
                .invoice table {
                    margin-top: 50px;
                    padding:5px;
                    border-bottom: solid 1px #93273d;
                }
        
                .invoice h4 {
                    margin-left: 15px;
                }
        
                .user_information {
                    background-color: #fff;
                    padding:5px;
                }

                .information {
                    border-bottom: solid 1px #93273d;
                    
                }
        
                .information .logo {
                    margin: 5px;
                }
        
                .information table {
                    padding: 10px;
                    padding-bottom:-10px;
                }

                .header th {
                   height: 40px;
                   line-height: 2px;
                }
            </style>
        
        </head>
        <body>
        
        <div class="information">
            <table width="100%">
                <tr>
                    <td align="left" style="width: 50%;">
                    <img 
                        src="logo.jpg"
                        alt="Logo"
                        width="120"
                        class="logo"
                    />
                    <h3>Inmobiliaria Terepaima, C.A.
                        <br/><span style="font-size:9px;">RIF J-284892934</span>
                    </h3>
                    <p>
                        Telefono: 0212 245 12 11
                        <br />
                        Email: inmobiliariaterepaima@gmail.com
                        <br />
                        www.inmobiliariaterepaima.com
                    </p>
                    </td>
                    <td align="right" style="width: 50%;">
                        <h2 style="text-size:16px;">Comprobante de Pago #{{{ $data['id'] }}}  </h2>
                        <h3 style="text-size:16px;">Fecha {{{ $data['created_at'] }}}  </h3>
                        <p style="font-size:16px;">                             
                            <br />
                            {{{ $data['account_residence']['residence']['name'] }}}
                            <br />
                            RIF: {{{ $data['account_residence']['residence']['rif'] }}}                    
                        </p>   
                    </td>
                </tr>        
            </table>
        </div>
        <div class="user_information">
            <table width="100%" style="height:65px;">
                <tr>
                    <td align="left" style="width: 50%;">
                        <table style="float:left;">
                            <tbody>
                                <tr>
                                    <td> Propietario </td>
                                    <td> {{{ $data['user']['name'] }}} </td>
                                </tr>
                                <tr>
                                    <td> Email </td>
                                    <td> {{{ $data['user']['email'] }}}  </td>
                                </tr>
                                <tr>
                                    <td> Telefono </td>
                                    <td> {{{ $data['user']['phone'] }}}  </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td align="right" style="width: 50%; ">
                        <table style="float:right; font-size:12px; background-color:#e2e2e2; padding:5px;">
                            <tbody>
                                <tr>
                                    <td> Pagado a: </td>
                                    <td align="right"><strong>  {{{ $data['account_residence']['name'] }}} </strong></td>
                                </tr>
                                <tr>
                                    <td> Titular: </td>
                                    <td align="right"><strong>  {{{ $data['account_residence']['account_holder'] }}} </strong></td>
                                </tr>
                                @if ($data['account_residence']['type'] == 1)
                                <tr>
                                    <td> Numero de cuenta: </td>
                                    <td align="right"><strong>  {{{ $data['account_residence']['number'] }}} </strong></td>
                                </tr>
                                @endif
                                @if ($data['account_residence']['type'] == 2)
                                <tr>
                                    <td> Telefono: </td>
                                    <td align="right"><strong>  {{{ $data['account_residence']['phone'] }}} </strong></td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </td>
                </tr>        
            </table>
        </div>
        <div class="invoice">
            <table width="100%" class="header">
                <thead >
                    <tr>
                        <th width="250">DESCRIPCIÓN</th>
                        <th width="85" align='right'>PAGO UNITARIO</th>
                        <th width="85" align='right'>TOTAL</th>
                    </tr>
                </thead>
                <tbody>                          
                    <tr>

                        <td width='250'>PAGO DE RECIBO DE CONDOMINIO</td>
                        @if ($data['currency_code'] == 'VES')
                        <td width='130' align='right'> @moneyFormat($data['amount']) Bs.</td>
                        @else
                        <td width='130' align='right'> @moneyFormat($data['amount']) {{{ $data['currency_code'] }}}</td>
                        @endif

                        @if ($data['currency_code'] == 'VES')
                        <td width='130' align='right'> @moneyFormat($data['amount']) Bs.</td>
                        @else
                        <td width='130' align='right'> @moneyFormat($data['amount']) {{{ $data['currency_code'] }}}</td>
                        @endif
                        
                    </tr>
                </tbody>
            </table>
        </div>        
        <div class="information" style="position: absolute; bottom: 46px;">
            <table width="100%" style="height:95px;">
                <tr>
                    <td align="left" style="width: 50%;">
                        <table style="float:left;">
                            <tbody>
                                <tr>
                                    <td style="text-align:center;"> 
                                        Al acumular 3 recibo(s) sin cancelar su cuenta sera pasada a Departamento Legal sin previo aviso. Evite cargos adicionales
                                        <br/>
                                        El pago del recibo, no implica la cancelacion de anteriores y es nulo son firma autorizada.                                       
                                        </td> 
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td align="right" style="width: 50%;">
                        <table style="float:right; font-size:13px;">
                            <tbody>
                                <tr>
                                    <td style="text-align:center;"> 

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>        
            </table>
        </div>
        <div class="information" style="position: absolute; bottom: 0;">
            <table width="100%">
                <tr>
                    <td align="left" style="width: 50%;">
                        www.inmobiliariaterepaima.com
                    </td>
                    <td align="right" style="width: 50%;">
                        Siguenos en Instagram &nbsp; 
                        <img src="instagram.png" style="width:20px;" />
                        <strong>@inmobiliariaterepaima</strong>
                    </td>
                </tr>        
            </table>
        </div>
        </body>
        </html>