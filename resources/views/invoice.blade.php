@php
   \Carbon\Carbon::setLocale('es');
@endphp
<!doctype html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Recibo de condominio #{{ $data['id'] }}</title>
        
            <style type="text/css">
                @page {
                    margin: 0px;
                }
        
                body {
                    margin: 0px;
                }
        
                * {
                    font-family: Verdana, Arial, sans-serif;
                }
        
                a {
                    color: #fff;
                    text-decoration: none;
                }
        
                table {
                    font-size: x-small;
                }
        
                tfoot tr td {
                    font-weight: bold;
                    font-size: x-small;
                }
        
                .invoice table {
                    margin: 0px;
                }
        
                .invoice h4 {
                    margin-left: 15px;
                }
        
                .user_information {
                    background-color: #fff;
                    padding:5px;
                }

                .information {
                    border-bottom: solid 1px #93273d;
                    
                }
        
                .information .logo {
                    margin: 5px;
                }
        
                .information table {
                    padding: 10px;
                    padding-bottom:-10px;
                }
            </style>
        
        </head>
        <body>
        
        <div class="information">
            <table width="100%">
                <tr>
                    <td align="left" style="width: 70%;">
                    <img 
                        src="logo.jpg"
                        alt="Logo"
                        width="120"
                        class="logo"
                    />
                    <h3>Inmobiliaria Terepaima, C.A.
                        <br/><span style="font-size:9px;">RIF J-29720334-6</span>
                    </h3>
                    <p>
                        Telefono: (0212) 322 02 77 / (0212) 323 80 53 
                        <br />
                        Email: inmobiliariaterepaima@gmail.com
                        <br />
                        www.inmobiliariaterepaima.com
                        <br />
                            <span style="font-size:9px;">Sede Los Teques: Av. Bermúdez, Edif. Torre Construcción, piso 7, oficina 7D</span>
                        <br />
                            <span style="font-size:9px;">Sede San Antonio: CC Galería las Américas, Piso 2, Local 100</span>
                    </p>
                    </td>
                    <td align="right" style="width: 30%;">
                        <h2 style="text-size:16px;">Recibo de condominio {{ \Carbon\Carbon::parse($data['created_at'])->translatedFormat('F')  }}  </h2>
                        <p style="font-size:16px;">
                            {{  $data['property']['user']['name'] }}
                            <br />
                            {{ $data['property']['residence']['name'] }} - {{  $data['property']['number_property'] }}
                            <br />
                            RIF: {{ $data['property']['residence']['rif'] }}                            
                        </p>                        
                        <p>
                            Alicuota: {{ $data['property']['aliquot'] }}%
                        </p>
                    </td>
                </tr>        
            </table>
        </div>
        <div class="account_information">
            <table width="100%" style="height:70px;">
                <tr>
                    <td align="left" style="width: 50%;">
                        <table >
                            <tbody>
                                <tr>
                                    <td colspan="1" style="text-align:center;">Datos para acceder a tu cuenta </td>
                                </tr>
                                <tr>
                                    <td colspan="1" style="text-align:center; font-size: 10px;"><strong>http://condominio.inmobiliariaterepaima.com/</strong></td>
                                </tr>
                                <tr>
                                    <td> Email: {{  $data['property']['user']['email'] }}</td>
                                    <td>  </td>
                                </tr>
                                <tr>
                                    <td> Clave: {{  $data['property']['user']['password_decode'] }}</td>
                                    <td>  </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td align="right" style="width: 50%;">
                        @foreach($data['property']['residence']['accounts'] as $accounts)
                        <table style="float:right; font-size:11px; width:68%;">
                            <tbody>                                
                                <tr>
                                    <td> Para depositos o transferencias utilice 
                                    @if($accounts['type'] =='1') 

                                        el número  de Cuenta: {{ $accounts['number'] }} {{ $accounts['bank'] }} CI/RIF: {{ $accounts['document_number'] }} a nombre de: {{ $accounts['account_holder'] }}
                                    
                                    @elseif($accounts['type'] =='2') 
                                    
                                        el pago movil: {{ $accounts['number'] }} Telefono: {{ $accounts['phone'] }}
                                    
                                    @endif
                                </tr>
                                @if($accounts['type'] =='3')    
                                <tr>
                                    <td> Cuenta en Dolares (ZELLE) </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                        @endforeach
                    </td>
                </tr>        
            </table>
        </div>
        <div class="user_information">
            <table width="100%" style="height:80px;">
                <tr>
                    <td align="left" style="width: 50%;">
                        <table style="float:left;">
                            <tbody>
                                <tr>
                                    <td> Tasa </td>
                                    <td> {{  $data['invoices_exchanges'][0]['exchange']['name'] }} </td>
                                </tr>
                                <tr>
                                    <td> Cambio del dia </td>
                                    <td> {{ \Carbon\Carbon::parse($data['invoices_exchanges'][0]['created_at'])->format('d-m-Y')   }}  </td>
                                </tr>
                                <tr>
                                    <td> Monto de la tasa </td>
                                    <td> {{  $data['invoices_exchanges'][0]['amount'] }}  Bs. </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td align="right" style="width: 50%; ">
                        <table style="border-collapse: collapse; float:right; font-size:12px; background-color:#e2e2e2; padding:5px;">
                            <tbody>
                                @if($data['count_comun'] > 0)
                                 <tr>
                                    <td> Monto comun </td>
                                    <td align="right"><strong> @moneyFormat($data['amount_comun'] )  Bs. / </strong></td>
                                    <td align="right"><strong> @moneyFormat($data['amount_comun'] / $data['invoices_exchanges'][0]['amount']) $</strong> </td>
                                </tr>
                                @endif
                                @if($data['count_no_comun'] > 0)
                                 <tr>
                                    <td> Monto no comun </td>
                                    <td align="right"><strong> @moneyFormat($data['amount_no_comun'] )  Bs. / </strong></td>
                                    <td align="right"><strong> @moneyFormat($data['amount_no_comun'] / $data['invoices_exchanges'][0]['amount']) $</strong> </td>
                                </tr>
                                @endif
                                <tr style="border-top: 1pt solid #999898;">
                                    <td> Monto del recibo </td>
                                    <td align="right"><strong> @moneyFormat($data['amount'])  Bs. / </strong></td>
                                    <td align="right"><strong> @moneyFormat($data['amount'] / $data['invoices_exchanges'][0]['amount']) $</strong> </td>
                                </tr>
                                @if($data['property']['balances_last']['balance'] > 0)    
                                <tr>
                                    <td>  Abonos </td>
                                    <td align="right"> <strong> @moneyFormat($data['property']['balances_last']['balance']) Bs. /</strong>  </td>
                                    <td align="right"> <strong> @moneyFormat($data['property']['balances_last']['balance'] / $data['invoices_exchanges'][0]['amount']) $</strong>  </td>
                                </tr>
                                @endif
                                @if($data['property']['balances_last']['balance'] > 0)    
                                <tr>
                                    <td> Total (Deuda)</td>
                                    <td align="right"> <strong> Bs.</strong>  </td>
                                    <td align="right"> <strong> $</strong> </td>
                                </tr>
                                @else
                                <tr>
                                    <td> Total (Deuda)</td>
                                    <td align="right"> <strong> 0,00 Bs. / </strong>  </td>
                                    <td align="right"> <strong> 0,00 $</strong> </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </td>
                </tr>        
            </table>
        </div>
        <hr/>
        <div class="invoice" style="padding:20px;">
            <!-- TABLA GASTO COMUN INICIO -->
            @if($data['count_comun'] > 0)
            <table width="100%" style="font-size:10px;">
                <thead>
                <tr>
                    <th width="250"align='left'>GASTOS COMUNES</th>
                    <th width="70" align='left'>Fecha</th>
                    <th width="85" align='left'>Total</th>
                    <th width="85" align='left'>Tu aporte</th>
                </tr>
                </thead>
                <tbody>
                    <?php $total_price = 0 ?>
                    <?php $total_aliquot = 0 ?>                
                    @foreach($data['expenses_invoices'] as $expenses)
                        @if($expenses['expense_fee']['expense']['category'] == 'gasto_comun')    
                            <tr>
                                <td width='250'>{{ $expenses['expense_fee']['concept']['name']  }} 
                                @if($expenses['description'] != '') 
                                    ({{ $expenses['description'] }}) 
                                @endif
                                </td>
                                <td width='70'> {{ \Carbon\Carbon::parse($expenses['created_at'])->format('m-Y')   }} </td>
                                <td width='85' align='left'> @moneyFormat($expenses['amount_fee']) Bs.</td>
                                <td width='85' align='left'> @moneyFormat($expenses['amount']) Bs.</td>
                            </tr>
                            <?php $total_price += $expenses['amount_fee'] ?>
                            <?php $total_aliquot += $expenses['amount']  ?>
                        @endif
                    @endforeach
                </tbody>        
                <tfoot>
                    <tr>
                        <td colspan="2" width="320" align="center"  style="font-size:10px;">Total gastos comunes</td>
                        <td width="85" align="left"  style="font-size:10px;"> @moneyFormat($total_price) Bs.</td>
                        <td width="85" align="left"  style="font-size:10px;"> @moneyFormat($total_aliquot) Bs.</td>
                    </tr>
                </tfoot>
            </table>
            @endif
            <!-- TABLA GASTO COMUN FIN -->
            <hr/>
            <!-- TABLA GASTO NO COMUN INICIO -->
            @if($data['count_no_comun'] > 0)
            <table width="100%" style="font-size:10px;">
                <thead>
                    <tr>
                        <th width="250"align='left'>GASTOS NO COMUNES</th>
                        <th width="70" align='left'>Fecha</th>
                        <th width="85" align='left'>Total</th>
                        <th width="85" align='left'>Tu aporte</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $total_price = 0 ?>
                    <?php $total_aliquot = 0 ?>               
                    @foreach($data['expenses_invoices'] as $expenses)
                        @if($expenses['expense_fee']['expense']['category'] == 'gasto_no_comun')    
                            <tr>
                                <td width='250'>{{ $expenses['expense_fee']['concept']['name']  }}
                                @if($expenses['description'] != '') 
                                    ({{ $expenses['description'] }}) 
                                @endif
                                </td>
                                <td width='70'> {{ \Carbon\Carbon::parse($expenses['created_at'])->format('m-Y')   }} </td>
                                <td width='85' align='left'> @moneyFormat($expenses['amount_fee']) Bs.</td>
                                <td width='85' align='left'> @moneyFormat($expenses['amount']) Bs.</td>
                            </tr>
                            <?php $total_price += $expenses['amount_fee'] ?>
                            <?php $total_aliquot += $expenses['amount'] ?>
                        @endif
                    @endforeach
                </tbody>        
                <tfoot>
                    <tr>
                        <td colspan="2" width="320" align="center"  style="font-size:10px;">Total gastos no comunes</td>
                        <td width="85" align="left"  style="font-size:10px;"> @moneyFormat($total_price) Bs.</td>
                        <td width="85" align="left"  style="font-size:10px;"> @moneyFormat($total_aliquot) Bs.</td>
                    </tr>
                </tfoot>
            </table>
            @endif
            <!-- TABLA GASTO NO COMUN FIN -->

            <table width="100%" style="font-size:10px;">
                <tbody>                
                    <tr>
                        <td width='320'>TOTAL GASTOS Y CUENTAS POR PAGAR</td>
                        <td width='85' align="left"  style="font-size:10px; font-weight:bold; border: 1px solid #000; ">@moneyFormat($data['amount_fee']) Bs.</td>
                        <td width='85' align="left"  style="font-size:10px; font-weight:bold; border: 1px solid #000; ">@moneyFormat($data['amount'])  Bs.</td>
                    </tr>
                </tbody>
            </table>
        </div>




        <hr/>
        <div class="invoice" style="padding:20px;">
        <table width="100%" style="font-size:10px;">
                <thead>
                <tr>
                    <th width="250"align='left'>MOVIMIENTO DE FONDOS</th>
                    <th width="60" align='left'>Saldo anterior</th>
                    <th width="60" align='left'>Debe</th>
                    <th width="60" align='left'>Haber</th>
                    <th width="60" align='left'>Saldo</th>
                </tr>
                </thead>
                <tbody>
                    <?php $total_found = 0 ?>
                    <?php $total_amount_fee = 0 ?> 
                    <?php $total_sum_found = 0 ?>  
                    @foreach($data['property']['residence']['expenses'] as $founds)  
                        @if($founds['type'] == 7)             
                            @foreach($data['expenses_invoices'] as $expenses)
                                @if($expenses['expense_fee']['expense']['type'] == 5)    
                                    <?php $total_found += $expenses['expense_fee']['expense']['found']['balances_last']['balance'] ?>
                                    <?php $total_amount_fee += $expenses['amount_fee'] ?>
                                    <?php $total_sum_found = $total_sum_found + ($total_amount_fee + $total_found) ?>
                                    <tr>
                                        <td width='250'>{{ $expenses['expense_fee']['expense']['found']['name']  }}</td>
                                        <td width='60' align='left'> @moneyFormat($total_found) Bs.</td>
                                        <td width='60' align='left'> @moneyFormat($total_amount_fee) Bs.</td>
                                        <td width='60' align='left'> 0.00 Bs.</td>
                                        <td width='60' align='left'> @moneyFormat($total_sum_found) Bs.</td>
                                    </tr>  
                                @endif
                            @endforeach    
                            <tr>
                                <td width='250'>{{ $founds['name']  }}</td>
                                <td width='60' align='left'> @moneyFormat(0) Bs.</td>
                                <td width='60' align='left'> @moneyFormat(0) Bs.</td>
                                <td width='60' align='left'> 0.00 Bs.</td>
                                <td width='60' align='left'> @moneyFormat(0) Bs.</td>
                            </tr>                        
                        @endif
                    @endforeach
                </tbody>        
                <tfoot>
                    <tr>
                        <td width="250" align="center"  style="font-size:10px;">Totales</td>
                        <td width="60" align="left"  style="font-size:10px;"> @moneyFormat($total_found) Bs.</td>
                        <td width="60" align="left"  style="font-size:10px;"> @moneyFormat($total_amount_fee) Bs.</td>
                        <td width="60" align="left"  style="font-size:10px;"> 0.00 Bs.</td>
                        <td width="60" align="left"  style="font-size:10px;"> @moneyFormat($total_sum_found) Bs.</td>
                    </tr>
                </tfoot>
            </table>
        </div>


        <div class="information" style="position: absolute; bottom: 0;">
            <table width="100%">
                <tr>
                    <td align="center"  width="100%">
                        <span style="text-align:center;" >
                            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; www.inmobiliariaterepaima.com - 
                            Siguenos en Instagram &nbsp; 
                            <img src="instagram.png" style="width:10px;" />
                            <strong>inmobiliariaterepaima</strong>
                        </span>                        
                    </td>
                </tr>        
            </table>
        </div>
        </body>
        </html>