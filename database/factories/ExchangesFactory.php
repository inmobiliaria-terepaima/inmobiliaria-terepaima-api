<?php

namespace Database\Factories;

use App\Models\Exchanges;
use Illuminate\Database\Eloquent\Factories\Factory;

class ExchangesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Exchanges::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
