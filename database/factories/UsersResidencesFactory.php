<?php

namespace Database\Factories;

use App\Models\Users_residences;
use Illuminate\Database\Eloquent\Factories\Factory;

class UsersResidencesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Users_residences::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
