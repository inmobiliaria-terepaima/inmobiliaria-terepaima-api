<?php

namespace Database\Factories;

use App\Models\Expenses_invoices;
use Illuminate\Database\Eloquent\Factories\Factory;

class ExpensesInvoicesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Expenses_invoices::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
