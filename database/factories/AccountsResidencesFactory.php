<?php

namespace Database\Factories;

use App\Models\Accounts_residences;
use Illuminate\Database\Eloquent\Factories\Factory;

class AccountsResidencesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Accounts_residences::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
