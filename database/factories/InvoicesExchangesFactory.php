<?php

namespace Database\Factories;

use App\Models\Invoices_exchanges;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvoicesExchangesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Invoices_exchanges::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
