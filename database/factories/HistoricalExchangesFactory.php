<?php

namespace Database\Factories;

use App\Models\Historical_exchanges;
use Illuminate\Database\Eloquent\Factories\Factory;

class HistoricalExchangesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Historical_exchanges::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
