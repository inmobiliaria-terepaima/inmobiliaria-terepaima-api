<?php

namespace Database\Factories;

use App\Models\Payments_invoices;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaymentsInvoicesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Payments_invoices::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
