<?php

namespace Database\Factories;

use App\Models\Configurations_residences;
use Illuminate\Database\Eloquent\Factories\Factory;

class ConfigurationsResidencesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Configurations_residences::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
