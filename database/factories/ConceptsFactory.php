<?php

namespace Database\Factories;

use App\Models\Concepts;
use Illuminate\Database\Eloquent\Factories\Factory;

class ConceptsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Concepts::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
