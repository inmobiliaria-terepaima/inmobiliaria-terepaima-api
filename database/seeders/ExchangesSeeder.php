<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ExchangesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         DB::table('exchanges')->insert([
            'name' => 'BCV',
            'method' => 1,
        ]);
        DB::table('exchanges')->insert([
            'name' => 'Dolar Today',
            'method' => 1,
        ]);
        DB::table('exchanges')->insert([
            'name' => 'BCV (Dolar Today)',
            'method' => 1,
        ]);
        DB::table('exchanges')->insert([
            'name' => 'Instagram @monitordolar3',
            'method' => 1,
        ]);
    }
}
