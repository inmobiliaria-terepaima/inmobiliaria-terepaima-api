<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class HistoricalExchangesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('historical_exchanges')->insert([
            'idexchange' => 1,
            'amount' => 1,
        ]);
        DB::table('historical_exchanges')->insert([
            'idexchange' => 2,
            'amount' => 1,
        ]);
        DB::table('historical_exchanges')->insert([
            'idexchange' => 3,
            'amount' => 1,
        ]);
        DB::table('historical_exchanges')->insert([
            'idexchange' => 4,
            'amount' => 1,
        ]);
    }
}
