<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigurationsResidences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configurations_residences', function (Blueprint $table) {
            $table->id();
            $table->foreignId('idresidence');
            $table->string('source_exchange', 3);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('idresidence')
                ->references('id')
                ->on('residences')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configurations_residences');
    }
}
