<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExchangesToConfigurationsResidences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configurations_residences', function (Blueprint $table) {
            //
            $table->timestamp('expiration_date')->nullable()->default(null);;
            $table->integer('indexing_method');
            $table->boolean('dollar');
            $table->boolean('minimum_rate');
            $table->timestamp('emit_date')->nullable()->default(null);;
            $table->foreignId('idexchange');
            $table->foreign('idexchange')
                ->references('id')
                ->on('exchanges')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('configurations_residences', function (Blueprint $table) {
            //
            $table->dropForeign(['idexchange']);
            $table->dropColumn('idexchange');
            $table->dropColumn('expiration_date');
            $table->dropColumn('indexing_method');
            $table->dropColumn('dollar');
            $table->dropColumn('minimum_rate');
            $table->dropColumn('emit_date');
        });
    }
}
