<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsConfigurationsResidencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('configurations_residences', function (Blueprint $table) {
            //
            $table->boolean('enabled_payment')->default(false);
            $table->boolean('enabled_residence')->default(false);
            $table->boolean('lock_exchange')->default(false);
            $table->string('whatsapp_link')->nullable();
            $table->string('telegram_link')->nullable();
            $table->string('twitter_link')->nullable();
            $table->string('facebook_link')->nullable();
            $table->string('instagram_link')->nullable();            
            $table->string('footer_invoice')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('configurations_residences', function (Blueprint $table) {
            //
            $table->dropColumn('enabled_payment');
            $table->dropColumn('enabled_residence');
            $table->dropColumn('lock_exchange');
            $table->dropColumn('whatsapp_link');
            $table->dropColumn('telegram_link');
            $table->dropColumn('twitter_link');
            $table->dropColumn('facebook_link');
            $table->dropColumn('instagram_link');            
            $table->dropColumn('footer_invoice');
        });
    }
}
