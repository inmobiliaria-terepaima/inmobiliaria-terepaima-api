<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsInExpensesPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses_properties', function (Blueprint $table) {
            //
            $table->decimal('amount', 15, 2)->nullable();
            $table->string('currency_code', 3)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses_properties', function (Blueprint $table) {
            //
             $table->dropColumn('amount');
             $table->dropColumn('currency_code');
        });
    }
}
