<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdexchageExchanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('expenses', function (Blueprint $table) {
            //
            $table->foreignId('idexchange')->nullable();
            $table->foreign('idexchange')
                ->references('id')
                ->on('exchanges')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('expenses', function (Blueprint $table) {
            //
            $table->dropForeign(['idexchange'])->nullable();
            $table->dropColumn('idexchange');
        });
    }
}
