<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddConceptExpensesFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('expenses_fees', function (Blueprint $table) {
            //
            $table->foreignId('concept_id');

            $table->foreign('concept_id')
                ->references('id')
                ->on('concepts')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('expenses_fees', function (Blueprint $table) {
            //
            $table->dropForeign(['concept_id']);
            $table->dropColumn('concept_id');
        });
    }
}
