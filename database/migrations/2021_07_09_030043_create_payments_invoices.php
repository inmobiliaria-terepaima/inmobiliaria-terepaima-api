<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('iduser');
            $table->foreignId('idaccount_residence');
            $table->decimal('amount');
            $table->string('currency_code', 3);
            $table->integer('reference');
            $table->integer('status');
            $table->string('email');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('iduser')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('idaccount_residence')
                ->references('id')
                ->on('accounts_residences')
                ->onDelete('cascade');
        });

        Schema::create('payments_invoices', function (Blueprint $table) {
            $table->id();
            $table->foreignId('idinvoice');
            $table->foreignId('idpayment');
            $table->boolean('enable')->default(true);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('idinvoice')
                ->references('id')
                ->on('invoices')
                ->onDelete('cascade');

            $table->foreign('idpayment')
                ->references('id')
                ->on('payments')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
        Schema::dropIfExists('payments_invoices');
    }
}
