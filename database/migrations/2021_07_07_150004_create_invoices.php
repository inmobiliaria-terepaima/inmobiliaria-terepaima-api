<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('residences', function (Blueprint $table) {
            
            $table->id();
            $table->string('name');
            $table->string('direction');
            $table->string('rif');
            $table->softDeletes();
            $table->timestamps();

        });

        Schema::create('properties', function (Blueprint $table) {
            
            $table->id();
            $table->foreignId('iduser');
            $table->foreignId('idresidence');
            $table->decimal('aliquot', $precision = 8, $scale = 8);
            $table->string('number_property', 10);
            $table->boolean('enable')->default(false);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('iduser')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('idresidence')
                ->references('id')
                ->on('residences')
                ->onDelete('cascade');

        });

        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->foreignId('idproperty');
            $table->timestamp('expiration_date');
            $table->decimal('amount');
            $table->string('currency_code', 3);
            $table->integer('status');
            $table->boolean('active')->default(false);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('idproperty')
                ->references('id')
                ->on('properties')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('residences');
        Schema::dropIfExists('properties');
        Schema::dropIfExists('invoices');
    }
}
