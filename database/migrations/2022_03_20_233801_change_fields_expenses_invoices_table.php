<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFieldsExpensesInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('expenses_invoices', function (Blueprint $table) {
            //
            $table->dropForeign(['idexpense']);
            $table->dropColumn('idexpense');

            $table->foreignId('expense_fee_id');

            $table->foreign('expense_fee_id')
                ->references('id')
                ->on('expenses_fees')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
