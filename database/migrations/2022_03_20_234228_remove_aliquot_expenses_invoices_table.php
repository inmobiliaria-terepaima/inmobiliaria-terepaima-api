<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveAliquotExpensesInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('expenses_invoices', function (Blueprint $table) {
            //            
            $table->dropColumn('aliquot');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('expenses_invoices', function (Blueprint $table) {
            //            
            $table->decimal('aliquot', $precision = 8, $scale = 8);
        });
    }
}
