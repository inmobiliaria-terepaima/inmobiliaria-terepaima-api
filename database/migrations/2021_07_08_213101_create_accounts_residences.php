<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsResidences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts_residences', function (Blueprint $table) {
            $table->id();
            $table->foreignId('idresidence');
            $table->integer('type');
            $table->string('name');
            $table->string('number');
            $table->string('email');
            $table->string('phone');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('idresidence')
                ->references('id')
                ->on('residences')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts_residences');
    }
}
