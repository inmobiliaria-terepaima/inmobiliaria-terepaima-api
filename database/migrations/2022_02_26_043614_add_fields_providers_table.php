<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('providers', function (Blueprint $table) {
            //
            $table->foreignId('idresidence')->nullable();
            $table->string('contact_name')->nullable();
            $table->string('phone');
            $table->string('local_phone')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->integer('calification')->nullable(); 
            $table->text('observation')->nullable();

            $table->foreign('idresidence')
                ->references('id')
                ->on('residences')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('providers', function (Blueprint $table) {
            //
            $table->dropForeign(['idresidence'])->nullable();
            $table->dropColumn('contact_name');
            $table->dropColumn('phone');
            $table->dropColumn('local_phone');
            $table->dropColumn('email');
            $table->dropColumn('website');
            $table->dropColumn('calification'); 
            $table->dropColumn('observation');
        });
    }
}
