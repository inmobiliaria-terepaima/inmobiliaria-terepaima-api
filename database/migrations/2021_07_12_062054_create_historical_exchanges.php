<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoricalExchanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historical_exchanges', function (Blueprint $table) {
            $table->id();
            $table->foreignId('idexchange');
            $table->decimal('amount', 15, 2);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('idexchange')
                ->references('id')
                ->on('exchanges')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historical_exchanges');
    }
}
