<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FixResidecesToResidences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function(Blueprint $table) {
            $table->dropForeign(['idresidence']);
            $table->renameColumn('idresidence', 'residence_id');
            $table->foreign('residence_id')
                ->references('id')
                ->on('residences')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function(Blueprint $table) {
            $table->dropForeign(['idresidence']);
            $table->renameColumn('idresidence', 'residence_id');
            $table->foreign('residence_id')
                ->references('id')
                ->on('residences')
                ->onDelete('cascade');
        });
    }
}
