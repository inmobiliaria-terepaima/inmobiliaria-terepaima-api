<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExpensesToBalances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('balances', function (Blueprint $table) {
            //
            $table->string('currency_code', 3);
            $table->integer('idexpense');
            $table->integer('idresidence');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('balances', function (Blueprint $table) {
            //
            $table->dropColumn('idexpense');
            $table->dropColumn('idresidence');
            $table->dropColumn('currency_code');
        });
    }
}
