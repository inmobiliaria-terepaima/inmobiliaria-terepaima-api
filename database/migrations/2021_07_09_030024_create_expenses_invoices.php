<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpensesInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses_invoices', function (Blueprint $table) {
            $table->id();
            $table->foreignId('idinvoice');
            $table->foreignId('idexpense');
            $table->decimal('aliquot', $precision = 8, $scale = 8);
            $table->string('description');
            $table->decimal('amount');
            $table->string('currency_code', 3);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('idinvoice')
                ->references('id')
                ->on('invoices')
                ->onDelete('cascade');

            $table->foreign('idexpense')
                ->references('id')
                ->on('expenses')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses_invoices');
    }
}
