<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsHistoricalExchangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('historical_exchanges', function (Blueprint $table) {
            //           
            $table->string('currency_code', 3)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('historical_exchanges', function (Blueprint $table) {
            //
            $table->dropColumn('currency_code');
        });
    }
}
