<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configurations', function (Blueprint $table) {
            $table->id();
            $table->string('primary_color');
            $table->string('email');
            $table->string('email_secondary');
            $table->string('company_name');
            $table->string('direction');
            $table->string('phone');
            $table->string('local_phone');
            $table->string('footer_invoice');
            $table->string('rif');
            $table->string('website');
            $table->string('slogan');
            $table->string('whatsapp_link');
            $table->string('intagram_link');
            $table->string('facebook_link');
            $table->string('twitter_link');
            $table->string('smtp_host');
            $table->string('smtp_port');
            $table->string('smtp_user');
            $table->string('smtp_password');
            $table->string('smtp_ssl');
            $table->string('smtp_email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configurations');
    }
}
