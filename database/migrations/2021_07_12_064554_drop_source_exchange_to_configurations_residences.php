<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropSourceExchangeToConfigurationsResidences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('configurations_residences', function (Blueprint $table) {
            //
            $table->dropColumn('source_exchange');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('configurations_residences', function (Blueprint $table) {
            //
            $table->dropColumn('source_exchange');
        });
    }
}
