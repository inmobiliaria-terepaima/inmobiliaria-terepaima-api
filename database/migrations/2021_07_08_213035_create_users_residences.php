<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersResidences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_residences', function (Blueprint $table) {
            $table->id();
            $table->foreignId('idresidence');
            $table->foreignId('iduser');
            $table->integer('type');
            $table->boolean('enabled')->default(false);
            $table->timestamps();

            $table->foreign('idresidence')
                ->references('id')
                ->on('residences')
                ->onDelete('cascade');
            
            $table->foreign('iduser')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_residences');
    }
}
