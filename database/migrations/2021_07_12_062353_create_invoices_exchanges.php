<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesExchanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices_exchanges', function (Blueprint $table) {
            $table->id();
            $table->foreignId('idinvoice');
            $table->foreignId('idhistorical_exchange');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('idinvoice')
                ->references('id')
                ->on('invoices')
                ->onDelete('cascade');

            $table->foreign('idhistorical_exchange')
                ->references('id')
                ->on('historical_exchanges')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices_exchanges');
    }
}
