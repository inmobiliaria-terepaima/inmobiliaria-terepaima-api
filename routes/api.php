<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\Api\ConceptsController;
use App\Http\Controllers\Api\PropertiesController;
use App\Http\Controllers\Api\ResidencesController;
use App\Http\Controllers\Api\ProvidersController;
use App\Http\Controllers\Api\InvoicesController;
use App\Http\Controllers\Api\ExpensesController;
use App\Http\Controllers\Api\ExpensesFeesController;
use App\Http\Controllers\Api\ExpensesInvoicesController;
use App\Http\Controllers\Api\ExchangesController;
use App\Http\Controllers\Api\HistoricalExchangesController;
use App\Http\Controllers\Api\InvoicesExchangesController;
use App\Http\Controllers\Api\ConfigurationsResidencesController;
use App\Http\Controllers\Api\BalancesController;
use App\Http\Controllers\Api\UsersResidencesController;
use App\Http\Controllers\Api\PaymentsController;
use App\Http\Controllers\Api\AccountsResidencesController;
use App\Http\Controllers\Api\StatsController;
use App\Http\Controllers\Api\ReportsController;
use App\Http\Controllers\Api\ConfigurationsController;
use App\Http\Controllers\Api\EventsController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
|--------------------------------------------------------------------------
| Auth routes
|--------------------------------------------------------------------------
*/
Route::prefix('auth')->group(function () {
    Route::post('login', [AuthController::class, 'login']);  

    // Endpontis fuera de la autentificacion     
    Route::get('data-invoice', [InvoicesController::class, 'show']);
    Route::post('change-invoices', [InvoicesController::class, 'change_invoices']);
    Route::post('refresh-token', [AuthController::class, 'refresh_token']);   
    Route::get('pdf-invoice', [InvoicesController::class, 'pdf_invoice']);
    Route::get('pdf-payment', [PaymentsController::class, 'pdf_payment']); 
    Route::get('invoice-report', [ReportsController::class, 'invoice_report']); 
    Route::post('massive-payouts', [PaymentsController::class, 'massive_payouts']);
    Route::get('gallery', [ConfigurationsController::class, 'gallery']);        
    Route::post('import-properties', [PropertiesController::class, 'import']);
    Route::post('update-gallery', [ConfigurationsController::class, 'update_gallery']);

    Route::post('delete-gallery', [ConfigurationsController::class, 'delete_gallery']);

    Route::post('upload-document', [ResidencesController::class, 'upload_document']);
    Route::get('configuration', [ConfigurationsController::class, 'show']);   
    Route::get('data-payment-users', [PaymentsController::class, 'export_payments_to_excel']);   
    Route::get('update-exchange', [HistoricalExchangesController::class, 'updateExchangeRates']); 

    // maintenance routes
    Route::get('migrate', function(){
            Artisan::call('migrate');
            return response()->json([
                'message' => 'migrated',
            ], 201);
    });
    Route::get('optimize', function(){
            Artisan::call('optimize');
            return response()->json([
                'message' => 'optimized',
            ], 201);
    });
	
    Route::middleware(['auth:api'])->group(function () {
                  
        /** COLECCION DE ENDPOINTS PARA USUARIOS  */
        Route::post('signup', [AuthController::class, 'signup']);
        Route::get('logout', [AuthController::class, 'logout']);
        Route::get('user', [AuthController::class, 'user']);
        Route::get('validate-user', [AuthController::class, 'validate_user']);
        Route::get('list-users', [AuthController::class, 'users']);
        Route::post('change-password-user', [AuthController::class, 'change_password_user']);
        Route::post('edit-user', [AuthController::class, 'edit_user']);
        Route::get('data-user', [AuthController::class, 'show']);

        /** COLECCION DE ENDPOINTS PARA CONCEPTOS  */        
        Route::post('concepts', [ConceptsController::class, 'store']);
        Route::get('list-concepts', [ConceptsController::class, 'index']);

        /** COLECCION DE ENDPOINTS PARA GASTOS  */
        Route::post('expenses', [ExpensesController::class, 'store']);
        Route::post('founds', [ExpensesController::class, 'store_found']);
        Route::get('list-expenses', [ExpensesController::class, 'index']);
        Route::post('delete-expense', [ExpensesController::class, 'destroy']);
        Route::get('get-balance', [ExpensesController::class, 'get_balance']);
        Route::get('data-expense', [ExpensesController::class, 'show']);

        /** COLECCION DE ENDPOINTS PARA PROPIEDADES  */
        Route::post('properties', [PropertiesController::class, 'store']);
        Route::get('property', [PropertiesController::class, 'show']);
        Route::get('list-properties', [PropertiesController::class, 'index']);        
        Route::post('delete-property', [PropertiesController::class, 'destroy']);
        Route::post('edit-property', [PropertiesController::class, 'update']);

        /** COLECCION DE ENDPOINTS PARA GASTOS DE PROPIEDADES */
        Route::post('expenses-fees', [ExpensesFeesController::class, 'store']);
        Route::get('list-expenses-fees', [ExpensesFeesController::class, 'index']);

        /** COLECCION DE ENDPOINTS PARA RESIDENCIAS  */
        Route::post('residences', [ResidencesController::class, 'store']);
        Route::post('edit-residence', [ResidencesController::class, 'edit']);
        Route::get('data-residence', [ResidencesController::class, 'show']);
        Route::get('list-residences', [ResidencesController::class, 'index']);
        Route::get('list-residences-users', [ResidencesController::class, 'list_users']);
        Route::post('delete-residence', [ResidencesController::class, 'destroy']);

        /** COLECCION DE ENDPOINTS PARA LA CONFIGURACION DE LAS RESIDENCIAS  */
        Route::post('add-configuration-residence', [ConfigurationsResidencesController::class, 'store']);
        Route::get('configuration-residence', [ConfigurationsResidencesController::class, 'show']);
        Route::post('edit-configuration-residence', [ConfigurationsResidencesController::class, 'update']);

        /** COLECCION DE ENDPOINT PARA PROVEEDORES */
        Route::post('providers', [ProvidersController::class, 'store']);
        Route::get('list-providers', [ProvidersController::class, 'index']);
        Route::get('data-provider', [ProvidersController::class, 'show']);

        /** COLECCION DE ENDPOINT PARA RECIBOSS */
        Route::post('invoices', [InvoicesController::class, 'store']);
        Route::get('list-invoices', [InvoicesController::class, 'index']);        
        Route::get('batch-invoice', [InvoicesController::class, 'batch_invoice']);
        Route::post('delete-invoice', [InvoicesController::class, 'destroy']);
        

        /** COLECCION DE ENDPOINT PARA LOS GASTOS DE LOS RECIBOS */
        Route::post('expenses-invoices', [ExpensesInvoicesController::class, 'store']);
        Route::get('list-expenses-invoices', [ExpensesInvoicesController::class, 'index']);
        Route::post('update-expenses-invoices', [ExpensesInvoicesController::class,'update']);
        
        /** COLECCION DE ENDPOINT PARA TASAS DE CAMBIO */
        Route::post('add-exchange', [ExchangesController::class, 'store']);
        Route::get('list-exchanges', [ExchangesController::class, 'index']);        
        Route::post('add-invoice-exchange', [InvoicesExchangesController::class, 'store']);

        

        /** COLECCION DE ENDPOINT PARA HISTORICOS DE TASAS DE CAMBIO */
        Route::get('list-historical', [HistoricalExchangesController::class, 'index']);
        Route::post('add-historical-exchange', [HistoricalExchangesController::class, 'store']);
        Route::post('update-historical-exchange', [HistoricalExchangesController::class, 'update']);
        
        /** COLECCION DE ENDPOINT PARA USUARIOS DE RESIDENCIAS */
        Route::post('add-user-residence', [UsersResidencesController::class, 'store']);

        /** COLECCION DE ENDPOINT PARA PAGOS */
        Route::post('add-payment-residence', [PaymentsController::class, 'store']);
        Route::get('list-payment-residence', [PaymentsController::class, 'index']);
        Route::get('data-payment', [PaymentsController::class, 'show']);    
        Route::post('change-payment', [PaymentsController::class, 'change_payment']);    


        /** COLECCION DE ENDPOINT PARA CUENTAS DE LA RESIDENCIA */
        Route::post('add-account-residence', [AccountsResidencesController::class, 'store']);
        Route::get('list-account-residence', [AccountsResidencesController::class, 'index']);
        Route::post('delete-account-residence', [AccountsResidencesController::class, 'destroy']);
        Route::post('edit-account-residence', [AccountsResidencesController::class, 'edit']);

        /** COLECCION DE ENDPOINTS PARA BALANCES  */
        Route::post('add-balance', [BalancesController::class, 'store']);
        Route::get('balance-list', [BalancesController::class, 'index']);

        /** COLECCION DE ENDPOINTS PARA ESTADISTICAS  */
        Route::get('payment-stats', [StatsController::class, 'payment_stats']);
        Route::get('global-stats', [StatsController::class, 'global_stats']);
        Route::get('payment-historics', [StatsController::class, 'payment_historics']);

        /** COLECCION DE ENDPOINTS PARA EVENTOS  */
        Route::get('list-events', [EventsController::class, 'index']);

        /** COLECCION DE ENDPOINTS PARA CONFIGURACIONES  */
        
         
        

        /** COLECCION DE ENDPOINTS PARA REPORTES  */
		
		
        
    });    
});

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

