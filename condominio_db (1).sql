-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-05-2023 a las 02:35:41
-- Versión del servidor: 10.4.20-MariaDB
-- Versión de PHP: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `condominio_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accounts_residences`
--

CREATE TABLE `accounts_residences` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idresidence` bigint(20) UNSIGNED NOT NULL,
  `type` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `bank` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_holder` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `accounts_residences`
--

INSERT INTO `accounts_residences` (`id`, `idresidence`, `type`, `name`, `number`, `email`, `phone`, `deleted_at`, `created_at`, `updated_at`, `bank`, `document_number`, `account_holder`) VALUES
(1, 1, 1, 'Cuenta banesco', '0134-2334-5553-2212', 'residencia@pago.com', '04169231991', '2022-08-12 05:49:57', '2021-08-08 23:19:39', '2022-08-12 05:49:57', '', 'J-2131231231', 'Pedro Perez'),
(2, 1, 1, 'Cuenta banesco 77', '0134-2334-5553-2212', 'residencia@pago.com', '04169231991', NULL, '2021-08-08 23:28:04', '2022-08-12 18:51:58', 'Banesco', 'J-311668403', 'PEdro'),
(3, 1, 1, 'test', 'test', 'sdasdasd@gmail.com', '23423423', NULL, '2022-08-10 22:13:24', '2022-08-10 22:13:24', 'BANCO ACTIVO BANCO COMERCIAL, C.A.', '123234234', 'test'),
(4, 2, 2, 'qeqwe', '1231231231231231', 'asdasd@gmail.com', '1323123', NULL, '2022-08-12 19:02:35', '2022-08-12 19:02:35', 'ABN AMRO BANK', '21313123', 'asdasdasd'),
(5, 9, 1, 'asdasda', '21312313123123', 'asdasd@asd.xom', '324234234234', NULL, '2022-09-26 09:12:41', '2022-09-26 09:12:41', 'BANCAMIGA BANCO MICROFINANCIERO, C.A.', '2131313123', 'asda asdasd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `balances`
--

CREATE TABLE `balances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idpayment` int(11) NOT NULL,
  `idinvoice` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  `expense` decimal(15,2) NOT NULL,
  `income` decimal(15,2) NOT NULL,
  `balance` decimal(15,2) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` int(11) NOT NULL,
  `currency_code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idexpense` int(11) NOT NULL,
  `idresidence` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `balances`
--

INSERT INTO `balances` (`id`, `idpayment`, `idinvoice`, `property_id`, `expense`, `income`, `balance`, `deleted_at`, `created_at`, `updated_at`, `type`, `currency_code`, `idexpense`, `idresidence`, `description`) VALUES
(1, 0, 42, 1, '42.22', '0.00', '42.22', NULL, '2022-02-02 05:09:42', '2022-02-02 05:09:42', 1, 'VES', 0, 1, NULL),
(2, 0, 43, 2, '27.02', '0.00', '27.02', NULL, '2022-02-02 05:09:43', '2022-02-02 05:09:43', 1, 'VES', 0, 1, NULL),
(3, 0, 44, 3, '42.22', '0.00', '42.22', NULL, '2022-02-02 05:09:43', '2022-02-02 05:09:43', 1, 'VES', 0, 1, NULL),
(4, 0, 45, 4, '42.22', '0.00', '42.22', NULL, '2022-02-02 05:09:43', '2022-02-02 05:09:43', 1, 'VES', 0, 1, NULL),
(5, 0, 46, 5, '42.22', '0.00', '42.22', NULL, '2022-02-02 05:09:43', '2022-02-02 05:09:43', 1, 'VES', 0, 1, NULL),
(6, 0, 47, 6, '42.22', '0.00', '42.22', NULL, '2022-02-02 05:09:43', '2022-02-02 05:09:43', 1, 'VES', 0, 1, NULL),
(7, 0, 51, 10, '50.62', '0.00', '50.62', NULL, '2022-02-02 05:09:43', '2022-02-02 05:09:43', 1, 'VES', 0, 1, NULL),
(8, 0, 52, 11, '33.74', '0.00', '33.74', NULL, '2022-02-02 05:09:43', '2022-02-02 05:09:43', 1, 'VES', 0, 1, NULL),
(9, 0, 53, 12, '18.54', '0.00', '18.54', NULL, '2022-02-02 05:09:43', '2022-02-02 05:09:43', 1, 'VES', 0, 1, NULL),
(10, 0, 54, 13, '6.69', '0.00', '6.69', NULL, '2022-02-02 05:09:43', '2022-02-02 05:09:43', 1, 'VES', 0, 1, NULL),
(11, 0, 55, 14, '151.85', '0.00', '151.85', NULL, '2022-02-02 05:09:43', '2022-02-02 05:09:43', 1, 'VES', 0, 1, NULL),
(12, 0, 56, 15, '135.28', '0.00', '135.28', NULL, '2022-02-02 05:09:43', '2022-02-02 05:09:43', 1, 'VES', 0, 1, NULL),
(13, 0, 57, 16, '67.49', '0.00', '67.49', NULL, '2022-02-02 05:09:43', '2022-02-02 05:09:43', 1, 'VES', 0, 1, NULL),
(14, 0, 58, 17, '33.74', '0.00', '33.74', NULL, '2022-02-02 05:09:43', '2022-02-02 05:09:43', 1, 'VES', 0, 1, NULL),
(15, 35, 0, 6, '0.00', '50.00', '-7.78', NULL, '2022-02-06 05:28:47', '2022-02-06 05:28:47', 1, 'VES', 0, 1, NULL),
(16, 36, 0, 6, '0.00', '150.00', '-157.78', NULL, '2022-02-06 06:30:55', '2022-02-06 06:30:55', 1, 'VES', 0, 1, NULL),
(17, 37, 0, 2, '0.00', '226.50', '-199.48', NULL, '2022-02-06 18:33:49', '2022-02-06 18:33:49', 1, 'VES', 0, 1, NULL),
(18, 38, 0, 2, '0.00', '362.40', '-561.88', NULL, '2022-02-06 18:36:00', '2022-02-06 18:36:00', 1, 'VES', 0, 1, NULL),
(19, 39, 0, 2, '0.00', '226.50', '-788.38', NULL, '2022-02-06 18:37:41', '2022-02-06 18:37:41', 1, 'VES', 0, 1, NULL),
(20, 40, 0, 3, '0.00', '226.50', '-184.28', NULL, '2022-02-06 18:38:22', '2022-02-06 18:38:22', 1, 'VES', 0, 1, NULL),
(21, 41, 0, 6, '0.00', '226.50', '-384.28', NULL, '2022-02-06 21:37:21', '2022-02-06 21:37:21', 1, 'VES', 0, 1, NULL),
(22, 42, 0, 6, '0.00', '50.00', '-434.28', NULL, '2022-02-09 03:44:32', '2022-02-09 03:44:32', 1, 'VES', 0, 1, NULL),
(23, 34, 0, 6, '0.00', '33363.18', '-33797.46', NULL, '2022-02-09 22:56:16', '2022-02-09 22:56:16', 1, 'VES', 0, 1, NULL),
(24, 16, 0, 3, '0.00', '111.00', '-295.28', NULL, '2022-02-09 22:56:40', '2022-02-09 22:56:40', 1, 'VES', 0, 1, NULL),
(25, 46, 0, 2, '0.00', '67.95', '-856.33', NULL, '2022-02-13 08:55:40', '2022-02-13 08:55:40', 1, 'VES', 0, 1, NULL),
(26, 47, 0, 6, '0.00', '10.00', '-33807.46', NULL, '2022-02-14 21:08:04', '2022-02-14 21:08:04', 1, 'VES', 0, 1, NULL),
(27, 47, 0, 14, '0.00', '10.00', '141.85', NULL, '2022-02-14 21:08:04', '2022-02-14 21:08:04', 1, 'VES', 0, 1, NULL),
(28, 48, 0, 1, '0.00', '300.00', '-257.78', NULL, '2022-02-15 04:44:50', '2022-02-15 04:44:50', 1, 'VES', 0, 1, NULL),
(29, 49, 0, 2, '0.00', '212.00', '-1068.33', NULL, '2022-03-15 04:50:19', '2022-03-15 04:50:19', 1, 'VES', 0, 1, NULL),
(30, 0, 0, 132, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:08', '2022-09-26 02:03:08', 1, 'VES', 0, 9, NULL),
(31, 0, 0, 133, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:08', '2022-09-26 02:03:08', 1, 'VES', 0, 9, NULL),
(32, 0, 0, 134, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09', 1, 'VES', 0, 9, NULL),
(33, 0, 0, 135, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09', 1, 'VES', 0, 9, NULL),
(34, 0, 0, 136, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09', 1, 'VES', 0, 9, NULL),
(35, 0, 0, 137, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09', 1, 'VES', 0, 9, NULL),
(36, 0, 0, 138, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09', 1, 'VES', 0, 9, NULL),
(37, 0, 0, 139, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09', 1, 'VES', 0, 9, NULL),
(38, 0, 0, 140, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09', 1, 'VES', 0, 9, NULL),
(39, 0, 0, 141, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09', 1, 'VES', 0, 9, NULL),
(40, 0, 0, 142, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10', 1, 'VES', 0, 9, NULL),
(41, 0, 0, 143, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10', 1, 'VES', 0, 9, NULL),
(42, 0, 0, 144, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10', 1, 'VES', 0, 9, NULL),
(43, 0, 0, 145, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10', 1, 'VES', 0, 9, NULL),
(44, 0, 0, 146, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10', 1, 'VES', 0, 9, NULL),
(45, 0, 0, 147, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10', 1, 'VES', 0, 9, NULL),
(46, 0, 0, 148, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10', 1, 'VES', 0, 9, NULL),
(47, 0, 0, 149, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:11', '2022-09-26 02:03:11', 1, 'VES', 0, 9, NULL),
(48, 0, 0, 150, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:11', '2022-09-26 02:03:11', 1, 'VES', 0, 9, NULL),
(49, 0, 0, 151, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:11', '2022-09-26 02:03:11', 1, 'VES', 0, 9, NULL),
(50, 0, 0, 152, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:11', '2022-09-26 02:03:11', 1, 'VES', 0, 9, NULL),
(51, 0, 0, 153, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:11', '2022-09-26 02:03:11', 1, 'VES', 0, 9, NULL),
(52, 0, 0, 154, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:12', '2022-09-26 02:03:12', 1, 'VES', 0, 9, NULL),
(53, 0, 0, 155, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:14', '2022-09-26 02:03:14', 1, 'VES', 0, 9, NULL),
(54, 0, 0, 156, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:15', '2022-09-26 02:03:15', 1, 'VES', 0, 9, NULL),
(55, 0, 0, 157, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:15', '2022-09-26 02:03:15', 1, 'VES', 0, 9, NULL),
(56, 0, 0, 158, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:16', '2022-09-26 02:03:16', 1, 'VES', 0, 9, NULL),
(57, 0, 0, 159, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:16', '2022-09-26 02:03:16', 1, 'VES', 0, 9, NULL),
(58, 0, 0, 160, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:16', '2022-09-26 02:03:16', 1, 'VES', 0, 9, NULL),
(59, 0, 0, 161, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:16', '2022-09-26 02:03:16', 1, 'VES', 0, 9, NULL),
(60, 0, 0, 162, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:17', '2022-09-26 02:03:17', 1, 'VES', 0, 9, NULL),
(61, 0, 0, 163, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:17', '2022-09-26 02:03:17', 1, 'VES', 0, 9, NULL),
(62, 0, 0, 164, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:17', '2022-09-26 02:03:17', 1, 'VES', 0, 9, NULL),
(63, 0, 0, 165, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:17', '2022-09-26 02:03:17', 1, 'VES', 0, 9, NULL),
(64, 0, 0, 166, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:17', '2022-09-26 02:03:17', 1, 'VES', 0, 9, NULL),
(65, 0, 0, 167, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:17', '2022-09-26 02:03:17', 1, 'VES', 0, 9, NULL),
(66, 0, 0, 168, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18', 1, 'VES', 0, 9, NULL),
(67, 0, 0, 169, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18', 1, 'VES', 0, 9, NULL),
(68, 0, 0, 170, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18', 1, 'VES', 0, 9, NULL),
(69, 0, 0, 171, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18', 1, 'VES', 0, 9, NULL),
(70, 0, 0, 172, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18', 1, 'VES', 0, 9, NULL),
(71, 0, 0, 173, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18', 1, 'VES', 0, 9, NULL),
(72, 0, 0, 174, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18', 1, 'VES', 0, 9, NULL),
(73, 0, 0, 175, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19', 1, 'VES', 0, 9, NULL),
(74, 0, 0, 176, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19', 1, 'VES', 0, 9, NULL),
(75, 0, 0, 177, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19', 1, 'VES', 0, 9, NULL),
(76, 0, 0, 178, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19', 1, 'VES', 0, 9, NULL),
(77, 0, 0, 179, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19', 1, 'VES', 0, 9, NULL),
(78, 0, 0, 180, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19', 1, 'VES', 0, 9, NULL),
(79, 0, 0, 181, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19', 1, 'VES', 0, 9, NULL),
(80, 0, 0, 182, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19', 1, 'VES', 0, 9, NULL),
(81, 0, 0, 183, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:20', '2022-09-26 02:03:20', 1, 'VES', 0, 9, NULL),
(82, 0, 0, 184, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:20', '2022-09-26 02:03:20', 1, 'VES', 0, 9, NULL),
(83, 0, 0, 185, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:20', '2022-09-26 02:03:20', 1, 'VES', 0, 9, NULL),
(84, 0, 0, 186, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:20', '2022-09-26 02:03:20', 1, 'VES', 0, 9, NULL),
(85, 0, 0, 187, '0.00', '0.00', '0.00', NULL, '2022-09-26 02:03:20', '2022-09-26 02:03:20', 1, 'VES', 0, 9, NULL),
(86, 0, 0, 188, '0.00', '20.00', '20.00', NULL, '2022-09-26 05:56:20', '2022-09-26 05:56:20', 1, 'VES', 0, 9, 'SALDO INICIAL'),
(87, 0, 0, 189, '0.00', '20.00', '20.00', NULL, '2022-09-26 05:57:02', '2022-09-26 05:57:02', 1, 'VES', 0, 9, 'SALDO INICIAL'),
(88, 0, 0, 190, '0.00', '20.00', '20.00', NULL, '2022-09-26 05:57:57', '2022-09-26 05:57:57', 1, 'VES', 0, 9, 'SALDO INICIAL'),
(89, 0, 0, 191, '0.00', '20.00', '20.00', NULL, '2022-09-26 05:58:27', '2022-09-26 05:58:27', 1, 'VES', 0, 9, 'SALDO INICIAL'),
(90, 0, 0, 192, '0.00', '55.00', '55.00', NULL, '2022-09-26 06:03:51', '2022-09-26 06:03:51', 1, 'VES', 0, 9, 'SALDO INICIAL'),
(91, 0, 0, 0, '0.00', '100.00', '100.00', NULL, '2022-10-01 18:30:22', '2022-10-01 18:30:22', 1, 'VES', 113, 0, NULL),
(92, 0, 0, 0, '0.00', '100.00', '100.00', NULL, '2022-10-01 18:30:41', '2022-10-01 18:30:41', 1, 'VES', 114, 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `concepts`
--

CREATE TABLE `concepts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `concepts`
--

INSERT INTO `concepts` (`id`, `name`, `enabled`, `deleted_at`, `created_at`, `updated_at`) VALUES
(164, 'MARZO 2022', 1, NULL, '2022-03-25 23:12:16', '2022-03-25 23:12:16'),
(165, 'luz ABRIL 2022', 1, NULL, '2022-03-25 23:12:16', '2022-03-25 23:12:16'),
(166, 'MARZO 2022', 1, NULL, '2022-03-25 23:13:11', '2022-03-25 23:13:11'),
(167, 'luz ABRIL 2022', 1, NULL, '2022-03-25 23:13:11', '2022-03-25 23:13:11'),
(168, 'MARZO 2022', 1, NULL, '2022-03-25 23:13:11', '2022-03-25 23:13:11'),
(169, 'luz ABRIL 2022', 1, NULL, '2022-03-25 23:13:11', '2022-03-25 23:13:11'),
(170, 'MARZO 2022', 1, NULL, '2022-03-25 23:13:11', '2022-03-25 23:13:11'),
(171, 'luz ABRIL 2022', 1, NULL, '2022-03-25 23:13:11', '2022-03-25 23:13:11'),
(172, 'MARZO 2022', 1, NULL, '2022-03-25 23:13:11', '2022-03-25 23:13:11'),
(173, 'luz ABRIL 2022', 1, NULL, '2022-03-25 23:13:11', '2022-03-25 23:13:11'),
(174, 'MARZO 2022', 1, NULL, '2022-03-25 23:14:05', '2022-03-25 23:14:05'),
(175, 'luz ABRIL 2022', 1, NULL, '2022-03-25 23:14:05', '2022-03-25 23:14:05'),
(176, 'MARZO 2022', 1, NULL, '2022-03-25 23:14:05', '2022-03-25 23:14:05'),
(177, 'luz ABRIL 2022', 1, NULL, '2022-03-25 23:14:05', '2022-03-25 23:14:05'),
(178, 'MARZO 2022', 1, NULL, '2022-03-25 23:14:05', '2022-03-25 23:14:05'),
(179, 'luz ABRIL 2022', 1, NULL, '2022-03-25 23:14:05', '2022-03-25 23:14:05'),
(180, 'MARZO 2022', 1, NULL, '2022-03-25 23:14:05', '2022-03-25 23:14:05'),
(181, 'luz ABRIL 2022', 1, NULL, '2022-03-25 23:14:05', '2022-03-25 23:14:05'),
(182, 'paco  MARZO 2022', 1, NULL, '2022-03-25 23:14:55', '2022-03-25 23:14:55'),
(183, 'paquito   luz ABRIL 2022', 1, NULL, '2022-03-25 23:14:55', '2022-03-25 23:14:55'),
(184, 'paco  MARZO 2022', 1, NULL, '2022-03-25 23:14:55', '2022-03-25 23:14:55'),
(185, 'paquito   luz ABRIL 2022', 1, NULL, '2022-03-25 23:14:55', '2022-03-25 23:14:55'),
(186, 'paco  MARZO 2022', 1, NULL, '2022-03-25 23:14:55', '2022-03-25 23:14:55'),
(187, 'paquito   luz ABRIL 2022', 1, NULL, '2022-03-25 23:14:55', '2022-03-25 23:14:55'),
(188, 'paco  MARZO 2022', 1, NULL, '2022-03-25 23:14:55', '2022-03-25 23:14:55'),
(189, 'paquito   luz ABRIL 2022', 1, NULL, '2022-03-25 23:14:55', '2022-03-25 23:14:55'),
(190, 'MARZO 2022', 1, NULL, '2022-03-25 23:16:12', '2022-03-25 23:16:12'),
(191, 'ELECTRICICSD MARZO 2022', 1, NULL, '2022-03-25 23:16:44', '2022-03-25 23:16:44'),
(192, 'MANTENIMIENTO DE LA SISTERNA [MES] [AÑO]', 1, NULL, '2022-03-31 06:02:23', '2022-03-31 06:02:23'),
(193, 'agua', 1, NULL, '2022-09-26 09:13:32', '2022-09-26 09:13:32'),
(194, 'luz', 1, NULL, '2022-09-26 09:13:56', '2022-09-26 09:13:56'),
(195, 'MANTENIMIENTO', 1, NULL, '2022-10-01 05:32:46', '2022-10-01 05:32:46'),
(196, 'FONDO DE RESERVA', 1, NULL, '2022-10-01 18:30:21', '2022-10-01 18:30:21'),
(197, 'FONDO PARA PENDEJADAS', 1, NULL, '2022-10-01 18:30:39', '2022-10-01 18:30:39'),
(198, 'aporte al fonfo', 1, NULL, '2022-10-03 06:12:25', '2022-10-03 06:12:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configurations`
--

CREATE TABLE `configurations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `primary_color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_secondary` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direction` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `local_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `footer_invoice` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rif` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slogan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `whatsapp_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `intagram_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `smtp_host` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `smtp_port` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `smtp_user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `smtp_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `smtp_ssl` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `smtp_email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name_app` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `configurations`
--

INSERT INTO `configurations` (`id`, `primary_color`, `email`, `email_secondary`, `company_name`, `direction`, `phone`, `local_phone`, `footer_invoice`, `rif`, `website`, `slogan`, `whatsapp_link`, `intagram_link`, `facebook_link`, `twitter_link`, `smtp_host`, `smtp_port`, `smtp_user`, `smtp_password`, `smtp_ssl`, `smtp_email`, `created_at`, `updated_at`, `name_app`, `deleted_at`) VALUES
(1, 'dfsdf', 'dfsdf', 'dfsdf', 'dfsdf', 'dfsdf', 'dfsdf', 'dfsdf', 'dfsdf', 'dfsdf', 'dfsdf', 'dfsdf', 'dfsdf', 'dfsdf', 'dfsdf', 'dfsdf', 'dfsdf', 'dfsdf', 'dfsdf', 'dfsdf', 'dfsdf', 'dfsdf', '2022-03-09 04:05:31', '2022-03-09 04:05:31', 'TEREPAIMA', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configurations_residences`
--

CREATE TABLE `configurations_residences` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idresidence` bigint(20) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expiration_date` timestamp NULL DEFAULT NULL,
  `indexing_method` int(11) NOT NULL,
  `dollar` tinyint(1) NOT NULL,
  `minimum_rate` tinyint(1) NOT NULL,
  `emit_date` timestamp NULL DEFAULT NULL,
  `idexchange` bigint(20) UNSIGNED NOT NULL,
  `enabled_payment` tinyint(1) NOT NULL DEFAULT 0,
  `enabled_residence` tinyint(1) NOT NULL DEFAULT 0,
  `lock_exchange` tinyint(1) NOT NULL DEFAULT 0,
  `whatsapp_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telegram_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_invoice` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `previous_invoice` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `configurations_residences`
--

INSERT INTO `configurations_residences` (`id`, `idresidence`, `deleted_at`, `created_at`, `updated_at`, `expiration_date`, `indexing_method`, `dollar`, `minimum_rate`, `emit_date`, `idexchange`, `enabled_payment`, `enabled_residence`, `lock_exchange`, `whatsapp_link`, `telegram_link`, `twitter_link`, `facebook_link`, `instagram_link`, `footer_invoice`, `previous_invoice`) VALUES
(1, 1, NULL, '2022-01-18 13:24:57', '2022-01-18 13:24:57', '2022-01-04 13:24:57', 1, 1, 1, '2022-01-31 13:24:57', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(2, 2, NULL, '2022-01-18 13:24:57', '2022-01-18 13:24:57', '2022-01-04 13:24:57', 1, 1, 1, '2022-01-28 13:24:57', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(3, 8, NULL, '2022-08-12 18:57:16', '2022-08-12 18:57:16', '2022-08-12 18:57:16', 0, 1, 0, '2022-08-12 18:57:16', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 9, NULL, '2022-09-26 01:14:14', '2022-10-01 05:19:52', '2022-09-25 04:00:00', 0, 1, 0, '2022-10-01 04:00:00', 1, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `events`
--

CREATE TABLE `events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idresidence` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `events`
--

INSERT INTO `events` (`id`, `idresidence`, `name`, `description`, `start_date`, `end_date`, `link`, `status`, `type`, `enabled`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'se mantiene la tarifa?', 'sfsdfsd', '2022-03-07', '2022-03-07', '', 1, 1, 1, NULL, '2022-03-14 04:12:32', '2022-03-14 04:12:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `exchanges`
--

CREATE TABLE `exchanges` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `exchanges`
--

INSERT INTO `exchanges` (`id`, `name`, `method`, `deleted_at`, `created_at`, `updated_at`, `description`) VALUES
(1, 'BCV', 1, NULL, '2021-07-23 16:12:17', '2021-07-23 16:12:17', NULL),
(2, 'Dolar Today', 1, NULL, NULL, NULL, NULL),
(3, '@monitordolar3', 1, NULL, NULL, NULL, NULL),
(4, 'INPC', 2, NULL, '2022-02-22 13:44:47', '2022-02-22 13:44:47', NULL),
(6, 'TASA MANUAL', 2, NULL, '2022-03-08 16:42:12', '2022-03-08 16:42:12', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expenses`
--

CREATE TABLE `expenses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idresidence` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idexpense` int(11) NOT NULL,
  `idexchange` bigint(20) UNSIGNED DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiration_date` date DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `expenses`
--

INSERT INTO `expenses` (`id`, `idresidence`, `description`, `type`, `deleted_at`, `created_at`, `updated_at`, `idexpense`, `idexchange`, `status`, `category`, `nic`, `expiration_date`, `name`) VALUES
(103, 1, 'N/A', 3, NULL, '2022-03-25 23:12:16', '2022-03-25 23:12:16', 0, 1, NULL, 'gasto_comun', NULL, NULL, 'holis'),
(104, 1, 'N/A', 3, NULL, '2022-03-25 23:13:11', '2022-03-25 23:13:11', 0, 1, NULL, 'gasto_comun', NULL, NULL, 'holis'),
(105, 1, 'N/A', 1, NULL, '2022-03-25 23:14:05', '2022-03-25 23:14:05', 0, 1, NULL, 'gasto_comun', NULL, NULL, 'holis'),
(106, 1, 'N/A', 1, NULL, '2022-03-25 23:14:55', '2022-03-25 23:14:55', 0, 1, NULL, 'gasto_comun', NULL, NULL, 'holis'),
(107, 1, 'N/A', 1, NULL, '2022-03-25 23:16:12', '2022-03-25 23:16:12', 0, 1, NULL, 'gasto_comun', NULL, NULL, 'holis'),
(108, 1, 'N/A', 1, NULL, '2022-03-25 23:16:44', '2022-03-25 23:16:44', 0, 1, NULL, 'gasto_comun', NULL, NULL, 'holis'),
(109, 1, 'N/A', 1, NULL, '2022-03-31 06:02:23', '2022-03-31 06:02:23', 0, 1, NULL, 'gasto_comun', NULL, NULL, 'holis'),
(110, 9, 'N/A', 1, NULL, '2022-09-26 09:13:32', '2022-09-26 09:13:32', 0, NULL, NULL, 'gasto_comun', NULL, NULL, 'agua'),
(111, 9, 'N/A', 1, NULL, '2022-09-26 09:13:56', '2022-09-26 09:13:56', 0, NULL, NULL, 'gasto_comun', NULL, NULL, 'luz'),
(112, 9, 'N/A', 1, NULL, '2022-10-01 05:32:46', '2022-10-01 05:32:46', 0, NULL, NULL, 'gasto_comun', NULL, NULL, 'MANTENIMIENTO'),
(113, 9, 'N/A', 7, NULL, '2022-10-01 18:30:22', '2022-10-01 18:30:22', 0, NULL, NULL, 'found', NULL, NULL, 'FONDO DE RESERVA'),
(114, 9, 'N/A', 7, NULL, '2022-10-01 18:30:41', '2022-10-01 18:30:41', 0, NULL, NULL, 'found', NULL, NULL, 'FONDO PARA PENDEJADAS'),
(115, 9, 'N/A', 5, NULL, '2022-10-03 06:12:25', '2022-10-03 06:12:25', 114, NULL, NULL, 'gasto_comun', NULL, NULL, 'aporte al fonfo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expenses_fees`
--

CREATE TABLE `expenses_fees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `property_id` bigint(20) UNSIGNED DEFAULT NULL,
  `expense_id` bigint(20) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `amount` decimal(15,2) DEFAULT NULL,
  `currency_code` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `concept_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `expenses_fees`
--

INSERT INTO `expenses_fees` (`id`, `property_id`, `expense_id`, `status`, `date`, `created_at`, `updated_at`, `deleted_at`, `amount`, `currency_code`, `concept_id`) VALUES
(49, NULL, 103, 0, '2022-03-25', '2022-03-25 23:12:16', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 164),
(50, NULL, 103, 0, '2022-03-25', '2022-03-25 23:12:16', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 165),
(51, 1, 104, 0, '2022-03-25', '2022-03-25 23:13:11', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 166),
(52, 1, 104, 0, '2022-03-25', '2022-03-25 23:13:11', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 167),
(53, 2, 104, 0, '2022-03-25', '2022-03-25 23:13:11', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 168),
(54, 2, 104, 0, '2022-03-25', '2022-03-25 23:13:11', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 169),
(55, 3, 104, 0, '2022-03-25', '2022-03-25 23:13:11', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 170),
(56, 3, 104, 0, '2022-03-25', '2022-03-25 23:13:11', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 171),
(57, 4, 104, 0, '2022-03-25', '2022-03-25 23:13:11', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 172),
(58, 4, 104, 0, '2022-03-25', '2022-03-25 23:13:11', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 173),
(59, 1, 105, 0, '2022-03-25', '2022-03-25 23:14:05', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 174),
(60, 1, 105, 0, '2022-03-25', '2022-03-25 23:14:05', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 175),
(61, 2, 105, 0, '2022-03-25', '2022-03-25 23:14:05', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 176),
(62, 2, 105, 0, '2022-03-25', '2022-03-25 23:14:05', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 177),
(63, 3, 105, 0, '2022-03-25', '2022-03-25 23:14:05', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 178),
(64, 3, 105, 0, '2022-03-25', '2022-03-25 23:14:05', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 179),
(65, 4, 105, 0, '2022-03-25', '2022-03-25 23:14:05', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 180),
(66, 4, 105, 0, '2022-03-25', '2022-03-25 23:14:05', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 181),
(67, 1, 106, 0, '2022-03-25', '2022-03-25 23:14:55', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 182),
(68, 1, 106, 0, '2022-03-25', '2022-03-25 23:14:55', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 183),
(69, 2, 106, 0, '2022-03-25', '2022-03-25 23:14:55', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 184),
(70, 2, 106, 0, '2022-03-25', '2022-03-25 23:14:55', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 185),
(71, 3, 106, 0, '2022-03-25', '2022-03-25 23:14:55', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 186),
(72, 3, 106, 0, '2022-03-25', '2022-03-25 23:14:55', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 187),
(73, 4, 106, 0, '2022-03-25', '2022-03-25 23:14:55', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 188),
(74, 4, 106, 0, '2022-03-25', '2022-03-25 23:14:55', '2022-03-31 06:09:30', NULL, '172.50', 'USD', 189),
(75, NULL, 107, 0, '2022-03-25', '2022-03-25 23:16:12', '2022-03-31 06:09:30', NULL, '200.00', 'USD', 190),
(76, NULL, 108, 0, '2022-03-25', '2022-03-25 23:16:44', '2022-03-31 06:09:30', NULL, '200.00', 'USD', 191),
(77, NULL, 109, 0, '2022-03-25', '2022-03-31 06:02:23', '2022-03-31 06:09:30', NULL, '200.00', 'USD', 192),
(78, NULL, 110, 0, '2022-09-01', '2022-09-26 09:13:32', '2022-09-26 09:13:32', NULL, '123123.00', 'VES', 193),
(79, NULL, 111, 0, '2022-09-01', '2022-09-26 09:13:56', '2022-09-26 09:13:56', NULL, '234.00', 'VES', 194),
(80, NULL, 112, 0, '2022-09-01', '2022-10-01 05:32:46', '2022-10-01 05:32:46', NULL, '150.00', 'USD', 195),
(81, NULL, 115, 0, '2022-10-01', '2022-10-03 06:12:25', '2022-10-03 06:12:25', NULL, '5.00', '%', 198);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `expenses_invoices`
--

CREATE TABLE `expenses_invoices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idinvoice` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` decimal(15,2) NOT NULL,
  `currency_code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expense_fee_id` bigint(20) UNSIGNED NOT NULL,
  `amount_fee` decimal(15,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `expenses_invoices`
--

INSERT INTO `expenses_invoices` (`id`, `idinvoice`, `description`, `amount`, `currency_code`, `deleted_at`, `created_at`, `updated_at`, `expense_fee_id`, `amount_fee`) VALUES
(1636, 454, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:18', 78, '10.00'),
(1637, 454, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1638, 454, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1639, 455, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:18', 78, '10.00'),
(1640, 455, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1641, 455, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1642, 456, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:18', 78, '10.00'),
(1643, 456, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1644, 456, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1645, 457, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:18', 78, '10.00'),
(1646, 457, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1647, 457, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1648, 458, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:18', 78, '10.00'),
(1649, 458, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1650, 458, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1651, 459, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:18', 78, '10.00'),
(1652, 459, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1653, 459, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1654, 460, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:18', 78, '10.00'),
(1655, 460, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1656, 460, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1657, 461, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:18', 78, '10.00'),
(1658, 461, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1659, 461, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1660, 462, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:18', 78, '10.00'),
(1661, 462, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1662, 462, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1663, 463, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:18', 78, '10.00'),
(1664, 463, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1665, 463, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1666, 464, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:19', 78, '10.00'),
(1667, 464, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1668, 464, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1669, 465, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:20', 78, '10.00'),
(1670, 465, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1671, 465, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1672, 466, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:21', 78, '10.00'),
(1673, 466, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1674, 466, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1675, 467, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:21', 78, '10.00'),
(1676, 467, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1677, 467, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1678, 468, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:21', 78, '10.00'),
(1679, 468, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1680, 468, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1681, 469, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:21', 78, '10.00'),
(1682, 469, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1683, 469, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1684, 470, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1685, 470, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1686, 470, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1687, 471, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1688, 471, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1689, 471, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1690, 472, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1691, 472, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1692, 472, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1693, 473, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1694, 473, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1695, 473, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1696, 474, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1697, 474, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1698, 474, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1699, 475, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1700, 475, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1701, 475, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1702, 476, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1703, 476, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1704, 476, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1705, 477, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1706, 477, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1707, 477, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1708, 478, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1709, 478, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1710, 478, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1711, 479, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1712, 479, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1713, 479, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1714, 480, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1715, 480, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1716, 480, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1717, 481, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1718, 481, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1719, 481, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1720, 482, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1721, 482, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1722, 482, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1723, 483, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1724, 483, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1725, 483, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1726, 484, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1727, 484, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1728, 484, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1729, 485, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1730, 485, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1731, 485, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1732, 486, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1733, 486, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1734, 486, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1735, 487, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1736, 487, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1737, 487, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1738, 488, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1739, 488, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1740, 488, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1741, 489, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1742, 489, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1743, 489, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1744, 490, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1745, 490, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1746, 490, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1747, 491, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1748, 491, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1749, 491, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:42', 80, '10.00'),
(1750, 492, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1751, 492, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1752, 492, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:43', 80, '10.00'),
(1753, 493, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1754, 493, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:32', 79, '10.00'),
(1755, 493, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:43', 80, '10.00'),
(1756, 494, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1757, 494, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:33', 79, '10.00'),
(1758, 494, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:43', 80, '10.00'),
(1759, 495, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1760, 495, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:33', 79, '10.00'),
(1761, 495, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:43', 80, '10.00'),
(1762, 496, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:22', 78, '10.00'),
(1763, 496, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:33', 79, '10.00'),
(1764, 496, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:43', 80, '10.00'),
(1765, 497, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:23', 78, '10.00'),
(1766, 497, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:33', 79, '10.00'),
(1767, 497, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:43', 80, '10.00'),
(1768, 498, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:23', 78, '10.00'),
(1769, 498, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:33', 79, '10.00'),
(1770, 498, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:43', 80, '10.00'),
(1771, 499, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:23', 78, '10.00'),
(1772, 499, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:33', 79, '10.00'),
(1773, 499, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:43', 80, '10.00'),
(1774, 500, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:23', 78, '10.00'),
(1775, 500, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:33', 79, '10.00'),
(1776, 500, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:43', 80, '10.00'),
(1777, 501, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:23', 78, '10.00'),
(1778, 501, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:33', 79, '10.00'),
(1779, 501, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:43', 80, '10.00'),
(1780, 502, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:23', 78, '10.00'),
(1781, 502, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:33', 79, '10.00'),
(1782, 502, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:43', 80, '10.00'),
(1783, 503, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:23', 78, '10.00'),
(1784, 503, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:33', 79, '10.00'),
(1785, 503, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:43', 80, '10.00'),
(1786, 504, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:23', 78, '10.00'),
(1787, 504, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:33', 79, '10.00'),
(1788, 504, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:43', 80, '10.00'),
(1789, 505, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:23', 78, '10.00'),
(1790, 505, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:33', 79, '10.00'),
(1791, 505, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:43', 80, '10.00'),
(1792, 506, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:23', 78, '10.00'),
(1793, 506, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:33', 79, '10.00'),
(1794, 506, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:43', 80, '10.00'),
(1795, 507, '', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:23', 78, '10.00'),
(1796, 507, 'diciembre 2022', '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:33', 79, '10.00'),
(1797, 507, NULL, '0.17', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:43', 80, '10.00'),
(1798, 508, '', '0.29', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:23', 78, '10.00'),
(1799, 508, 'diciembre 2022', '0.29', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:33', 79, '10.00'),
(1800, 508, NULL, '0.29', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:43', 80, '10.00'),
(1801, 509, '', '0.19', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:23', 78, '10.00'),
(1802, 509, 'diciembre 2022', '0.19', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:33', 79, '10.00'),
(1803, 509, NULL, '0.19', 'VES', NULL, '2022-09-01 07:28:51', '2022-10-02 01:21:43', 80, '10.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historical_exchanges`
--

CREATE TABLE `historical_exchanges` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idexchange` bigint(20) UNSIGNED NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `currency_code` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `historical_exchanges`
--

INSERT INTO `historical_exchanges` (`id`, `idexchange`, `amount`, `deleted_at`, `created_at`, `updated_at`, `currency_code`) VALUES
(1, 1, '3988000.00', NULL, '2021-07-23 16:12:29', '2021-07-23 16:12:29', NULL),
(2, 1, '4.60', NULL, '2022-01-03 23:28:36', '2022-01-03 23:28:36', NULL),
(3, 1, '4.59', NULL, '2022-01-04 00:54:26', '2022-01-04 00:54:26', NULL),
(4, 1, '4.59', NULL, '2022-01-04 01:01:02', '2022-01-04 01:01:02', NULL),
(5, 1, '4.59', NULL, '2022-01-04 01:20:59', '2022-01-04 01:20:59', NULL),
(6, 1, '4.59', NULL, '2022-01-04 01:22:44', '2022-01-04 01:22:44', NULL),
(7, 2, '1.00', NULL, NULL, NULL, NULL),
(8, 3, '1.00', NULL, NULL, NULL, NULL),
(10, 1, '4.59', NULL, '2022-01-04 01:24:54', '2022-01-04 01:24:54', NULL),
(11, 1, '4.59', NULL, '2022-01-04 01:25:14', '2022-01-04 01:25:14', NULL),
(12, 1, '4.63', NULL, '2022-01-16 01:44:31', '2022-01-16 01:44:31', NULL),
(13, 1, '4.63', NULL, '2022-01-16 03:27:37', '2022-01-16 03:27:37', NULL),
(14, 1, '4.63', NULL, '2022-01-16 03:34:51', '2022-01-16 03:34:51', NULL),
(15, 1, '4.63', NULL, '2022-01-16 03:37:17', '2022-01-16 03:37:17', NULL),
(16, 1, '4.63', NULL, '2022-01-16 03:38:53', '2022-01-16 03:38:53', NULL),
(17, 1, '4.63', NULL, '2022-01-16 04:57:15', '2022-01-16 04:57:15', NULL),
(18, 1, '4.63', NULL, '2022-01-16 19:55:51', '2022-01-16 19:55:51', NULL),
(19, 1, '4.63', NULL, '2022-01-16 20:36:56', '2022-01-16 20:36:56', NULL),
(20, 1, '4.63', NULL, '2022-01-16 20:38:45', '2022-01-16 20:38:45', NULL),
(21, 1, '4.63', NULL, '2022-01-16 21:18:06', '2022-01-16 21:18:06', NULL),
(22, 1, '4.63', NULL, '2022-01-16 21:18:13', '2022-01-16 21:18:13', NULL),
(23, 1, '4.63', NULL, '2022-01-16 21:34:17', '2022-01-16 21:34:17', NULL),
(24, 1, '4.63', NULL, '2022-01-16 21:34:54', '2022-01-16 21:34:54', NULL),
(25, 1, '4.63', NULL, '2022-01-17 03:27:33', '2022-01-17 03:27:33', NULL),
(26, 1, '4.63', NULL, '2022-01-17 03:28:26', '2022-01-17 03:28:26', NULL),
(27, 1, '4.63', NULL, '2022-01-17 04:35:44', '2022-01-17 04:35:44', NULL),
(28, 1, '4.63', NULL, '2022-01-17 04:57:48', '2022-01-17 04:57:48', NULL),
(29, 1, '4.63', NULL, '2022-01-17 06:07:37', '2022-01-17 06:07:37', NULL),
(30, 1, '4.63', NULL, '2022-01-17 06:08:43', '2022-01-17 06:08:43', NULL),
(31, 1, '4.63', NULL, '2022-01-17 06:09:54', '2022-01-17 06:09:54', NULL),
(32, 1, '4.63', NULL, '2022-01-17 06:19:16', '2022-01-17 06:19:16', NULL),
(33, 1, '4.63', NULL, '2022-01-17 06:28:28', '2022-01-17 06:28:28', NULL),
(34, 1, '4.63', NULL, '2022-01-17 06:34:15', '2022-01-17 06:34:15', NULL),
(35, 1, '4.63', NULL, '2022-01-17 06:34:28', '2022-01-17 06:34:28', NULL),
(36, 1, '4.63', NULL, '2022-01-17 06:35:06', '2022-01-17 06:35:06', NULL),
(37, 1, '4.63', NULL, '2022-01-17 06:37:25', '2022-01-17 06:37:25', NULL),
(38, 1, '4.63', NULL, '2022-01-18 04:30:07', '2022-01-18 04:30:07', NULL),
(39, 1, '4.63', NULL, '2022-01-18 20:19:32', '2022-01-18 20:19:32', NULL),
(40, 1, '4.63', NULL, '2022-01-18 21:22:19', '2022-01-18 21:22:19', NULL),
(41, 1, '4.63', NULL, '2022-01-18 21:32:20', '2022-01-18 21:32:20', NULL),
(42, 1, '4.63', NULL, '2022-01-18 21:54:48', '2022-01-18 21:54:48', NULL),
(43, 1, '4.63', NULL, '2022-01-18 22:16:29', '2022-01-18 22:16:29', NULL),
(44, 1, '4.63', NULL, '2022-01-18 22:21:21', '2022-01-18 22:21:21', NULL),
(45, 1, '4.63', NULL, '2022-01-18 22:27:34', '2022-01-18 22:27:34', NULL),
(46, 1, '4.63', NULL, '2022-01-19 00:46:47', '2022-01-19 00:46:47', NULL),
(47, 1, '4.63', NULL, '2022-01-19 00:47:09', '2022-01-19 00:47:09', NULL),
(48, 1, '4.63', NULL, '2022-01-19 00:54:26', '2022-01-19 00:54:26', NULL),
(49, 1, '4.63', NULL, '2022-01-19 01:01:53', '2022-01-19 01:01:53', NULL),
(50, 1, '4.63', NULL, '2022-01-19 01:04:19', '2022-01-19 01:04:19', NULL),
(51, 1, '4.63', NULL, '2022-01-19 02:33:03', '2022-01-19 02:33:03', NULL),
(52, 1, '4.63', NULL, '2022-01-19 02:33:19', '2022-01-19 02:33:19', NULL),
(53, 1, '4.64', NULL, '2022-01-20 04:57:14', '2022-01-20 04:57:14', NULL),
(54, 1, '4.64', NULL, '2022-01-20 05:07:08', '2022-01-20 05:07:08', NULL),
(55, 1, '4.64', NULL, '2022-01-20 09:04:37', '2022-01-20 09:04:37', NULL),
(56, 1, '4.64', NULL, '2022-01-20 17:58:01', '2022-01-20 17:58:01', NULL),
(57, 1, '4.64', NULL, '2022-01-20 18:25:26', '2022-01-20 18:25:26', NULL),
(58, 1, '4.64', NULL, '2022-01-20 18:27:26', '2022-01-20 18:27:26', NULL),
(59, 1, '4.61', NULL, '2022-01-22 19:38:54', '2022-01-22 19:38:54', NULL),
(60, 1, '4.61', NULL, '2022-01-22 21:19:21', '2022-01-22 21:19:21', NULL),
(61, 1, '4.61', NULL, '2022-01-22 23:24:24', '2022-01-22 23:24:24', NULL),
(62, 1, '4.61', NULL, '2022-01-23 02:47:47', '2022-01-23 02:47:47', NULL),
(63, 1, '4.61', NULL, '2022-01-23 02:55:39', '2022-01-23 02:55:39', NULL),
(64, 1, '4.61', NULL, '2022-01-23 02:55:53', '2022-01-23 02:55:53', NULL),
(65, 1, '4.61', NULL, '2022-01-23 03:03:06', '2022-01-23 03:03:06', NULL),
(66, 1, '4.61', NULL, '2022-01-23 03:03:24', '2022-01-23 03:03:24', NULL),
(67, 1, '4.61', NULL, '2022-01-24 00:01:52', '2022-01-24 00:01:52', NULL),
(68, 1, '4.61', NULL, '2022-01-24 00:42:03', '2022-01-24 00:42:03', NULL),
(69, 1, '4.61', NULL, '2022-01-24 00:47:35', '2022-01-24 00:47:35', NULL),
(70, 1, '4.61', NULL, '2022-01-24 00:48:09', '2022-01-24 00:48:09', NULL),
(71, 1, '4.61', NULL, '2022-01-24 00:49:23', '2022-01-24 00:49:23', NULL),
(72, 1, '4.61', NULL, '2022-01-24 00:50:24', '2022-01-24 00:50:24', NULL),
(73, 1, '4.61', NULL, '2022-01-24 00:51:53', '2022-01-24 00:51:53', NULL),
(74, 1, '4.61', NULL, '2022-01-24 00:59:40', '2022-01-24 00:59:40', NULL),
(75, 1, '4.61', NULL, '2022-01-24 01:04:14', '2022-01-24 01:04:14', NULL),
(76, 1, '4.61', NULL, '2022-01-24 01:12:56', '2022-01-24 01:12:56', NULL),
(77, 1, '4.61', NULL, '2022-01-24 01:16:02', '2022-01-24 01:16:02', NULL),
(78, 1, '4.61', NULL, '2022-01-24 01:17:24', '2022-01-24 01:17:24', NULL),
(79, 1, '4.61', NULL, '2022-01-24 02:14:36', '2022-01-24 02:14:36', NULL),
(80, 1, '4.61', NULL, '2022-01-24 02:19:19', '2022-01-24 02:19:19', NULL),
(81, 1, '4.61', NULL, '2022-01-24 02:19:49', '2022-01-24 02:19:49', NULL),
(82, 1, '4.61', NULL, '2022-01-24 02:20:39', '2022-01-24 02:20:39', NULL),
(83, 1, '4.61', NULL, '2022-01-24 02:27:13', '2022-01-24 02:27:13', NULL),
(84, 1, '4.61', NULL, '2022-01-24 02:27:29', '2022-01-24 02:27:29', NULL),
(85, 1, '4.61', NULL, '2022-01-24 02:28:41', '2022-01-24 02:28:41', NULL),
(86, 1, '4.61', NULL, '2022-01-24 02:32:55', '2022-01-24 02:32:55', NULL),
(87, 1, '4.61', NULL, '2022-01-24 02:33:25', '2022-01-24 02:33:25', NULL),
(88, 1, '4.61', NULL, '2022-01-24 02:33:51', '2022-01-24 02:33:51', NULL),
(89, 1, '4.61', NULL, '2022-01-24 02:34:15', '2022-01-24 02:34:15', NULL),
(90, 1, '4.61', NULL, '2022-01-24 02:35:45', '2022-01-24 02:35:45', NULL),
(91, 1, '4.61', NULL, '2022-01-24 02:43:48', '2022-01-24 02:43:48', NULL),
(92, 1, '4.61', NULL, '2022-01-24 03:58:14', '2022-01-24 03:58:14', NULL),
(93, 1, '4.61', NULL, '2022-01-24 04:02:21', '2022-01-24 04:02:21', NULL),
(94, 1, '4.61', NULL, '2022-01-24 04:04:07', '2022-01-24 04:04:07', NULL),
(95, 1, '4.61', NULL, '2022-01-24 04:12:34', '2022-01-24 04:12:34', NULL),
(96, 1, '4.61', NULL, '2022-01-24 04:13:40', '2022-01-24 04:13:40', NULL),
(97, 1, '4.61', NULL, '2022-01-24 04:15:38', '2022-01-24 04:15:38', NULL),
(98, 1, '4.61', NULL, '2022-01-24 04:16:50', '2022-01-24 04:16:50', NULL),
(99, 1, '4.61', NULL, '2022-01-24 04:17:14', '2022-01-24 04:17:14', NULL),
(100, 1, '4.61', NULL, '2022-01-24 04:17:22', '2022-01-24 04:17:22', NULL),
(101, 1, '4.61', NULL, '2022-01-24 04:18:37', '2022-01-24 04:18:37', NULL),
(102, 1, '4.61', NULL, '2022-01-24 04:30:46', '2022-01-24 04:30:46', NULL),
(103, 1, '4.61', NULL, '2022-01-24 04:41:36', '2022-01-24 04:41:36', NULL),
(104, 2, '4.83', NULL, '2022-01-24 06:40:17', '2022-01-24 06:40:17', NULL),
(105, 2, '4.83', NULL, '2022-01-24 06:40:56', '2022-01-24 06:40:56', NULL),
(106, 1, '4.61', NULL, '2022-01-24 06:49:26', '2022-01-24 06:49:26', NULL),
(107, 2, '4.83', NULL, '2022-01-24 06:49:29', '2022-01-24 06:49:29', NULL),
(108, 1, '4.61', NULL, '2022-01-24 06:51:45', '2022-01-24 06:51:45', NULL),
(109, 2, '4.83', NULL, '2022-01-24 06:51:48', '2022-01-24 06:51:48', NULL),
(110, 1, '4.61', NULL, '2022-01-24 06:57:10', '2022-01-24 06:57:10', NULL),
(111, 2, '4.83', NULL, '2022-01-24 06:57:17', '2022-01-24 06:57:17', NULL),
(112, 1, '4.61', NULL, '2022-01-24 06:59:43', '2022-01-24 06:59:43', NULL),
(113, 2, '4.83', NULL, '2022-01-24 06:59:52', '2022-01-24 06:59:52', NULL),
(114, 1, '4.61', NULL, '2022-01-24 07:00:20', '2022-01-24 07:00:20', NULL),
(115, 2, '4.83', NULL, '2022-01-24 07:00:29', '2022-01-24 07:00:29', NULL),
(116, 1, '4.61', NULL, '2022-01-24 07:03:05', '2022-01-24 07:03:05', NULL),
(117, 2, '4.83', NULL, '2022-01-24 07:03:07', '2022-01-24 07:03:07', NULL),
(118, 1, '4.61', NULL, '2022-01-24 07:09:52', '2022-01-24 07:09:52', NULL),
(119, 2, '4.83', NULL, '2022-01-24 07:09:58', '2022-01-24 07:09:58', NULL),
(120, 1, '4.61', NULL, '2022-01-24 07:18:03', '2022-01-24 07:18:03', NULL),
(121, 2, '4.83', NULL, '2022-01-24 07:18:09', '2022-01-24 07:18:09', NULL),
(122, 1, '4.61', NULL, '2022-01-24 07:22:40', '2022-01-24 07:22:40', NULL),
(123, 2, '4.83', NULL, '2022-01-24 07:22:41', '2022-01-24 07:22:41', NULL),
(124, 1, '4.61', NULL, '2022-01-24 07:23:07', '2022-01-24 07:23:07', NULL),
(125, 2, '4.83', NULL, '2022-01-24 07:23:09', '2022-01-24 07:23:09', NULL),
(126, 2, '4.83', NULL, '2022-01-24 07:25:59', '2022-01-24 07:25:59', NULL),
(127, 1, '4.61', NULL, '2022-01-24 07:26:03', '2022-01-24 07:26:03', NULL),
(128, 1, '4.61', NULL, '2022-01-24 07:32:08', '2022-01-24 07:32:08', NULL),
(129, 2, '4.83', NULL, '2022-01-24 07:32:17', '2022-01-24 07:32:17', NULL),
(130, 1, '4.61', NULL, '2022-01-24 07:33:43', '2022-01-24 07:33:43', NULL),
(131, 2, '4.83', NULL, '2022-01-24 07:33:45', '2022-01-24 07:33:45', NULL),
(132, 1, '4.61', NULL, '2022-01-24 07:36:36', '2022-01-24 07:36:36', NULL),
(133, 2, '4.83', NULL, '2022-01-24 07:36:38', '2022-01-24 07:36:38', NULL),
(134, 2, '4.83', NULL, '2022-01-24 07:40:51', '2022-01-24 07:40:51', NULL),
(135, 1, '4.61', NULL, '2022-01-24 07:40:56', '2022-01-24 07:40:56', NULL),
(136, 1, '4.61', NULL, '2022-01-24 07:42:04', '2022-01-24 07:42:04', NULL),
(137, 2, '4.83', NULL, '2022-01-24 07:42:05', '2022-01-24 07:42:05', NULL),
(138, 1, '4.61', NULL, '2022-01-24 07:45:30', '2022-01-24 07:45:30', NULL),
(139, 2, '4.83', NULL, '2022-01-24 07:45:32', '2022-01-24 07:45:32', NULL),
(140, 1, '4.61', NULL, '2022-01-24 07:47:28', '2022-01-24 07:47:28', NULL),
(141, 2, '4.83', NULL, '2022-01-24 07:47:30', '2022-01-24 07:47:30', NULL),
(142, 1, '4.61', NULL, '2022-01-24 07:50:05', '2022-01-24 07:50:05', NULL),
(143, 2, '4.83', NULL, '2022-01-24 07:50:08', '2022-01-24 07:50:08', NULL),
(144, 1, '4.61', NULL, '2022-01-24 07:53:19', '2022-01-24 07:53:19', NULL),
(145, 2, '4.83', NULL, '2022-01-24 07:53:21', '2022-01-24 07:53:21', NULL),
(146, 1, '4.61', NULL, '2022-01-24 07:55:13', '2022-01-24 07:55:13', NULL),
(147, 2, '4.83', NULL, '2022-01-24 07:55:15', '2022-01-24 07:55:15', NULL),
(148, 1, '4.61', NULL, '2022-01-24 07:58:28', '2022-01-24 07:58:28', NULL),
(149, 2, '4.83', NULL, '2022-01-24 07:58:32', '2022-01-24 07:58:32', NULL),
(150, 1, '4.61', NULL, '2022-01-24 08:00:43', '2022-01-24 08:00:43', NULL),
(151, 2, '4.83', NULL, '2022-01-24 08:00:45', '2022-01-24 08:00:45', NULL),
(152, 1, '4.61', NULL, '2022-01-24 08:03:20', '2022-01-24 08:03:20', NULL),
(153, 2, '4.83', NULL, '2022-01-24 08:03:21', '2022-01-24 08:03:21', NULL),
(154, 1, '4.61', NULL, '2022-01-24 08:04:37', '2022-01-24 08:04:37', NULL),
(155, 2, '4.83', NULL, '2022-01-24 08:04:38', '2022-01-24 08:04:38', NULL),
(156, 1, '4.61', NULL, '2022-01-24 08:06:21', '2022-01-24 08:06:21', NULL),
(157, 2, '4.83', NULL, '2022-01-24 08:06:26', '2022-01-24 08:06:26', NULL),
(158, 1, '4.61', NULL, '2022-01-24 08:08:12', '2022-01-24 08:08:12', NULL),
(159, 2, '4.83', NULL, '2022-01-24 08:08:14', '2022-01-24 08:08:14', NULL),
(160, 1, '4.61', NULL, '2022-01-24 08:11:03', '2022-01-24 08:11:03', NULL),
(161, 2, '4.83', NULL, '2022-01-24 08:11:09', '2022-01-24 08:11:09', NULL),
(162, 2, '4.83', NULL, '2022-01-24 08:13:11', '2022-01-24 08:13:11', NULL),
(163, 1, '4.61', NULL, '2022-01-24 08:13:13', '2022-01-24 08:13:13', NULL),
(164, 1, '4.61', NULL, '2022-01-24 08:15:19', '2022-01-24 08:15:19', NULL),
(165, 2, '4.83', NULL, '2022-01-24 08:15:21', '2022-01-24 08:15:21', NULL),
(166, 1, '4.61', NULL, '2022-01-24 08:17:39', '2022-01-24 08:17:39', NULL),
(167, 2, '4.83', NULL, '2022-01-24 08:17:42', '2022-01-24 08:17:42', NULL),
(168, 1, '4.61', NULL, '2022-01-24 08:18:50', '2022-01-24 08:18:50', NULL),
(169, 2, '4.83', NULL, '2022-01-24 08:18:54', '2022-01-24 08:18:54', NULL),
(170, 1, '4.61', NULL, '2022-01-24 08:19:27', '2022-01-24 08:19:27', NULL),
(171, 2, '4.83', NULL, '2022-01-24 08:19:30', '2022-01-24 08:19:30', NULL),
(172, 1, '4.61', NULL, '2022-01-24 08:21:44', '2022-01-24 08:21:44', NULL),
(173, 2, '4.83', NULL, '2022-01-24 08:21:46', '2022-01-24 08:21:46', NULL),
(174, 1, '4.61', NULL, '2022-01-24 08:23:28', '2022-01-24 08:23:28', NULL),
(175, 2, '4.83', NULL, '2022-01-24 08:23:29', '2022-01-24 08:23:29', NULL),
(176, 1, '4.61', NULL, '2022-01-24 08:31:59', '2022-01-24 08:31:59', NULL),
(177, 2, '4.83', NULL, '2022-01-24 08:32:01', '2022-01-24 08:32:01', NULL),
(178, 1, '4.61', NULL, '2022-01-24 18:41:22', '2022-01-24 18:41:22', NULL),
(179, 2, '4.81', NULL, '2022-01-24 18:41:24', '2022-01-24 18:41:24', NULL),
(180, 3, '4.76', NULL, '2022-01-24 18:41:26', '2022-01-24 18:41:26', NULL),
(181, 1, '4.61', NULL, '2022-01-24 18:41:59', '2022-01-24 18:41:59', NULL),
(182, 2, '4.81', NULL, '2022-01-24 18:42:00', '2022-01-24 18:42:00', NULL),
(183, 3, '4.76', NULL, '2022-01-24 18:42:02', '2022-01-24 18:42:02', NULL),
(184, 1, '4.61', NULL, '2022-01-24 19:13:30', '2022-01-24 19:13:30', NULL),
(185, 2, '4.81', NULL, '2022-01-24 19:13:31', '2022-01-24 19:13:31', NULL),
(186, 1, '4.61', NULL, '2022-01-24 19:14:18', '2022-01-24 19:14:18', NULL),
(187, 2, '4.81', NULL, '2022-01-24 19:14:19', '2022-01-24 19:14:19', NULL),
(188, 1, '4.61', NULL, '2022-01-24 19:56:47', '2022-01-24 19:56:47', NULL),
(189, 2, '4.81', NULL, '2022-01-24 19:56:49', '2022-01-24 19:56:49', NULL),
(190, 1, '4.61', NULL, '2022-01-24 19:58:19', '2022-01-24 19:58:19', NULL),
(191, 2, '4.81', NULL, '2022-01-24 19:58:21', '2022-01-24 19:58:21', NULL),
(192, 1, '4.61', NULL, '2022-01-24 19:58:53', '2022-01-24 19:58:53', NULL),
(193, 2, '4.81', NULL, '2022-01-24 19:58:54', '2022-01-24 19:58:54', NULL),
(194, 1, '4.61', NULL, '2022-01-24 20:00:08', '2022-01-24 20:00:08', NULL),
(195, 2, '4.81', NULL, '2022-01-24 20:00:08', '2022-01-24 20:00:08', NULL),
(196, 2, '4.81', NULL, '2022-01-24 20:39:53', '2022-01-24 20:39:53', NULL),
(197, 1, '4.61', NULL, '2022-01-24 21:41:49', '2022-01-24 21:41:49', NULL),
(198, 2, '4.81', NULL, '2022-01-24 21:41:52', '2022-01-24 21:41:52', NULL),
(199, 1, '4.61', NULL, '2022-01-24 21:44:48', '2022-01-24 21:44:48', NULL),
(200, 2, '4.81', NULL, '2022-01-24 21:44:49', '2022-01-24 21:44:49', NULL),
(201, 1, '4.61', NULL, '2022-01-24 21:57:34', '2022-01-24 21:57:34', NULL),
(202, 2, '4.81', NULL, '2022-01-24 21:57:35', '2022-01-24 21:57:35', NULL),
(203, 1, '4.61', NULL, '2022-01-24 22:24:48', '2022-01-24 22:24:48', NULL),
(204, 2, '4.81', NULL, '2022-01-24 22:24:51', '2022-01-24 22:24:51', NULL),
(205, 1, '4.59', NULL, '2022-01-25 01:56:33', '2022-01-25 01:56:33', NULL),
(206, 2, '4.81', NULL, '2022-01-25 01:56:35', '2022-01-25 01:56:35', NULL),
(207, 1, '4.59', NULL, '2022-01-25 05:02:38', '2022-01-25 05:02:38', NULL),
(208, 2, '4.81', NULL, '2022-01-25 05:02:41', '2022-01-25 05:02:41', NULL),
(209, 1, '4.59', NULL, '2022-01-25 08:23:56', '2022-01-25 08:23:56', NULL),
(210, 2, '4.81', NULL, '2022-01-25 08:23:59', '2022-01-25 08:23:59', NULL),
(211, 1, '4.59', NULL, '2022-01-25 08:35:31', '2022-01-25 08:35:31', NULL),
(212, 2, '4.81', NULL, '2022-01-25 08:35:32', '2022-01-25 08:35:32', NULL),
(213, 1, '4.59', NULL, '2022-01-25 08:48:11', '2022-01-25 08:48:11', NULL),
(214, 2, '4.81', NULL, '2022-01-25 08:48:23', '2022-01-25 08:48:23', NULL),
(215, 1, '4.59', NULL, '2022-01-25 08:48:48', '2022-01-25 08:48:48', NULL),
(216, 2, '4.81', NULL, '2022-01-25 08:48:51', '2022-01-25 08:48:51', NULL),
(217, 1, '4.59', NULL, '2022-01-25 08:49:08', '2022-01-25 08:49:08', NULL),
(218, 2, '4.81', NULL, '2022-01-25 08:49:10', '2022-01-25 08:49:10', NULL),
(219, 1, '4.59', NULL, '2022-01-25 08:49:32', '2022-01-25 08:49:32', NULL),
(220, 2, '4.81', NULL, '2022-01-25 08:49:35', '2022-01-25 08:49:35', NULL),
(221, 1, '4.59', NULL, '2022-01-25 08:50:23', '2022-01-25 08:50:23', NULL),
(222, 2, '4.81', NULL, '2022-01-25 08:50:34', '2022-01-25 08:50:34', NULL),
(223, 2, '4.81', NULL, '2022-01-25 08:50:47', '2022-01-25 08:50:47', NULL),
(224, 1, '4.59', NULL, '2022-01-25 08:50:54', '2022-01-25 08:50:54', NULL),
(225, 1, '4.59', NULL, '2022-01-25 08:51:21', '2022-01-25 08:51:21', NULL),
(226, 2, '4.81', NULL, '2022-01-25 08:51:30', '2022-01-25 08:51:30', NULL),
(227, 1, '4.59', NULL, '2022-01-25 08:52:58', '2022-01-25 08:52:58', NULL),
(228, 2, '4.81', NULL, '2022-01-25 08:53:01', '2022-01-25 08:53:01', NULL),
(229, 1, '4.59', NULL, '2022-01-25 08:53:50', '2022-01-25 08:53:50', NULL),
(230, 2, '4.81', NULL, '2022-01-25 08:53:53', '2022-01-25 08:53:53', NULL),
(231, 1, '4.59', NULL, '2022-01-25 08:55:16', '2022-01-25 08:55:16', NULL),
(232, 2, '4.81', NULL, '2022-01-25 08:55:27', '2022-01-25 08:55:27', NULL),
(233, 1, '4.59', NULL, '2022-01-25 08:57:08', '2022-01-25 08:57:08', NULL),
(234, 2, '4.81', NULL, '2022-01-25 08:57:09', '2022-01-25 08:57:09', NULL),
(235, 1, '4.59', NULL, '2022-01-25 08:58:10', '2022-01-25 08:58:10', NULL),
(236, 2, '4.81', NULL, '2022-01-25 08:58:13', '2022-01-25 08:58:13', NULL),
(237, 1, '4.59', NULL, '2022-01-25 08:59:45', '2022-01-25 08:59:45', NULL),
(238, 2, '4.81', NULL, '2022-01-25 08:59:49', '2022-01-25 08:59:49', NULL),
(239, 1, '4.59', NULL, '2022-01-25 09:00:32', '2022-01-25 09:00:32', NULL),
(240, 2, '4.81', NULL, '2022-01-25 09:00:34', '2022-01-25 09:00:34', NULL),
(241, 1, '4.59', NULL, '2022-01-25 09:00:53', '2022-01-25 09:00:53', NULL),
(242, 2, '4.81', NULL, '2022-01-25 09:00:55', '2022-01-25 09:00:55', NULL),
(243, 1, '4.59', NULL, '2022-01-25 09:01:24', '2022-01-25 09:01:24', NULL),
(244, 2, '4.81', NULL, '2022-01-25 09:01:28', '2022-01-25 09:01:28', NULL),
(245, 1, '4.59', NULL, '2022-01-25 09:01:43', '2022-01-25 09:01:43', NULL),
(246, 2, '4.81', NULL, '2022-01-25 09:01:45', '2022-01-25 09:01:45', NULL),
(247, 2, '4.81', NULL, '2022-01-25 09:03:34', '2022-01-25 09:03:34', NULL),
(248, 1, '4.59', NULL, '2022-01-25 09:03:41', '2022-01-25 09:03:41', NULL),
(249, 1, '4.59', NULL, '2022-01-25 09:03:49', '2022-01-25 09:03:49', NULL),
(250, 2, '4.81', NULL, '2022-01-25 09:03:51', '2022-01-25 09:03:51', NULL),
(251, 1, '4.59', NULL, '2022-01-25 09:04:06', '2022-01-25 09:04:06', NULL),
(252, 2, '4.81', NULL, '2022-01-25 09:04:08', '2022-01-25 09:04:08', NULL),
(253, 1, '4.59', NULL, '2022-01-25 18:19:08', '2022-01-25 18:19:08', NULL),
(254, 2, '4.79', NULL, '2022-01-25 18:19:10', '2022-01-25 18:19:10', NULL),
(255, 1, '4.59', NULL, '2022-01-25 18:22:52', '2022-01-25 18:22:52', NULL),
(256, 2, '4.79', NULL, '2022-01-25 18:22:53', '2022-01-25 18:22:53', NULL),
(257, 1, '4.59', NULL, '2022-01-25 18:35:50', '2022-01-25 18:35:50', NULL),
(258, 2, '4.79', NULL, '2022-01-25 18:35:51', '2022-01-25 18:35:51', NULL),
(259, 1, '4.59', NULL, '2022-01-25 18:36:34', '2022-01-25 18:36:34', NULL),
(260, 2, '4.79', NULL, '2022-01-25 18:36:35', '2022-01-25 18:36:35', NULL),
(261, 1, '4.59', NULL, '2022-01-26 17:58:53', '2022-01-26 17:58:53', NULL),
(262, 3, '4.72', NULL, '2022-01-26 17:59:03', '2022-01-26 17:59:03', NULL),
(263, 1, '4.59', NULL, '2022-01-26 18:12:37', '2022-01-26 18:12:37', NULL),
(264, 3, '4.72', NULL, '2022-01-26 18:12:41', '2022-01-26 18:12:41', NULL),
(265, 1, '4.59', NULL, '2022-01-26 18:13:09', '2022-01-26 18:13:09', NULL),
(266, 3, '4.72', NULL, '2022-01-26 18:13:13', '2022-01-26 18:13:13', NULL),
(267, 1, '4.58', NULL, '2022-01-27 10:52:22', '2022-01-27 10:52:22', NULL),
(268, 2, '4.74', NULL, '2022-01-27 10:52:23', '2022-01-27 10:52:23', NULL),
(269, 1, '4.58', NULL, '2022-01-27 18:24:20', '2022-01-27 18:24:20', NULL),
(270, 2, '4.69', NULL, '2022-01-27 18:24:24', '2022-01-27 18:24:24', NULL),
(271, 3, '4.71', NULL, '2022-01-27 18:24:28', '2022-01-27 18:24:28', NULL),
(272, 1, '4.58', NULL, '2022-01-27 18:34:52', '2022-01-27 18:34:52', NULL),
(273, 2, '4.69', NULL, '2022-01-27 18:34:55', '2022-01-27 18:34:55', NULL),
(274, 3, '4.71', NULL, '2022-01-27 18:34:59', '2022-01-27 18:34:59', NULL),
(275, 1, '4.58', NULL, '2022-01-27 18:47:04', '2022-01-27 18:47:04', NULL),
(276, 2, '4.69', NULL, '2022-01-27 18:47:10', '2022-01-27 18:47:10', NULL),
(277, 3, '4.71', NULL, '2022-01-27 18:47:15', '2022-01-27 18:47:15', NULL),
(278, 1, '4.58', NULL, '2022-01-27 18:50:41', '2022-01-27 18:50:41', NULL),
(279, 2, '4.69', NULL, '2022-01-27 18:50:43', '2022-01-27 18:50:43', NULL),
(280, 3, '4.71', NULL, '2022-01-27 18:50:46', '2022-01-27 18:50:46', NULL),
(281, 1, '4.55', NULL, '2022-01-29 01:15:34', '2022-01-29 01:15:34', NULL),
(282, 2, '4.69', NULL, '2022-01-29 01:15:36', '2022-01-29 01:15:36', NULL),
(283, 1, '4.55', NULL, '2022-01-29 01:20:55', '2022-01-29 01:20:55', NULL),
(284, 2, '4.69', NULL, '2022-01-29 01:20:57', '2022-01-29 01:20:57', NULL),
(285, 1, '4.55', NULL, '2022-01-29 01:46:26', '2022-01-29 01:46:26', NULL),
(286, 2, '4.69', NULL, '2022-01-29 01:46:31', '2022-01-29 01:46:31', NULL),
(287, 1, '4.55', NULL, '2022-01-29 01:47:03', '2022-01-29 01:47:03', NULL),
(288, 2, '4.69', NULL, '2022-01-29 01:47:04', '2022-01-29 01:47:04', NULL),
(289, 1, '4.55', NULL, '2022-01-29 01:47:33', '2022-01-29 01:47:33', NULL),
(290, 2, '4.69', NULL, '2022-01-29 01:47:34', '2022-01-29 01:47:34', NULL),
(291, 1, '4.55', NULL, '2022-01-29 07:07:00', '2022-01-29 07:07:00', NULL),
(292, 2, '4.69', NULL, '2022-01-29 07:07:02', '2022-01-29 07:07:02', NULL),
(293, 1, '4.55', NULL, '2022-01-30 01:50:29', '2022-01-30 01:50:29', NULL),
(294, 1, '4.55', NULL, '2022-01-31 04:55:37', '2022-01-31 04:55:37', NULL),
(295, 2, '4.69', NULL, '2022-01-31 04:55:38', '2022-01-31 04:55:38', NULL),
(296, 1, '4.55', NULL, '2022-01-31 05:13:10', '2022-01-31 05:13:10', NULL),
(297, 2, '4.69', NULL, '2022-01-31 05:13:11', '2022-01-31 05:13:11', NULL),
(298, 1, '4.55', NULL, '2022-01-31 20:13:28', '2022-01-31 20:13:28', NULL),
(299, 2, '4.71', NULL, '2022-01-31 20:13:31', '2022-01-31 20:13:31', NULL),
(300, 1, '4.55', NULL, '2022-01-31 21:33:49', '2022-01-31 21:33:49', NULL),
(301, 2, '4.71', NULL, '2022-01-31 21:33:50', '2022-01-31 21:33:50', NULL),
(302, 3, '4.67', NULL, '2022-01-31 21:33:53', '2022-01-31 21:33:53', NULL),
(303, 1, '4.54', NULL, '2022-02-01 17:46:42', '2022-02-01 17:46:42', NULL),
(304, 2, '4.65', NULL, '2022-02-01 17:46:43', '2022-02-01 17:46:43', NULL),
(305, 3, '4.66', NULL, '2022-02-01 17:46:46', '2022-02-01 17:46:46', NULL),
(306, 1, '4.54', NULL, '2022-02-01 19:19:58', '2022-02-01 19:19:58', NULL),
(307, 2, '4.65', NULL, '2022-02-01 19:19:59', '2022-02-01 19:19:59', NULL),
(308, 1, '4.54', NULL, '2022-02-01 22:32:19', '2022-02-01 22:32:19', NULL),
(309, 2, '4.65', NULL, '2022-02-01 22:32:22', '2022-02-01 22:32:22', NULL),
(310, 3, '4.64', NULL, '2022-02-01 22:32:30', '2022-02-01 22:32:30', NULL),
(311, 1, '4.54', NULL, '2022-02-01 22:33:35', '2022-02-01 22:33:35', NULL),
(312, 2, '4.65', NULL, '2022-02-01 22:33:36', '2022-02-01 22:33:36', NULL),
(313, 3, '4.64', NULL, '2022-02-01 22:33:40', '2022-02-01 22:33:40', NULL),
(314, 1, '4.54', NULL, '2022-02-01 22:34:03', '2022-02-01 22:34:03', NULL),
(315, 2, '4.65', NULL, '2022-02-01 22:34:04', '2022-02-01 22:34:04', NULL),
(316, 3, '4.64', NULL, '2022-02-01 22:34:07', '2022-02-01 22:34:07', NULL),
(317, 1, '4.53', NULL, '2022-02-02 04:21:57', '2022-02-02 04:21:57', NULL),
(318, 2, '4.65', NULL, '2022-02-02 04:21:59', '2022-02-02 04:21:59', NULL),
(319, 3, '4.61', NULL, '2022-02-10 22:42:52', '2022-02-10 22:42:52', NULL),
(320, 4, '1.00', NULL, '2022-02-22 13:46:26', '2022-02-22 13:46:26', NULL),
(321, 3, '4.60', NULL, '2022-02-26 01:38:50', '2022-02-26 01:38:50', NULL),
(322, 1, '4.39', NULL, '2022-02-26 01:42:43', '2022-02-26 01:42:43', NULL),
(323, 2, '4.61', NULL, '2022-02-26 01:42:45', '2022-02-26 01:42:45', NULL),
(324, 3, '4.60', NULL, '2022-02-26 01:42:48', '2022-02-26 01:42:48', NULL),
(325, 1, '4.39', NULL, '2022-02-26 01:48:19', '2022-02-26 01:48:19', NULL),
(326, 2, '4.61', NULL, '2022-02-26 01:48:21', '2022-02-26 01:48:21', NULL),
(327, 3, '4.60', NULL, '2022-02-26 01:48:24', '2022-02-26 01:48:24', NULL),
(328, 1, '4.39', NULL, '2022-02-26 01:50:28', '2022-02-26 01:50:28', NULL),
(329, 2, '4.61', NULL, '2022-02-26 01:50:32', '2022-02-26 01:50:32', NULL),
(330, 3, '4.60', NULL, '2022-02-26 01:50:37', '2022-02-26 01:50:37', NULL),
(331, 1, '4.39', NULL, '2022-02-26 01:50:43', '2022-02-26 01:50:43', NULL),
(332, 2, '4.61', NULL, '2022-02-26 01:50:47', '2022-02-26 01:50:47', NULL),
(333, 3, '4.60', NULL, '2022-02-26 01:50:52', '2022-02-26 01:50:52', NULL),
(334, 1, '4.39', NULL, '2022-02-26 01:51:15', '2022-02-26 01:51:15', NULL),
(335, 2, '4.61', NULL, '2022-02-26 01:51:16', '2022-02-26 01:51:16', NULL),
(336, 3, '4.60', NULL, '2022-02-26 01:51:20', '2022-02-26 01:51:20', NULL),
(337, 1, '4.39', NULL, '2022-02-26 01:52:37', '2022-02-26 01:52:37', NULL),
(338, 2, '4.61', NULL, '2022-02-26 01:52:39', '2022-02-26 01:52:39', NULL),
(339, 1, '4.39', NULL, '2022-02-26 01:53:45', '2022-02-26 01:53:45', NULL),
(340, 2, '4.61', NULL, '2022-02-26 01:53:47', '2022-02-26 01:53:47', NULL),
(341, 1, '4.39', NULL, '2022-02-26 01:54:49', '2022-02-26 01:54:49', NULL),
(342, 2, '4.61', NULL, '2022-02-26 01:54:51', '2022-02-26 01:54:51', NULL),
(343, 1, '4.39', NULL, '2022-02-26 01:56:15', '2022-02-26 01:56:15', NULL),
(344, 2, '4.61', NULL, '2022-02-26 01:56:17', '2022-02-26 01:56:17', NULL),
(345, 1, '4.39', NULL, '2022-02-26 02:08:09', '2022-02-26 02:08:09', NULL),
(346, 2, '4.61', NULL, '2022-02-26 02:08:11', '2022-02-26 02:08:11', NULL),
(347, 1, '4.33', NULL, '2022-03-06 18:07:15', '2022-03-06 18:07:15', NULL),
(348, 2, '4.49', NULL, '2022-03-06 18:07:17', '2022-03-06 18:07:17', NULL),
(349, 1, '4.33', NULL, '2022-03-07 08:29:54', '2022-03-07 08:29:54', NULL),
(350, 2, '4.49', NULL, '2022-03-07 08:29:56', '2022-03-07 08:29:56', NULL),
(351, 1, '4.33', NULL, '2022-03-08 19:07:40', '2022-03-08 19:07:40', NULL),
(352, 2, '4.41', NULL, '2022-03-08 19:07:42', '2022-03-08 19:07:42', NULL),
(353, 6, '1.00', NULL, '2022-03-08 17:41:05', '2022-03-08 17:41:05', 'VES'),
(354, 1, '4.33', NULL, '2022-03-11 09:03:29', '2022-03-11 09:03:29', NULL),
(355, 2, '4.41', NULL, '2022-03-11 09:03:30', '2022-03-11 09:03:30', NULL),
(356, 1, '1.20', NULL, '2022-03-11 09:38:52', '2022-03-11 09:38:52', NULL),
(357, 6, '10.00', NULL, '2022-03-11 09:44:41', '2022-03-11 09:44:41', NULL),
(358, 6, '6.00', NULL, '2022-03-11 09:50:48', '2022-03-11 09:50:48', NULL),
(359, 6, '7.00', NULL, '2022-03-11 09:51:22', '2022-03-11 09:51:22', NULL),
(360, 6, '6.00', NULL, '2022-03-11 10:24:47', '2022-03-11 10:24:47', NULL),
(361, 4, '1.10', NULL, '2022-03-11 17:50:05', '2022-03-11 17:50:05', NULL),
(362, 1, '4.22', NULL, '2022-03-14 23:27:10', '2022-03-14 23:27:10', NULL),
(363, 2, '4.34', NULL, '2022-03-14 23:27:11', '2022-03-14 23:27:11', NULL),
(364, 1, '4.24', NULL, '2022-03-15 03:28:15', '2022-03-15 03:28:15', NULL),
(365, 2, '4.34', NULL, '2022-03-15 03:28:20', '2022-03-15 03:28:20', NULL),
(366, 6, '1.00', NULL, '2022-03-15 03:34:32', '2022-03-15 03:34:32', NULL),
(367, 4, '1.30', NULL, '2022-03-15 04:55:54', '2022-03-15 04:55:54', NULL),
(368, 4, '8.00', NULL, '2022-03-15 04:56:24', '2022-03-15 04:56:24', NULL),
(369, 1, '4.29', NULL, '2022-03-17 22:32:14', '2022-03-17 22:32:14', NULL),
(370, 2, '4.36', NULL, '2022-03-17 22:32:17', '2022-03-17 22:32:17', NULL),
(371, 4, '1.10', NULL, '2022-03-17 22:32:22', '2022-03-17 22:32:22', NULL),
(372, 6, '50.00', NULL, '2022-03-27 04:49:22', '2022-03-27 04:49:22', NULL),
(373, 1, '5.90', NULL, '2022-08-10 10:55:08', '2022-08-10 10:55:08', NULL),
(374, 1, '8.12', NULL, '2022-09-26 01:07:59', '2022-09-26 01:07:59', NULL),
(375, 2, '8.41', NULL, '2022-09-26 01:08:01', '2022-09-26 01:08:01', NULL),
(376, 2, '8.32', NULL, '2022-10-01 05:23:25', '2022-10-01 05:23:25', NULL),
(377, 1, '8.20', NULL, '2022-10-01 05:23:34', '2022-10-01 05:23:34', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invoices`
--

CREATE TABLE `invoices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idproperty` bigint(20) UNSIGNED NOT NULL,
  `expiration_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `amount` decimal(15,2) NOT NULL,
  `currency_code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `observation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount_usd` decimal(15,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `invoices`
--

INSERT INTO `invoices` (`id`, `idproperty`, `expiration_date`, `amount`, `currency_code`, `status`, `active`, `deleted_at`, `created_at`, `updated_at`, `observation`, `amount_usd`) VALUES
(454, 132, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:30', '2022-10-02 01:21:42', NULL, NULL),
(455, 133, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:30', '2022-10-02 01:21:42', NULL, NULL),
(456, 134, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:30', '2022-10-02 01:21:42', NULL, NULL),
(457, 135, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:30', '2022-10-02 01:21:42', NULL, NULL),
(458, 136, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:30', '2022-10-02 01:21:42', NULL, NULL),
(459, 137, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:30', '2022-10-02 01:21:42', NULL, NULL),
(460, 138, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:30', '2022-10-02 01:21:42', NULL, NULL),
(461, 139, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:30', '2022-10-02 01:21:42', NULL, NULL),
(462, 140, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:30', '2022-10-02 01:21:42', NULL, NULL),
(463, 141, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:30', '2022-10-02 01:21:42', NULL, NULL),
(464, 142, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:30', '2022-10-02 01:21:42', NULL, NULL),
(465, 143, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(466, 144, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(467, 145, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(468, 146, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(469, 147, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(470, 148, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(471, 149, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(472, 150, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(473, 151, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(474, 152, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(475, 153, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(476, 154, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(477, 155, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(478, 156, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(479, 157, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(480, 158, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(481, 159, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(482, 160, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(483, 161, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:31', '2022-10-02 01:21:42', NULL, NULL),
(484, 162, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:32', '2022-10-02 01:21:42', NULL, NULL),
(485, 163, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:32', '2022-10-02 01:21:42', NULL, NULL),
(486, 164, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:32', '2022-10-02 01:21:42', NULL, NULL),
(487, 165, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:32', '2022-10-02 01:21:42', NULL, NULL),
(488, 166, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:32', '2022-10-02 01:21:42', NULL, NULL),
(489, 167, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:32', '2022-10-02 01:21:42', NULL, NULL),
(490, 168, '2022-10-01 21:21:42', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:32', '2022-10-02 01:21:42', NULL, NULL),
(491, 169, '2022-10-01 21:21:43', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:32', '2022-10-02 01:21:43', NULL, NULL),
(492, 170, '2022-10-01 21:21:43', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:33', '2022-10-02 01:21:43', NULL, NULL),
(493, 171, '2022-10-01 21:21:43', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:33', '2022-10-02 01:21:43', NULL, NULL),
(494, 172, '2022-10-01 21:21:43', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:33', '2022-10-02 01:21:43', NULL, NULL),
(495, 173, '2022-10-01 21:21:43', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:33', '2022-10-02 01:21:43', NULL, NULL),
(496, 174, '2022-10-01 21:21:43', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:33', '2022-10-02 01:21:43', NULL, NULL),
(497, 175, '2022-10-01 21:21:43', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:33', '2022-10-02 01:21:43', NULL, NULL),
(498, 176, '2022-10-01 21:21:43', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:33', '2022-10-02 01:21:43', NULL, NULL),
(499, 177, '2022-10-01 21:21:43', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:33', '2022-10-02 01:21:43', NULL, NULL),
(500, 178, '2022-10-01 21:21:43', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:33', '2022-10-02 01:21:43', NULL, NULL),
(501, 179, '2022-10-01 21:21:43', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:33', '2022-10-02 01:21:43', NULL, NULL),
(502, 180, '2022-10-01 21:21:43', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:33', '2022-10-02 01:21:43', NULL, NULL),
(503, 181, '2022-10-01 21:21:43', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:34', '2022-10-02 01:21:43', NULL, NULL),
(504, 182, '2022-10-01 21:21:43', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:34', '2022-10-02 01:21:43', NULL, NULL),
(505, 183, '2022-10-01 21:21:43', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:34', '2022-10-02 01:21:43', NULL, NULL),
(506, 184, '2022-10-01 21:21:43', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:34', '2022-10-02 01:21:43', NULL, NULL),
(507, 185, '2022-10-01 21:21:43', '0.51', 'VES', 0, 1, NULL, '2022-10-01 05:33:34', '2022-10-02 01:21:43', NULL, NULL),
(508, 186, '2022-10-01 21:21:43', '0.87', 'VES', 0, 1, NULL, '2022-10-01 05:33:34', '2022-10-02 01:21:43', NULL, NULL),
(509, 187, '2022-10-01 21:21:43', '0.57', 'VES', 0, 1, NULL, '2022-10-01 05:33:34', '2022-10-02 01:21:43', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `invoices_exchanges`
--

CREATE TABLE `invoices_exchanges` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idinvoice` bigint(20) UNSIGNED NOT NULL,
  `idhistorical_exchange` bigint(20) UNSIGNED NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `invoices_exchanges`
--

INSERT INTO `invoices_exchanges` (`id`, `idinvoice`, `idhistorical_exchange`, `deleted_at`, `created_at`, `updated_at`) VALUES
(447, 454, 377, NULL, '2022-10-01 05:33:30', '2022-10-01 05:33:30'),
(448, 455, 377, NULL, '2022-10-01 05:33:30', '2022-10-01 05:33:30'),
(449, 456, 377, NULL, '2022-10-01 05:33:30', '2022-10-01 05:33:30'),
(450, 457, 377, NULL, '2022-10-01 05:33:30', '2022-10-01 05:33:30'),
(451, 458, 377, NULL, '2022-10-01 05:33:30', '2022-10-01 05:33:30'),
(452, 459, 377, NULL, '2022-10-01 05:33:30', '2022-10-01 05:33:30'),
(453, 460, 377, NULL, '2022-10-01 05:33:30', '2022-10-01 05:33:30'),
(454, 461, 377, NULL, '2022-10-01 05:33:30', '2022-10-01 05:33:30'),
(455, 462, 377, NULL, '2022-10-01 05:33:30', '2022-10-01 05:33:30'),
(456, 463, 377, NULL, '2022-10-01 05:33:30', '2022-10-01 05:33:30'),
(457, 464, 377, NULL, '2022-10-01 05:33:30', '2022-10-01 05:33:30'),
(458, 465, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(459, 466, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(460, 467, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(461, 468, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(462, 469, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(463, 470, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(464, 471, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(465, 472, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(466, 473, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(467, 474, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(468, 475, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(469, 476, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(470, 477, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(471, 478, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(472, 479, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(473, 480, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(474, 481, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(475, 482, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(476, 483, 377, NULL, '2022-10-01 05:33:31', '2022-10-01 05:33:31'),
(477, 484, 377, NULL, '2022-10-01 05:33:32', '2022-10-01 05:33:32'),
(478, 485, 377, NULL, '2022-10-01 05:33:32', '2022-10-01 05:33:32'),
(479, 486, 377, NULL, '2022-10-01 05:33:32', '2022-10-01 05:33:32'),
(480, 487, 377, NULL, '2022-10-01 05:33:32', '2022-10-01 05:33:32'),
(481, 488, 377, NULL, '2022-10-01 05:33:32', '2022-10-01 05:33:32'),
(482, 489, 377, NULL, '2022-10-01 05:33:32', '2022-10-01 05:33:32'),
(483, 490, 377, NULL, '2022-10-01 05:33:32', '2022-10-01 05:33:32'),
(484, 491, 377, NULL, '2022-10-01 05:33:32', '2022-10-01 05:33:32'),
(485, 492, 377, NULL, '2022-10-01 05:33:33', '2022-10-01 05:33:33'),
(486, 493, 377, NULL, '2022-10-01 05:33:33', '2022-10-01 05:33:33'),
(487, 494, 377, NULL, '2022-10-01 05:33:33', '2022-10-01 05:33:33'),
(488, 495, 377, NULL, '2022-10-01 05:33:33', '2022-10-01 05:33:33'),
(489, 496, 377, NULL, '2022-10-01 05:33:33', '2022-10-01 05:33:33'),
(490, 497, 377, NULL, '2022-10-01 05:33:33', '2022-10-01 05:33:33'),
(491, 498, 377, NULL, '2022-10-01 05:33:33', '2022-10-01 05:33:33'),
(492, 499, 377, NULL, '2022-10-01 05:33:33', '2022-10-01 05:33:33'),
(493, 500, 377, NULL, '2022-10-01 05:33:33', '2022-10-01 05:33:33'),
(494, 501, 377, NULL, '2022-10-01 05:33:33', '2022-10-01 05:33:33'),
(495, 502, 377, NULL, '2022-10-01 05:33:33', '2022-10-01 05:33:33'),
(496, 503, 377, NULL, '2022-10-01 05:33:34', '2022-10-01 05:33:34'),
(497, 504, 377, NULL, '2022-10-01 05:33:34', '2022-10-01 05:33:34'),
(498, 505, 377, NULL, '2022-10-01 05:33:34', '2022-10-01 05:33:34'),
(499, 506, 377, NULL, '2022-10-01 05:33:34', '2022-10-01 05:33:34'),
(500, 507, 377, NULL, '2022-10-01 05:33:34', '2022-10-01 05:33:34'),
(501, 508, 377, NULL, '2022-10-01 05:33:34', '2022-10-01 05:33:34'),
(502, 509, 377, NULL, '2022-10-01 05:33:34', '2022-10-01 05:33:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2021_07_07_150004_create_invoices', 1),
(10, '2021_07_08_212928_create_concepts', 1),
(11, '2021_07_08_213020_create_configurations_residences', 1),
(12, '2021_07_08_213035_create_users_residences', 1),
(13, '2021_07_08_213101_create_accounts_residences', 1),
(14, '2021_07_08_213126_create_expenses', 1),
(15, '2021_07_08_213226_create_providers', 1),
(16, '2021_07_09_030024_create_expenses_invoices', 1),
(17, '2021_07_09_030043_create_payments_invoices', 1),
(18, '2021_07_09_030222_create_balances', 1),
(19, '2021_07_09_044407_add_data_to_users_table', 1),
(20, '2021_07_09_180032_fix_amount_expenses', 1),
(21, '2021_07_09_183014_fix_amount_two_decimal_expenses', 1),
(22, '2021_07_09_184621_fix_aliqout_properties', 1),
(23, '2021_07_09_190304_fix_aliqout_new_properties', 1),
(24, '2021_07_09_223228_add_softdelete_users', 1),
(25, '2021_07_09_224123_fix_expenses_invoices', 1),
(26, '2021_07_10_043135_fix_invoices', 1),
(27, '2021_07_12_061736_create_exchanges', 1),
(28, '2021_07_12_062054_create_historical_exchanges', 1),
(29, '2021_07_12_062353_create_invoices_exchanges', 1),
(30, '2021_07_12_062925_add_exchanges_to_configurations_residences', 1),
(31, '2021_07_12_064554_drop_source_exchange_to_configurations_residences', 1),
(32, '2021_07_12_090140_add_type_to_balances', 1),
(33, '2021_07_12_091003_add_password_decode_to_users', 1),
(34, '2021_07_12_092405_fix_local_phone_to_users', 1),
(35, '2021_07_12_223234_add_expenses_to_balances', 1),
(36, '2021_07_12_224032_add_expenses_to_expenses', 1),
(37, '2021_07_12_231527_fix_amounts_balances', 1),
(38, '2021_07_13_170249_fix_iduser_properties', 1),
(39, '2021_07_13_211947_fix_resideces_to_residences', 1),
(40, '2021_07_13_214520_fix_property_to_balances', 1),
(41, '2021_07_15_054956_fix_soft_delete_users_residences', 1),
(42, '2021_09_16_124628_fix_accounts_residences', 2),
(43, '2021_09_16_130227_fix_drop_description_accounts_residences', 2),
(44, '2021_09_16_151239_fix_add_accunt_holder_accounts_residences', 2),
(45, '2021_09_16_162734_fix_add_date_payments', 2),
(46, '2021_09_16_163035_fix_add_amounts_payments', 3),
(47, '2021_10_10_221456_add_idexchage_exchanges', 3),
(48, '2022_01_25_235232_add_status_to_expenses_table', 4),
(49, '2022_01_26_000405_create_expenses_properties_table', 5),
(50, '2022_01_26_002012_fix_soft_delete_expenses_properties', 6),
(51, '2022_02_01_163505_add_observation_to_invoices_table', 7),
(52, '2022_02_26_041619_create_events_table', 8),
(53, '2022_02_26_043614_add_fields_providers_table', 9),
(54, '2022_02_26_044809_add_fields_configurations_residences_table', 10),
(55, '2022_02_26_045848_add_fields_exchanges_table', 11),
(56, '2022_02_26_050136_add_fields_historical_exchanges_table', 12),
(57, '2022_02_26_050446_add_fields_expenses_table', 13),
(58, '2022_02_26_051339_create_configurations_table', 14),
(59, '2022_03_06_154629_create_files_table', 14),
(60, '2022_03_06_180546_add_fields_payments_table', 14),
(61, '2022_03_06_180619_add_fields_users_table', 14),
(62, '2022_03_06_221210_add_fields_configurations_table', 15),
(63, '2022_03_09_015047_fix_fields_files_table', 16),
(64, '2022_03_09_033853_drop_files_table', 17),
(65, '2022_03_09_034101_create_files2_table', 18),
(66, '2022_03_09_040408_fix_configurations_table', 19),
(67, '2022_03_09_040853_fix_files_table', 20),
(68, '2022_03_09_045748_fix_unsig_files_table', 21),
(69, '2022_03_09_133510_create_uploads_table', 22),
(70, '2022_03_09_133914_drop_files2_table', 22),
(71, '2022_03_11_041147_add_fields_in_uploads_table', 23),
(72, '2022_03_20_141546_add_fields_in_expenses_properties_table', 24),
(73, '2022_03_20_225152_rename_expenses_properties_table', 25),
(74, '2022_03_20_230445_remove_fields_expenses_table', 26),
(75, '2022_03_20_230804_remove_concept_expenses_table', 27),
(76, '2022_03_20_232431_add_name_in_expenses_table', 28),
(77, '2022_03_20_233321_add_concept_expenses_fees_table', 29),
(78, '2022_03_20_233801_change_fields_expenses_invoices_table', 30),
(79, '2022_03_20_234228_remove_aliquot_expenses_invoices_table', 31),
(80, '2022_03_21_014543_fix_property_expenses_fees_table', 32),
(81, '2022_03_29_224351_add_previous_invoice_configurations_residences_table', 33),
(82, '2022_08_10_061830_add_enable_to_residences_table', 34),
(83, '2022_09_06_032415_add_amount_fee_to_expenses_invoices_table', 35),
(84, '2022_09_16_023544_add_amount_usd_to_invoices_table', 35),
(85, '2022_09_16_023712_add_amount_usd_to_payments_table', 35),
(86, '2022_09_25_230303_add_description_to_balances_table', 36),
(87, '2022_09_25_231732_fix_aliquot_to_properties_table', 37),
(88, '2022_10_01_131225_nulleable_description_to_expenses_invoices', 38);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0088f225fd5b920423f440804fa6e2ef96b09215ac2cf9cd09ebb356b5401fb72205df92c71b72b4', 10, 1, 'Personal Access Token', '[]', 0, '2022-03-31 06:00:30', '2022-03-31 06:00:30', '2023-03-31 02:00:30'),
('00f335f5bf69c549110a557afceaad5dcc2aad297b4912cf3756e709d302fb9b8db5df71a65f5122', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:19:42', '2022-02-02 23:19:42', '2023-02-02 19:19:42'),
('028637a5fc08a37e25dff9ef8d008990fa3137cde410327d95bacbbef4270b711f54de0c718ef9b5', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-07 02:56:22', '2022-02-07 02:56:22', '2023-02-06 22:56:22'),
('037d8a9cb049bd3ae18fab4ebb203ac72573d8a3a9bf97b5ed03233fd7ee56727e171eeca2f8fd3b', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-05 04:43:07', '2022-02-05 04:43:07', '2023-02-05 00:43:07'),
('03e59178571d8a0325d7fd7a204387d6ea0dbee0fd0f23bc9fd06d4d94dc923e6612869ce46ba060', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-14 21:04:17', '2022-02-14 21:04:17', '2023-02-14 17:04:17'),
('057e2816aa86e4bac13856f9012c098b648952b66ad176ec0cf63022b628e7389a63453655f2cfea', 9, 1, 'Personal Access Token', '[]', 0, '2021-09-03 04:39:41', '2021-09-03 04:39:41', '2022-09-03 00:39:41'),
('05a4ce1eed5e33eea4c6c51e903efebf10cf44194924a885b91bcd2641c0cb8fe14e81d0240dc0f4', 11, 1, 'Personal Access Token', '[]', 1, '2022-03-08 18:17:54', '2022-03-08 18:17:54', '2023-03-08 14:17:54'),
('05b74ffd89d20cdc9874ac45f335fa157787a63de226e28c2c37b74662157a66083acddfb789e27e', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-20 05:07:05', '2022-01-20 05:07:05', '2023-01-20 01:07:05'),
('06e51c01ae2a8ef07903fbd338ebc14b8076166392dc747c1e3af13d0e177d94192ceab9dd289396', 6, 1, 'Personal Access Token', '[]', 0, '2022-01-15 18:53:40', '2022-01-15 18:53:40', '2023-01-15 14:53:40'),
('08e83208be3a3202a1ce6bbcb8b9eacaec7594d4460ed116990cd6d0a29c320e743131de44418f18', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 00:49:07', '2022-02-06 00:49:07', '2022-02-12 20:49:07'),
('09329098d2be02110675fc75fce7fae921ce884c9d065a501bcc9ea33497f8058852033fb266fc35', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 21:53:13', '2022-02-06 21:53:13', '2023-02-06 17:53:13'),
('0a8e1ba68d338650c7c088689d02c9105c9946150a1d0fda65412e826958a209b40e7ca708d7da27', 10, 1, 'Personal Access Token', '[]', 1, '2022-03-07 03:46:23', '2022-03-07 03:46:23', '2023-03-06 23:46:23'),
('0b992934b1f151301751278545aa2752cd42cd93f98d442d6d87972d13d16d378ea583a1d93de6eb', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 08:49:59', '2022-02-06 08:49:59', '2023-02-06 04:49:59'),
('0c8a7d25b6c9820713eac3d19fa6075d43598511fd8d545308be44df0de6211a730822d13daa5d68', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-05 17:54:23', '2022-02-05 17:54:23', '2023-02-05 13:54:23'),
('0d3de8f17c3fbf5d35e9b88c04912981a663881517007a176840a392099a00ba59b9804eb58d297b', 10, 1, 'Personal Access Token', '[]', 0, '2021-07-23 21:48:05', '2021-07-23 21:48:05', '2022-07-23 17:48:05'),
('0e8271ebd6db055799bf99729ed335a5c43670797bd37d659a74159a82f7e18f7fc0ed34ccd9b331', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-09 21:41:23', '2022-02-09 21:41:23', '2023-02-09 17:41:23'),
('113659b40ff2e5ee6bae003765bc165aa7e54c2907a7935e1226ff8ce4da15e29457e79d915eeffa', 9, 1, 'Personal Access Token', '[]', 0, '2021-07-23 22:21:25', '2021-07-23 22:21:25', '2022-07-23 18:21:25'),
('1182b540af2607b2db5b681d3df5adf9667f656311d1cd99fea5681699ab955d56bf94c4fc5477ac', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-06 05:14:39', '2022-02-06 05:14:39', '2023-02-06 01:14:39'),
('118f24160b2470b5c3c321dc998a9a804c570416ba28507dee804f087c743a5be57893f1a62f6570', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-13 04:18:15', '2022-02-13 04:18:15', '2023-02-13 00:18:15'),
('119fbf88d8951978f1762021207d24903a22e74bd1286e0eef040e4a5b437e0378bac9de768b151e', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 23:38:32', '2022-02-06 23:38:32', '2023-02-06 19:38:32'),
('1579443054595c1cde4324f21c889a02d23b7b42fdbecea77f5d2b3b8ccb9cd13260927114acc292', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-06 05:11:55', '2022-02-06 05:11:55', '2023-02-06 01:11:55'),
('17724e5a981f17f92cbc5b1c617ffc980b949aab0959257ad0910c3e267c762bf41598661f42dfc7', 6, 1, 'Personal Access Token', '[]', 1, '2022-03-11 22:09:53', '2022-03-11 22:09:53', '2023-03-11 18:09:53'),
('17eaa22b424b3f104155a183b0492720cf811fda99af187dcaed7942c878f72755a859226a975f47', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 23:37:49', '2022-02-06 23:37:49', '2023-02-06 19:37:49'),
('19737a1da072bde6b300dae39f66ab08de4e5e445d5f90536e9b1913e442b41fe83c1a499f923055', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-09 21:34:10', '2022-02-09 21:34:10', '2023-02-09 17:34:10'),
('197f62f8f0218f58e7200a2a7b250814171f07f00c68cf853073330906038489d32017f0992f0707', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-12 17:27:05', '2022-02-12 17:27:05', '2023-02-12 13:27:05'),
('19ca132fe4553f20ef7195e178cd9c5750f1e39afe1c61ad638ce862e863014042f2255cbcdba7c8', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-08 06:17:14', '2022-02-08 06:17:14', '2023-02-08 02:17:14'),
('1aa56be432645bbf128cfe711b764ccc1d0297d47a9ac59f9ac414d0375922d1cb708e31ae0b1c63', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 22:33:18', '2022-02-02 22:33:18', '2023-02-02 18:33:18'),
('1ab10cd52c1aef167824b51381ea3d7bb8d9319085afa6662032fc57d1f5fae841ec43108b6ede38', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-13 05:27:11', '2022-02-13 05:27:11', '2023-02-13 01:27:11'),
('1b3bcf8ccabe25fce940b436f3913e1f9a335826c8ac5e7c7f54c230644572d597511d0693e797d2', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-12 08:49:10', '2022-02-12 08:49:10', '2023-02-12 04:49:10'),
('1b53c544fce66f9ddeeae7876cbac4a9fd905792a4ce7a58f8a4237a4a80a6b910087a2682ead25c', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-07 23:54:36', '2022-02-07 23:54:36', '2023-02-07 19:54:36'),
('1dee9bf0dbcd56c704dca3b40850e5d13f1b8e2a6def3dd739ad61aa8e097db2556e15dc83f172fe', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-12 00:45:43', '2022-02-12 00:45:43', '2023-02-11 20:45:43'),
('1e7ff0c9154222ab2d64fbb2fa409efebac3ef1c2574937e70dc6630cee00ea5efb0fc459da761a8', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-10 02:57:22', '2022-02-10 02:57:22', '2023-02-09 22:57:22'),
('1e8dbf59b27b132cb61a891aa20fbfb53cb3b8009741e7c0c47c78c47fb722e8fda48588aac2235b', 6, 1, 'Personal Access Token', '[]', 0, '2022-01-24 04:18:50', '2022-01-24 04:18:50', '2023-01-24 00:18:50'),
('1ef401219d9c0e6b35323e84dc1fc05dc3a011987b8ea94f0e6779f2d66b3e3fbc9d1d86e474e3ef', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-08 05:41:42', '2022-02-08 05:41:42', '2023-02-08 01:41:42'),
('1f19735c7800e5566ff0daa92c889ae644657c270b350ab0d91fcb23ae4a6d3ddf676a89cfab1ccc', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-06 00:53:51', '2022-02-06 00:53:51', '2023-02-05 20:53:51'),
('1f483499a5a36f05f460ab0940ccb611a4b76122d736856eec66285c12898094d311560623f59b04', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-13 04:08:56', '2022-02-13 04:08:56', '2023-02-13 00:08:56'),
('1f4f58c775b0df6a56d2516dc2d259fb9887342bc4846e4a465973536ee059a65cbd5e16fed83871', 10, 1, 'Personal Access Token', '[]', 1, '2022-03-11 07:14:42', '2022-03-11 07:14:42', '2023-03-11 03:14:42'),
('2037f16f4219333e855dd67b58a2b14ac37fd8acf16e574042bea90f05e6b99e609eee9ff5aa1d56', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-05 04:52:51', '2022-02-05 04:52:51', '2023-02-05 00:52:51'),
('20b8f1105c75bbf0b81b710e25f290c468cb56e5964557e4d5f90e76f71a4b269977837d9ecd66d6', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-05 04:41:47', '2022-02-05 04:41:47', '2023-02-05 00:41:47'),
('20d9a84374c2be98a37c7fee4e6fcd177401b2736d45f5bba57890f591978ba21219bd4dbaa59cd9', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-08 04:11:01', '2022-02-08 04:11:01', '2023-02-08 00:11:01'),
('20dc5a05560693948ac7f1dd90d9f0893c48f50d0b335a39e826addbd3366d1ec899a3c92448570d', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-27 18:24:14', '2022-01-27 18:24:14', '2023-01-27 14:24:14'),
('220b1a09d21575c64ea543b593db9e3b626ba7f7dbd2b002dcebffb085560baa864e088be9448bb6', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-09 03:44:05', '2022-02-09 03:44:05', '2023-02-08 23:44:05'),
('22286430d9d452de801f6831d3ba6f2504ad3418c9cf45d3520905f2ed71ee6eea5fed5062723787', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-10 19:07:02', '2022-02-10 19:07:02', '2023-02-10 15:07:02'),
('22dafd679a9127b77143612fb5c7c3a7a293e3de873e157ec6bac89004a62229a760703352c80266', 10, 1, 'Personal Access Token', '[]', 0, '2022-08-10 09:02:26', '2022-08-10 09:02:26', '2023-08-10 05:02:26'),
('23088700a6ba8ebe3190691b2ab012a5caeeb049ad84beb245303fde18bdbb9a5b40303242aef02a', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-06 22:27:39', '2022-02-06 22:27:39', '2023-02-06 18:27:39'),
('233c7a3d94b5d49ebc5e97c846e2c20ad0340fc26215835201d35e305d6486e9e6903121c3d29c5b', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-08 00:38:20', '2022-02-08 00:38:20', '2023-02-07 20:38:20'),
('235739a9b34679a236833d3347e52bcc59707cb29f91028d4d121b459e7b88c1b773c00d3636af5b', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-03 00:57:35', '2022-02-03 00:57:35', '2023-02-02 20:57:35'),
('2398c9c2cca013ef98eb7587f997a365ff0f8971cea88312fc02c7f5ef587ab94a0752c935db0e4e', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-08 00:32:04', '2022-02-08 00:32:04', '2023-02-07 20:32:04'),
('2596d1a107ed000cbbaabb54d41e4ce421ee977570d1ab46e5ec8a3b51fb0b177f5bb4cf3f020522', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-11 18:27:11', '2022-02-11 18:27:11', '2023-02-11 14:27:11'),
('25de041ab6756143f827ff1718a5e95bdbe35832a44a36bdf2a3bbae43875260f5d55e5bd14fc951', 10, 1, 'Personal Access Token', '[]', 1, '2022-03-06 23:48:38', '2022-03-06 23:48:38', '2023-03-06 19:48:38'),
('263fe09b517f5a31d2c6037c64b0276dddb83bb12f63fccd04f71c087f69399f8ef4c6937f43e0e5', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-03 02:37:41', '2022-02-03 02:37:41', '2022-02-09 22:37:41'),
('271545623c9469d1462cfbba4e53c8627c99b921643564e4160f6520dbd01bb834121c54a06337ef', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-05 04:49:25', '2022-02-05 04:49:25', '2023-02-05 00:49:25'),
('27482a23c1b7337b3062da255f723c35dce67eb8d4766c32536140f25174e9879b3e82df299e9de0', 10, 1, 'Personal Access Token', '[]', 1, '2022-03-08 18:29:40', '2022-03-08 18:29:40', '2023-03-08 14:29:40'),
('27987cc5a1d41475346dcc72af9fc2c7de760a09b848c1fb56c66a6d254b71a5d8ad7a377e0cfffc', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-08 05:45:33', '2022-02-08 05:45:33', '2023-02-08 01:45:33'),
('279f8ab604d1ec6c70b0abf836b98720035c0ba21efe42146059fbb61fb657ff65e4b80fe7e43b5d', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-23 07:13:47', '2022-02-23 07:13:47', '2022-03-02 03:13:47'),
('290d192b1482b757865f558701abd9f5c7502e97a05739474198fb7f51d72bd9f23d8ce8ef594027', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 22:51:45', '2022-02-02 22:51:45', '2023-02-02 18:51:45'),
('2a3c399f1f296182c5c39db883ee58d00bfbd97191d8ed816ae61748c7b1ebd361a3cf2f16bafd02', 6, 1, 'Personal Access Token', '[]', 0, '2021-09-21 22:11:25', '2021-09-21 22:11:25', '2022-09-21 18:11:25'),
('2b005980606daca81f3144fa539c9e26b78d9d68f44ff348e2d3940b88803bc2d920b2277535787e', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-24 20:00:03', '2022-01-24 20:00:03', '2023-01-24 16:00:03'),
('2c3211cc7dffe499593138e4e0bf8f9cf451691835d0bd94703b9e06929d72cc1233033c0b60fc56', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-12 00:59:37', '2022-02-12 00:59:37', '2023-02-11 20:59:37'),
('2ca546259606d73dd8ab581a5cb170754e30ad00d1aa6e4afeb6877b1c4e743663549c01c75a8946', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-11 07:45:14', '2022-02-11 07:45:14', '2023-02-11 03:45:14'),
('2e16918b950fcad92172991bbdd98bcb273e1725564f9cdb258bea67e760248adb92dff98228f49d', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:13:11', '2022-02-02 23:13:11', '2023-02-02 19:13:11'),
('2e3bc9ca2eee3fd4ac370d740a99032bedcdc1f2bbbbf2424c4b7ccbcbd0be217d9ebd1d2a3d13a1', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-09 04:21:47', '2022-02-09 04:21:47', '2023-02-09 00:21:47'),
('2e734400c197c43af048a83d40d8c938c79b6a1d026234bf0ae777734e519b22915a0a8d1367a411', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 21:06:53', '2022-02-02 21:06:53', '2022-02-09 17:06:53'),
('2e82e5a335cdfc0e93b2b741af88280d27f8c0678a2846f6974d529f46dafa114898e5eaa91cc550', 5, 1, 'Personal Access Token', '[]', 0, '2021-08-07 23:41:50', '2021-08-07 23:41:50', '2022-08-07 19:41:50'),
('2ea64acba9628e031dcb5fc7e7ebfb318483b760bf42b47a49ef73aa9e7975066fdb9b93a2ea4798', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-10 00:36:27', '2022-02-10 00:36:27', '2023-02-09 20:36:27'),
('2eae9304ac9d5fc7172bf360be5db58232bac396a1f60f9015d6a6c7f66d68577e58926e03096382', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-11 17:38:47', '2022-02-11 17:38:47', '2023-02-11 13:38:47'),
('2fbf041a233711d5b49895bcc6f1ab2cbf2e8b81c20451437709646b4afc7ec1dcbfdb4f9c1935af', 10, 1, 'Personal Access Token', '[]', 0, '2021-07-23 22:04:18', '2021-07-23 22:04:18', '2022-07-23 18:04:18'),
('30277c583c376c6e8a6afd44a0ffcbdda7628df8c855d9d518f7748de540e0803326db5240365fab', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-20 05:06:45', '2022-01-20 05:06:45', '2023-01-20 01:06:45'),
('3084dcf1def5b96cc458f27283606a8f03467f52f7f65365fcca4e2d7ce2f02e984a6df8e81a287e', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-12 00:30:44', '2022-02-12 00:30:44', '2023-02-11 20:30:44'),
('3350fba5c6bb4cfe144df7aecbf09093d87161b16430fa769ee90fe4ecc2bcafc0c273f53b209c77', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-20 05:07:02', '2022-01-20 05:07:02', '2023-01-20 01:07:02'),
('33716a8c42dd59f180ef366ec7013864293a6daf32b6190e75ffcbb57730e8619ffb402942ad8102', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-14 20:38:19', '2022-02-14 20:38:19', '2023-02-14 16:38:19'),
('3481ab0db76f60bc782a3841d9d19df92fa7f467a217113d91d34fcff5a846f2e5f112b1d887645f', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-10 00:37:38', '2022-02-10 00:37:38', '2023-02-09 20:37:38'),
('34d6cf0022f2d85c8aa0cf7102b1d8dff31886334745a1f1a78f13934e5488c7ba3cae7cac7cdf65', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 21:52:14', '2022-02-02 21:52:14', '2023-02-02 17:52:14'),
('350cd76b29c8e7218329940af53cc6c59e21e17244df9fbb60a96a4b1a0e4a6e634b39f7d1556b0a', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-16 01:44:26', '2022-01-16 01:44:26', '2023-01-15 21:44:26'),
('35bc578940408769e8c765d25c66dab146575a1a0af9d544e9c6208f56dc8991e43ca85dbe0515a0', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-11 17:34:48', '2022-02-11 17:34:48', '2023-02-11 13:34:48'),
('35ff6cdb0414309a3aa960866e207682e323ff78b81fcb9f44902d7a509be2058aa5d7deaa579b2b', 11, 1, 'Personal Access Token', '[]', 1, '2022-03-15 05:06:09', '2022-03-15 05:06:09', '2023-03-15 01:06:09'),
('378b0eacce4f2cc73d4cdffca0c5d5fbb7754b30d71820433e8d7d363ca2f75612d8be64a7a4db45', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:20:36', '2022-02-02 23:20:36', '2023-02-02 19:20:36'),
('39726b684f16ade07380041f043b074cae679e1cf9ff6f0e7f1e87a1ea3adcc2a4d54d74fd5fdb35', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-15 19:27:49', '2022-01-15 19:27:49', '2023-01-15 15:27:49'),
('397e0818ac3cbd34a112f6331ba2b7510f0d0dd0c9867c1e13cf17ab6527ee24c1727663066b942c', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-12 00:28:45', '2022-02-12 00:28:45', '2023-02-11 20:28:45'),
('3a702715072005bdcd31131b95367316e4e897aeb23db17f7d680494ee5630d8322db2389107f648', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-04 19:34:41', '2022-02-04 19:34:41', '2023-02-04 15:34:41'),
('3b26e10927e88b184b8f9b8e91b65c2814a317e68195852482f4c5b1f8ef4058d45b7d3f61b83007', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-04 19:43:35', '2022-02-04 19:43:35', '2023-02-04 15:43:35'),
('3bc057cccce8efccfab62ac4f27c5d54013016f087efda7f01372042415a0723092d55f18d6e3f8c', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-15 19:27:44', '2022-01-15 19:27:44', '2023-01-15 15:27:44'),
('3c1e3c348452aa493699041edf1b8e7ba1de5e3be59be1727817d68f0273bf690fb6f25275993ac3', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 00:01:54', '2022-02-06 00:01:54', '2023-02-05 20:01:54'),
('4098d4a289f74a409f8ddcbbbf335486092ef9a884ed2d95a92bab0b0ca81354e171a4f06517684c', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 23:36:12', '2022-02-06 23:36:12', '2023-02-06 19:36:12'),
('40b94ef80324d785c4e263c98d3a7d3ef7f63aa56b0f7d908eb5d8f7b04d3bd2007aed5b780c6639', 10, 1, 'Personal Access Token', '[]', 0, '2021-07-23 22:04:20', '2021-07-23 22:04:20', '2022-07-23 18:04:20'),
('41620d948b209dab32ce00fa4c3d5e87050ab8071a33686174bf6512ee10b705b7b75ccf88206623', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-02 20:46:05', '2022-02-02 20:46:05', '2023-02-02 16:46:05'),
('42de416f83fe02c42ada3bfa87b9481f803fe37ae66eb9df060f40045437908d71c81d44d61acb26', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-10 00:23:43', '2022-02-10 00:23:43', '2023-02-09 20:23:43'),
('4395d9e7f6355e0751072d0a6823db4f058a385b52dc5c93aa71d9d3db56ac3f8bd30c01359da3fb', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-11 07:27:29', '2022-02-11 07:27:29', '2023-02-11 03:27:29'),
('44061b229ca5403accf532aa3976de42f993d20d87443e2da7589b45320187a2c7abbb5aed8cf0ea', 10, 1, 'Personal Access Token', '[]', 1, '2022-03-18 06:24:27', '2022-03-18 06:24:27', '2023-03-18 02:24:27'),
('453cd0c1c77d7b51c6d831c82e3db9b93597dd2814b760b529c00be0b4075c9de5cd594921955e9a', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-03 00:43:27', '2022-02-03 00:43:27', '2023-02-02 20:43:27'),
('46a33becb0353a9a1c65b4ffaa0ee46fb471a1c3e7a2c1eccad198e43243f032a0e285c115320268', 6, 1, 'Personal Access Token', '[]', 0, '2021-09-21 22:11:23', '2021-09-21 22:11:23', '2022-09-21 18:11:23'),
('47ce5285044821efc553df529148419783c7a333b5f5e5a392cd4c069338cc522779c8dd2b5ef725', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-04 19:38:17', '2022-02-04 19:38:17', '2023-02-04 15:38:17'),
('4bf236ab471191326fd40e338cae5367a8b1e9fbcd330c7dd8492e3b685f917caec97ca649a7aedc', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-13 04:36:20', '2022-02-13 04:36:20', '2023-02-13 00:36:20'),
('4c09dd9131859af644efbb3d3ac1a281538be0a16f7c3329fb88b0c754632d04e0b1ad731fa136ce', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-22 19:38:45', '2022-01-22 19:38:45', '2023-01-22 15:38:45'),
('4ca2977910ca24b0cbb577b85b3184f8f0e3772edfb6d58c64741f246aef833c7c8eeafc36f0842b', 22, 1, 'Personal Access Token', '[]', 1, '2022-03-18 01:54:47', '2022-03-18 01:54:47', '2023-03-17 21:54:47'),
('4cb7056d3d1415cf2a221bdd5e9efaa811023e088c992d2a3e8628f1b40195e3f0620b2aabba69e3', 10, 1, 'Personal Access Token', '[]', 0, '2021-09-21 22:13:06', '2021-09-21 22:13:06', '2022-09-21 18:13:06'),
('4d0d7f01fcd7fce90bb7b0b623741722f4541df3a5a4563a9b92613d159ec9638cc7fe180d0f366c', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-24 08:24:08', '2022-02-24 08:24:08', '2023-02-24 04:24:08'),
('4d1f80b0367006953aa684330f898f8c22dc8865be21090ecb03d2388a52c0d30aa864614ccad44c', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-04 19:38:42', '2022-02-04 19:38:42', '2023-02-04 15:38:42'),
('4d294ea7497480e9bcab114dc5de90d3adfad3dad986516fa8d17eda103e975936ef58ce11a276ca', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:24:26', '2022-02-02 23:24:26', '2023-02-02 19:24:26'),
('4d7ad54cdfbc8ebcc11da45fb6794e566abb7aa297dbfbcab3b6282e413c83c31346f299c1e312f1', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-06 01:26:05', '2022-02-06 01:26:05', '2023-02-05 21:26:05'),
('4e44161444ad64fca8c13e66594148ee0cd6f7b4877b0dd317be65fde337bf8027c89ef500bc2d3e', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-05 03:15:05', '2022-02-05 03:15:05', '2023-02-04 23:15:05'),
('4fae70e28ad012b6a3f55c66b044f945d3c24c69c5d2dc027f50dee99eadf4d082d6e5923b2ac9f2', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-09 22:05:00', '2022-02-09 22:05:00', '2023-02-09 18:05:00'),
('4ff228a7b16a54826dfacb0f6c3007eb6d0942af07da3f6b08381f5dc14999e6fc7e3d085841d52e', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-09 19:06:11', '2022-02-09 19:06:11', '2023-02-09 15:06:11'),
('5164afaff25928bba9ce767cb94270a623d0af09f6537aecf42f05ccbf55f48e860662f1603cf0bc', 10, 1, 'Personal Access Token', '[]', 1, '2022-03-17 23:59:03', '2022-03-17 23:59:03', '2023-03-17 19:59:03'),
('51c3bab6b3956439e53446e3f681f6a64a1cb2e6efb4930231df820a8fdc2750f13a974d3094f823', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 23:39:51', '2022-02-06 23:39:51', '2023-02-06 19:39:51'),
('522382c1eb1bc37c9cb43eb1c5ab0f2e804717379cf5037a5455c212cab577d9977e6647e847a75e', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-04 20:54:43', '2022-02-04 20:54:43', '2023-02-04 16:54:43'),
('525710a4bf46d6155099fb543c716443714f6952e84a9a2807a2829f0400c394d1cb99eae8e2b1f8', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-08 06:17:51', '2022-02-08 06:17:51', '2023-02-08 02:17:51'),
('53353b5c4ff3a273c42d1581d6748ac6eb08e5bb55071f9a3b1c20457915cafb04ded6f3c4490f8c', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 00:56:45', '2022-02-06 00:56:45', '2023-02-05 20:56:45'),
('53fb78de7c7cec54f8a25431a5d2c936adcef6e3b0b1d13fd1ae6827706a25ca67d0db6fbc5cb1c5', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-10 04:28:06', '2022-02-10 04:28:06', '2023-02-10 00:28:06'),
('54a260d2bf99e46641ec297067b2781a1db2fcb4da38e4d26fb897b721224e22b022570d13b320e3', 6, 1, 'Personal Access Token', '[]', 0, '2022-01-12 02:57:03', '2022-01-12 02:57:03', '2023-01-11 22:57:03'),
('54deb0b1b185a495150226aba1f5f16f5d98db846e392bb721be778a0a38e2d2d5eaa6c11e97fdd9', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-03 02:14:31', '2022-02-03 02:14:31', '2023-02-02 22:14:31'),
('5569f400a9252f5950732975b773d4f858cbd5259d7df681548938bcdbbfe1aa305f52718d483c8e', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-09 21:34:59', '2022-02-09 21:34:59', '2023-02-09 17:34:59'),
('5657b84bbc353b18b324068f0857b39afe770bd1cf71a9fd9aaa1995d37c71170c13748b0145eae0', 6, 1, 'Personal Access Token', '[]', 1, '2022-03-27 04:49:53', '2022-03-27 04:49:53', '2023-03-27 00:49:53'),
('56ecdcc68e8b7283686b9ec6c683a46b9bfde3951b20f30921f6adc4c659a7d3b01be0db8c0518e7', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-06 05:17:00', '2022-02-06 05:17:00', '2023-02-06 01:17:00'),
('574ba164a2f22b9ea42e0405cfdb967dd4017f8eb86758e2828c805c45dc1141e7d2ed7a6986f729', 10, 1, 'Personal Access Token', '[]', 1, '2022-03-11 22:10:30', '2022-03-11 22:10:30', '2023-03-11 18:10:30'),
('57b7db7d3bf2d2629b2cd15dd98271d056707c4b4f817f6e74d01c2062a05946f03eb14f2e77fbb3', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:27:11', '2022-02-02 23:27:11', '2023-02-02 19:27:11'),
('58192f4d4908926d6fdf2708f376a38c2c5c1a6f9377fec8f71b1e6c3270a3360d6f8847aeafe917', 22, 1, 'Personal Access Token', '[]', 1, '2022-02-24 08:23:53', '2022-02-24 08:23:53', '2023-02-24 04:23:53'),
('5aaf16d48af687db54b6c7dfaab8f95cf852b758d8bcbc59c23eba83bc01dfad1732d0902436680c', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:22:11', '2022-02-02 23:22:11', '2023-02-02 19:22:11'),
('5bc9083f30c6df588d35b938d18aab306135f549afb497e0bed7fd47f5b2b5479bd950d65e5e677e', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-05 08:34:31', '2022-02-05 08:34:31', '2023-02-05 04:34:31'),
('5d6f547e69c21ee31099b894bc9cb8ecd814cf11fc81531e039f5ca77d0db9ca854b4c6b86959264', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 01:27:11', '2022-02-06 01:27:11', '2023-02-05 21:27:11'),
('5db1a7cacbb790c6e69c2e3a92200f11fe52f45a3b76b88a9ac0c05d8ac46b0bb02bf79b88871bb7', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-07 01:47:29', '2022-02-07 01:47:29', '2023-02-06 21:47:29'),
('5ee6da37fd40e23c59af1a921c43d627598154ef276ccfead335558e190265cff51e8bbfb8f3a4b5', 10, 1, 'Personal Access Token', '[]', 1, '2022-03-08 20:37:14', '2022-03-08 20:37:14', '2023-03-08 16:37:14'),
('664b5f3318bae31eee9897dd5b362e8d1d0a05a0b535b9d59a4ff8cdcff888344fb7dd9f1b9573b2', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-09 23:22:14', '2022-02-09 23:22:14', '2023-02-09 19:22:14'),
('6675315a484e881af57b5addbd073e3d919e69820454316a940d6946b208f71ed286bbc73ec43c28', 6, 1, 'Personal Access Token', '[]', 0, '2022-01-03 23:28:28', '2022-01-03 23:28:28', '2023-01-03 19:28:28'),
('6722fb5f667e014e035ea2afae7fcb40480e0bf8f0732016ba921da70e5ceab3eae1e49db70e6778', 11, 1, 'Personal Access Token', '[]', 1, '2022-03-08 17:03:17', '2022-03-08 17:03:17', '2023-03-08 13:03:17'),
('68e8c6f0ad2f59524a2b901ba573e038a3a1b24ade74ea90177d9f894305b8c63fdd265dc9a3bb9d', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-12 01:15:28', '2022-02-12 01:15:28', '2023-02-11 21:15:28'),
('6ad8ca9b52947872e524255f930209455d1451c64079c407604b4798f00a10fa0828345f61153c28', 6, 1, 'Personal Access Token', '[]', 0, '2021-07-23 15:40:04', '2021-07-23 15:40:04', '2021-07-30 11:40:04'),
('6b962a59ece96a652ee3082c32ecb6a868a0e8d899aa5a45e66842f5b377420ca5902e7728489e2a', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-06 08:59:44', '2022-02-06 08:59:44', '2023-02-06 04:59:44'),
('6c332c3c345f5bdfea7f6abe1d7a9bd123a56966a024ed91a15c488ec8aa487dfd623ce2927e802c', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-07 02:59:33', '2022-02-07 02:59:33', '2023-02-06 22:59:33'),
('6e391dc2e31d2e5e6839d54d2648bd88f45b30070114e4f08437080a0950e52e72aef226dc50113e', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-06 05:12:33', '2022-02-06 05:12:33', '2023-02-06 01:12:33'),
('6f09b342b6f1632268bd5f7908cbfe152ea0be3e11fb587cf463cfea8678955701c59ce5e2dc4786', 10, 1, 'Personal Access Token', '[]', 0, '2021-07-23 18:31:55', '2021-07-23 18:31:55', '2022-07-23 14:31:55'),
('6f19b2e3561cca2da189c4496cf14a7c54367a0d2724562068b3733392136c86f7c22ed8dd5dca93', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-12 17:30:54', '2022-02-12 17:30:54', '2023-02-12 13:30:54'),
('6f55d27ecac6d6bcfe9e8eff9e3cd2568e34d5a0ca7fb361b03fc55a4e5ef85917d69727d23bb13a', 7, 1, 'Personal Access Token', '[]', 0, '2021-09-03 04:38:59', '2021-09-03 04:38:59', '2022-09-03 00:38:59'),
('6f94a160abd735df2874f061c210b62622c7c92ce4fce9fd904e4c3dfa2db9c90121f8fb65e39a42', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 00:48:41', '2022-02-06 00:48:41', '2022-02-12 20:48:41'),
('6fad8ed1f5e78e7fd5a8cea5569af528751b1f9dfaa5f51772bc2e992727a8c491e0c4f1d341033f', 9, 1, 'Personal Access Token', '[]', 0, '2021-07-23 21:49:12', '2021-07-23 21:49:12', '2022-07-23 17:49:12'),
('71b5606dd631c07ce14580e8bff2c2f2ee775422c9a519be7698816859e6ba3a77dd83ea2dfd0b41', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-09 22:43:53', '2022-02-09 22:43:53', '2023-02-09 18:43:53'),
('73052791700c6e738941e5f10cbeb4408dfdec5d4583c8be3e2cc0a89406417ebcff9ca3f7bc115a', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-20 17:57:59', '2022-01-20 17:57:59', '2023-01-20 13:57:59'),
('73eb441a85b209e5c8bb2b62b85ce5f4967535d8bc771dd6a5439254bd6fef801cc2d1c9d91bb00b', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-08 05:51:13', '2022-02-08 05:51:13', '2023-02-08 01:51:13'),
('749980f5bc983bbd549f2312476c7a9843ffe37a7614cfeac8e8ae6450baf6d3282a36dd2ddac8a5', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-04 19:16:26', '2022-02-04 19:16:26', '2023-02-04 15:16:26'),
('74b4c67f250b74c2aeafa7e2701c8438c68a66433ae66aeea2c68787dd74225436eb435c9a1800fc', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-07 02:58:31', '2022-02-07 02:58:31', '2023-02-06 22:58:31'),
('75f17b2a2c2626692241f3a5b70e8f407497c03217abc1343395a77296c242dcc94020eb2b0385d4', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-10 01:11:43', '2022-02-10 01:11:43', '2023-02-09 21:11:43'),
('780394906a001fd8f0cd74852386b736448aa1ecdd4e5b6ef18e0a602164173e2c50818a9ea693f5', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-13 07:06:22', '2022-02-13 07:06:22', '2023-02-13 03:06:22'),
('78d14ac5d3cb796d0c5226c45c1d32310946d23ab3e2b41e59f9ecc73e9802b38c1e8851594c7405', 10, 1, 'Personal Access Token', '[]', 0, '2021-07-23 21:48:02', '2021-07-23 21:48:02', '2022-07-23 17:48:02'),
('793fa35811993df29f5ef5fc6049219e1304fe25d886e5a8794f3da2128b836bcbb26cc0f2c92872', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:35:45', '2022-02-02 23:35:45', '2023-02-02 19:35:45'),
('798b32ecf61d8a93dd207affb5250976cda4e93bd4e5dc26e40070e99169553d31f82a2a801cec51', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-10 09:08:56', '2022-02-10 09:08:56', '2023-02-10 05:08:56'),
('7ab88804d783eee41754317e5c47d0ea21c2c39212035076ad09f6594f6ef4bd8a6462e7dcc0ec34', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-04 19:48:21', '2022-02-04 19:48:21', '2023-02-04 15:48:21'),
('7b7683daa8d9380ca79830840c2e12d95ecd83bb2ab4ff8c7ceb7e58eca0c505470b46afab88cfc6', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:18:26', '2022-02-02 23:18:26', '2023-02-02 19:18:26'),
('7d841bf1793b972ec6bb404bd34e837507708713a4ea5bb618351aa6980d507d1af5e24583f477a2', 9, 1, 'Personal Access Token', '[]', 0, '2021-09-03 04:39:39', '2021-09-03 04:39:39', '2022-09-03 00:39:39'),
('7e80b26ed619c48c82c16463ec6ce7675d40f558df4610bcdd2bd192f1ad45097e2a1b25bb903a2f', 10, 1, 'Personal Access Token', '[]', 0, '2022-10-13 05:39:08', '2022-10-13 05:39:08', '2023-10-13 01:39:08'),
('7f69bf1d2044e1225c6bd6bbd97bd5e8995498d33fa249c0ae7316fd13ff9a8928a35aad380730c8', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:22:12', '2022-02-02 23:22:12', '2023-02-02 19:22:12'),
('80a789bc2f030c6b00977932c288f5d75a3d053bf2059dfdd7e07ad0e8b8245693342440c45250db', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-09 21:43:32', '2022-02-09 21:43:32', '2023-02-09 17:43:32'),
('84f904d60a679a5ccdfa5d9664b9f4985ed3b5def16534a14d7defc908320ae58460e2e9ba4c7e60', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 23:41:10', '2022-02-06 23:41:10', '2023-02-06 19:41:10'),
('853f58b3749952133e8a3eec6c6e15c41c4a07bf08a9d3a9418c702c553948434370598f76381df4', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-06 08:48:07', '2022-02-06 08:48:07', '2023-02-06 04:48:07'),
('856d9e1b6a1feb2ed7c2d84d7aafd137170c152f111539b0a6707219b8644359390a6e72c1a8c246', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:23:33', '2022-02-02 23:23:33', '2023-02-02 19:23:33'),
('858a59933e03ee83b72b7c14e6e662a58705730bbe6d8be42aa2380cdda8c3d5fb484869f8b089ed', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-07 03:00:11', '2022-02-07 03:00:11', '2023-02-06 23:00:11'),
('858a831d332828373ef64a6050d16c7000ceda1845d3e4c640822d67b6e1c05ae7b81a5b2ed67110', 11, 1, 'Personal Access Token', '[]', 1, '2022-03-08 01:12:56', '2022-03-08 01:12:56', '2023-03-07 21:12:56'),
('86bec8ea23360320029045d7cd621a9580c8e47b2a952f963e394939cc2b6613832188743827622a', 9, 1, 'Personal Access Token', '[]', 0, '2021-07-23 22:21:23', '2021-07-23 22:21:23', '2022-07-23 18:21:23'),
('86def1589eb7e4ca93fae378f7651d59d56fcb3e4e95c4c6f558b517c24e13cbac8e8977cf02fdbb', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 00:53:29', '2022-02-06 00:53:29', '2023-02-05 20:53:29'),
('870601d4cc7784bc4be90daa073ac615581e5e34668bb5874d14e99cff8296418a4ba905e4a3d350', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-14 20:59:58', '2022-02-14 20:59:58', '2023-02-14 16:59:58'),
('87ddf140c07f0183cce3e5b19aecd7f45ce8e8017e6450217331aa3fb2fbba5066b91f6b9648438c', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-10 01:12:11', '2022-02-10 01:12:11', '2023-02-09 21:12:11'),
('88028bc55d1ce5d520257cacd26114f058c3288f1bdbb1cfac454039f9ca1be839f9ecc4a9914a55', 6, 1, 'Personal Access Token', '[]', 0, '2022-01-15 21:12:03', '2022-01-15 21:12:03', '2023-01-15 17:12:03'),
('8863ada64d9537f99c4af5ec3d00e72eb41ba95a64b2156ec2acb9b0a32c04b689d92e02bf613cfc', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-20 17:57:52', '2022-01-20 17:57:52', '2023-01-20 13:57:52'),
('888dfc460e26665d83c457b0002df041bd0a821f755ae548572edace9f1c068f08817ddd2df1164a', 10, 1, 'Personal Access Token', '[]', 1, '2022-03-18 01:53:58', '2022-03-18 01:53:58', '2023-03-17 21:53:58'),
('88cfea038adf35b2e9b2f4090c004e592423420c87fcd7aed3bc962bf5a6412a2440ffe608ad1f25', 11, 1, 'Personal Access Token', '[]', 0, '2022-02-07 01:08:35', '2022-02-07 01:08:35', '2023-02-06 21:08:35'),
('894bf192f1aab83a63b315105e5092dde9405b9640bc24b0057a6aac8f11989e96ab9c1f8b2c2d6a', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-05 04:47:21', '2022-02-05 04:47:21', '2023-02-05 00:47:21'),
('89c251f1685a45afaf5a941b9ddbd029666bd4eede1cf074f2af4e8237844ef499935b4bd3722ab2', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-03 00:41:53', '2022-02-03 00:41:53', '2023-02-02 20:41:53'),
('8a232c3ff9c8f49a5bf25b181d88df1a306e75eace7dba8cbf4a61fa02751cdf657ff84610f3f099', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 00:51:18', '2022-02-06 00:51:18', '2022-02-12 20:51:18'),
('8a8e3658e2561d6abd6174c833a68187004f1523c553a56b97a3d152833e3f25a41ee7de7accf327', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-04 19:35:44', '2022-02-04 19:35:44', '2023-02-04 15:35:44'),
('8dd50b86ed8117d4978f230eaf2125ee26449affcc2b2a89b18635bde27736a42641e022d2123d3c', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-18 21:32:03', '2022-01-18 21:32:03', '2023-01-18 17:32:03'),
('8edc2b356db24e7ab85b9a26514eb1df9eedd57ce41b1817143fc3482fc4a83351868e1ed1ac6824', 6, 1, 'Personal Access Token', '[]', 1, '2022-10-13 05:38:28', '2022-10-13 05:38:28', '2023-10-13 01:38:28'),
('8efac601c8c4820ab4a0efba34223d19554c17be88b652415cf6f2f6b1f400ee2328abf69c98464c', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-12 00:49:54', '2022-02-12 00:49:54', '2023-02-11 20:49:54'),
('8f39f8dcd0b8ddb0301661b07377c89bfb51bb45cc8cbf03acd503b52a72f7ebe6a4a2dc52288da9', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-03 02:40:52', '2022-02-03 02:40:52', '2022-02-09 22:40:52'),
('8f81be21391e4162c48aa2af08b559f44dc96777a941d552e05bdf68e09d13c05f0f219bb0f0dd11', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-07 23:55:44', '2022-02-07 23:55:44', '2023-02-07 19:55:44'),
('901a3b1fe0894343706623eca944cb1da06502cc8af7467ee814a6be824a5e03e2e12e9d5165f524', 10, 1, 'Personal Access Token', '[]', 1, '2022-03-07 02:41:17', '2022-03-07 02:41:17', '2023-03-06 22:41:17'),
('908541a89d17729d115d68b0c80d8e68477a10a55411fedd52a2c700ad62442e224b5b4344e6a0f8', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 00:45:00', '2022-02-06 00:45:00', '2022-02-12 20:45:00'),
('936c2103107ced828ad4de300995624416fbc69e388cb7bf0a28f7ceaf03bc027ae088abf9b5617b', 5, 1, 'Personal Access Token', '[]', 0, '2021-08-07 23:41:45', '2021-08-07 23:41:45', '2022-08-07 19:41:45'),
('94a7c3eb54679fea12fa268388dbb70aecbd2119dca7e6fcdcc3ea04d9bf1fa655f0a15c1729d66c', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-09 22:53:20', '2022-02-09 22:53:20', '2023-02-09 18:53:20'),
('960c360989a791938a69b63289afed05158c0954cf80f724a3da1919c9b754110b2bd5c08c31c1bb', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-29 01:15:30', '2022-01-29 01:15:30', '2023-01-28 21:15:30'),
('973afc815a93ef9aea287ab6a4f97468ee5f860ceba523f8af738b6ced760370b2f17ddbc118dd07', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-10 04:36:40', '2022-02-10 04:36:40', '2023-02-10 00:36:40'),
('97df9518afdc56ddc715ac3a14700758b4c1911c8eccd6582e8bddae6c9ddd689aca490e5859ff59', 11, 1, 'Personal Access Token', '[]', 0, '2022-02-06 23:45:56', '2022-02-06 23:45:56', '2023-02-06 19:45:56'),
('984de73f2aabf1bc513539b6531c31ff657abd7f6fe7abace16b4c9ae9a57dbe71cae30a2df8d704', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-26 02:27:53', '2022-01-26 02:27:53', '2023-01-25 22:27:53'),
('99f3496716d529ff74b8e8c223c765fb7728bad58496a53030e026dc39f7b929efc46498d314d8d0', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-09 23:17:09', '2022-02-09 23:17:09', '2023-02-09 19:17:09'),
('9b4f579e929cafcf5e1b8aa72c25029e1f5d7f13bf6cbd742014ff0429d5109e8a917d2ede29178d', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-07 05:18:49', '2022-02-07 05:18:49', '2023-02-07 01:18:49'),
('9ba184c61da28571de22b707f1226d3a43dbeb1bfba84dca681ee2d3498cbcf9e1466e136dc47945', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-25 05:02:18', '2022-01-25 05:02:18', '2023-01-25 01:02:18'),
('9d2f0ae5c3e0a4c19905912428c5b40889e54b88bde18dcc847f7120ecdb984b531e30dac94f4e3c', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 22:32:31', '2022-02-02 22:32:31', '2023-02-02 18:32:31'),
('9d7c2974a9a281ddf4ef64f2db35884c67ff572f052fd3aacaaebb4fb21455a104b1c06f2f8d5fdc', 6, 1, 'Personal Access Token', '[]', 0, '2021-09-21 22:11:41', '2021-09-21 22:11:41', '2022-09-21 18:11:41'),
('9db6331bb5f3e47a5035a6d54ecdb7e0b907c72e0f391525353d44e7dee3b5a26eb4c134d6a9634d', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-08 00:28:02', '2022-02-08 00:28:02', '2023-02-07 20:28:02'),
('9dd8a78e42424daa1b2e7f49a88ab17d4d7856d10568d1a3c8f9722d2d153f75a5d922a2cbd89dd7', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 22:16:34', '2022-02-06 22:16:34', '2023-02-06 18:16:34'),
('9e8d1fb03369868335392918bce5de33a4101857b303c8f286cbe33a2e4620789860911207596780', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-13 04:15:29', '2022-02-13 04:15:29', '2023-02-13 00:15:29'),
('9eaee6c9d5a4851dd6fef7a2aaf92e469eb06e449f643e7a8942d26d6cad0fe5a0acf68d372b80e6', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 22:08:01', '2022-02-06 22:08:01', '2023-02-06 18:08:01'),
('9efbdfd1896a4eaa23c8d04165f08b25f04e23154cd8773c4555f32ad1b2c4a6468893d605748c35', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-16 01:44:23', '2022-01-16 01:44:23', '2023-01-15 21:44:23'),
('a2f7a08da5b35994e4b88641af04d39843f750e69c6fc5fc0639bce1c4f877b2daae4e2b7f07bbeb', 10, 1, 'Personal Access Token', '[]', 0, '2022-03-21 03:56:56', '2022-03-21 03:56:56', '2023-03-20 23:56:56'),
('a327d6714a99e5532264aa913a9e5a459787672e20d96f9ab61136a0b7146c33d2424e80aa642632', 11, 1, 'Personal Access Token', '[]', 1, '2022-02-12 01:18:19', '2022-02-12 01:18:19', '2023-02-11 21:18:19'),
('a3b0fc18eb7f087c2b9d9aab60b7ed2b87ef99cbff5c72cf4eca5b3e4ad2880798299569201afd49', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-03 02:41:40', '2022-02-03 02:41:40', '2022-02-09 22:41:40'),
('a5c6ebd5afc1c68650d9894a6f183d68c3f65c3dcef1a492127623dc0ff873597836bda1ad59731d', 10, 1, 'Personal Access Token', '[]', 0, '2021-07-23 23:00:03', '2021-07-23 23:00:03', '2022-07-23 19:00:03'),
('a68fda51fafbfbecf6993b9c0441798d123dc73b90aa91dc6e828fd2f815dd9fe3294fbd3734e13b', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-09 21:35:19', '2022-02-09 21:35:19', '2023-02-09 17:35:19'),
('a7c307b73482aac1f7f0b911acedce7284e6431d2c9bd1081e5f8a10aec1d4e1c65bf8f3dac5dbbd', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:32:07', '2022-02-02 23:32:07', '2023-02-02 19:32:07'),
('a7d66bec1151e8d99f68fd1c670686c8fa0b6ede0cc7a7a5c93ff3290229f6ecfd3c73166905c5a8', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-12 17:27:37', '2022-02-12 17:27:37', '2023-02-12 13:27:37'),
('a7e30cdb290addebecf7a2d8e176eadfc3a29c5d25431d77a27d7677c57531300714c95bf955a107', 6, 1, 'Personal Access Token', '[]', 0, '2021-09-03 04:38:08', '2021-09-03 04:38:08', '2022-09-03 00:38:08'),
('a80dbc4e91138e6e2345c71ee731479ca17cfeaae3388df1711fa79855efac80da9ef4c8081730ab', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-04 19:43:59', '2022-02-04 19:43:59', '2023-02-04 15:43:59'),
('a89cd07a1525acdca0db456b0d90f8a1308085c1c61e71794923caab810077b4549560f1e7d78871', 6, 1, 'Personal Access Token', '[]', 1, '2022-03-11 19:39:01', '2022-03-11 19:39:01', '2023-03-11 15:39:01'),
('a979477c96640fc0f06fcf0c0765228634a564caa0e553bc09e6140c25dd3e1d7acd01071105880b', 10, 1, 'Personal Access Token', '[]', 0, '2022-03-15 05:12:07', '2022-03-15 05:12:07', '2023-03-15 01:12:07'),
('aaa37b8fefb6c7386b6a7c962bdf29b890c0628b011d0a93de4e47eaff3da62c3ba721c53926611c', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-08 04:11:51', '2022-02-08 04:11:51', '2023-02-08 00:11:51'),
('ab258eeb485115b47b0a0a0f0c59be3128238a14c9d69edb5aef6dc60cf2bf38f023238cf59956a2', 6, 1, 'Personal Access Token', '[]', 0, '2022-10-05 20:46:13', '2022-10-05 20:46:13', '2022-10-12 16:46:15'),
('abefbedd54a5e96e10d08d0764e33ad287ba785960485b6d5732aa06377ba678eb94d837a95e94a1', 6, 1, 'Personal Access Token', '[]', 0, '2022-01-20 04:57:08', '2022-01-20 04:57:08', '2023-01-20 00:57:08'),
('ac1890ec58104a4fc28507bcf3221ad81ef061026a30f70961efcbf798cdbf433f95fab81d274740', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-13 05:26:57', '2022-02-13 05:26:57', '2023-02-13 01:26:57'),
('ac943397809f621ce2e7c03be5232a5000eee2c499ea9bada2fc038f49a302e01cdd59476b605c91', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-18 21:32:15', '2022-01-18 21:32:15', '2023-01-18 17:32:15'),
('ad6a9bd01008dd8b409fbf5fed6b3b9837eb984e7790b2222f9a3b003bdd494213e9c9e3dc4a4eda', 11, 1, 'Personal Access Token', '[]', 1, '2022-02-12 08:52:00', '2022-02-12 08:52:00', '2023-02-12 04:52:00'),
('ad7017756135e2b15e448985406eae6f383d7afe220c448c8259912fb79e634957cc9c90103ff675', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-07 05:31:22', '2022-02-07 05:31:22', '2023-02-07 01:31:22'),
('ad9661f6fd1ca2664cc6b824bece326de4d2a88cb864809bff617e79286b0ad2bf8f7eae8f872b9f', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-08 05:53:00', '2022-02-08 05:53:00', '2023-02-08 01:53:00'),
('ae4599d27b3271de0a882c2e584db301ac6b141dd394f61a6d39167d9ee952a540f036b067363653', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-09 01:58:55', '2022-02-09 01:58:55', '2023-02-08 21:58:55'),
('af97dfa486bdb3e5963cc27ec9bc80a144a84a47d5c1fcb5e04492298fa8ff4b41c40696afe54a60', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-10 00:22:08', '2022-02-10 00:22:08', '2023-02-09 20:22:08'),
('b01ee209c2b2a8028cee5a49f3186f1eea28aa4ff353c47237db43a140ea167df69af885f6134205', 6, 1, 'Personal Access Token', '[]', 0, '2022-09-30 22:13:58', '2022-09-30 22:13:58', '2022-10-07 18:13:58'),
('b12b4fc7bdfc212b07d6a38c6b81b3d151dc0cd60deb6f86e139185f61bc1bac1cd12953a6c22085', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-09 04:09:01', '2022-02-09 04:09:01', '2023-02-09 00:09:01'),
('b12dc0afc17d07867beac927e0b3b245dab3deb6370175176d73a24a17cf08855aafc6276fcd63f2', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-07 23:56:08', '2022-02-07 23:56:08', '2023-02-07 19:56:08'),
('b14d5d61aaa875afc5089ae6fc52c109228b3bd25e2573804e2ce928c51526c78401d12af0c73072', 6, 1, 'Personal Access Token', '[]', 0, '2022-01-15 21:11:59', '2022-01-15 21:11:59', '2023-01-15 17:11:59'),
('b1686c68d802fd323aa8c6f5a5de28b463a794a7b0e8242ba96e6b12edffa205152a3ba94cc6b878', 6, 1, 'Personal Access Token', '[]', 0, '2022-01-24 04:18:47', '2022-01-24 04:18:47', '2023-01-24 00:18:47'),
('b363f2a024eef773c2cb2772e3e0164fcc3d82cd507fb77bc9170ccede00448a20f2395a11a9946c', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 22:26:01', '2022-02-06 22:26:01', '2023-02-06 18:26:01'),
('b4a9d04055a4f2d0fbd1983676b5920d1f1aa3f3ea533166d1d8d3fc058b68dcf78f0a5965a080ff', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-10 03:03:46', '2022-02-10 03:03:46', '2023-02-09 23:03:46'),
('b4bcf8fc1d438920c18da290f5ebd00fdcc4625f70c1e4c59b1036c955134390495b88749adc2d8e', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-24 20:00:00', '2022-01-24 20:00:00', '2023-01-24 16:00:00'),
('b5781b267812b900eb048cfce2a6255551ad5b61b191e1a21d709217e0a121a997513c1829392afc', 11, 1, 'Personal Access Token', '[]', 0, '2022-02-08 00:44:06', '2022-02-08 00:44:06', '2023-02-07 20:44:06'),
('b6345d9f624d72d3a4e05ffa8c7092146900edf55a510122d7e8fe695cc4e22914fe06ffb9226735', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-06 08:43:25', '2022-02-06 08:43:25', '2023-02-06 04:43:25'),
('b7a4506006629fb778d6b473f276b6bfd861d8395726dea22623920a4869c1d109a6052423cca88d', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-24 04:30:42', '2022-01-24 04:30:42', '2023-01-24 00:30:42'),
('b99b60d5aef626ff5f3fa82bb4374f72809d14e6481cd065db7d537b1731388387f8f2c773bbbde7', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 21:53:36', '2022-02-02 21:53:36', '2023-02-02 17:53:36'),
('bb1dad9515e59a503e91857406fd53acd4b6bf2defc7a98121fda687da86e62ec71bc5a8156feb1a', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 23:44:44', '2022-02-06 23:44:44', '2023-02-06 19:44:44'),
('bb42ead31caffc8f19b7392f54e4a1500342c0c88813021a3e9dc8601bd4f320e95106a33c982b04', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 21:38:12', '2022-02-06 21:38:12', '2023-02-06 17:38:12'),
('bb47ec4a9e01b60a24dc16ca7318febc1bd7818bb92944454c1469bf5bb4b2f9d0ac8d550d33d27f', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-24 04:30:39', '2022-01-24 04:30:39', '2023-01-24 00:30:39'),
('bc0c69d3a761e4487031f88ae02e71a025a171b5980bfb8c2c1cbc38388d54125a8f859a73d04687', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:11:17', '2022-02-02 23:11:17', '2023-02-02 19:11:17'),
('bc4765bf5f1a99f71b8b2427c5d2f3fefd38bf7d6cc1d8ee66d543b14c57d65cd1c5454fd399e5e5', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:28:09', '2022-02-02 23:28:09', '2023-02-02 19:28:09'),
('bdfd88d6db4aec5906f982a24c5d95befbba1c750f05a4438542818edf795013b6ab26b2c6ed3680', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-12 00:58:15', '2022-02-12 00:58:15', '2023-02-11 20:58:15'),
('bf9c792cde1aeb6a63f014fdd0039d46540489f72fc4412bc45c2a1d03e047c718f03d0938140bd6', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-08 00:42:14', '2022-02-08 00:42:14', '2023-02-07 20:42:14'),
('c0617e809dd8a9da0721a0fb0c4ea8e77f893b3765ab9a940a3fb61f5517d670443386a25a7d0f2d', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-25 05:02:25', '2022-01-25 05:02:25', '2023-01-25 01:02:25'),
('c2129c3ae5da1bc64363413ff86b9485bc7f5a65e3caf5a19e42dad65da5af13f1fa7356ba22ff84', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-12 06:17:10', '2022-02-12 06:17:10', '2023-02-12 02:17:10'),
('c2279bda31a6799146c9c9bc9c23d0d21fe81a077ee7765aab80bc3efc6b86c6a8c58a6da5a45b16', 10, 1, 'Personal Access Token', '[]', 0, '2021-07-23 18:31:51', '2021-07-23 18:31:51', '2022-07-23 14:31:51'),
('c2dfca187c16563f50849700dcfe7dbaa457fd3bc226312ee0aa1eb84eced4fcedac75b0c3f1a231', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:02:59', '2022-02-02 23:02:59', '2023-02-02 19:02:59'),
('c2e7fec1179ea91945ae8b26a661d01a222bcc2345d360b3f48829ac3565c8d7c8a24e6f4a2e4e81', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-13 05:00:33', '2022-02-13 05:00:33', '2023-02-13 01:00:33'),
('c3ee6ecd1ced9313e989eacebe0864dd9e63c1aa7f6e239764de9032b63a87862eb3cfc11ac6bf25', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 22:12:13', '2022-02-06 22:12:13', '2023-02-06 18:12:13'),
('c519a39a3ebbd238ccae3c58d85247d4ff91978080891aa4f4f7f1b7b4fb266db688a27887970437', 6, 1, 'Personal Access Token', '[]', 0, '2022-10-01 05:14:59', '2022-10-01 05:14:59', '2022-10-08 01:15:01'),
('c5f1524112a53038bc889a373364a663540a775de517f0d9e42f32065dc118ebb35d3985b2d4efbc', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-10 00:26:12', '2022-02-10 00:26:12', '2023-02-09 20:26:12'),
('c731cdae5b0eb6c857ce9090401f2393bb2932c21ee5f75cf94718c1a4a7b87dafbbb9be57988f81', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-03 02:39:52', '2022-02-03 02:39:52', '2022-02-09 22:39:52'),
('c7b02b30e5ceefeaa7a2702568af1b2f559f940fbd403b7a640b32f089c2e292a59344bad57c7e99', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-04 19:13:39', '2022-02-04 19:13:39', '2023-02-04 15:13:39'),
('c87312169b36dd8d908bc958e0140dcb94269bf730e0c52c964f7e3c2431fafcad008bf0e20a53cb', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-03 02:45:57', '2022-02-03 02:45:57', '2022-02-09 22:45:57'),
('c88ac79dd836ea9199cbea4302cb687f5452d77449dcdf310e70af680206e93d47c69ff1a927e158', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 23:41:34', '2022-02-06 23:41:34', '2023-02-06 19:41:34'),
('c94b23e91222de9d309c114bc659b9ec4dc63a47e94bdb9abe0ec29ee25982dc0e2bc8ac3b871dc0', 10, 1, 'Personal Access Token', '[]', 0, '2021-09-21 22:13:03', '2021-09-21 22:13:03', '2022-09-21 18:13:03'),
('c97a91fbbadadc91eb73dce06c5746bf86d672cd5eed488c8522f6f7af1505246a24c0b53cf91219', 10, 1, 'Personal Access Token', '[]', 1, '2022-03-14 04:14:35', '2022-03-14 04:14:35', '2023-03-14 00:14:35'),
('ca2716e7a192214799e0db02c7617b9f9197cf471d177457a28e5188c961d2ec9beb65dd85a87e01', 6, 1, 'Personal Access Token', '[]', 1, '2022-03-15 05:01:46', '2022-03-15 05:01:46', '2023-03-15 01:01:46'),
('caf759c66deda13f4d41f69f30554bbea487547580b3f6adcfe4a3596ed24949fe7d923ebd56cc8a', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-10 04:42:54', '2022-02-10 04:42:54', '2023-02-10 00:42:54'),
('cb7768e93e2f066bf6fa985785401fe09487480e0d87068bc096d1bea2237f142f11b03cdcd941bf', 6, 1, 'Personal Access Token', '[]', 0, '2022-10-05 20:42:31', '2022-10-05 20:42:31', '2022-10-12 16:42:31'),
('cbc2c70f1e598f1217ae8459534da6335fcaa14c1accbf01adb2175999f2a3a5f639006c84f6396d', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-12 00:59:24', '2022-02-12 00:59:24', '2023-02-11 20:59:24'),
('cc3ba78817bdb5330e4f838719a1c36d299ac29e5fc3b454064a0115a14aa93a143a28dbacba521c', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-06 01:26:43', '2022-02-06 01:26:43', '2023-02-05 21:26:43'),
('cc5df1279fdf6903eb817a9cfebb78c2eb485d744c30d449572216ccf942ed4eac6532591e9efaa8', 6, 1, 'Personal Access Token', '[]', 0, '2022-01-18 21:23:23', '2022-01-18 21:23:23', '2023-01-18 17:23:23'),
('ce38b5808a7681f089390c30bf0bf554f66896b9c0f251000583c2948225a380d1d1b63960f6ef6d', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-14 17:29:24', '2022-02-14 17:29:24', '2023-02-14 13:29:24'),
('cecaf3ff41dbcf072e1c1384a75797ad87f09e41beb153dea784bab0c84ebee9dfbf14e237324866', 11, 1, 'Personal Access Token', '[]', 0, '2021-07-23 22:33:24', '2021-07-23 22:33:24', '2022-07-23 18:33:24'),
('cfb0d65d0960981d78deed0ed94b69dc5c5b176292ed1a2feee80c85064d0a35432cb259b8f2407f', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-03 02:47:05', '2022-02-03 02:47:05', '2023-02-02 22:47:05'),
('d2ec3b3235c262876312d801eb2e7508366a1d6287d8ff830c3302487e357fb70843c76577b6a147', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-05 02:49:44', '2022-02-05 02:49:44', '2023-02-04 22:49:44'),
('d2f687b2f66270b316d1c492ac9b63df795612ab1b42e7c1696003b53d690d806a6d3ec9c1b1badd', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-04 19:37:24', '2022-02-04 19:37:24', '2023-02-04 15:37:24'),
('d3306fd8357cecb0434a45a37c44deda60c69e0f09dd24f2a2560bdd1f5a7859a56dc66523d61d9a', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 22:50:24', '2022-02-02 22:50:24', '2023-02-02 18:50:24');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('d37b2bad6f5ea65d6569ed2eef93d579ba6ac2a3a2d2f129851f5a7784d8e6effeccab7ed0ef1493', 6, 1, 'Personal Access Token', '[]', 0, '2022-01-04 02:10:15', '2022-01-04 02:10:15', '2023-01-03 22:10:15'),
('d3dab82ee7d74140c62afab384dfc1b7d64c7d32b605effe3b42f0e7d049b980f59719eae3e73714', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:16:52', '2022-02-02 23:16:52', '2023-02-02 19:16:52'),
('d433c200bb7e0bb1f6d425ebcbbeb81e5e1436b8f97a45f18bf9d4f4fcf8c3fd5495eafb7c305d16', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 22:56:14', '2022-02-02 22:56:14', '2023-02-02 18:56:14'),
('d4da56da5bc7585f2cf963978e4d109577ea2f12dc9052c82479001c718140d42384e06cf25964ef', 6, 1, 'Personal Access Token', '[]', 0, '2021-09-21 22:11:38', '2021-09-21 22:11:38', '2022-09-21 18:11:38'),
('d67088b57808e246957f2be7975631264d3cfacddb14f0eb03d10349d1918799092457690eb7f71d', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-10 01:00:21', '2022-02-10 01:00:21', '2023-02-09 21:00:21'),
('d716637e476bf71150e8c9c7f6134814de7f7cbd15439edecc2a9a8afe49e35ffdbbec445bc2f86b', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-03 02:48:07', '2022-02-03 02:48:07', '2023-02-02 22:48:07'),
('d7c6ae48e99520a39146feb7dd89f63c2ff11e489e540607c1aa93a088ab7ffe9484716a482dc4fd', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-10 04:46:23', '2022-02-10 04:46:23', '2023-02-10 00:46:23'),
('d7f568ffe7ef24a1dd0e084fcb4a546c1ad3b4099bfe5172cf31e557035e1183ca2dd8ca5d2b81b5', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:37:29', '2022-02-02 23:37:29', '2023-02-02 19:37:29'),
('d8d92b9032956458e915ce60a19552a07f86059affaafb2a855c9af4b4eac4ad823d0d6b96ba3e82', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:29:51', '2022-02-02 23:29:51', '2023-02-02 19:29:51'),
('d9a289250fc6323d70aab309fc68955749dea6e3b5ce90d778c1ef246b3f33898f6723e60287efc2', 11, 1, 'Personal Access Token', '[]', 0, '2022-03-12 01:05:40', '2022-03-12 01:05:40', '2023-03-11 21:05:40'),
('d9b7be93c5f6157d9aec9947ee5d5e1a8e9864815be1dee7119bb6550ede53b93954cdf7748bdfe1', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-06 06:30:35', '2022-02-06 06:30:35', '2023-02-06 02:30:35'),
('da789d16e0b8fc1870977bc906dc3df81a07c39a28b7d49e46203513cb6ee00f459be6cafe8c3183', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-10 01:04:09', '2022-02-10 01:04:09', '2023-02-09 21:04:09'),
('dca48574f631e60862e92f515d980ba1831a4da93f15076a01d7eff30d8bce9eecaf17a85e74ec0d', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-08 01:10:08', '2022-02-08 01:10:08', '2023-02-07 21:10:08'),
('dd86dfb39659962f231dbec29d9265d69dc88474a81e6017aaf19b8b76c0ebb12055ac91959f644f', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 08:44:32', '2022-02-06 08:44:32', '2023-02-06 04:44:32'),
('ddf097d455cce2d92363df8dcc0afdcad330093af18253f175a63e7cecddce5ed2a245f75594964c', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-06 23:43:28', '2022-02-06 23:43:28', '2023-02-06 19:43:28'),
('df5d18cc4db5dbbd7a9cd9f6de9957a0a85adc077447d4d591d8ec5e36a8a8c5ee76d56fa945e43c', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-04 19:11:58', '2022-02-04 19:11:58', '2023-02-04 15:11:58'),
('e08a663ff9c4f0a50233b9f4cfe488a90b0f312c2324397122bf3b0cf7164b0551bd67e971ad85ff', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-12 01:35:41', '2022-02-12 01:35:41', '2023-02-11 21:35:41'),
('e0e2f833dc45b90e6ed1bda77ec02917f443ff279a95d6fc0df1e5a9299f5a115164c58db770ff8d', 10, 1, 'Personal Access Token', '[]', 0, '2021-07-23 23:00:00', '2021-07-23 23:00:00', '2022-07-23 19:00:00'),
('e0f0a021791cf0131ecec964caffd5d1cedd388f0cd9892a29777e6ce5e92d65103dab1e61d5e0fb', 11, 1, 'Personal Access Token', '[]', 0, '2021-07-23 22:33:21', '2021-07-23 22:33:21', '2022-07-23 18:33:21'),
('e17e45eb481def6c59c5e0937a47d5d69e600f74e65ff4afc24f66d4207726a698fd6454fd23dd7b', 11, 1, 'Personal Access Token', '[]', 1, '2022-03-11 20:11:23', '2022-03-11 20:11:23', '2023-03-11 16:11:23'),
('e1b4ab4246a7a378336e44dc372997151d11499f19e095dc46af53c0f60cf3651bf4fdabcaab1275', 6, 1, 'Personal Access Token', '[]', 1, '2022-03-12 00:55:23', '2022-03-12 00:55:23', '2023-03-11 20:55:23'),
('e31ffadaae69398716cb707060f732331e717c78f060468fc5d1f96208fb896f92aed27e5678de7d', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-25 18:19:04', '2022-01-25 18:19:04', '2023-01-25 14:19:04'),
('e37acb1b3da055004b6b7982d9dd244f09d707fc42a01f39065e3c6419574dcba3c300503eb82bc7', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-05 08:38:41', '2022-02-05 08:38:41', '2023-02-05 04:38:41'),
('e3cada941b0be3d30072db4cfd1efe1a954bcf0f9b452f2f382bd2904926a0df2c43e2f81e776b69', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 05:14:57', '2022-02-06 05:14:57', '2023-02-06 01:14:57'),
('e53f0b5dfdc248722f89cbe717a91f0ff04961e27bc662d032866cce50745961575cc8cfd1d028ba', 6, 1, 'Personal Access Token', '[]', 1, '2022-08-10 09:01:22', '2022-08-10 09:01:22', '2023-08-10 05:01:22'),
('e5425597ead798ab006a2f2a98320c5b52d55e54b91b4ccd8b8acc2a5ac49ed1dd865bd31cee3431', 6, 1, 'Personal Access Token', '[]', 0, '2022-01-18 21:23:28', '2022-01-18 21:23:28', '2023-01-18 17:23:28'),
('e559585f040b6a4d8cafd13361e2ae16767c8db19bc7ce4cd6ad5b673c4093b5848da2d2b6086602', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-08 05:51:38', '2022-02-08 05:51:38', '2023-02-08 01:51:38'),
('e66063cb348c9ed590819be98e165c2bbbc383b2c3f1a11efeea06058b4d14b547fa331277670a8b', 10, 1, 'Personal Access Token', '[]', 1, '2022-03-14 09:46:49', '2022-03-14 09:46:49', '2023-03-14 05:46:49'),
('e7ccd664121994cbea31b79db9909f6a969cf4291f81ab0fa3fc04b795d3670bbd910adf100bf805', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 22:02:59', '2022-02-06 22:02:59', '2023-02-06 18:02:59'),
('e89c1df2e4fe75a8bfcd5efde0b7852ef54ba1386f2660456e2eaf19a6982f70a7c2c33ee40cd669', 10, 1, 'Personal Access Token', '[]', 1, '2022-02-11 07:27:56', '2022-02-11 07:27:56', '2023-02-11 03:27:56'),
('eaac15f08e2f87b6dcce1d104913ac46f6a41752a4c0a3ac4f14e80ef1affbdba18dabc4071e7746', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 06:31:08', '2022-02-06 06:31:08', '2023-02-06 02:31:08'),
('eb068dedc4d23950938a724f06aa88866a158ad576697e764956e1b3a2c9cd8cc44746917507d884', 6, 1, 'Personal Access Token', '[]', 0, '2021-09-03 04:38:03', '2021-09-03 04:38:03', '2022-09-03 00:38:03'),
('ed0a1c432e1b1f383bc5d63d4255daa5f6e4cf58573c0f87e7365e5517a7aad9112433011cfb1036', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-03 02:45:14', '2022-02-03 02:45:14', '2022-02-09 22:45:14'),
('ee1959e988e5f17ad76c476a3f9e9f4fb611ee710547a36970021101f9e7f88e9209ae65d3b893ec', 10, 1, 'Personal Access Token', '[]', 1, '2022-03-08 18:14:16', '2022-03-08 18:14:16', '2023-03-08 14:14:16'),
('eeb2ec1ceaaeb60cdf712f25d573bf36c008ee7d7fd9048fe069b237d5c57f467dbf65b595845249', 9, 1, 'Personal Access Token', '[]', 0, '2021-07-23 21:49:11', '2021-07-23 21:49:11', '2022-07-23 17:49:11'),
('ef4a686bade4fdc9ceea5f27939930814860292bf28636aa5c1cee47ad6cfe0bd7a56c096416930c', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-09 22:43:39', '2022-02-09 22:43:39', '2023-02-09 18:43:39'),
('ef5292cc980fe3df69c6c08cfc2797d3004aa015b37ee28c16322d0ea0043e9cb6388ce449f31e96', 10, 1, 'Personal Access Token', '[]', 0, '2021-07-23 22:34:29', '2021-07-23 22:34:29', '2022-07-23 18:34:29'),
('ef8e9a01eaf4a04a2aa54430e279a630cbed76c558fd46e0d8df4d4697063d9f85258f860af42b76', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:39:34', '2022-02-02 23:39:34', '2023-02-02 19:39:34'),
('efc8ab4cd2a0abfc3f1b382112fa3b6ecf63e78342426fd1ce70a5d4b9a7169c18475eb66326c216', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-10 00:24:55', '2022-02-10 00:24:55', '2023-02-09 20:24:55'),
('f1db916c1c67bdc62b309cd27ca102c318ec8b29832ff6fa8cc012bb413e504ef5aa1befbe05934f', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-05 08:04:27', '2022-02-05 08:04:27', '2023-02-05 04:04:27'),
('f1f51c44653aee9a241e96b1dfc02a399ffa5cc3a90d36edc73e5a96a6dc3435e3619e17d84190d9', 6, 1, 'Personal Access Token', '[]', 0, '2021-07-23 18:10:08', '2021-07-23 18:10:08', '2022-07-23 14:10:08'),
('f31932f722da0188fea3d56d4a03d23854157ee1f341510ca4774afd0571bb0663cd271b7daf806b', 6, 1, 'Personal Access Token', '[]', 1, '2022-02-12 01:32:00', '2022-02-12 01:32:00', '2023-02-11 21:32:00'),
('f5190a6f69d7550c4b9b0388540e82c202008bb5b8c1b35d4d8dbb8cc5f15a98557821e6ae5b2a13', 10, 1, 'Personal Access Token', '[]', 0, '2022-02-09 21:44:25', '2022-02-09 21:44:25', '2023-02-09 17:44:25'),
('f64307be4c745db48342f54ea3b3973dc5c24f2028bfdc241d394203ea946aef149e10f82bf1f81f', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-03 02:42:43', '2022-02-03 02:42:43', '2022-02-09 22:42:43'),
('f6f6686b9bb2b310ad3031468d8044b21dc14c96c93d7d2060ba12563d9345505293661541733c1d', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-03 02:49:54', '2022-02-03 02:49:54', '2023-02-02 22:49:54'),
('f8ff82f1e740f32da41bf19479af03e4c1a406b9f6cf3d31502f1df80f097a039292eb396e17e278', 7, 1, 'Personal Access Token', '[]', 0, '2021-09-03 04:39:01', '2021-09-03 04:39:01', '2022-09-03 00:39:01'),
('f95b91bdad0b54873007c0aaee198d17e1ca6a9fc3db3800447b98b796a44027c18be74ec1c409fd', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-26 17:58:45', '2022-01-26 17:58:45', '2023-01-26 13:58:45'),
('fa40999d63e644dbbe8656861dd16a28c597b7dcb48f56c2bac2dc7dec5cfebacc92a311d6c11693', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-04 19:43:14', '2022-02-04 19:43:14', '2022-02-11 15:43:14'),
('faaca0cc072e726fd5baa80afc6e3ecf10ff88ab6cf0f24d5027a769098b05bc1f0d6229a950e7c2', 10, 1, 'Personal Access Token', '[]', 0, '2021-07-23 22:34:32', '2021-07-23 22:34:32', '2022-07-23 18:34:32'),
('facfe51dade86cf150ac7720e1752342ed3c16362c4d8052a0af4f0eace9a73789c41b0ebc228954', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-02 23:26:23', '2022-02-02 23:26:23', '2023-02-02 19:26:23'),
('fc0c607582d0e975fa82439fb5d715285a4dad334e0f84607a24ef29d6c44df6ce95fe405c68c94c', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 00:02:42', '2022-02-06 00:02:42', '2023-02-05 20:02:42'),
('fca3d30fab906ddf516e602794a94a2a919c627ca19e975e27604ebdcfa4de21d60c4554c80c9a8e', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-03 01:23:28', '2022-02-03 01:23:28', '2023-02-02 21:23:28'),
('fe81d9795438eaf928c7a11e3e854d2cb0dfc5cfb831ab490e4f18c0ae36ce437c25461293bcdfbb', 10, 1, 'Personal Access Token', '[]', 0, '2022-01-29 01:46:22', '2022-01-29 01:46:22', '2023-01-28 21:46:22'),
('ffb9e3347e7eea6f0e1a16fa57578a2be39453664be14723d30b1088062d95571ed9df6fd82b0c9d', 6, 1, 'Personal Access Token', '[]', 0, '2022-02-06 05:29:03', '2022-02-06 05:29:03', '2023-02-06 01:29:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 't4bZ1jrneTVOheuds3OutFLf7WuFBZgNvlqyCTZU', NULL, 'http://localhost', 1, 0, 0, '2021-07-23 15:08:17', '2021-07-23 15:08:17'),
(2, NULL, 'Laravel Password Grant Client', 'IumXfaOkEQjDbpfQkt4eD76xM0bjdmEhxIu5qZlw', 'users', 'http://localhost', 0, 1, 0, '2021-07-23 15:08:17', '2021-07-23 15:08:17'),
(3, NULL, 'Willy', 'dWiprzGQa2kxIMfaoHflEKUd5sfBL9SyqGuCMB88', 'users', 'http://localhost', 0, 1, 0, '2022-02-02 20:59:48', '2022-02-02 20:59:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-07-23 15:08:17', '2021-07-23 15:08:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `iduser` bigint(20) UNSIGNED NOT NULL,
  `idaccount_residence` bigint(20) UNSIGNED NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `currency_code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `date` date NOT NULL,
  `type` int(11) DEFAULT NULL,
  `amount_usd` decimal(15,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `payments`
--

INSERT INTO `payments` (`id`, `iduser`, `idaccount_residence`, `amount`, `currency_code`, `reference`, `status`, `email`, `deleted_at`, `created_at`, `updated_at`, `date`, `type`, `amount_usd`) VALUES
(4, 6, 2, '1.00', 'VES', 213123, 1, 'riverawilly@gmail.com', NULL, '2022-01-04 01:29:48', '2022-01-04 01:29:48', '2022-01-05', NULL, NULL),
(5, 6, 1, '200.00', 'VES', 3232333, 2, 'willyrivera@gmai.com', NULL, '2022-01-15 19:07:55', '2022-01-17 04:39:02', '2022-01-17', NULL, NULL),
(6, 6, 1, '80.00', 'VES', 13123, 1, 'riverawilly@gmail.com', NULL, '2022-01-15 19:15:34', '2022-01-17 06:35:00', '2022-01-05', NULL, NULL),
(7, 6, 2, '130.00', 'VES', 222333, 1, 'willy@fff.com', NULL, '2022-01-15 19:17:13', '2022-01-17 06:35:19', '2022-01-05', NULL, NULL),
(8, 6, 2, '130.00', 'VES', 222333, 1, 'willy@fff.com', NULL, '2022-01-15 19:20:47', '2022-01-17 06:35:56', '2022-01-05', NULL, NULL),
(9, 6, 2, '250.00', 'VES', 34534534, 1, 'willy@hotms.com', NULL, '2022-01-15 19:26:46', '2022-01-18 21:21:29', '2022-01-12', NULL, NULL),
(10, 6, 2, '120.00', 'VES', 34234234, 2, 'riverawilly@gmail.com', NULL, '2022-01-15 21:12:25', '2022-01-25 08:50:08', '2022-01-11', NULL, NULL),
(11, 6, 2, '120.00', 'VES', 213123, 2, 'riverawilly@gmail.com', NULL, '2022-01-15 21:13:49', '2022-02-10 04:54:59', '2022-01-12', NULL, NULL),
(12, 6, 2, '230.00', 'VES', 23233, 2, 'riverawilly@gmail.com', NULL, '2022-01-16 01:19:40', '2022-02-10 04:55:24', '2022-01-12', NULL, NULL),
(13, 6, 2, '33363.18', 'VES', 4444444, 2, 'willy@cogfranquicias.com', NULL, '2022-01-16 01:22:07', '2022-02-10 04:54:38', '2022-01-19', NULL, NULL),
(14, 6, 2, '111.00', 'VES', 12122, 2, 'argenisk@hotmail.com', NULL, '2022-01-16 01:42:06', '2022-02-10 04:54:01', '2022-01-12', NULL, NULL),
(15, 3, 1, '0.63', 'VES', 123123, 1, 'riverawilly@gmail.com', NULL, '2022-01-16 03:42:43', '2022-01-16 03:42:43', '2021-02-02', NULL, NULL),
(16, 3, 1, '111.00', 'VES', 12122, 1, 'argenisk@hotmail.com', NULL, '2022-01-17 03:20:21', '2022-02-09 22:56:40', '2022-01-05', NULL, NULL),
(17, 2, 1, '111.00', 'USD', 12122, 1, 'riverawilly@gmail.com', NULL, '2022-01-17 03:22:56', '2022-01-17 03:22:56', '2022-01-12', NULL, NULL),
(18, 2, 1, '111.00', 'VES', 12122, 1, 'willy@cogfranquicias.com', NULL, '2022-01-17 03:27:05', '2022-01-17 03:27:05', '2022-01-13', NULL, NULL),
(19, 3, 1, '111.00', 'USD', 12122, 1, 'riverawilly@gmail.com', NULL, '2022-01-17 06:10:22', '2022-01-17 06:10:22', '2022-01-06', NULL, NULL),
(20, 2, 1, '500.00', 'USD', 12122, 1, 'alirioviel2208@hotmail.com', NULL, '2022-01-17 06:20:25', '2022-01-17 06:20:25', '2022-01-20', NULL, NULL),
(21, 4, 1, '80.00', 'USD', 12122, 1, 'alirioviel2208@hotmail.com', NULL, '2022-01-17 06:21:00', '2022-01-17 06:21:00', '2022-01-16', NULL, NULL),
(22, 6, 2, '80.00', 'VES', 123123, 1, 'riverawilly@gmail.com', NULL, '2022-01-18 21:38:43', '2022-01-18 21:38:43', '2021-02-02', NULL, NULL),
(23, 6, 2, '80.00', 'VES', 123123, 1, 'riverawilly@gmail.com', NULL, '2022-01-18 21:39:14', '2022-01-18 21:39:14', '2021-02-02', NULL, NULL),
(24, 6, 2, '80.00', 'VES', 123123, 1, 'riverawilly@gmail.com', NULL, '2022-01-18 21:39:42', '2022-01-18 21:39:42', '2021-02-02', NULL, NULL),
(25, 6, 2, '80.00', 'VES', 123123, 1, 'riverawilly@gmail.com', NULL, '2022-01-18 21:40:01', '2022-01-18 21:40:01', '2021-02-02', NULL, NULL),
(26, 6, 2, '80.00', 'VES', 123123, 1, 'riverawilly@gmail.com', NULL, '2022-01-18 21:43:01', '2022-01-18 21:43:01', '2021-02-02', NULL, NULL),
(27, 6, 2, '80.00', 'VES', 123123, 1, 'riverawilly@gmail.com', NULL, '2022-01-18 21:43:49', '2022-01-18 21:43:49', '2021-02-02', NULL, NULL),
(28, 6, 2, '80.00', 'VES', 123123, 1, 'riverawilly@gmail.com', NULL, '2022-01-18 21:47:25', '2022-01-18 21:47:25', '2021-02-02', NULL, NULL),
(29, 6, 2, '80.00', 'VES', 123123, 1, 'riverawilly@gmail.com', NULL, '2022-01-18 21:47:38', '2022-01-18 21:47:38', '2021-02-02', NULL, NULL),
(30, 6, 1, '50.00', 'USD', 12122, 1, 'riverawilly@gmail.com', NULL, '2022-01-18 22:02:55', '2022-01-18 22:02:55', '2022-01-05', NULL, NULL),
(31, 6, 1, '50.00', 'USD', 12122, 1, 'argenisk@hotmail.com', NULL, '2022-01-18 22:04:49', '2022-01-18 22:04:49', '2022-01-12', NULL, NULL),
(32, 6, 1, '231.50', 'USD', 12122, 1, 'argenisk@hotmail.com', NULL, '2022-01-18 22:07:41', '2022-01-18 22:07:41', '2022-01-12', NULL, NULL),
(33, 6, 1, '4610.00', 'USD', 12122, 1, 'willy@malllikeu.com', NULL, '2022-01-22 23:41:40', '2022-01-22 23:41:40', '2022-01-11', NULL, NULL),
(34, 6, 2, '33363.18', 'VES', 12122, 1, 'alirioviel2208@hotmail.com', NULL, '2022-01-24 04:20:29', '2022-02-09 22:56:16', '2022-01-12', NULL, NULL),
(35, 6, 1, '50.00', 'VES', 12122, 1, 'willy@malllikeu.com', NULL, '2022-02-06 04:56:18', '2022-02-06 05:28:47', '2022-02-09', NULL, NULL),
(36, 6, 2, '150.00', 'VES', 45345345, 1, 'riverawilly@gmail.com', NULL, '2022-02-06 06:30:15', '2022-02-06 06:30:55', '2022-02-10', NULL, NULL),
(37, 2, 1, '226.50', 'USD', 45345345, 1, 'argenisk@hotmail.com', NULL, '2022-02-06 18:33:49', '2022-02-06 18:33:49', '2022-02-09', NULL, NULL),
(38, 2, 2, '362.40', 'USD', 45345345, 1, 'argenisk@hotmail.com', NULL, '2022-02-06 18:36:00', '2022-02-06 18:36:00', '2022-02-09', NULL, NULL),
(39, 2, 1, '226.50', 'USD', 45345345, 1, 'argenisk@hotmail.com', NULL, '2022-02-06 18:37:41', '2022-02-06 18:37:41', '2022-02-10', NULL, NULL),
(40, 3, 1, '226.50', 'USD', 45345345, 1, 'willy@cogfranquicias.com', NULL, '2022-02-06 18:38:22', '2022-02-06 18:38:22', '2022-02-15', NULL, NULL),
(41, 6, 1, '50.00', 'USD', 45345345, 1, 'administracion@terepaima.com', NULL, '2022-02-06 21:37:21', '2022-02-06 21:37:21', '2022-02-15', NULL, NULL),
(42, 6, 1, '50.00', 'VES', 45345345, 1, 'riverawilly@gmail.com', NULL, '2022-02-07 02:59:55', '2022-02-09 03:44:32', '2022-02-16', NULL, NULL),
(43, 6, 2, '50.00', 'VES', 45345345, 0, 'riverawilly@gmail.com', NULL, '2022-02-11 07:27:45', '2022-02-11 07:27:45', '2022-02-10', NULL, NULL),
(44, 6, 2, '500.00', 'VES', 45345345, 0, 'riverawilly@gmail.com', NULL, '2022-02-12 08:50:20', '2022-02-12 08:50:20', '2022-02-09', NULL, NULL),
(45, 6, 1, '50.00', 'VES', 45345345, 0, 'riverawilly@gmail.com', NULL, '2022-02-12 17:54:59', '2022-02-12 17:54:59', '2022-02-10', NULL, NULL),
(46, 2, 1, '15.00', 'USD', 45345345, 1, 'riverawilly@gmail.com', NULL, '2022-02-13 08:55:40', '2022-02-13 08:55:40', '2022-02-16', NULL, NULL),
(47, 6, 1, '10.00', 'VES', 4647686, 1, 'argenisk@hotmail.com', NULL, '2022-02-14 21:03:33', '2022-02-14 21:08:04', '2022-02-14', NULL, NULL),
(48, 1, 1, '300.00', 'VES', 45345345, 1, 'alirioviel2208@hotmail.com', NULL, '2022-02-15 04:44:50', '2022-02-15 04:44:50', '2022-02-03', NULL, NULL),
(49, 2, 1, '50.00', 'USD', 45345345, 1, 'riverawilly@gmail.com', NULL, '2022-03-15 04:50:19', '2022-03-15 04:50:19', '2022-03-10', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payments_invoices`
--

CREATE TABLE `payments_invoices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idinvoice` bigint(20) UNSIGNED NOT NULL,
  `idpayment` bigint(20) UNSIGNED NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT 1,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `properties`
--

CREATE TABLE `properties` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `residence_id` bigint(20) UNSIGNED NOT NULL,
  `aliquot` decimal(9,6) NOT NULL,
  `number_property` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enable` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `properties`
--

INSERT INTO `properties` (`id`, `user_id`, `residence_id`, `aliquot`, `number_property`, `enable`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2.777800', '10', 1, NULL, '2021-07-23 15:45:09', '2021-07-23 15:45:09'),
(2, 2, 1, '1.777800', '11', 1, NULL, '2021-07-23 15:45:15', '2021-07-23 15:45:15'),
(3, 3, 1, '2.777800', '12', 1, NULL, '2021-07-23 15:45:20', '2021-07-23 15:45:20'),
(4, 4, 1, '2.777800', '21', 1, NULL, '2021-07-23 15:45:41', '2021-07-23 15:45:41'),
(5, 5, 1, '2.777800', '50', 1, NULL, '2021-07-23 15:46:27', '2021-07-23 15:46:27'),
(6, 6, 1, '2.777800', '99', 1, NULL, '2021-07-23 15:46:54', '2021-07-23 15:46:54'),
(7, 7, 2, '0.630000', 'APB-2', 1, NULL, '2021-07-23 15:47:51', '2021-07-23 15:47:51'),
(8, 8, 2, '1.730000', 'BPB-1', 1, NULL, '2021-07-23 15:48:10', '2021-07-23 15:48:10'),
(9, 9, 2, '0.630000', 'BPB-2', 1, NULL, '2021-07-23 15:48:53', '2021-07-23 15:48:53'),
(10, 12, 1, '3.330000', '222', 1, NULL, '2022-01-27 10:53:11', '2022-01-27 10:53:11'),
(11, 14, 1, '2.220000', '223', 1, NULL, '2022-01-27 10:54:16', '2022-01-27 10:54:16'),
(12, 15, 1, '1.220000', '223', 1, NULL, '2022-01-27 10:54:30', '2022-01-27 10:54:30'),
(13, 16, 1, '0.440000', '223', 1, NULL, '2022-01-27 10:54:51', '2022-01-27 10:54:51'),
(14, 6, 1, '9.990000', '223', 1, NULL, '2022-01-27 10:55:09', '2022-01-27 10:55:09'),
(15, 18, 1, '8.900000', '223', 1, NULL, '2022-01-27 10:55:31', '2022-01-27 10:55:31'),
(16, 19, 1, '4.440000', '223', 1, NULL, '2022-01-27 10:55:45', '2022-01-27 10:55:45'),
(17, 20, 1, '2.220000', '223', 1, NULL, '2022-01-27 10:56:00', '2022-01-27 10:56:00'),
(91, 100, 3, '0.630000', 'CPB-1', 1, NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54'),
(92, 101, 3, '0.630000', 'CPB2', 1, NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54'),
(93, 102, 3, '0.630000', 'A01-1', 1, NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54'),
(94, 103, 3, '0.650000', 'A01-2', 1, NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54'),
(95, 104, 3, '0.650000', 'A01-3', 1, NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54'),
(96, 105, 3, '0.630000', 'A01-4', 1, NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54'),
(97, 106, 3, '0.630000', 'B01-1', 1, NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54'),
(98, 107, 3, '0.650000', 'B01-2', 1, NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54'),
(99, 108, 3, '0.650000', 'B01-3', 1, NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54'),
(100, 109, 3, '0.630000', 'B01-4', 1, NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54'),
(101, 110, 3, '0.630000', 'C01-1', 1, NULL, '2022-08-05 19:43:55', '2022-08-05 19:43:55'),
(102, 111, 3, '0.650000', 'C01-2', 1, NULL, '2022-08-05 19:43:55', '2022-08-05 19:43:55'),
(103, 112, 3, '0.650000', 'C01-3', 1, NULL, '2022-08-05 19:43:55', '2022-08-05 19:43:55'),
(104, 113, 3, '0.630000', 'C01-4', 1, NULL, '2022-08-05 19:43:55', '2022-08-05 19:43:55'),
(105, 114, 3, '0.630000', 'A02-1', 1, NULL, '2022-08-05 19:43:55', '2022-08-05 19:43:55'),
(106, 115, 3, '0.650000', 'A02-2', 1, NULL, '2022-08-05 19:43:55', '2022-08-05 19:43:55'),
(107, 116, 3, '0.650000', 'A02-3', 1, NULL, '2022-08-05 19:43:55', '2022-08-05 19:43:55'),
(108, 117, 3, '0.630000', 'A02-4', 1, NULL, '2022-08-05 19:43:55', '2022-08-05 19:43:55'),
(109, 118, 2, '4.300000', '1', 1, NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03'),
(110, 119, 2, '4.300000', '2', 1, NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03'),
(111, 120, 2, '4.300000', '3', 1, NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03'),
(112, 121, 2, '4.300000', '4', 1, NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03'),
(113, 122, 2, '4.300000', '5', 1, NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03'),
(114, 123, 2, '4.300000', '6', 1, NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03'),
(115, 124, 2, '4.300000', '7', 1, NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03'),
(116, 125, 2, '4.300000', '8', 1, NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03'),
(117, 126, 2, '4.300000', '9', 1, NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03'),
(118, 127, 2, '4.300000', '10', 1, NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03'),
(119, 128, 2, '4.300000', '11', 1, NULL, '2022-08-05 21:03:04', '2022-08-05 21:03:04'),
(120, 129, 2, '4.300000', '12', 1, NULL, '2022-08-05 21:03:04', '2022-08-05 21:03:04'),
(121, 130, 2, '4.300000', '13', 1, NULL, '2022-08-05 21:03:04', '2022-08-05 21:03:04'),
(122, 131, 2, '4.300000', '14', 1, NULL, '2022-08-05 21:03:04', '2022-08-05 21:03:04'),
(123, 132, 2, '4.300000', '15', 1, NULL, '2022-08-05 21:03:04', '2022-08-05 21:03:04'),
(124, 133, 2, '4.300000', '16', 1, NULL, '2022-08-05 21:03:04', '2022-08-05 21:03:04'),
(125, 134, 2, '4.300000', '17', 1, NULL, '2022-08-05 21:03:04', '2022-08-05 21:03:04'),
(126, 135, 2, '4.300000', '18', 1, NULL, '2022-08-05 21:03:04', '2022-08-05 21:03:04'),
(127, 136, 2, '4.300000', '19', 1, NULL, '2022-08-05 21:03:04', '2022-08-05 21:03:04'),
(128, 137, 2, '4.300000', '20', 1, NULL, '2022-08-05 21:03:05', '2022-08-05 21:03:05'),
(129, 138, 2, '5.120000', 'PB', 1, NULL, '2022-08-05 21:03:05', '2022-08-05 21:03:05'),
(130, 139, 2, '4.440000', 'PH-01', 1, NULL, '2022-08-05 21:03:05', '2022-08-05 21:03:05'),
(131, 140, 2, '4.440000', 'PH-02', 1, NULL, '2022-08-05 21:03:05', '2022-08-05 21:03:05'),
(132, 141, 9, '1.682700', '1C', 1, NULL, '2022-09-26 02:03:08', '2022-09-26 02:03:08'),
(133, 142, 9, '1.682700', '1D', 1, NULL, '2022-09-26 02:03:08', '2022-09-26 02:03:08'),
(134, 143, 9, '1.682700', '2A', 1, NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09'),
(135, 144, 9, '1.682700', '2B', 1, NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09'),
(136, 145, 9, '1.682700', '2C', 1, NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09'),
(137, 146, 9, '1.682700', '2D', 1, NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09'),
(138, 147, 9, '1.682700', '3A', 1, NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09'),
(139, 148, 9, '1.682700', '3B', 1, NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09'),
(140, 149, 9, '1.682700', '3C', 1, NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09'),
(141, 150, 9, '1.682700', '3D', 1, NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09'),
(142, 151, 9, '1.682700', '4A', 1, NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10'),
(143, 152, 9, '1.682700', '4B', 1, NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10'),
(144, 153, 9, '1.682700', '4C', 1, NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10'),
(145, 154, 9, '1.682700', '4D', 1, NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10'),
(146, 155, 9, '1.682700', '5A', 1, NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10'),
(147, 156, 9, '1.682700', '5B', 1, NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10'),
(148, 157, 9, '1.682700', '5C', 1, NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10'),
(149, 158, 9, '1.682700', '5D', 1, NULL, '2022-09-26 02:03:11', '2022-09-26 02:03:11'),
(150, 159, 9, '1.682700', '6A', 1, NULL, '2022-09-26 02:03:11', '2022-09-26 02:03:11'),
(151, 160, 9, '1.682700', '6B', 1, NULL, '2022-09-26 02:03:11', '2022-09-26 02:03:11'),
(152, 161, 9, '1.682700', '6C', 1, NULL, '2022-09-26 02:03:11', '2022-09-26 02:03:11'),
(153, 162, 9, '1.682700', '6D', 1, NULL, '2022-09-26 02:03:11', '2022-09-26 02:03:11'),
(154, 163, 9, '1.682700', '7A', 1, NULL, '2022-09-26 02:03:12', '2022-09-26 02:03:12'),
(155, 164, 9, '1.682700', '7B', 1, NULL, '2022-09-26 02:03:13', '2022-09-26 02:03:13'),
(156, 165, 9, '1.682700', '7C', 1, NULL, '2022-09-26 02:03:15', '2022-09-26 02:03:15'),
(157, 166, 9, '1.682700', '7D', 1, NULL, '2022-09-26 02:03:15', '2022-09-26 02:03:15'),
(158, 167, 9, '1.682700', '8A', 1, NULL, '2022-09-26 02:03:15', '2022-09-26 02:03:15'),
(159, 168, 9, '1.682700', '8B', 1, NULL, '2022-09-26 02:03:16', '2022-09-26 02:03:16'),
(160, 169, 9, '1.682700', '8C', 1, NULL, '2022-09-26 02:03:16', '2022-09-26 02:03:16'),
(161, 170, 9, '1.682700', '8D', 1, NULL, '2022-09-26 02:03:16', '2022-09-26 02:03:16'),
(162, 171, 9, '1.682700', '9A', 1, NULL, '2022-09-26 02:03:16', '2022-09-26 02:03:16'),
(163, 172, 9, '1.682700', '9B', 1, NULL, '2022-09-26 02:03:17', '2022-09-26 02:03:17'),
(164, 173, 9, '1.682700', '9C', 1, NULL, '2022-09-26 02:03:17', '2022-09-26 02:03:17'),
(165, 174, 9, '1.682700', '9D', 1, NULL, '2022-09-26 02:03:17', '2022-09-26 02:03:17'),
(166, 175, 9, '1.682700', '10A', 1, NULL, '2022-09-26 02:03:17', '2022-09-26 02:03:17'),
(167, 176, 9, '1.682700', '10B', 1, NULL, '2022-09-26 02:03:17', '2022-09-26 02:03:17'),
(168, 177, 9, '1.682700', '10C', 1, NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18'),
(169, 178, 9, '1.682700', '10D', 1, NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18'),
(170, 179, 9, '1.682700', '11A', 1, NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18'),
(171, 180, 9, '1.682700', '11B', 1, NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18'),
(172, 181, 9, '1.682700', '11C', 1, NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18'),
(173, 182, 9, '1.682700', '11D', 1, NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18'),
(174, 183, 9, '1.682700', '12A', 1, NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18'),
(175, 184, 9, '1.682700', '12B', 1, NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19'),
(176, 185, 9, '1.682700', '12C', 1, NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19'),
(177, 186, 9, '1.682700', '12D', 1, NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19'),
(178, 187, 9, '1.682700', '13A', 1, NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19'),
(179, 188, 9, '1.682700', '13B', 1, NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19'),
(180, 189, 9, '1.682700', '13C', 1, NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19'),
(181, 190, 9, '1.682700', '13D', 1, NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19'),
(182, 191, 9, '1.682700', '14A', 1, NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19'),
(183, 192, 9, '1.682701', '14B', 1, NULL, '2022-09-26 02:03:20', '2022-09-26 08:04:57'),
(184, 193, 9, '1.682700', '14C', 1, NULL, '2022-09-26 02:03:20', '2022-09-26 02:03:20'),
(185, 194, 9, '1.682700', '14D', 1, NULL, '2022-09-26 02:03:20', '2022-09-26 07:59:58'),
(186, 195, 9, '2.885400', 'PH-1', 1, NULL, '2022-09-26 02:03:20', '2022-09-26 02:03:20'),
(187, 196, 9, '1.885400', 'PH-2', 1, NULL, '2022-09-26 02:03:20', '2022-09-26 08:13:06'),
(188, 198, 9, '1.222222', '103', 0, NULL, '2022-09-26 05:56:20', '2022-09-26 07:58:06'),
(189, 199, 9, '0.455555', '104', 0, NULL, '2022-09-26 05:57:02', '2022-09-26 07:58:28'),
(190, 200, 9, '11.912930', '104', 0, NULL, '2022-09-26 05:57:57', '2022-09-26 07:56:21'),
(191, 201, 9, '0.893949', '104', 0, NULL, '2022-09-26 05:58:27', '2022-09-26 07:58:19'),
(192, 202, 9, '0.000001', '202', 0, NULL, '2022-09-26 06:03:51', '2022-09-26 07:54:17');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `providers`
--

CREATE TABLE `providers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rif` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direction` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `idresidence` bigint(20) UNSIGNED DEFAULT NULL,
  `contact_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `local_phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `calification` int(11) DEFAULT NULL,
  `observation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `providers`
--

INSERT INTO `providers` (`id`, `name`, `rif`, `direction`, `deleted_at`, `created_at`, `updated_at`, `idresidence`, `contact_name`, `phone`, `local_phone`, `email`, `website`, `calification`, `observation`) VALUES
(1, 'Hidrocapital', 'J-234234234', 'por ahi', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `residences`
--

CREATE TABLE `residences` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direction` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rif` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `enable` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `residences`
--

INSERT INTO `residences` (`id`, `name`, `direction`, `rif`, `deleted_at`, `created_at`, `updated_at`, `enable`) VALUES
(1, 'CONDOMINO M-1-B EDIF. BUCARE 2', 'SECTOR LA ARBOLEDA EDIF. BUCARE M-1-B SAN ANTONIO DE LOS ALTOS. MIRANDA', 'J-3101857', NULL, '2021-07-23 15:40:43', '2022-08-12 05:39:05', 1),
(2, 'RESIDENCIAS MONT BLANC', 'SAN ANTONIO DE LOS ALTOS', 'J-302730015', NULL, '2021-07-23 15:41:31', '2021-07-23 15:41:31', 0),
(3, 'RESIDENCIAS MONT BLANC 4', 'SAN ANTONIO DE LOS ALTOS', 'J-302730015', NULL, '2022-03-18 20:17:46', '2022-08-12 18:51:46', NULL),
(4, 'RESIDENCIAS MONT BLANC prueba 2', 'SAN ANTONIO DE LOS ALTOS', 'J-302730015', NULL, '2022-08-12 18:54:50', '2022-08-12 18:54:50', 1),
(5, 'RESIDENCIAS MONT BLANC prueba 2', 'SAN ANTONIO DE LOS ALTOS', 'J-302730015', NULL, '2022-08-12 18:55:16', '2022-08-12 18:55:16', 1),
(6, 'RESIDENCIAS MONT BLANC prueba 2', 'SAN ANTONIO DE LOS ALTOS', 'J-302730015', NULL, '2022-08-12 18:56:28', '2022-08-12 18:56:28', 1),
(7, 'RESIDENCIAS MONT BLANC prueba 2', 'SAN ANTONIO DE LOS ALTOS', 'J-302730015', NULL, '2022-08-12 18:56:55', '2022-08-12 18:56:55', 1),
(8, 'RESIDENCIAS MONT BLANC prueba 2', 'SAN ANTONIO DE LOS ALTOS', 'J-302730015', NULL, '2022-08-12 18:57:16', '2022-08-12 18:57:16', 1),
(9, 'RESIDENCIA DE PRUEBAS 55', 'ESTA ES UNA PRUEBA', 'J92834923-3', NULL, '2022-09-26 01:14:14', '2022-09-26 01:14:14', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `uploads`
--

CREATE TABLE `uploads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uploadable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uploadable_id` bigint(20) UNSIGNED NOT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'video, image, file, voice, other',
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `outstanding` tinyint(1) NOT NULL DEFAULT 0,
  `order` int(11) NOT NULL DEFAULT 0,
  `format` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `uploads`
--

INSERT INTO `uploads` (`id`, `uploadable_type`, `uploadable_id`, `route`, `type`, `active`, `outstanding`, `order`, `format`, `deleted_at`, `created_at`, `updated_at`, `name`) VALUES
(2, 'App\\Models\\Residences', 1, 'http://localhost:8000/files/90c062265ae6d72f043c486ac381dc21ca2becd7.jpg', 'document', 0, 0, 0, NULL, NULL, '2022-03-14 08:48:06', '2022-03-14 08:48:06', NULL),
(3, 'App\\Models\\Residences', 1, 'http://localhost:8000/files/e7b1cf3294312bbbbab520bdc5a13f0ca44f239c.png', 'document', 0, 0, 0, NULL, NULL, '2022-03-14 08:49:27', '2022-03-14 08:49:27', NULL),
(4, 'App\\Models\\Residences', 1, 'http://localhost:8000/files/b7d09f61afdbe5144b1c39319a6aea4533715e85.png', 'document', 0, 0, 0, NULL, NULL, '2022-03-14 09:10:09', '2022-03-14 09:10:09', NULL),
(5, 'App\\Models\\Configurations', 1, 'http://localhost:8000/files/ea6b908149dc01a85e5dfe8214f6e38385c81049.png', 'gallery', 0, 0, 0, NULL, NULL, '2022-03-14 09:33:47', '2022-03-14 09:33:47', NULL),
(6, 'App\\Models\\Configurations', 1, 'http://localhost:8000/files/6f6ffa080581d62981888211fff146bd299c6e60.png', 'gallery', 0, 0, 0, NULL, NULL, '2022-03-14 09:40:14', '2022-03-14 09:40:14', NULL),
(7, 'App\\Models\\Configurations', 1, 'http://localhost:8000/files/2dc62dfd5074bccd77702b0e30b61dedcee8570f.jpg', 'gallery', 0, 0, 0, NULL, NULL, '2022-08-10 10:05:19', '2022-08-10 10:05:19', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `document_number` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `password_decode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `local_phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` int(11) NOT NULL,
  `birth_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `document_number`, `deleted_at`, `password_decode`, `phone`, `local_phone`, `role`, `birth_date`) VALUES
(1, 'JESUS SALVADOR RODRIGUEZ', 'fumigaciones_trece03@hotmail.com', NULL, '$2y$10$AfvAxQY12icNwr.bVhwtoOxH1CZcroTeY0yqrjzTmXcRC9AmKhZT6', NULL, '2021-07-23 15:30:20', '2022-09-26 09:20:37', 23184123, NULL, '5674', '02123712653', '02124455334', 3, NULL),
(2, 'GUILLERMINA PEDRAZA', 'guillerminapedraza9@gmail.com', NULL, '$2y$10$5Mdg8Grg3bsxfoCgb.nEgujsa0570f6A3RFYtpb0ClZboiBaB4GHm', NULL, '2021-07-23 15:30:52', '2021-07-23 15:30:52', 0, NULL, '1234', '04142392315', NULL, 3, NULL),
(3, 'MARIA CAROLINA BELLO', 'carolabello0808@hotmail.com', NULL, '$2y$10$MQzKoPFjwVSX6sA1lIRf2egxbFvNK4Sab1rFCX4h45qWQLshNRlPu', NULL, '2021-07-23 15:31:39', '2021-07-23 15:31:39', 0, NULL, '1234', '04143174445', NULL, 3, NULL),
(4, 'MERY FOMBONA R.', 'samaryceo@yahoo.com', NULL, '$2y$10$W.hZFpw3yoGwrharVqagoOtvpr1Eqy1mK6GgteQkzku2kM0Y.Wdtq', NULL, '2021-07-23 15:33:08', '2021-07-23 15:33:08', 0, NULL, '1234', '02123710341', NULL, 3, NULL),
(5, 'VICENTE FERNANDEZ', 'rodriguezdpc3@gmail.com', NULL, '$2y$10$q31WQBi55dwJk9PVPDIhyeZSTrgDNP7NalpyhnkPbNW9mG.vRASha', NULL, '2021-07-23 15:34:45', '2021-07-23 15:34:45', 0, NULL, '1234', '02123714310', NULL, 3, NULL),
(6, 'WILLY RIVERA', 'riverawilly@gmail.com', NULL, '$2y$10$KU19LmLKInX0MZGBSnUSxO5elqsHsy0ivYCEuEmZb39gy3oUbI4Em', NULL, '2021-07-23 15:39:53', '2021-07-23 15:39:53', 0, NULL, '1234', '02123714310', NULL, 3, NULL),
(7, 'ALIRIO VIELMA', 'alirioviel2208@hotmail.com', NULL, '$2y$10$f5ZA5Y8WvePYxGVnUKoMAODRy3zv4UjFr9iLuGblieHa5TX2od29.', NULL, '2021-07-23 15:42:38', '2021-07-23 15:42:38', 0, NULL, '1234', '02126393217', NULL, 3, NULL),
(8, 'MORAIMA MARCANO', 'moraima.marcano@hotmail.com', NULL, '$2y$10$bl7svk/PGsDM.xuRFRLdMuQgZK5vCF3YeciB2fH3eidih4BrqrfDC', NULL, '2021-07-23 15:43:13', '2021-07-23 15:43:13', 0, NULL, '1234', '04241883644', NULL, 3, NULL),
(9, 'ESPERANZA BOZO', 'luisignacio142@gmail.com', NULL, '$2y$10$rHzwaPy01HtyDNirIV5IuO3/5qMRwP19bybhElLoHZwor3ss4/Gsy', NULL, '2021-07-23 15:43:41', '2022-02-08 02:36:29', 0, NULL, '5674', '04164037597', NULL, 3, NULL),
(10, 'Admin Terepaima', 'administracion@terepaima.com', NULL, '$2y$10$IE6UMqEySxVsbv0MlnunT.o/gZpgnC.yzO0/Aa5Qq9vFwCxKlVYOq', NULL, '2021-07-23 18:21:51', '2021-07-23 18:21:51', 0, NULL, '1234', '04164037597', NULL, 1, NULL),
(11, 'Jerry Rivera', 'jerry@terepaima.com', NULL, '$2y$10$4knXQAY6Vm/pKLwltK3XGuJ6e1/7iPc2GetF26Qt7BIbC.mpHRmWW', NULL, '2021-07-23 22:30:33', '2021-07-23 22:30:33', 0, NULL, '1234', '04164037597', NULL, 2, NULL),
(12, 'Pedro peres', 'asdasd@gmail.com', NULL, '$2y$10$iUMUV9tnBmc68UxXUxjun.rJgGdvIWwk/gp1iJGbNL9lTFRwHidXG', NULL, '2022-01-27 10:53:10', '2022-01-27 10:53:10', 0, NULL, '1111', '02123332211', '02124332232', 3, NULL),
(13, 'Pedro peres', 'asdasd22@gmail.com', NULL, '$2y$10$DoBM5ll70XQKCzy.1LKC5OAK/TtczFjHpv9xhnWLYr.Dy/8FNgkjy', NULL, '2022-01-27 10:53:25', '2022-01-27 10:53:25', 0, NULL, '1111', '02123332211', '02124332232', 3, NULL),
(14, 'Pedro peres', 'asdasd2222@gmail.com', NULL, '$2y$10$0Wyh3SsjaIIl4NQxMDVxIewj.FIIlKcddS7dTJnbDoVCWVD9YKb5u', NULL, '2022-01-27 10:54:16', '2022-01-27 10:54:16', 0, NULL, '1111', '02123332211', '02124332232', 3, NULL),
(15, 'Pedro peres', 'asdasd112@gmail.com', NULL, '$2y$10$HvIvizt7W0F2FayLkjEuQe3QSqj8MkYi/UvJYiAftNE0okB4cCjC2', NULL, '2022-01-27 10:54:29', '2022-01-27 10:54:29', 0, NULL, '1111', '02123332211', '02124332232', 3, NULL),
(16, 'Pedro peres', 'asd112222@gmail.com', NULL, '$2y$10$a.2zIYrbVrUaFMvGTJiIoul8zieYzN1IYQt5uSb0NOWpiV06NsiHq', NULL, '2022-01-27 10:54:51', '2022-01-27 10:54:51', 0, NULL, '1111', '02123332211', '02124332232', 3, NULL),
(17, 'Pedro peres', 'asd11290@gmail.com', NULL, '$2y$10$GdfBo2ScekyeQjcn2S5x/ORC7MObveFIXxveXzhOQWLXyRVVdBGIO', NULL, '2022-01-27 10:55:08', '2022-01-27 10:55:08', 0, NULL, '1111', '02123332211', '02124332232', 3, NULL),
(18, 'Pedro peres', 'asd144@gmail.com', NULL, '$2y$10$xi/j41z7RhDiAgP0YmBeBeLRfepgHb9h4phQmjhl1noe.ew8P6jp2', NULL, '2022-01-27 10:55:30', '2022-01-27 10:55:30', 0, NULL, '1111', '02123332211', '02124332232', 3, NULL),
(19, 'Pedro peres', 'asd122222@gmail.com', NULL, '$2y$10$nrSBQFSifonuvTEGGODNOOlF5byRmfqTa.1UNT/555vF9bAlcOJb.', NULL, '2022-01-27 10:55:44', '2022-01-27 10:55:44', 0, NULL, '1111', '02123332211', '02124332232', 3, NULL),
(20, 'Pedro peres', 'asd1902@gmail.com', NULL, '$2y$10$lUMXHtX6kPaXKb0saSUjFOdku7cnArOWoiCJANScnL2BhTLgACgYS', NULL, '2022-01-27 10:55:59', '2022-01-27 10:55:59', 0, NULL, '1111', '02123332211', '02124332232', 3, NULL),
(21, 'Willy Ramirez', 'algo@gmail.com', NULL, '$2y$10$lMT.oOzzlHW3jquSwkGuVujHEj9K6GOx90xi70cp5cNIJoq02X2b2', NULL, '2022-02-24 08:21:33', '2022-02-24 08:21:33', 0, NULL, '123456', '04169231992', '04169231997', 2, NULL),
(22, 'Willy Ramirez', 'algo2@gmail.com', NULL, '$2y$10$ElmAqQa7o/UIN4yScwiZpua0BrXVgyBhjcn9P1p1FYeXP11gPTP3W', NULL, '2022-02-24 08:23:18', '2022-02-24 08:23:18', 0, NULL, '1234', '04169231993', '04169231996', 2, NULL),
(100, 'NERY JOSEFINA PEÑA DE HOFFMANN', 'belkisrada@hotmail.com', NULL, '$2y$10$74DiuGJQ97VTbuRngCUGhOHLT0x.rgcP2T5iqmEhzm8eRfHOvMQaG', NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54', 19658935, NULL, 'HL0LN', '0', '0', 3, NULL),
(101, 'JACQUELINE VILLAR', 'jackieduplat@gmail.com', NULL, '$2y$10$Tb4Q.3HGTJCjyNzHNszPAeRlyY6ivuFbBMhTlO7vtGFv4xFjkR992', NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54', 0, NULL, 'HL0MO', '0', '0', 3, NULL),
(102, 'AZILDE VIVAS MASROU', 'gorditazilde@hotmail.com', NULL, '$2y$10$Dzc6I.Z8IEAjIH17ap7taOp.0k1HPPjDq3ew89JVj8eeJhzbRkYnG', NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54', 0, NULL, 'HL0NP', '0414-2640076', '0', 3, NULL),
(103, 'NELSON TELLERIA', 'njtelleria@gmail.com', NULL, '$2y$10$iREpRGFKdxdB8.F7PFAdWuzJnSJl2Ogim.pTphdvO1Db0/2Mi04C.', NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54', 0, NULL, 'HL0OQ', '04266071076', '0', 3, NULL),
(104, 'I..P.S.F.A', 'marialejandralbarran@gmail.com', NULL, '$2y$10$GhNzQ/KmiN9LKdB4zcdvJ.qMM7GsaxnBTctsFhEandhetQbVlS4T.', NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54', 0, NULL, 'HL0PR', '0', '0', 3, NULL),
(105, 'GUSTAVO CASTILLO', 'guscast2@hotmail.com', NULL, '$2y$10$y3Gat9dNze/v.H.OQEVWdO2rz2I29pJ6XeMqoga0VDrHGbU0YKMWG', NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54', 0, NULL, 'HL0QS', '04129642002', '0', 3, NULL),
(106, 'MARIA DE AMARE', 'mandregil21@gmail.com', NULL, '$2y$10$stmp6Z8hdULTzRJhnGlUY.k2rWkDq.9K9tfBwcZ9Th3D8pbc1EyQG', NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54', 0, NULL, 'HL0RT', '0', '0', 3, NULL),
(107, 'CARMEN GARCIA', 'ariana.ascanio@gmail.com', NULL, '$2y$10$O0buqxCclZp5D8wMJnoV7eUtcNg5Z7R2kh9y/tpRfIEalqmYJod6G', NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54', 0, NULL, 'HL0SU', '0', '0', 3, NULL),
(108, 'ANTONIO VASQUEZ', 'adrianamariadelbiase@gmail.com', NULL, '$2y$10$xfBDGiVMaxbvTZc8FMyd2u7o7IE/W5msUyJia1jwqyXYAF4Pd94IC', NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54', 0, NULL, 'HL0TV', '04127303897', '0', 3, NULL),
(109, 'RAFAEL VILLEGAS', 'mercedit@hotmail.com', NULL, '$2y$10$61uL76SxY69eDIXOj0QE4unNcNneZnqDy4ysfDpPyJi7L4Z2PeDVu', NULL, '2022-08-05 19:43:54', '2022-08-05 19:43:54', 0, NULL, 'HL0UW', '0034684188083', '0', 3, NULL),
(110, 'JOSE DE SOUSA', 'guillemo@gmail.com', NULL, '$2y$10$WKvxLDk3DYz0Q8Eu0X9cHuDWtdKVlVmjPRxgNkXE/1j.VTOppqXOm', NULL, '2022-08-05 19:43:55', '2022-08-05 19:43:55', 0, NULL, 'HL0VX', '04125617608', '0', 3, NULL),
(111, 'RAFAEL MORALES', 'arturomorales45@hotmail.com', NULL, '$2y$10$ZxyMGDfmNE9JTDdxmstAOOtsLg6Utu2MV85SQOem4ZifAZlISGAiK', NULL, '2022-08-05 19:43:55', '2022-08-05 19:43:55', 0, NULL, 'HL0WY', '04242996472', '0', 3, NULL),
(112, 'TRINO RAFAEL VALDEZ ODREMAN', 'trinovaldez@hotmail.com', NULL, '$2y$10$FJYT03Uftqj1rUwVJoVhLu1u6fhaawn6xLrrVyhrxHzcfgpO3KT2y', NULL, '2022-08-05 19:43:55', '2022-08-05 19:43:55', 0, NULL, 'HL0XZ', '04164257440', '0', 3, NULL),
(113, 'ANA COLINA', 'gisela_econ@hotmail.com', NULL, '$2y$10$NTQGZWPnGvHrYzx3UbxxiuKlzMM5t5IN2nZvKyS8KJq0pDGvMEqQ2', NULL, '2022-08-05 19:43:55', '2022-08-05 19:43:55', 0, NULL, 'HL0Y0', '0', '0', 3, NULL),
(114, 'MIREYA LOBO', 'mireyalobo52@gmail.com', NULL, '$2y$10$cK6QJNhU4EwcMg06fJAM..6g4prjpj45Zb.9pXX4zq3H4VyjlDsfi', NULL, '2022-08-05 19:43:55', '2022-08-05 19:43:55', 0, NULL, 'HL0Z1', '04142877293', '0', 3, NULL),
(115, 'MARIA TERESA INFANTE', 'guillermogudino7@gmail.com', NULL, '$2y$10$ns1f4WhiCKmZAS8hd37ywuwNd/GYY3j7dIwrYtkKTkkLWJCn0/gBy', NULL, '2022-08-05 19:43:55', '2022-08-05 19:43:55', 0, NULL, 'HL103', '0', '0', 3, NULL),
(116, 'ESPERANZA ROJAS', 'rdlopezm@gmail.com', NULL, '$2y$10$moYwQRcc.O2IV.w9qx2OG.POZ1XkQFRgHIb.pXkwD4rnrmS6742I2', NULL, '2022-08-05 19:43:55', '2022-08-05 19:43:55', 0, NULL, 'HL114', '04166118416', '0', 3, NULL),
(117, 'AURA DILENA', 'auratotia@hotmail.com', NULL, '$2y$10$9aE.M0XkQRqyZWS8tHajBeYvcQbn9ZIUzP7D4l/doW1JXpBVU9oMC', NULL, '2022-08-05 19:43:55', '2022-08-05 19:43:55', 0, NULL, 'HL125', '0', '0', 3, NULL),
(118, 'ROSA IRENE ORTIZ MEJIAS', 'rortiz2964@gmail.com', NULL, '$2y$10$c2geflrMrnMgwlQlw.GTzOj9X/jlDi5lag8Zz01lkwSq.oy7QsqF2', NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03', 19658935, NULL, 'VBFL6', '0424-1065544', '0', 3, NULL),
(119, 'NINA CIANCI', 'ninacianci898@gmail.com;nciana1@hotmail.com', NULL, '$2y$10$tZbqXrDIHCeiR4fxVRmU3uHmxk8.20vkeyPz08t177FF6qzlCCUjK', NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03', 0, NULL, 'VBFM7', '0416-643288', '0', 3, NULL),
(120, 'PRUDENCIO CHACON', 'quiquilez@gmail.com', NULL, '$2y$10$UZRvTgoHVxpFoCpUJ1hypO8lmiYORjSeCGlhdC2FlLPwztGUIz3vK', NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03', 0, NULL, 'VBFN8', '0', '0', 3, NULL),
(121, 'JOSE MAYORCA/ JASMIN M. DE MAYORCA', 'jasminmonsalve@hotmail.com', NULL, '$2y$10$YaSmNWIaJdzk02T0JZNMJ.4aX5QKuWb3EgCUKOIssuhboTwDas1XW', NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03', 0, NULL, 'VBFO9', '0414-3373530/02123724622', '0', 3, NULL),
(122, 'LUIS MENDOZA/ADRIANI ALIMENTI', 'aaalimenti@gmail.com', NULL, '$2y$10$Xoun3nQkxVSa2lxdm.xTk.cOa9SllUNcDLrMHLGRk0tV4Q6EXa8hi', NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03', 0, NULL, 'VBFPA', '0414-1973201', '0', 3, NULL),
(123, 'VINCENZA ASSUNTA SEMERARO DE BORRELL', 'vsmeraro16@gmail.com;enzasemeraro@yahoo.com', NULL, '$2y$10$nUJYukNVMPHN.R2CTwUl3eJB39WpYTChhZYVr2/eOW.q2RHt.L7ca', NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03', 0, NULL, 'VBFQB', '0412-9960879/+17863990712', '0', 3, NULL),
(124, 'JULIAN GONZALO/ MAYRA R. DE GONZALO', 'julian.gonzalo1@gmail.com', NULL, '$2y$10$2nUL0pJe43mL2ZRzecOvzOwmZfVmhbEmwyfKbnbT.wkOskcmFAK/6', NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03', 0, NULL, 'VBFRC', '0414-2257333/0412-3042414', '0', 3, NULL),
(125, 'LESBIA MEDINA DE GRANADO', 'lesbiamedinar@gmail.com/ lesbia_m_r@hotmail.com', NULL, '$2y$10$MbB0iVGNDemWGmcmHG5XH.grEQWiFoeic7N7widqTX4OhVKoAtlvW', NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03', 0, NULL, 'VBFSD', '0424-2304906', '0', 3, NULL),
(126, 'NORELYZ BERMUDEZ/ HECTOR JOSE ALVAREZ', 'hector-eltoro@hotmail.com', NULL, '$2y$10$xP5Nbx46QiAEzeqK0wS6Su2uIwcpfIRc78EIVaMgz98Xg9QNm7tWy', NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03', 0, NULL, 'VBFTE', '0412-7223661', '0', 3, NULL),
(127, 'OMAR COLMENARES', 'colmenareov@gmail.com;colmenaresor@gmail.com', NULL, '$2y$10$.z4NhxPvkoY1DVVieY1aA.8DAuFARNmyosmSQB.quV/5fvMlGqACG', NULL, '2022-08-05 21:03:03', '2022-08-05 21:03:03', 0, NULL, 'VBFUF', '0212-3720908', '0', 3, NULL),
(128, 'ANDY DELGADO', 'andydelgadob@gmail.com', NULL, '$2y$10$w5S05fi1vgpzyL9CCq7cR.NfqZMGEGRMONx/z6gB1anPhuJWiMExe', NULL, '2022-08-05 21:03:04', '2022-08-05 21:03:04', 0, NULL, 'VBFVG', '0424-1115042', '0', 3, NULL),
(129, 'PABLO MANZO/ BERTA SALCEDO', 'berthica55@gmail.com', NULL, '$2y$10$pa0QnCJ5jXVecnU.XPOF0emJQJximEBSglNoqM4QNyPMMBgngrab6', NULL, '2022-08-05 21:03:04', '2022-08-05 21:03:04', 0, NULL, 'VBFWH', '0424-2131125', '0', 3, NULL),
(130, 'PANFILO MASCIANGLIOLI', 'margaritanavas@hotmail.com', NULL, '$2y$10$qNRU.lQJAqRIHSQVaXA.3OKpZ7jE1jtUakey88wxROuZAsDyEs6HO', NULL, '2022-08-05 21:03:04', '2022-08-05 21:03:04', 0, NULL, 'VBFXI', '00 34 652402383', '0', 3, NULL),
(131, 'ORLANDO DAVILA', 'georgette1212@gmail.com', NULL, '$2y$10$WEOcdvrNBLC5c/J.mommouFF8oJivmbIDyznr6H99hf4PUa3J3XYC', NULL, '2022-08-05 21:03:04', '2022-08-05 21:03:04', 0, NULL, 'VBFYJ', '0414-2740522', '0', 3, NULL),
(132, 'MARIA GUADALUPE MOLINA AFRICANO', 'moli-01@hotmail.com;nidi_01@hotmail.com', NULL, '$2y$10$qTCQwhE5S/1ijG0vE5ghlulh/ja5nzcj7gXeuXAh1clkI13CCOgUG', NULL, '2022-08-05 21:03:04', '2022-08-05 21:03:04', 0, NULL, 'VBFZK', '0414-9076433', '0', 3, NULL),
(133, 'OLGA DE ZERPA', 'olgadezerpa@gmail.com;cruzrobertozerpa@gmail.com', NULL, '$2y$10$kZp9FW6a2IbEnLuQ0ca3cu/uuJct2a82lbXKcembNTnWNr/23qgkO', NULL, '2022-08-05 21:03:04', '2022-08-05 21:03:04', 0, NULL, 'VBG0M', '0426-5360081', '0', 3, NULL),
(134, 'NEVEMKA BONILLA/ ELDER LOPEZ', 'nevemka.bonilla@gmail.com/elderlopez2904@gmail.com', NULL, '$2y$10$BHtU1r5zsHvlw1n9wxvxE.R3RH/abj9rOZUyysPDtw2r6gB5jW1um', NULL, '2022-08-05 21:03:04', '2022-08-05 21:03:04', 0, NULL, 'VBG1N', '0414-3964693/0424-1790982', '0', 3, NULL),
(135, 'GIOVANNA CIANCI', 'andreinamarchan@hotmail.com', NULL, '$2y$10$SUw/WJsfR9nci1lyBuEyhu4cjbi9m.MhbSzZG1/rDt6PdeF60trKm', NULL, '2022-08-05 21:03:04', '2022-08-05 21:03:04', 0, NULL, 'VBG2O', '0', '0', 3, NULL),
(136, 'RAINER GUTIERREZ', 'gutierrez.rainer@gmail.com', NULL, '$2y$10$zLZQIq/tJE0reYHUdnD59OzFNOAA.PaPB8GnKhfLwdow6ZMPMvXqS', NULL, '2022-08-05 21:03:04', '2022-08-05 21:03:04', 0, NULL, 'VBG3P', '3464 2194868', '0', 3, NULL),
(137, 'ERNESTO CIANCI (JOSE DE SOUSA INQUILINO)', 'susanafernandezgomes@hotmail.com', NULL, '$2y$10$6RLrSKI8TNAqhKxZzGkllel7Cxt1bVndCTxPzKHl8bsBR6TGc1QMW', NULL, '2022-08-05 21:03:05', '2022-08-05 21:03:05', 0, NULL, 'VBG4Q', '0416-7080107', '0', 3, NULL),
(138, 'PIO GALINDEZ', 'piogalindez@hotmail.com', NULL, '$2y$10$gN9La6iKSZXQfFRO3e3PEehjme3ALTguYYjjn.kVqbbrw8wpaupUu', NULL, '2022-08-05 21:03:05', '2022-08-05 21:03:05', 0, NULL, 'VBG5R', '0414-2495478', '0', 3, NULL),
(139, 'GERARDO CERRADA', 'vbenitez647@hotmail.com', NULL, '$2y$10$cOLlbRS8I1P/9gmnfwkrueFRV62WOmzsZipnb1iRxX5D4dT6.k7ui', NULL, '2022-08-05 21:03:05', '2022-08-05 21:03:05', 0, NULL, 'VBG6S', '0416-8008802', '0', 3, NULL),
(140, 'LUIS GARCIA', 'luisgaga_@hotmail.com', NULL, '$2y$10$zQw/xBACYGhXsQRU52Djmu0OnLalBJiBv/Xes1p58XoIcySLwSMM6', NULL, '2022-08-05 21:03:05', '2022-08-05 21:03:05', 0, NULL, 'VBG7T', '3934 69605177', '0', 3, NULL),
(141, 'NICOLA MEGARO', 'rinamegaro@hotmail.com.2', NULL, '$2y$10$k39v/cVHDSZH47RqfNS.H.e9Xaxubj/Ui8AFrSa4jHOwl3VSkqgIO', NULL, '2022-09-26 02:03:08', '2022-09-26 02:03:08', 0, NULL, 'O0HV0', '0212-4725754', '0', 3, NULL),
(142, 'YOVANY E. RODRIGUEZ', 'olivaresmarlyn@gmail.com.2', NULL, '$2y$10$js1kB9pQVWSXQiNAQCSDf.SGLFIGok5AIVWc8Az296WKL6P5VKQgK', NULL, '2022-09-26 02:03:08', '2022-09-26 02:03:08', 0, NULL, 'O0HW1', '0426-4036821', '0', 3, NULL),
(143, 'ALFONSO BACALLADO', 'alfonsobacallado@hotmail.com', NULL, '$2y$10$OgrZX8JhV8d0qHZrgc/YOuTEn1G0nwZxpX0T0AQgW46Vot2IOhc26', NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09', 0, NULL, 'O0HX2', '0212-3736107/0412-9307127', '0', 3, NULL),
(144, 'JOSE PULIDO', 'rioazul111244@hotmail.com', NULL, '$2y$10$I8Iw1.tTRa2kQWWzw4KWp.eyeCiodVp2lHb7A5r6XknC7I2WNIVoe', NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09', 0, NULL, 'O0HY3', '0212-3725052', '0', 3, NULL),
(145, 'IRIS CORMOTO LOPEZ PACHANC', 'renatafer2324@gmail.com', NULL, '$2y$10$DhcvfSjxPq0/F3xJVhYMqeCp6.TP/agHQZI7k0/eqh8.AcGVgohd6', NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09', 0, NULL, 'O0HZ4', '04242336100', '0', 3, NULL),
(146, 'GRACIELA BOLIVAR', 'antgbo@gmail.com', NULL, '$2y$10$gEcFxn.scQ5P4U/CjmAypu8WfIafuoCMB.3lz1o3K2/fBZmZ0iZc.', NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09', 0, NULL, 'O0I06', '0212-3729223/0416-7266895', '0', 3, NULL),
(147, 'RODRIGO RAMIREZ', 'aracelisjosefinaflamarichgil@gmail.com', NULL, '$2y$10$zKBBAqZW6doRl1N0c9FPYO9dYgS00JNgDwrkiOicAY0.mSE.8IlA6', NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09', 0, NULL, 'O0I17', '0416-4926132/0424-9677549', '0', 3, NULL),
(148, 'FLOR MARIA SANCHEZ APONTE', 'florsanchez231252@gmail.com', NULL, '$2y$10$cjL3LGqp/dDOjFGBu6LxMuSJvN1IZQ8VfOAVbNU8E1/A69hj9kmIa', NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09', 0, NULL, 'O0I28', '0414-1267065', '0', 3, NULL),
(149, 'SILVA GUAIDO GIMENEZ', 'silviaguaido@gmail.com', NULL, '$2y$10$ldBKtP2QRIdX2Gxq/rLLp.TDQ5FptH17xQhE3n09QEP80a4gAipAu', NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09', 0, NULL, 'O0I39', '0426-5369360', '0', 3, NULL),
(150, 'ORIAMA QUINTERO', 'oriama53@hotmail.com', NULL, '$2y$10$OKXj05r8mdqRXfSYIt8uteqSTNckl5mRpL//C5P/YxKc10pH2Q/3O', NULL, '2022-09-26 02:03:09', '2022-09-26 02:03:09', 0, NULL, 'O0I4A', '0414-3995018', '0', 3, NULL),
(151, 'LUCIO LUGO', 'petricagladys@gmail.com', NULL, '$2y$10$8QVr9FLuwZ5IpA/fjNIoveEZJfw9LJx7c7AvbD6qh8EDjPDq/1lA6', NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10', 0, NULL, 'O0I5B', '0414-21647777/0212-3738229', '0', 3, NULL),
(152, 'VITO GRASSA', 'kelmark69@hotmail.com', NULL, '$2y$10$kbSuPTT5KqXpNNlmAiNhcevz6neZENFifTgz/Uq6Afx01W3Nr4KXO', NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10', 0, NULL, 'O0I6C', '0212-4520873', '0', 3, NULL),
(153, 'ALFONSO ATRIO', 'adelatrio@hotmail.com', NULL, '$2y$10$GWM0euDsGbPSS6qw5UAk7ePBEq5KChcFXJyINv/n3EeKcI.vkQiCy', NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10', 0, NULL, 'O0I7D', '0212-3728786/04127083443', '0', 3, NULL),
(154, 'FEBLES RODRIGO ASENCIO', 'delifebles@gmail.com', NULL, '$2y$10$gjnBrOeB8.OPv7xo1pv5eOSF5oOCJQK/AMLxOdn/ACbHYcZaHtnUi', NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10', 0, NULL, 'O0I8E', '0414-2395765', '0', 3, NULL),
(155, 'MARIA MERLINO LUISIO', 'mariamerlino@yahoo.com', NULL, '$2y$10$zsvQ1Dy9OKyFskYGnbm6K.fw1bsnoA0PjVncInpIwppEP9T2PDute', NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10', 0, NULL, 'O0I9F', '0212-3725613/0414-2892722', '0', 3, NULL),
(156, 'LUIS OMAR OVIEDO', 'emir.oviedo@hotmail.com', NULL, '$2y$10$l9oY.f1f5AMOd5WFAya/9u4YCl9G0qfT/erqEFJ1H4vpxFWE5Dr/W', NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10', 0, NULL, 'O0IAG', '04147372305', '0', 3, NULL),
(157, 'ALEXIS RODRIGUEZ', 'renaupartesf1@gmail.com', NULL, '$2y$10$7/9QJKZFpG7Dtzm7GkYwE.Mcl3s.7quUuHRzucSeKqFj9O1r5JeZe', NULL, '2022-09-26 02:03:10', '2022-09-26 02:03:10', 0, NULL, 'O0IBH', '04140184173', '0', 3, NULL),
(158, 'BERNABE LINARES', 'linaresb11@gmail.com', NULL, '$2y$10$Di.qxJiblvPW92qqKSAvKOBC2j7Y8RBKG8cekZQvvf/HM/ayH3kP2', NULL, '2022-09-26 02:03:11', '2022-09-26 02:03:11', 0, NULL, 'O0ICI', '0251-2610421/0212-3728752', '0', 3, NULL),
(159, 'JOSE ALBERTO BOTERO', 'jbotero@erpya.com', NULL, '$2y$10$6KcCvXRzRDtWKz7OKDy1cOL25BbOyVQIrW69z7Y15UxXPBj3rGN02', NULL, '2022-09-26 02:03:11', '2022-09-26 02:03:11', 0, NULL, 'O0IDJ', '0414-3357317/0412-2223824', '0', 3, NULL),
(160, 'LEONARDO RODRIGUEZ', 'mgordones@gmail.com', NULL, '$2y$10$obOJXGcxKlxORDs9sDhOSOGGT5df.QE29WE4qlfCB0HiPRwzJT9ky', NULL, '2022-09-26 02:03:11', '2022-09-26 02:03:11', 0, NULL, 'O0IEK', '0424-2123608', '0', 3, NULL),
(161, 'JOSEFA R. DE SANCHEZ', 'y.lara72@hotmail.com', NULL, '$2y$10$N2lRxHPu96M8jg9siC6qP.JLDzSO7QU8LG3oCFVXDWcXKohl5w486', NULL, '2022-09-26 02:03:11', '2022-09-26 02:03:11', 0, NULL, 'O0IFL', '0', '0', 3, NULL),
(162, 'JULIO MELO', 'juliomelo2012@gmail.com', NULL, '$2y$10$useafJT2kG11UzloQqcjYOnE8O2t75ukKH5pXY8uhayIrQIR6kSTG', NULL, '2022-09-26 02:03:11', '2022-09-26 02:03:11', 0, NULL, 'O0IGM', '04269196814', '0', 3, NULL),
(163, 'SILVESTRE OLIVEIRA', 'silvestreolisantos@gmail.com', NULL, '$2y$10$il3t5yag77cfDObSW50YJuBDOHMi6hAmcoZiiughPSmEdAMb7czNG', NULL, '2022-09-26 02:03:11', '2022-09-26 02:03:11', 0, NULL, 'O0IHN', '0414-1003810', '0', 3, NULL),
(164, 'MARIA ANTONIA MENDEZ HIDALGO', 'mendezmaria7@hotmail.com', NULL, '$2y$10$756NQPRMPr2ctWU0LF9RgOl9DewL.2QguVS3gEm57ESX7sfKFFs16', NULL, '2022-09-26 02:03:13', '2022-09-26 02:03:13', 0, NULL, 'O0IIO', '0', '0', 3, NULL),
(165, 'CARLOS COVIS', 'aluiscovis@gmail.com', NULL, '$2y$10$uXE/9nkmL9QEwQ3OcjYVFujGO8pPBqutcwwh2/3.CU.5AA35shr8y', NULL, '2022-09-26 02:03:15', '2022-09-26 02:03:15', 0, NULL, 'O0IJP', '04143680859', '0', 3, NULL),
(166, 'PEDRO MILLAN', 'xaviermillan1970@gmail.com', NULL, '$2y$10$e/VjGczqEjW7kjCvln6aRevER3G.22dgjDyiRNm4U3Sun8C0qGWGu', NULL, '2022-09-26 02:03:15', '2022-09-26 02:03:15', 0, NULL, 'O0IKQ', '0', '0', 3, NULL),
(167, 'EMILIO GARCIA', 'gonzalezpedro2159@gmail.com', NULL, '$2y$10$VhYKSfT1Q.7c6kdTStTzGOrcPGEarxR/IzsLCOths.e.ITT3sUfb6', NULL, '2022-09-26 02:03:15', '2022-09-26 02:03:15', 0, NULL, 'O0ILR', '0212-9862794', '0', 3, NULL),
(168, 'MARIA BEATRIZ RAMIREZ', 'juancaviles2012@gmail.com', NULL, '$2y$10$XPs6vvkTQ3EA7.rWyM1AXeu9aRurpZiqqVB.n9cybrKtP/pZVB9OG', NULL, '2022-09-26 02:03:16', '2022-09-26 02:03:16', 0, NULL, 'O0IMS', '04125560300', '0', 3, NULL),
(169, 'RITA GODOY BAUSSON', 'vartan68@gmail.com', NULL, '$2y$10$gNhVVHp2qaiHio5R73f8yufy/AG/V1CLZPqvbIt1j/GG/76X8Jlx.', NULL, '2022-09-26 02:03:16', '2022-09-26 02:03:16', 0, NULL, 'O0INT', '0426-9143836', '0', 3, NULL),
(170, 'ROMANO PEDER DORTA', 'meliann_25@hotmail.com', NULL, '$2y$10$3usLsUEnoonXHYNhQP4JYuzWIbM6kDgbvhfIWrBMsCBrelwZQMX6.', NULL, '2022-09-26 02:03:16', '2022-09-26 02:03:16', 0, NULL, 'O0IOU', '02123737032', '0', 3, NULL),
(171, 'LUIS PAGANELLI DELGADO', 'pagade_luis@hotmail.com', NULL, '$2y$10$tSH0NQt0qFLwpKdpYif52uMqF17LygUhFHaQQtmTbSlT8cG9uWjRe', NULL, '2022-09-26 02:03:16', '2022-09-26 02:03:16', 0, NULL, 'O0IPV', '0212-6717554/04142475780', '0', 3, NULL),
(172, 'JAVIER MELO RODRIGUEZ', 'javmel@gmail.com', NULL, '$2y$10$GKq98xQjsbaQLmuBjkcq8eTAadleUfnRYBgaRag3sHUywBXN3Vaou', NULL, '2022-09-26 02:03:17', '2022-09-26 02:03:17', 0, NULL, 'O0IQW', '0414-2727583/0212-3225133', '0', 3, NULL),
(173, 'RAFAEL ARCINIEGAS', 'francisgaray@hotmail.com', NULL, '$2y$10$Vo7ZiIrOwNNhELfxcg/Ipe86MmRE5WdUhE7q/Gq6o4FiZmAeeN03O', NULL, '2022-09-26 02:03:17', '2022-09-26 02:03:17', 0, NULL, 'O0IRX', '0212-3728175/0426-1188372', '0', 3, NULL),
(174, 'CARMEN ROMERO', 'juliomelo2012@gmail.com.2', NULL, '$2y$10$v5jq2sLjSD04fk3DfdNCz.oFp4939U2FDU0lI.sJLQ.LPVVUmKhum', NULL, '2022-09-26 02:03:17', '2022-09-26 02:03:17', 0, NULL, 'O0ISY', '0414-3980705', '0', 3, NULL),
(175, 'XIOMARA GONZALEZ PORRA', 'fernandoarias25@gmail.com', NULL, '$2y$10$X0pbBOjRKCdGavCQOFOFO.RtPUTj3ns/IMrgurDj2MwggwYBkgMaa', NULL, '2022-09-26 02:03:17', '2022-09-26 02:03:17', 0, NULL, 'O0ITZ', '0416-0113331/0416-4140408', '0', 3, NULL),
(176, 'JOSE ANGEL LAMAS Y  REYNA MERCEDES', 'reina3030@hotmail.com', NULL, '$2y$10$kMfjFarqXn2rmbXXDdrspuke.3lgeyA3pbMqtrSnt5JnpM6IFdscu', NULL, '2022-09-26 02:03:17', '2022-09-26 02:03:17', 0, NULL, 'O0IU0', '04143083187', '0', 3, NULL),
(177, 'RIBEIRO CELESTINO', 'felipet66@hotmail.com', NULL, '$2y$10$FSre01r85sMpmMAHa8b6Qu0gCOy5SzN9LqXr6jWDMnQn4iYJtM.WW', NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18', 0, NULL, 'O0IV1', '04141192777', '0', 3, NULL),
(178, 'LUIS RIOS', 'juancarlosjahn@gmail.com', NULL, '$2y$10$sbxLQST0N5jY9qRyuehO9.Cs53Rjri7sSoqKGK6Qdi9j8myILIBty', NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18', 0, NULL, 'O0IW2', '0412-5754390/0412-9501421', '0', 3, NULL),
(179, 'SALBINA NELINA BELLINGHIERI DURAN', 'salbinabellinghiere@gmail.com', NULL, '$2y$10$1f1XtIyYR/T2d65397af7exBqqGiL5jlWP53UagEGp5RPcpzgNSYq', NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18', 0, NULL, 'O0IX3', '0414-0919983', '0', 3, NULL),
(180, 'MARIO BASTIDAS', 'moreiramc@hotmail.com', NULL, '$2y$10$I5JOGyXwDGgyBYZicGplE.BmShBA.CTwWe34bQ7.doiZocPRUr1Di', NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18', 0, NULL, 'O0IY4', '0212-3728418/0424-2081018', '0', 3, NULL),
(181, 'NIRZA MILAGRO PIMENTEL', 'mecouttenye@gmail.com', NULL, '$2y$10$fQzweYidvzktY3i7lpp23O1xWBVsIXYjddvAE2U7whFTsm4gFV95S', NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18', 0, NULL, 'O0IZ5', '0212-3728033', '0', 3, NULL),
(182, 'SIMON RODRIGUEZ', 'anamariapr9@hotmail.com', NULL, '$2y$10$ApBwTV2MoHu0v4eEhLr2SusNJ0LYL7wSVmXCaNceAKQG4v.tXPule', NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18', 0, NULL, 'O0J07', '04142688143', '0', 3, NULL),
(183, 'POLIBIO GONZALEZ', 'finitalight@hotmail.com', NULL, '$2y$10$HvznPRHyb3Yu4umR9yi34.QtkzyjZTRJUzSfxyu2C8xQ79dmqQeja', NULL, '2022-09-26 02:03:18', '2022-09-26 02:03:18', 0, NULL, 'O0J18', '0', '0', 3, NULL),
(184, 'JAVIER E. MELO PEREZ', 'osneris.ga@gmail.com', NULL, '$2y$10$IKLnNa1N3PxJ5ipb915dWOO4D6TffxUFEay7/Y83/iky.YQaaan/C', NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19', 0, NULL, 'O0J29', '0424-1444244', '0', 3, NULL),
(185, 'ELVIRA MANTIÑAN DE GOMEZ', 'denyscarolina31@gmail.com', NULL, '$2y$10$E4ZglL7FzC8/S9y11HYol.7Zui4W7ckUx.tJ8cK1rTZbxfSrIrEzy', NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19', 0, NULL, 'O0J3A', '04144727761', '0', 3, NULL),
(186, 'FELIX ZIEGLER', 'zieglerfelixjesus@gmail.com', NULL, '$2y$10$KbpfR.j4.5/1rRIkG1NKZO3n0NxR5v72DXhS8GgnGIZ5dsTz7lLta', NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19', 0, NULL, 'O0J4B', '0416-9127473', '0', 3, NULL),
(187, 'JAVIER A. RODRIGUEZ DORANTE', 'ninoska_ninimar@hotmail.com', NULL, '$2y$10$U8eNR8fSdeAgWyerBGVfpu7aHMWSvGAy9btlS0KKVsW012yA.aZFe', NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19', 0, NULL, 'O0J5C', '0', '0', 3, NULL),
(188, 'MORELA NIETO REYES', 'morelanieto@hotmail.com', NULL, '$2y$10$aknhxk1RKkBBy4nYZnUhpeHd76yec9alo5kI.Z8hZDJbh0bteiGXS', NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19', 0, NULL, 'O0J6D', '0416-9051617', '0', 3, NULL),
(189, 'YARIXA SALAS/ JUDITH SALAS', 'ranses9@yahoo.com', NULL, '$2y$10$RXOYJjVGR1efLItW9N80teaGWz0zBe8hwuW.psdT4y5eik9wsR2Kq', NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19', 0, NULL, 'O0J7E', '04', '0', 3, NULL),
(190, 'JOAN OSIRIS ROJAS NUÑEZ', 'joanosirisrojas@gmail.com', NULL, '$2y$10$kisP8IyuP4mGeggwUGy0KOIRzdOjagVPmOILySQHN73aWvNWu1sei', NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19', 0, NULL, 'O0J8F', '0212-3736335/0412-733738    2', '0', 3, NULL),
(191, 'SANTIAGO EDUARDO HIBIRMAS', 'ivettehibirmas84@gmail.com', NULL, '$2y$10$TlY47pWd0e6tj9vqCWQmFeGHHGu8aUumpevXi0p/dk63oNYMw6U4O', NULL, '2022-09-26 02:03:19', '2022-09-26 02:03:19', 0, NULL, 'O0J9G', '0426-1175010', '0', 3, NULL),
(192, 'MELVA JOSEFINA DIAZ DE PEREZ', 'emilyjfc@hotmail.com', NULL, '$2y$10$TtZufsZPbRNZl/StZnHOQOsKzngab2YAHHGI5ba1U/aIyVjtlL8Eq', NULL, '2022-09-26 02:03:20', '2022-09-26 02:03:20', 0, NULL, 'O0JAH', '0416-4188340/04143806579', '0', 3, NULL),
(193, 'CARMEN JULIA ALVAREZ VARGAS', 'carmenjavmaga@gmail.com', NULL, '$2y$10$TRcB4UKvDQUBCdXwZHCRmu.Z8O0DtjdA4dZmLjxUM9wj/chGpFAae', NULL, '2022-09-26 02:03:20', '2022-09-26 02:03:20', 0, NULL, 'O0JBI', '0412-9978655', '0', 3, NULL),
(194, 'AUGUSTO FERREIRA', 'ferreiragisel@gmail.com', NULL, '$2y$10$yAAEGvBhZEGBGzon/VH1Bu1rQ8ZEM1LtPoOEbCtT7QYyMA00O.erm', NULL, '2022-09-26 02:03:20', '2022-09-26 02:03:20', 0, NULL, 'O0JCJ', '0416-4188340/0212-4284807', '0', 3, NULL),
(195, 'NICOLASA APARICIO DONAIRE', 'haar516@gmail.com', NULL, '$2y$10$nYrwYt5rd2w37njz9z.WQOv08anJ4IczAKsWPZbmboGImT/XL.np2', NULL, '2022-09-26 02:03:20', '2022-09-26 02:03:20', 0, NULL, 'O0JDK', '0414-2500399', '0', 3, NULL),
(196, 'EFRAIN JESUS ALVARADO', 'alvaradoefrain@hotmail.com', NULL, '$2y$10$LQ5ApeA6ASw1d82wuCZCRu7XjblJkW8tIX20iXIGVzcQviGRqgz8u', NULL, '2022-09-26 02:03:20', '2022-09-26 02:03:20', 0, NULL, 'O0JEL', '0', '0', 3, NULL),
(197, 'Willy Rivera', 'riverawilly8@gmail.com', NULL, '$2y$10$qIxsMeZkJNI.HoaqMuqlFu4uhSwq/Fi7L4eMRfgq7AgdwYGuI0n9.', NULL, '2022-09-26 05:51:46', '2022-09-26 05:51:46', 0, NULL, '1233', '02123338388', '04169231881', 3, NULL),
(198, 'Willy Rivera', 'riverawilly2@gmail.com', NULL, '$2y$10$KlO6LpbY8ZZRmf7tkTCz3OEArHMD8IXQequVTXwLezjj4QmbuSjFm', NULL, '2022-09-26 05:56:19', '2022-09-26 05:56:19', 0, NULL, '1234', '04169231991', '02123399994', 3, NULL),
(199, 'Willy Rivera', 'riverawilly33@gmail.com', NULL, '$2y$10$2KRiZPDvLKa.VRmK.qLVPeCjtnhcWrgtw.qXIjzWk8hdQ1Uoux7pm', NULL, '2022-09-26 05:57:01', '2022-09-26 05:57:01', 0, NULL, '1234', '04169231991', '02123399994', 3, NULL),
(200, 'Willy Rivera', 'riverawilly5@gmail.com', NULL, '$2y$10$LBYkKEWPuky5yNrPRVQKte5gXu2kRV5cFA7d5r/iFD.vc5yInRvdW', NULL, '2022-09-26 05:57:56', '2022-09-26 05:57:56', 0, NULL, '1234', '04169231991', '02123399994', 3, NULL),
(201, 'Willy Rivera', 'riverawilly345@gmail.com', NULL, '$2y$10$KIgHCMlRJUuin2I4iSXFyOi./PHicYAZDHm7Rxnp.gNDTURO/rMKe', NULL, '2022-09-26 05:58:26', '2022-09-26 05:58:26', 0, NULL, '1234', '04169231991', '02123399994', 3, NULL),
(202, 'Pedro peres', 'asd@gmail.com', NULL, '$2y$10$QnkXxc8hyWnrMeOS/Qmo6.q/SZm3trURmm5ABuQQnH/AfXvcdWs5S', NULL, '2022-09-26 06:03:50', '2022-09-26 06:03:50', 0, NULL, '213123', '123123123', '123123123', 3, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_residences`
--

CREATE TABLE `users_residences` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idresidence` bigint(20) UNSIGNED NOT NULL,
  `iduser` bigint(20) UNSIGNED NOT NULL,
  `type` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users_residences`
--

INSERT INTO `users_residences` (`id`, `idresidence`, `iduser`, `type`, `enabled`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 11, 1, 1, NULL, '2021-07-23 18:32:08', '2021-07-23 18:32:08'),
(3, 2, 9, 1, 1, '2022-02-24 08:16:40', '2022-02-24 08:16:40', NULL),
(4, 1, 22, 1, 1, '2022-02-24 08:23:19', '2022-02-24 08:23:19', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accounts_residences`
--
ALTER TABLE `accounts_residences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `accounts_residences_idresidence_foreign` (`idresidence`);

--
-- Indices de la tabla `balances`
--
ALTER TABLE `balances`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `concepts`
--
ALTER TABLE `concepts`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `configurations`
--
ALTER TABLE `configurations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `configurations_residences`
--
ALTER TABLE `configurations_residences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `configurations_residences_idresidence_foreign` (`idresidence`),
  ADD KEY `configurations_residences_idexchange_foreign` (`idexchange`);

--
-- Indices de la tabla `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `events_idresidence_foreign` (`idresidence`);

--
-- Indices de la tabla `exchanges`
--
ALTER TABLE `exchanges`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expenses_idresidence_foreign` (`idresidence`),
  ADD KEY `expenses_idexchange_foreign` (`idexchange`);

--
-- Indices de la tabla `expenses_fees`
--
ALTER TABLE `expenses_fees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expenses_properties_property_id_foreign` (`property_id`),
  ADD KEY `expenses_properties_expense_id_foreign` (`expense_id`),
  ADD KEY `expenses_fees_concept_id_foreign` (`concept_id`);

--
-- Indices de la tabla `expenses_invoices`
--
ALTER TABLE `expenses_invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expenses_invoices_idinvoice_foreign` (`idinvoice`),
  ADD KEY `expenses_invoices_expense_fee_id_foreign` (`expense_fee_id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indices de la tabla `historical_exchanges`
--
ALTER TABLE `historical_exchanges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `historical_exchanges_idexchange_foreign` (`idexchange`);

--
-- Indices de la tabla `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_idproperty_foreign` (`idproperty`);

--
-- Indices de la tabla `invoices_exchanges`
--
ALTER TABLE `invoices_exchanges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_exchanges_idinvoice_foreign` (`idinvoice`),
  ADD KEY `invoices_exchanges_idhistorical_exchange_foreign` (`idhistorical_exchange`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_iduser_foreign` (`iduser`),
  ADD KEY `payments_idaccount_residence_foreign` (`idaccount_residence`);

--
-- Indices de la tabla `payments_invoices`
--
ALTER TABLE `payments_invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_invoices_idinvoice_foreign` (`idinvoice`),
  ADD KEY `payments_invoices_idpayment_foreign` (`idpayment`);

--
-- Indices de la tabla `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `properties_user_id_foreign` (`user_id`),
  ADD KEY `properties_residence_id_foreign` (`residence_id`);

--
-- Indices de la tabla `providers`
--
ALTER TABLE `providers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `providers_idresidence_foreign` (`idresidence`);

--
-- Indices de la tabla `residences`
--
ALTER TABLE `residences`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `uploads`
--
ALTER TABLE `uploads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uploads_uploadable_type_uploadable_id_index` (`uploadable_type`,`uploadable_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `users_residences`
--
ALTER TABLE `users_residences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_residences_idresidence_foreign` (`idresidence`),
  ADD KEY `users_residences_iduser_foreign` (`iduser`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `accounts_residences`
--
ALTER TABLE `accounts_residences`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `balances`
--
ALTER TABLE `balances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT de la tabla `concepts`
--
ALTER TABLE `concepts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;

--
-- AUTO_INCREMENT de la tabla `configurations`
--
ALTER TABLE `configurations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `configurations_residences`
--
ALTER TABLE `configurations_residences`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `events`
--
ALTER TABLE `events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `exchanges`
--
ALTER TABLE `exchanges`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;

--
-- AUTO_INCREMENT de la tabla `expenses_fees`
--
ALTER TABLE `expenses_fees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT de la tabla `expenses_invoices`
--
ALTER TABLE `expenses_invoices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1804;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `historical_exchanges`
--
ALTER TABLE `historical_exchanges`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=378;

--
-- AUTO_INCREMENT de la tabla `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=510;

--
-- AUTO_INCREMENT de la tabla `invoices_exchanges`
--
ALTER TABLE `invoices_exchanges`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=503;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT de la tabla `payments_invoices`
--
ALTER TABLE `payments_invoices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `properties`
--
ALTER TABLE `properties`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=193;

--
-- AUTO_INCREMENT de la tabla `providers`
--
ALTER TABLE `providers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `residences`
--
ALTER TABLE `residences`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `uploads`
--
ALTER TABLE `uploads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=203;

--
-- AUTO_INCREMENT de la tabla `users_residences`
--
ALTER TABLE `users_residences`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `accounts_residences`
--
ALTER TABLE `accounts_residences`
  ADD CONSTRAINT `accounts_residences_idresidence_foreign` FOREIGN KEY (`idresidence`) REFERENCES `residences` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `configurations_residences`
--
ALTER TABLE `configurations_residences`
  ADD CONSTRAINT `configurations_residences_idexchange_foreign` FOREIGN KEY (`idexchange`) REFERENCES `exchanges` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `configurations_residences_idresidence_foreign` FOREIGN KEY (`idresidence`) REFERENCES `residences` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_idresidence_foreign` FOREIGN KEY (`idresidence`) REFERENCES `residences` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `expenses`
--
ALTER TABLE `expenses`
  ADD CONSTRAINT `expenses_idexchange_foreign` FOREIGN KEY (`idexchange`) REFERENCES `exchanges` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `expenses_idresidence_foreign` FOREIGN KEY (`idresidence`) REFERENCES `residences` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `expenses_fees`
--
ALTER TABLE `expenses_fees`
  ADD CONSTRAINT `expenses_fees_concept_id_foreign` FOREIGN KEY (`concept_id`) REFERENCES `concepts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `expenses_properties_expense_id_foreign` FOREIGN KEY (`expense_id`) REFERENCES `expenses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `expenses_properties_property_id_foreign` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `expenses_invoices`
--
ALTER TABLE `expenses_invoices`
  ADD CONSTRAINT `expenses_invoices_expense_fee_id_foreign` FOREIGN KEY (`expense_fee_id`) REFERENCES `expenses_fees` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `expenses_invoices_idinvoice_foreign` FOREIGN KEY (`idinvoice`) REFERENCES `invoices` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `historical_exchanges`
--
ALTER TABLE `historical_exchanges`
  ADD CONSTRAINT `historical_exchanges_idexchange_foreign` FOREIGN KEY (`idexchange`) REFERENCES `exchanges` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_idproperty_foreign` FOREIGN KEY (`idproperty`) REFERENCES `properties` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `invoices_exchanges`
--
ALTER TABLE `invoices_exchanges`
  ADD CONSTRAINT `invoices_exchanges_idhistorical_exchange_foreign` FOREIGN KEY (`idhistorical_exchange`) REFERENCES `historical_exchanges` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `invoices_exchanges_idinvoice_foreign` FOREIGN KEY (`idinvoice`) REFERENCES `invoices` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_idaccount_residence_foreign` FOREIGN KEY (`idaccount_residence`) REFERENCES `accounts_residences` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `payments_iduser_foreign` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `payments_invoices`
--
ALTER TABLE `payments_invoices`
  ADD CONSTRAINT `payments_invoices_idinvoice_foreign` FOREIGN KEY (`idinvoice`) REFERENCES `invoices` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `payments_invoices_idpayment_foreign` FOREIGN KEY (`idpayment`) REFERENCES `payments` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `properties`
--
ALTER TABLE `properties`
  ADD CONSTRAINT `properties_residence_id_foreign` FOREIGN KEY (`residence_id`) REFERENCES `residences` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `properties_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `providers`
--
ALTER TABLE `providers`
  ADD CONSTRAINT `providers_idresidence_foreign` FOREIGN KEY (`idresidence`) REFERENCES `residences` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `users_residences`
--
ALTER TABLE `users_residences`
  ADD CONSTRAINT `users_residences_idresidence_foreign` FOREIGN KEY (`idresidence`) REFERENCES `residences` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_residences_iduser_foreign` FOREIGN KEY (`iduser`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
