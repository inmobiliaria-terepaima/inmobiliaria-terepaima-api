<?php

namespace App\Http\Controllers\Api;

use App\Models\Providers;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class ProvidersController extends Controller
{
    //
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'direction' => 'required',
            'rif' => 'required',
            'phone' => 'required',
            'idresidence' => 'required'
        ]);

        $provider = Providers::create($validatedData);

        return response()->json([
            'message' => 'Successfully created Provider!',
            'category' => $provider
        ], 201);
    }


    public function show(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|sometimes|integer|exists:providers,id',
        ]);
        $provider = Providers::where('id', $request->id)
                                ->get();

        return response()->json([
            'provider' => $provider
        ], 201);
    }


    public function index(Request $request)
    {
        $request->validate([
            'order' => 'sometimes|string|in:id,created_at,updated_at',
            'by' => [Rule::requiredIf($request->has('order')), 'string', 'in:asc,desc'],
            'search' => 'sometimes|string',
            'idresidence' => 'sometimes|integer'
        ]);

        $provider = Providers::filters($request)
                              ->orderby('id', 'asc')
                              ->paginate($request->limit);

        return response()->json($provider, 201);
    }
}
