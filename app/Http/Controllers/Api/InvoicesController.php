<?php

namespace App\Http\Controllers\Api;

use App\Models\Invoices;
use App\Models\User;
use App\Models\Residences;
use App\Models\Expenses_invoices;
use App\Models\Expenses;
use App\Models\Expenses_fees;
use App\Models\Concepts;
use App\Models\Exchanges;
use App\Models\Historical_exchanges;
use App\Models\Invoices_exchanges;
use App\Models\Balances;
use Carbon\Carbon;
use App\Models\Properties;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use PDF;

class InvoicesController extends Controller
{

    public function index(Request $request)
    {
        $request->validate([
            'order' => 'sometimes|string|in:id,created_at,updated_at',
            'by' => [Rule::requiredIf($request->has('order')), 'string', 'in:asc,desc'],
            'residence_id' => 'sometimes|integer|exists:residences,id',
            'idproperty' => 'sometimes|integer|exists:properties,id',
            'limit' => 'sometimes|integer',
        ]);

        $invoices = Invoices::filters($request)
                ->with('property.user')
                ->with('property.residence')
                ->orderby('id', 'asc')
                ->paginate(10);

        $list_status[] = array(
            'pending' => count(Invoices::filters($request)->where('status', 0)->get()), 
            'approved_residence' => count(Invoices::filters($request)->where('status', 1)->get()), 
            'rejected_residence' => count(Invoices::filters($request)->where('status', 2)->get()), 
            'loaded' => count(Invoices::filters($request)->where('status', 3)->get()), 
            'paid' => count(Invoices::filters($request)->where('status', 4)->get()), 
            'defeated' => count(Invoices::filters($request)->where('status', 5)->get())
        );

        return response()->json([
            'stats' => $list_status[0],
            'invoices' => $invoices
        ], 201);

    }


    //
       /**
     * Show invoice data
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function show(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|integer|exists:invoices,id'
        ]);

        $invoice = Invoices::where('id', $request->id)
                   ->with([
                            'expenses_invoices.expense_fee.concept', 
                            'property.user', 
                            'invoices_exchanges.exchange',
                            'property.residence.accounts'
                        ])
                   ->get();

        return response()->json([
            'invoice' => $invoice
        ], 201);
    }


    //
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        
        $validatedData = $request->validate([
            'idproperty' => 'required',
            'expiration_date' => 'required',
            'amount' => 'required',
            'currency_code' => 'required',
            'status' => 'required',
            'active' => 'required'
        ]);

        $invoice = Invoices::create($validatedData);

        return response()->json([
            'message' => 'Successfully created Invoice!',
            'category' => $invoice
        ], 201);

    }


    //
    /**
     * Update status by check lot invoices 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function change_invoices(Request $request)
    {
        $validatedData = $request->validate([
            'year' => 'required|integer',
            'month' => 'required',
            'status' => 'required|integer',
            'residence_id' => 'sometimes|integer|exists:residences,id',
            'invoice_id' => 'sometimes|integer|exists:invoices,id'
        ]); 

        
        switch($request->status) {
            case(1): // aprobacion del recibo de parte de la residencia

                Invoices::where('id', $request->invoice_id)
                        ->update([
                                    'status' => $request->status
                                ]); 
 
                // Disparar una notificacion  
                break;
 
            case(2): // rechazo de los recibos de parte de la residencia 
                 
                // Guardar el comentario de la residencia 
                Invoices::where('id', $request->invoice_id)
                        ->update([
                                    'observation' => $request->observation,
                                    'status' => $request->status
                                ]); 
                break;

            case(3): // carga de los recibos en las cuentas de cada propiedad 
                   
                // recorrer cada propiedad de este lote                 
                $invoicesList = Invoices::join('properties', function($join)
                 {
                   $join->on('properties.id', '=', 'invoices.idproperty');
                 })
                 ->select('invoices.*') 
                 ->where('properties.residence_id', $request->residence_id)
                 ->whereMonth('invoices.created_at', '=', $request->month)
                 ->whereYear('invoices.created_at', '=', $request->year)
                 ->where('invoices.status', '!=' , 3)
                                         ->with(['property'])
                                         ->get();
                                       
                

                foreach($invoicesList as $invoice){
                    
                    // Obtener el saldo del propietario
                    $balance = Balances::where('property_id', $invoice->idproperty)
                                    ->orderby('created_at', 'desc')
                                    ->first();  
                    if(is_null($balance)){
                        $balance = (object) [
                            'balance' => 0
                        ];
                    }
                    
                    // dd($invoice->id);

                    //Registrar el cargo del recibo en la cuenta 
                    $amount_balance = $invoice->amount - $balance->balance;    
                    $values = [ 
                                'idpayment' => 0, 
                                'idinvoice' => $invoice->id, 
                                'property_id' => $invoice->idproperty, 
                                'expense' => $invoice->amount, 
                                'income' => 0, 
                                'balance' => $amount_balance, 
                                'type' => 1, 
                                'currency_code' => 'VES', 
                                'idexpense' => 0, 
                                'idresidence' => $invoice->property->residence_id
                              ];
                    Balances::create($values);
                    
                    // buscar y actualizar lote de recibos 
                    Invoices::where('id', $invoice->id)
                        ->update(['status' => $request->status]);
                        
                }

                

                break;
 
            default:
                $msg = '';
        }        

        return response()->json([
            'message' => 'Successfully update status lot Invoices!'
        ], 201);

    }

    //
    /**
     * Generate new invoices in batch
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function batch_invoice(Request $request)
    {        
        //obtener todas las propiedades activas 
        $properties = Properties::where('enable', 1)
                                  ->with('residence.configuration_residence')
                                  ->whereHas('residence.configuration_residence', function ($query) {
                                        return $query->whereDay('emit_date', date('d'));
                                   })
                                  ->with('residence.expenses.expenses_fee')
                                  ->get(); 

                    
                                    // dd($properties);
    
              
        foreach ($properties as $property) {   
                //crear una factura 
                $invoice = Invoices::create([
                    'idproperty' => $property->id,
                    'expiration_date' => Carbon::now(),
                    'amount' => 0.00,
                    'currency_code' => 'VES',
                    'status' => 0,
                    'active' => 1,
                ]);                
                //Insertar la tasa de cambio que le corresponde en funcion a la indexacion
                $invoices_exchange = Invoices_exchanges::create([
                    'idinvoice' => $invoice->id,
                    'idhistorical_exchange' =>  Historical_exchanges::where('idexchange', $property->residence->configuration_residence->idexchange)->latest()->first()->id,
                ]);
                //insertar todos los gastos a la factura
                foreach ($property->residence->expenses as $expense) {
                    // push en el array (gastos recurrentes y fondos) e insertar registro (valirdar si es proforma)
                    // monto de la tasa 
                    $amount_exchange = Historical_exchanges::where('idexchange', $expense->first()->idexchange)->latest()->first()->amount;
                    
                    // dd($amount_exchange);
                    
                    // PROCESO POR CADA TIPO DE GASTO
                    switch($expense->type) {
                        case 1: // GASTOS RECURRENTES (ejecutar procesar para insertar cuota recurrente)
                            foreach ($expense->expenses_fee as $fee) {
                                if($fee->status == 0){ // validar que el estatus sea 0 en caso de que este pausado
                                    $fee_amount = $fee->amount;
                                    $fee_amount_total = $fee->amount;
                                    
                                    
                                    if($fee->currency_code == 'USD'){
                                        // convertir el monto por alicuota 
                                        $fee_amount = $fee_amount * $amount_exchange; 
                                        // convertir el monto neto del gasto 
                                        $fee_amount_total = $fee->amount * $amount_exchange; 
                                    }                
                                    
                                    if(empty($fee->property_id)){    
                                        // dd($fee_amount * $property->aliquot / 100);
                                        $fee_amount = $fee_amount * ($property->aliquot / 100);
                                        
                                    };
                                    
                                    $expense_invoice = Expenses_invoices::create([
                                        'idinvoice' => $invoice->id,
                                        'expense_fee_id' => $fee->id,
                                        'currency_code' => 'VES',
                                        'description' => '',
                                        'created_at' => $fee->date." 03:28:51",
                                        'amount' => floatval($fee_amount),
                                        'amount_fee' => floatval($fee_amount_total)
                                    ]);
                                }                                
                            }  
                            break;

                        case 4: // gasto por cuotas     
                            foreach ($expense->expenses_fee as $fee) {
                                if($fee->status == 0){ // validar que el estatus sea 0 en caso de que este pausado
                                    // filtrar el año 
                                    if(date("Y", strtotime($fee->date)) == date("Y")){
                                        // filtrar el mes 
                                        if(date("F", strtotime($fee->date)) == date("F")){
                                            $fee_amount = $fee->amount; 
                                            $fee_amount_total = $fee->amount;
                                            
                                            if($fee->currency_code == 'USD'){
                                                $fee_amount = $fee_amount * $amount_exchange; 
                                                
                                                // convertir el monto neto del gasto 
                                        $fee_amount_total = $fee->amount * $amount_exchange;
                                        
                                            }                               
                                            if(empty($fee->property_id)){       
                                                $fee_amount = $fee_amount * ($property->aliquot / 100);
                                            };
                                            $expense_invoice = Expenses_invoices::create([
                                                'idinvoice' => $invoice->id,
                                                'expense_fee_id' => $fee->id,
                                                'currency_code' => 'VES',
                                                'description' => '',
                                                'created_at' => $fee->date." 03:28:51",
                                                'amount' => floatval($fee_amount),
                                                'amount_fee' => floatval($fee_amount_total)
                                            ]);
                                            // actualizar status de la couta
                                            Expenses_fees::where('id', $fee->id)->update(['status' => 1]);
                                        }
                                    }
                                }                                
                            }           
                            break;
                
                        case 5: // aportes al fondo  
                            foreach ($expense->expenses_fee as $fee) {
                                if($fee->status == 0){ // validar que el estatus sea 0 en caso de que este pausado


                                    // verificar si es un porcentaje o por cuota
                                    if($fee->currency_code == '%'){
                                        // el porcentaje es aplicado sobre el total de los gastos comunes
                                        $amount = Expenses_invoices::where('idinvoice', $invoice->id)
                                                                    ->sum('amount_fee') * ($fee->amount / 100);

                                    }else{ 
                                        // si es un monto en bolivares o dolares multiplicar por el numero de propietarios de la residencia
                                        $amount = $fee->amount * Properties::where('idresidence', $property->idresidence)->count();
                                        if($fee->currency_code == 'USD'){
                                            $amount = $amount * $amount_exchange;
                                        }                                        
                                    }

                                                          
                                    $fee_amount = $amount * ($property->aliquot / 100);
                                    $expense_invoice = Expenses_invoices::create([
                                        'idinvoice' => $invoice->id,
                                        'expense_fee_id' => $fee->id,
                                        'currency_code' => 'VES',
                                        'description' => '',
                                        'created_at' => $fee->date." 03:28:51",
                                        'amount' => floatval($fee_amount),
                                        'amount_fee' => floatval($amount)
                                    ]);

                                }                                
                            }          
                            break;
                
                        default :
                    }

                }                
                // llamar funcion para actualizar monto del recibo
                $this->update_invoice($invoice->id);                
        }
        return response()->json([
            'message' => 'Successfully created Invoice. Total invoices generated: '
        ], 201);
    }


        //
       /**
     * Function for update amount of a invoice
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public static function update_invoice($id)
    {
        // actualizar aportes a fondos 
        $founds = Expenses_invoices::where('idinvoice', $id)->with('expense_fee.expense')->get();   

        // bsucar la sumatoria de los gastos comunes 
        $amount_comun = 0;

        //buscar monto de la ultima tasa del recibo

        //construir arreglo con los aportes a excluir
        $exclude_found = [];
        foreach ($founds as $found) {           
            if($found->expense_fee->expense->type == 5){
                //construir arreglo con los aportes a excluir
                array_push($exclude_found, $found->id);
            }
        }
        foreach ($founds as $found) {


            if($found->expense_fee->expense->type == 5){ // filtrar por tipo de gasto

                if($found->expense_fee->currency_code == '%'){
                    $amount = Expenses_invoices::where('idinvoice', $id)
                                            ->whereNotIn('id', $exclude_found)
                                            ->sum('amount_fee')
                                             * 
                                            ($found->expense_fee->amount / 100);
                }else{
                    $amount = $fee->amount * Properties::where('idresidence', $property->idresidence)->count();
                    if($found->expense_fee->currency_code == 'USD'){
                        $amount = $amount * $amount_exchange;
                    }
                }

                
                
                $invoice = Invoices::where('id', $id)->with('property')->get();

                Expenses_invoices::where('id', $found->id)
                                 ->update(
                                        [
                                            'amount' => ($amount * ($invoice[0]->property->aliquot / 100)), 
                                            'amount_fee' => $amount
                                        ]
                                    );
            }
        }

        $amount = Expenses_invoices::where('idinvoice', $id)->sum('amount');
        Invoices::where('id', $id)->update(['amount' => $amount]);
    }

    //
    /**
     * Generate PDF invoice 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function pdf_invoice(Request $request)
    {       
        $request->validate([
            'id' => 'required|sometimes|integer|exists:invoices,id',
        ]);

        // inicarlizar las variables 
        // NOTA inclui contadores para cuotas para gastoscomunes, no comunes, etc.... 
        $invoice = Invoices::where('id', $request->id)
                           ->with([ 
                                    'expenses_invoices.expense_fee.concept', 
                                    'expenses_invoices.expense_fee.expense', 
                                    'expenses_invoices.expense_fee.expense.found.balances_last', 
                                    'property.user', 
                                    'property.balances_last', 
                                    'property.residence.configuration_residence', 
                                    'property.residence.expenses.found.balances_last', 
                                    'invoices_exchanges.exchange',
                                    'property.residence.accounts'
                                ])
                           ->get()
                           ->toArray();
        $count_comun = 0;
        $count_no_comun = 0;
        $amount_fee = 0;
        $amount_comun = 0;
        $amount_no_comun = 0;
        foreach ($invoice[0]['expenses_invoices'] as $count) {   
            if($count['expense_fee']['expense']['category'] == 'gasto_comun'){
                $count_comun ++;
                $amount_comun = $amount_comun + $count['amount'];
            }
            if($count['expense_fee']['expense']['category'] == 'gasto_no_comun'){
                $count_no_comun ++;
                $amount_no_comun = $amount_no_comun + $count['amount'];
            }
            $amount_fee = $amount_fee + $count['amount_fee'];
        }
        // dd($count_comun);
        $invoice = array_merge(
                        $invoice[0], 
                        array(
                            "amount_fee"=> $amount_fee,
                            "count_comun"=> $count_comun,
                            "count_no_comun"=>$count_no_comun,
                            "amount_comun"=> $amount_comun,
                            "amount_no_comun"=>$amount_no_comun
                        )
                    );
        
        $data = [            
            'data' => $invoice,
        ]; 

        $pdf = PDF::loadView('invoice', $data)->stream('Recibo de pago #'.$invoice['id'].'.pdf');
        $headers = [
            'Content-Type' => 'application/pdf',
        ]; 
        return $pdf; 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {        
        $request->validate([
            'idinvoice' => 'required|sometimes|integer|exists:invoices,id',
        ]);
        $invoice = Invoices::findOrFail($request->idexpense);
        $invoice->delete();
        return response()->json(null, 204);
    }
}
