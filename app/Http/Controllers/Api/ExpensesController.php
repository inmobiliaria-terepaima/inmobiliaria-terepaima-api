<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Expenses;
use Illuminate\Validation\Rule;
use Carbon\Carbon;
use App\Models\Expenses_fees;
use App\Models\Concepts;
use App\Models\Balances;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;

class ExpensesController extends Controller
{
    //
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'step1' => 'required',
            'step2' => 'required',
            'idresidence' => 'required'
        ]);

        if($request->step1['type']['id'] == 5)
        {
            $idexpense = $request->step1['found']['id'];
        }else{
            $idexpense = 0;
        }

        $values = [ 'name' => 'holis', 
                    'nic' => $request->step1['nic'], 
                    'category' => $request->step1['category'], 
                    'idexpense' => $idexpense, 
                    'idexchange' => $request->step2['exchange']['id'],
                    'type' => $request->step1['type']['id'], 
                    'description' => 'N/A', 
                    'idresidence' => $request->idresidence ];
        $expense = Expenses::create($values); 

        // verificar si existe una lsita de propietarios para asignarles la cuota 
        if(count($request->step1['propertiesList']) == 0){

                $this->insertFee($request->step2['fees'], null, $expense->id, $request->step2['currencyCode']['name']);

        }else{
            foreach($request->step1['propertiesList'] as $property){

                $this->insertFee($request->step2['fees'], $property['id'], $expense->id, $request->step2['currencyCode']['name'] );                    

            }
        }
        
        return response()->json([
            'message' => 'Successfully created Expense!',
            'expense' => $request->step2
        ], 201);
    }


        //
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_found(Request $request)
    {

        $validatedData = $request->validate([
            'idresidence' => 'required',
            'idconcept' => 'required',
            'type' => 'required',
            'name' => 'required',
            'category' => 'required',
            'idexpense' => 'required',
            'description' => 'required'
        ]);

        $expenses = Expenses::create($validatedData);

        $values = ['idpayment' => 0, 
                            'idinvoice' => 0, 
                            'property_id' => 0, 
                            'expense' => 0, 
                            'income' => $request->amount, 
                            'balance' => $request->amount, 
                            'type' => 1, 
                            'currency_code' => 'VES', 
                            'idexpense' => $expenses->id, 
                            'idresidence' => 0];
                Balances::create($values);

        return response()->json([
            'message' => 'Successfully created Found',
            'category' => $expenses
        ], 201);
    }


        //
    /**
     * Algortimo para actualizar el monto del gasto del recibos asi como el monto total del recibo.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function insertFee($dataFee, $proerty_id, $expense_id, $currecy_code)
    {

        // actualizar monto del gasto
        foreach($dataFee as $fee){

            // crear el concepto
            $valuesConcept = [ 
                                'name' => $fee['concept'], 
                                'enabled' => 1
                            ];
            $concept = Concepts::create($valuesConcept);

            // crear la couta             
            $valuesFee = [ 
                                'expense_id' => $expense_id, 
                                'property_id' => $proerty_id, 
                                'concept_id' => $concept->id, 
                                'date' => $fee['year'].'-'.$fee['month'].'-01', 
                                'status' => 0, 
                                'amount' => $fee['amount'], 
                                'currency_code' => $currecy_code 
                         ];
            $expenses_fees = Expenses_fees::create($valuesFee);

            // actualizar nombre del gasto para enlistar (REVISAR BIEN ESTE PASO MAS ADELANTE)
            Expenses::find($expense_id)->update(['name' => $fee['concept']]);
        }


    }


    public function index(Request $request)
    {

        $request->validate([
            'order' => 'sometimes|string|in:name,created_at,updated_at',
            'by' => [Rule::requiredIf($request->has('order')), 'string', 'in:asc,desc'],
            'idresidence' => 'sometimes|integer|exists:residences,id',
            'idexpense' => 'sometimes|integer|exists:expenses,id',
        ]);

        $expense = Expenses::filters($request)
                ->with('exchange')
                ->with('balances_last')
                ->with('expenses_fee_last.concept')
                ->orderby('id', 'asc')
                ->paginate($request->limit);

        return response()->json($expense, 201);
    }

    public function show(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|sometimes|integer|exists:expenses,id',
        ]);
        $expense = Expenses::where('id', $request->id)
                                ->get();

        return response()->json([
            'expense' => $expense
        ], 201);
    }



    public function get_balance(Request $request)
    {

        switch($request->service) {
            case(1): // Corpoelect
                
                // buscar tasa de dolar today    
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://ov-capital.corpoelec.gob.ve/index.php/Login/consultaSaldo");
                // SSL important
                // curl_setopt($ch, CURLOPT_REFERER,'https://ov-capital.corpoelec.gob.ve');
                curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux i686; rv:32.0) Gecko/20100101 Firefox/32.0');
                curl_setopt($ch,CURLOPT_FRESH_CONNECT,TRUE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,
                            [
                    'id' => '',
                    'ncc' => $request->nic,
                    'commit'   => 'Consultar',
                ]);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $output = curl_exec($ch);
                curl_close($ch);

                // nombre del titular 

                
                    $info = explode('id=l0004018>', $output);
                    if(count($info) == 1){

                        return response()->json([
                            'data' => 'Resource not found'
                        ], 404);

                    }else{ 

                        $info2 = explode('</nobr>', $info[1]);                
                        $clientName = trim(str_replace("&nbsp;", ' ', $info2[0]));

                        // direccion
                        unset($info);
                        unset($info2);
                        $info = explode('id=l0005018>', $output);
                        $info2 = explode('id=l0007002', $info[1]);                
                        $direction = str_replace("&nbsp;", ' ', $info2[0]);
                        $direction = str_replace('</nobr></b></font></span></font><br><font face="courier new" size="2">', '', $direction);
                        $direction = trim(str_replace(' <span style="white-space:nowrap"><font face="courier new" size="2"><b><nobr id=l0006002>', ' ', $direction));
                        $direction = str_replace('                                                              ', '', $direction);
                        $direction = trim(str_replace('<span style="white-space:nowrap"><font face="courier new" size="2"><nobr', '', $direction));

                        //balance
                        unset($info);
                        unset($info2);
                        $info = explode('id=l0013051>', $output);
                        $info2 = explode('</nobr>', $info[1]);                
                        $balance = trim(str_replace("&nbsp;", ' ', $info2[0]));
                        $balance = floatval(str_replace(',', '.', str_replace('.', '', $balance)));

                        $data_array = [
                            'client' => $clientName,
                            'direction' => $direction,
                            'balance' => $balance
                        ];

                        return response()->json([
                            'data' => $data_array
                        ], 201);

                    }
                    
                break;

            default:
                $expense = Expenses::create($validatedData);
                break;
        }

        
    }



    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {        
        $request->validate([
            'idexpense' => 'required|sometimes|integer|exists:expenses,id',
        ]);
        $expense = Expenses::findOrFail($request->idexpense);
        $expense->delete();
        return response()->json(null, 204);
    }
}
