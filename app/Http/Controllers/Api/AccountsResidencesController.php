<?php

namespace App\Http\Controllers\Api;

use App\Models\Accounts_residences;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class AccountsResidencesController extends Controller
{

    public function index(Request $request)
    {
        $request->validate([
            'order' => 'sometimes|string|in:name,created_at,updated_at',
            'by' => [Rule::requiredIf($request->has('order')), 'string', 'in:asc,desc'],
            'idresidence' => 'sometimes|integer|exists:residences,id',
        ]);

        $account_residence = Accounts_residences::filters($request)
                ->orderby('id', 'asc')
                ->paginate(10);

        return response()->json($account_residence);
    }


    public function edit(Request $request)
    {        
        $validatedData = $request->validate([
            'id' => 'required|sometimes|integer|exists:accounts_residences,id',
            'type' => 'required',
            'name' => 'required|string',
            'number' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'bank' => 'required',
            'document_number' => 'required',
            'account_holder' => 'required'        
        ]);    
        
        Accounts_residences::where('id', $request->id)
            ->update([
                'type' => $request->type, 
                'bank' => $request->bank, 
                'name' => $request->name,
                'number' => $request->number,
                'email' => $request->email,
                'phone' => $request->phone,
                'document_number' => $request->document_number,
                'account_holder' => $request->account_holder
            ]);

        return response()->json([
            'message' => 'Successfully update account residence'
        ], 201);
    }


    //
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'idresidence' => 'required',
            'type' => 'required',
            'name' => 'required',
            'number' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'bank' => 'required',
            'document_number' => 'required',
            'account_holder' => 'required'
        ]);

        $account_residence = Accounts_residences::create($validatedData);

        return response()->json([
            'message' => 'Successfully created Residence acount!',
            'category' => $account_residence
        ], 201);
    }


     /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {        
        $request->validate([
            'id' => 'required|sometimes|integer|exists:accounts_residences,id',
        ]);
        $accounts_residences = Accounts_residences::findOrFail($request->id);
        $accounts_residences->delete();
        return response()->json(null, 204);
    }
}
