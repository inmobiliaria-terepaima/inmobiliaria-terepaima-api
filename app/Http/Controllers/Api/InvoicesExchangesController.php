<?php

namespace App\Http\Controllers\Api;

use App\Models\Invoices_exchanges;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class InvoicesExchangesController extends Controller
{
//
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'idinvoice' => 'required',
            'idhistorical_exchange' => 'required',
        ]);

        $invoices_exchange = Invoices_exchanges::create($validatedData);

        return response()->json([
            'message' => 'Successfully created Invoice exchange!',
            'category' => $invoices_exchange
        ], 201);
    }
}
