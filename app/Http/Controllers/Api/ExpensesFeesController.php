<?php

namespace App\Http\Controllers\Api;

use App\Models\Expenses_fees;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class ExpensesFeesController extends Controller
{
    //
    //
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'expense_id' => 'required',
            'property_id' => 'sometimes',
            'concept_id' => 'required|integer|exists:concepts,id',
            'date' => 'required',
            'status' => 'required',
            'amount' => 'required',
            'currency_code' => 'required',
        ]);

        $expenses_fees = Expenses_fees::create($validatedData);

        return response()->json([
            'message' => 'Successfully created Expenses properties!',
            'expenses_fees' => $expenses_fees
        ], 201);
    }

     public function index(Request $request)
    {

        $request->validate([
            'order' => 'sometimes|string|in:name,created_at,updated_at',
            'by' => [Rule::requiredIf($request->has('order')), 'string', 'in:asc,desc'],
            'idexpense' => 'sometimes|integer|exists:expenses,id',
        ]);

        $expense = Expenses_fees::filters($request)
                ->with(['concept', 'expense'])
                ->orderby('id', 'asc')
                ->paginate($request->limit);

        return response()->json($expense, 201);
    }
}
