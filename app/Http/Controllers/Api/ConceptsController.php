<?php

namespace App\Http\Controllers\Api;

use App\Models\Concepts;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class ConceptsController extends Controller
{
    //
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'enabled' => 'required',
        ]);

        $concept = Concepts::create($validatedData);

        return response()->json([
            'message' => 'Successfully created Concept!',
            'concept' => $concept
        ], 201);
    }

    public function index(Request $request)
    {
        $request->validate([
            'order' => 'sometimes|string|in:id,created_at,updated_at',
            'by' => [Rule::requiredIf($request->has('order')), 'string', 'in:asc,desc'],
            'search' => 'sometimes|string',
            'limit' => 'sometimes|integer'
        ]);

        $concepts = Concepts::filters($request)
                ->paginate(10);

        return response()->json($concepts, 201);
    }
}
