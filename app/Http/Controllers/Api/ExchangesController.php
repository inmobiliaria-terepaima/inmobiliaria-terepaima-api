<?php

namespace App\Http\Controllers\Api;

use App\Models\Exchanges;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class ExchangesController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'order' => 'sometimes|string|in:name,created_at,updated_at',
            'by' => [Rule::requiredIf($request->has('order')), 'string', 'in:asc,desc'],
            'idexchange' => 'sometimes|integer|exists:exchanges,id',
        ]);

        $exchanges = Exchanges::filters($request)
                ->with(['historical_exchanges_last'])
                ->orderby('id', 'asc')
                ->paginate($request->limit);

        return response()->json($exchanges);
    }
    //
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string',
            'method' => 'required',
        ]);

        $exchange = Exchanges::create($validatedData);

        return response()->json([
            'message' => 'Successfully created Exchange!',
            'category' => $exchange
        ], 201);
    }
}
