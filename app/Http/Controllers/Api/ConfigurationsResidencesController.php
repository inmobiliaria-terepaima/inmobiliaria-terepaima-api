<?php

namespace App\Http\Controllers\Api;

use App\Models\Configurations_residences;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class ConfigurationsResidencesController extends Controller
{
    public function show($id)
    {
        $configuration = Configurations_residences::where('idresidence', $id);  
        if (is_null($configuration)) {
            return response()->json([
                'message' => 'Configuration dont found'
            ], 404);
        }
   
        return response()->json($configuration, 201);
    }


    //
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function update(Request $request)
    {

        $validatedData = $request->validate([
            'idresidence' => 'required|sometimes|integer|exists:residences,id',
            'emit_date' => 'sometimes',
            'expiration_date' => 'sometimes',
            'indexing_method' => 'sometimes',
            'footer_invoice' => 'sometimes'
        ]);
        $configuration_residence = Configurations_residences::where('idresidence', $request->idresidence)
                                                            ->update($validatedData);

        return response()->json([
            'message' => 'Successfully updated Configuration Residence',
            'configuration' => $configuration_residence
        ], 201);

    }


    //
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'idresidence' => 'required',
            'idexchange' => 'required',
            'indexing_method' => 'required',
            'expiration_date' => 'required',
            'dollar' => 'required',
            'minimum_rate' => 'required',
            'emit_date' => 'required',
        ]);

        $configuration_residence = Configurations_residences::create($validatedData);

        return response()->json([
            'message' => 'Successfully created Configuration residence!',
            'category' => $configuration_residence
        ], 201);
    }
}
