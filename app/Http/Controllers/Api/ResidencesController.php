<?php

namespace App\Http\Controllers\Api;

use App\Models\Residences;
use App\Models\Properties;
use App\Models\Configurations_residences;
use App\Models\Invoices;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class ResidencesController extends Controller
{
        //
       /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $residences = Residences::filters($request)
                                ->withCount(['properties'])
                                ->orderby('created_at', 'asc')
                                ->paginate(10);
        
        return response()->json($residences, 201);
    }


    //
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function update(Request $request,$id)
    {
        //
        $validatedData = $request->validate([
            'id' => 'required|sometimes|integer|exists:residences,id',
        ]);
        $residence=Residences::find($validatedData->id);
        $residence->update($validatedData);

        return response()->json([
            'message' => 'Successfully updated Residence!',
            'category' => $residence
        ], 201);

    }

    //
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // crear residencia
        $validatedData = $request->validate([
            'name' => 'required',
            'direction' => 'required',
            'enable' => 'required',
            'rif' => 'required',
        ]);

        $residence = Residences::create($validatedData);

        // guardar configuraciones de la residencia
        $configuration_residence = Configurations_residences::create([
                    'idresidence' => $residence->id,
                    'expiration_date' => Carbon::now(),
                    'indexing_method' => 0,
                    'idexchange' => 1,
                    'dollar' => 1,
                    'minimum_rate' => 0,
                    'emit_date' => Carbon::now(),
                    'enabled_payment' => 0,
                    'enabled_residence' => 0,
                    'lock_exchange' => 0,
                    'whatsapp_link' => 0,
                    'telegram_link' => 0,
                    'twitter_link' => 0,
                    'facebook_link' => 0,
                    'instagram_link' => 0,
                    'footer_invoice' => 0,
                    'previous_invoice' => 0
        ]);  

        return response()->json([
            'message' => 'Successfully created Residence!',
            'category' => $residence
        ], 201);
    }


    public function edit(Request $request)
    {        
        $validatedData = $request->validate([
            'id' => 'required|integer|exists:residences,id',
            'name' => 'sometimes|string',
            'rif' => 'sometimes',
            'direction' => 'sometimes',
            'enable' => 'sometimes'
        ]);    
        
        Residences::where('id', $request->id)
            ->update($validatedData);

        return response()->json([
            'message' => 'Successfully update residence'
        ], 201);
    }


    //
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|sometimes|integer|exists:residences,id',
        ]);
        
        $residence = Residences::where('id', $request->id)
                                ->with(['configuration_residence.exchange.historical_exchanges' 
                                            => function ($query) {
                                                    $query->latest('created_at')
                                                    ->first();
                                        }, 'accounts', 'users_residences', 'documents'])
                                ->withCount(['accounts'])
                                ->withCount(['expenses'])
                                ->withCount(['users_residences'])
                                ->withCount(['configuration_residence'])
                                ->get();
        $request->merge(
            [
                'residence_id' => $request->id
            ]
        );
        $properties = Properties::where('residence_id', $request->id)
                                ->where('enable', 1)
                                ->with('user')
                                ->paginate($request->limit);

        $aliquot_sum = Properties::filters($request)
                                ->sum('aliquot');

        return response()->json([
            'number_properties' => 0,
            'residence' => $residence,
            'property' => $properties,
            'aliquot_sum' => $aliquot_sum
        ], 201);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {        
        $request->validate([
            'idresidence' => 'required|sometimes|integer|exists:residences,id',
        ]);
        $residence = Residences::findOrFail($request->idresidence);
        $residence->delete();
        return response()->json(null, 204);
    }



        /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Configurations  $configurations
     * @return \Illuminate\Http\Response
     */
    public function upload_document(Request $request)
    {
        //
        $validatedData = $request->validate([
            'id' => 'required|sometimes|integer|exists:residences,id',
            'file' => 'sometimes|file|max:5120',
            'fileName' => 'sometimes',
            'fileType' => [
                Rule::requiredIf($request->has('file')),
                'string',
                'in:video,image,file,voice,logo,cof,cover,small-logo,infographic,gallery,login,document',
            ],
            'fileResize' => 'sometimes|array',
            'fileResize.width' => [
                Rule::requiredIf($request->has('fileResize')),
                'integer',
            ],
            'fileResize.height' => [
                Rule::requiredIf($request->has('fileResize')),
                'integer',
            ],
        ]);

        $residence = Residences::find($request->id);

        if ($request->has('file')) {
            $residence->addUpload(
                $request->file,
                $residence->getExtraData($request)
            );
        }

        return response()->json([
            'message' => 'Successfully upload document',
            'residence' => $residence,
        ], 201);

    }

       

    /**
     * SHow a list of users for resience
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function list_users(Request $request)
    {
        $validatedData = $request->validate([
            'type' => 'required',
            'id' => 'required|sometimes|integer|exists:residences,id',
        ]);
        // tipo 1 son los propietarios de una residencia
        // tipo 2 son las cuentas de los usuarios con rol de junta de condominio

        $properties = Residences::where('id', $request->id)
                                ->with('properties', 'users')
                                ->get()->pluck('properties')
                                ->flatten();
                                
        /* $properties = $properties->map(function ($president){
            $user = User::find($president->user_id);
            return [
              'id' => $president->id,
              'name' => $user->name,
              'email' => $user->email,
              'password' => $user->password_decode,
              'number_property' => $president->number_property,
              'aliquot' => $president->aliquot,
            ];
        }); */
        return response()->json([
            'property' => $properties
        ], 201);
    }
}
