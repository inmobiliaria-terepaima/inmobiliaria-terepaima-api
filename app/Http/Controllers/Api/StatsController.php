<?php

namespace App\Http\Controllers\Api;

use App\Models\Concepts;
use App\Models\Payments;
use App\Models\Residences;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;

class StatsController extends Controller
{
    //
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function payment_stats (Request $request)
    {
        $request->validate([
            'order' => 'sometimes|string|in:id,created_at,updated_at',
            'by' => [Rule::requiredIf($request->has('order')), 'string', 'in:asc,desc'],
            'iduser' => 'sometimes|integer|exists:users,id',
            'idaccount_residence' => 'sometimes|integer|exists:accounts_residences,id'
        ]);

        $payments = Payments::filters($request)
                            ->get();

        $counts = $payments->groupBy('status')
                            ->map
                            ->count();

        $data_array = [
            'payment_counter' => $counts,
            'payment_total' => $payments->count()
        ];

        return response()->json([
            'data' => $data_array
        ], 201);
    }


    public function global_stats (Request $request)
    {
        $request->validate([
            'order' => 'sometimes|string|in:id,created_at,updated_at',
            'by' => [Rule::requiredIf($request->has('order')), 'string', 'in:asc,desc'],
            'iduser' => 'sometimes|integer|exists:users,id',
            'idaccount_residence' => 'sometimes|integer|exists:accounts_residences,id'
        ]);

        $residences = Residences::where('enable', 1)->count();

        $users = User::get();

        $counts = $users->groupBy('role')
                            ->map
                            ->count();

        $data_array = [
            'user_counter' => $counts,
            'user_total' => $users->count(),
            'residence_total' => $residences
        ];

        return response()->json([
            'data' => $data_array
        ], 201);
    }

        //
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function payment_historics (Request $request)
    {
        $request->validate([
            'order' => 'sometimes|string|in:id,created_at,updated_at',
            'by' => [Rule::requiredIf($request->has('order')), 'string', 'in:asc,desc'],
            'iduser' => 'sometimes|integer|exists:users,id',
            'idaccount_residence' => 'sometimes|integer|exists:accounts_residences,id'
        ]);

        $payments_month = Payments::filters($request)
                            ->select(
                                DB::raw('sum(amount) as total'), 
                                DB::raw('count(id) as count'), 
                                DB::raw("DATE_FORMAT(created_at,'%M %Y') as months")
                          )
                          ->groupBy('months')
                          ->get();

        $historic_day = Payments::filters($request)
                            ->select(
                                DB::raw('sum(amount) as total'), 
                                DB::raw('count(id) as count'), 
                                DB::raw("DATE_FORMAT(created_at,'%d %M %Y') as days")
                          )
                          ->groupBy('days')
                          ->get();
        // dd($payments);

        $data_array = [
            'historic_month' => $payments_month,
            'historic_day' => $historic_day
        ];

        return response()->json([
            'data' => $data_array
        ], 201);
    }
}
