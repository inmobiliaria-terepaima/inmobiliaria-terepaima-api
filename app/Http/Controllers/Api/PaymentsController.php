<?php

namespace App\Http\Controllers\Api;

use App\Models\Payments;
use App\Models\User;
use App\Models\Balances;
use App\Models\Invoices;
use App\Models\Properties;
use App\Models\Configurations_residences;
use App\Models\Historical_exchanges;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use PDF;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class PaymentsController extends Controller
{

    public function index(Request $request)
    {
        $request->validate([
            'order' => 'sometimes|string|in:id,created_at,updated_at',
            'by' => [Rule::requiredIf($request->has('order')), 'string', 'in:asc,desc'],
            'idresidence' => 'sometimes|integer|exists:residences,id',
        ]);

        $payment = Payments::filters($request)
                ->with(['user'])
                ->paginate(10);

        return response()->json([
            'pending_payments' => count(Payments::where('status', 0)->get()),
            'payments' => $payment
        ], 201);
    }

    public function massive_payouts (Request $request)
    {
        /* $request->validate([
            'file' => 'required|mimes:csv,txt,pdf,xls,xlsx|max:2048'
        ]); */

        if ($file = $request->file('file')) {

            //upload file in public path
            $name = md5($file->getClientOriginalName()).'.'.$file->getClientOriginalExtension();
            $path = $file->move(public_path('files'), $name);
            $uploadFile = public_path('files').'/'.$name;
            
            //laod xls  
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($uploadFile);
            $spreadsheet = $spreadsheet->getActiveSheet();
            $data_array =  $spreadsheet->toArray(); 

            dd($data_array);

            $file_array = [
                'original_file' => $file->getClientOriginalName(),
                'new_file' => $name
            ];               

            return response()->json([
                'message' => "File successfully uploaded",
                'file' => $file_array
            ], 201);
        }
    }

    public function change_payment(Request $request)
    {
        $request->validate([
            'status' => 'required|integer',
            'amount' => 'required',
            'id' => 'sometimes|integer|exists:payments,id',
        ]);

        // Declaro variables
        $amount = $request->amount;

        // Obtener data del pago
        $payment_data = Payments::where('id', $request->id)->first();
              
        //Obtener propiedades del usuario
        $properties = User::where('id', $payment_data->iduser)
                            ->with(['properties'])
                            ->get(); 

        // Validar que el estatus del pago sea aprobado para insertar en el balance
        if($request->status == 1){ 

            // Recorrer la lista de propiedades del usuario 
            // (REPETIR TODO EL ALGORITMO POR CADA PROPIEDAD DEL USUARIO)      
            foreach ($properties[0]->properties as $property) {
                //INICIO

                // Obtener el saldo del propietario
                $balance = Balances::where('property_id', $property->id)
                                    ->orderby('created_at', 'desc')
                                    ->first();  
                                
                //si el pago es en dolares realizar la conversion 
                if($payment_data->currency_code == 'USD'){

                    // Buscar las configuraciones de la residencia
                    $configuration = Configurations_residences::where('idresidence', $property->residence_id)
                                                                ->first();  

                    // Obtener la ulitma tasa
                    $rate_exchange = Historical_exchanges::where('idexchange', $configuration->idexchange)
                                        ->orderby('created_at', 'desc')
                                        ->first();  

                    // Convertir el monto
                    $amount = $rate_exchange->amount * $amount;
                }
                
                //buscar todos los recibos de la propiedad con el status 3 y 5 
                $fiters = ['idproperty' => $property->id, 'status' => 3, 'status' => 5];
                $invoices = Invoices::where('idproperty', $fiters)
                                    ->get();  

                //recorrer todas los recibos            
                foreach ($invoices as $invoice) {
                    // validar si el pago supera el monto del recibo
                    if($amount > $invoice->amount){
                        // Cambiar el estatus a PAGADO
                        Invoices::where('id', $invoice->id)
                                ->update(['status' => 4]);
                    }
                }

                //Si el pago es aprobado, hacer el cargo en el balance 
                $amount_balance = $balance->balance - $amount;                           
                $values = ['idpayment' => $request->id, 
                            'idinvoice' => 0, 
                            'property_id' => $property->id, 
                            'expense' => 0, 
                            'income' => $amount, 
                            'balance' => $amount_balance, 
                            'type' => 1, 
                            'currency_code' => 'VES', 
                            'idexpense' => 0, 
                            'idresidence' => $property->residence_id];
                Balances::create($values);
                       
                //FIN
            }
        }
        
        // Actualizar Estatus 
        $values = array(
                            'status'=> $request->status
                        );

        $payment = Payments::where('id', $request->id)
                            ->update($values);

        return response()->json([
            'message' => 'Successfully update Payment'
        ], 201);
    }

    public function show(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|integer|exists:payments,id'
        ]);

        $payment = Payments::where('id', $request->id)
                   ->with(['user.properties.residence', 'account_residence.residence'])
                   ->get();

        return response()->json([
            'payment' => $payment
        ], 201);
    }

    public function export_payments_to_excel()
    {

        $fileName = 'payments.csv';
        $payments = Payments::whereNotNull('iduser')
                            ->with(['user.properties.residence', 'account_residence.residence'])
                            ->get();

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('Propietario', 'Residencia', 'Nro. Apto/Casa', 'Monto', 'Estatus', 'Numero de Referencia', 'Cuenta Bancaria');

        $callback = function() use($payments, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($payments as $payment_data) {

                $row['Propietario']  = $payment_data->user->name;
                $row['Residencia']  = $payment_data->user->properties[0]->residence->name;                
                $row['Nro. Apto/Casa']  = $payment_data->user->properties[0]->number_property;
                $row['Monto']  = $payment_data->amount.' '.$payment_data->currency_code;
                switch ($payment_data->status) {
                    case 0:
                        $row['Estatus']   = 'Esperando aprobación';
                        break;
                    case 1:
                        $row['Estatus']    = 'Aprobado';
                        break;
                    case 2:
                        $row['Estatus']    = 'Rechazado';
                        break;
                    default:
                        $row['Estatus']    = 'Estado desconocido';
                        break;
                }
                $row['Numero de Referencia'] = $payment_data->reference;

                $row['Cuenta Bancaria']  = $payment_data->account_residence->name;

             

                fputcsv($file, array($row['Propietario'], $row['Residencia'], $row['Nro. Apto/Casa'], $row['Monto'], $row['Estatus'], $row['Numero de Referencia']));

            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }


    //
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'iduser' => 'required',
            'idaccount_residence' => 'required',
            'amount' => 'required',
            'date' => 'required',
            'currency_code' => 'required',
            'reference' => 'required|string',
            'status' => 'required',
            'email' => 'required',
        ]);

        $payment = Payments::create($validatedData);
        // Agregar el ID al request
        $request->request->add(['id' => $payment->id]);
        //Llamar al algoritmo de aprobacion de pagos
        $this->change_payment($request);

        return response()->json([
            'message' => 'Successfully created Payment',
            'category' => $payment
        ], 201);
    }



        //
    /**
     * Generate PDF invoice 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function pdf_payment(Request $request)
    {       
        $request->validate([
            'idpayment' => 'required|sometimes|integer|exists:payments,id',
        ]);

        // inicarlizar las variables 
        $payment = Payments::where('id', $request->idpayment)
                            ->with(['account_residence.residence'])
                            ->with(['user'])
                            ->get()
                            ->toArray(); 

        // enviar data a la vista
        $data = [            
            'data' => $payment[0],
        ]; 

        $pdf = PDF::loadView('payment', $data)->stream('Comprobante de pago #'.$request->idpayment.'.pdf');
        $headers = [
            'Content-Type' => 'application/pdf',
        ];
        return $pdf;
    }

}
