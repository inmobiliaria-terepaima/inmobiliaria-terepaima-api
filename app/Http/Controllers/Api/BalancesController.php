<?php

namespace App\Http\Controllers\Api;

use App\Models\Balances;
use App\Models\User;
use App\Models\Properties;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class BalancesController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'order' => 'sometimes|string|in:name,created_at,updated_at',
            'by' => [Rule::requiredIf($request->has('order')), 'string', 'in:asc,desc'],
            'user_id' => 'sometimes|integer|exists:users,id',
            'property_id' => 'sometimes|integer|exists:properties,id',
            'search' => 'sometimes|string',
            'limit' => 'sometimes|integer|gte:1',
            'outstanding' => 'sometimes|boolean',
        ]);

        $balance = Balances::filters($request)
                ->with(['invoice', 'expense_data'])
                ->orderby('id', 'asc')
                ->paginate($request->limit);

        return response()->json($balance, 201);
    }

    //
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'idpayment' => 'required',
            'idinvoice' => 'required',
            'property_id' => 'required',
            'expense' => 'required',
            'income' => 'required',
            'balance' => 'required',
            'type' => 'required',
            'currency_code' => 'required',
            'idexpense' => 'required',
            'idresidence' => 'required',
        ]);
        
        $balance_amount = 0;

        // validar saldo por caso 
        if($request->idexpense != 0){
            // obtener ultimo saldo del fondo 
            $rate_exchange = Balances::where('idexpense', $request->idexpense)
                                        ->orderby('created_at', 'desc')
                                        ->first(); 
            if($request->income > 0)
            {
                $balance_amount = $rate_exchange->balance + $request->income;
            }else{
                $balance_amount = $rate_exchange->balance - $request->expense;
            }            
        }
        $values = ['idpayment' => 0, 
                    'idinvoice' => 0, 
                    'property_id' => 0, 
                    'expense' => $request->expense, 
                    'income' => $request->income, 
                    'balance' => $balance_amount, 
                    'type' => 1, 
                    'currency_code' => 'VES', 
                    'idexpense' => $request->idexpense, 
                    'idresidence' => 0];
        $balance = Balances::create($values);

        return response()->json([
            'message' => 'Successfully created Balance!',
            'category' => $balance
        ], 201);
    }
}
