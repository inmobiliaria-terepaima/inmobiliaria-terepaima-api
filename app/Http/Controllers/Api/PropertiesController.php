<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Properties;
use App\Models\Balances;
use App\Models\Residences;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PropertiesController extends Controller
{
    //
       /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validatedData = $request->validate([
            'user_id' => 'required|sometimes|integer|exists:users,id',
        ]);

        $properties = User::where('id', $request->user_id)
                        ->with(['properties.residence'])
                        ->with(['properties.balances' 
                                            => function ($query) {
                                                    $query->orderBy('id','desc');
                                        }])

                        ->get(); 

        return response()->json([
            'number_properties' => count($properties[0]->properties),
            'property' => $properties
        ]);
    }

    //
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'user_id' => 'required',
            'residence_id' => 'required',
            'aliquot' => 'required',
            'number_property' => 'required',
            'initial_balance' => 'required',
            'enable' => 'required'
        ]);

        $property = Properties::create($validatedData);

        // crear balance 
        Balances::create([ 
            'idpayment' => 0, 
            'idinvoice' => 0, 
            'property_id' => $property->id, 
            'expense' => 0, 
            'income' => $request->initial_balance, 
            'balance' => $request->initial_balance, 
            'type' => 1, 
            'currency_code' => 'VES', 
            'idexpense' => 0, 
            'description' => 'SALDO INICIAL', 
            'idresidence' => $request->residence_id
          ]);

        return response()->json([
            'message' => 'Successfully created Property!',
            'category' => $property
        ], 201);
    }



    public function import (Request $request)
    {
        /* $request->validate([
            'file' => 'required|mimes:csv,txt,pdf,xls,xlsx|max:2048'
        ]); */

        if ($file = $request->file('file')) {

            //upload file in public path
            $name = md5($file->getClientOriginalName()).'.'.$file->getClientOriginalExtension();
            $path = $file->move(public_path('files'), $name);
            $uploadFile = public_path('files').'/'.$name;
            
            //laod xls  
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($uploadFile);
            $spreadsheet = $spreadsheet->getActiveSheet();
            $data_array =  $spreadsheet->toArray(); 

            //variables para estadisticas 
            $repeat_list = [];
            $count_create = 0;

            $control = 0;
            foreach($data_array as $key => $value)
            {
              if($control != 0){

                // validar emails repetidos 
                if(User::where('email', $value[2])->count() == 0)
                {
                    // no existe 
                    if($value[3] == ''){
                        $phone = 0;
                    }else{
                        $phone = $value[3];
                    }
    
                    if($value[4] == ''){
                        $local_phone = 0;
                    }else{
                        $local_phone = $value[4];
                    }
    
                    if($value[1] == ''){
                        $document_number = 0;
                    }else{
                        $document_number = $value[1];
                    }
                    
                    // crear usuario
                    $user = User::create([
                        'name' => $value[0],
                        'email' => $value[2],
                        'email_verified_at' => Carbon::now(),
                        'password' => bcrypt($value[5]),
                        'password_decode' => $value[5],
                        'role' => 3,
                        'phone' => $phone,
                        'local_phone' => $local_phone,
                        'document_number' => $document_number
                    ]);
    
                    // crear propiedad 
                    $property = Properties::create([
                        'user_id' => $user->id,
                        'residence_id' => $request->residence_id,
                        'aliquot' =>  floatval($value[8]),
                        'number_property' => $value[7],
                        'enable' => 1
                    ]);
                    
                    // crear balance 
                    Balances::create([ 
                        'idpayment' => 0, 
                        'idinvoice' => 0, 
                        'property_id' => $property->id, 
                        'expense' => 0, 
                        'income' => 0, 
                        'balance' => 0, 
                        'type' => 1, 
                        'currency_code' => 'VES', 
                        'idexpense' => 0, 
                        'idresidence' => $request->residence_id
                      ]);
                      $count_create ++;
                }else{
                    // existe 
                    // llenar arreglo
                    array_push($repeat_list, $value[0].' / '.$value[2]);
                }                
              }              
              $control ++;
            }

            $file_array = [
                'original_file' => $file->getClientOriginalName(),
                'new_file' => $name
            ];   

            $stats = [
                'create_count' => $count_create,
                'repeat_list' => $repeat_list,
                'repeat_count' => count($repeat_list)
            ];               

            return response()->json([
                'message' => "File successfully uploaded",
                'file' => $file_array,
                'stats' => $stats
            ], 201);
        }
    }


    public function update(Request $request)
    {        
        $validatedData = $request->validate([
            'id' => 'required|sometimes|integer|exists:properties,id',
            'aliquot' => 'sometimes',
            'enable' => 'sometimes'
        ]);    
        
        $property = Properties::where('id', $request->id)
                                ->update($validatedData);

        return response()->json([
            'message' => 'Successfully updated Property',
            'property' => $property
        ], 201);

    }


        //
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|sometimes|integer|exists:properties,id',
        ]);
        $property = Properties::where('id', $request->id)
                                ->with(['balances' 
                                            => function ($query) {
                                                    $query->latest('created_at')
                                                    ->first();
                                        }, 'user', 'residence'])
                                ->get();

        return response()->json([
            'property' => $property
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {        
        $request->validate([
            'idproperty' => 'required|sometimes|integer|exists:properties,id',
        ]);
        $property = Properties::findOrFail($request->idproperty);
        $property->delete();
        return response()->json(null, 204);
    }
}
