<?php

namespace App\Http\Controllers\Api;

use App\Models\Users_residences;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class UsersResidencesController extends Controller
{
    //
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'iduser' => 'required',
            'idresidence' => 'required',
            'type' => 'required',
            'enabled' => 'required',
        ]);

        $residence = Users_residences::create($validatedData);

        return response()->json([
            'message' => 'Successfully created Residence!',
            'category' => $residence
        ], 201);
    }
}
