<?php

namespace App\Http\Controllers\Api;

use App\Models\Concepts;
use App\Models\Payments;
use App\Models\Residences;
use App\Models\Invoices;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use PDF;

class ReportsController extends Controller
{

    //
    /**
     * Generate report invoice in PDF format 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function invoice_report(Request $request)
    {       
        $request->validate([
            'idresidence' => 'required|sometimes|integer|exists:residences,id',
        ]);

        // inicarlizar las variables 
        $invoice = Invoices::filters($request)
                            ->with(['property.user'])
                            ->with(['property.residence'])                            
                            ->with(['invoices_exchanges' 
                                            => function ($query) {
                                                    $query->latest('id')
                                                    ->first();
                                        }])
                            ->get();
        // dd($invoice);
        // enviar data a la vista
        $data = [            
            'data' => $invoice
        ];         

        $pdf  = PDF::loadView('report', $data)
                ->stream('Recibos pendientes '.strtoupper($invoice[0]['property']['residence']['name']).'.pdf');

        $headers = [
            'Content-Type' => 'application/pdf',
        ];

        return $pdf;
    }

}
