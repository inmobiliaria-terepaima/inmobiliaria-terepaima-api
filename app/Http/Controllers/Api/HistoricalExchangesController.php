<?php

namespace App\Http\Controllers\Api;

require '../vendor/autoload.php';
use Goutte\Client;
use App\Models\Historical_exchanges;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;


class HistoricalExchangesController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'order' => 'sometimes|string|in:name,created_at,updated_at',
            'by' => [Rule::requiredIf($request->has('order')), 'string', 'in:asc,desc'],
            'idexchange' => 'sometimes|integer|exists:exchanges,id',
        ]);

        $historical_exchange = Historical_exchanges::filters($request)
                ->with('exchange')
                ->orderby('id', 'asc')
                ->paginate($request->limit);        

        return response()->json($historical_exchange, 201);
    }


    public function update(Request $request)
    {
        $validatedData = $request->validate([
            'amount' => 'sometimes|required|string',
            'idexchange' => 'sometimes|required|integer|exists:exchanges,id',
        ]);

        $historical_exchange = Historical_exchanges::create($validatedData);

        return response()->json([
            'message' => 'Successfully created Historical exchange!',
            'historical' => $historical_exchange
        ], 201);
    }

    //
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'idexchange' => 'required',
            'amount' => 'required',
        ]);

        switch ($validatedData['idexchange']) {            
            case '1':
                // buscar tasa del banco central
                $response = Http::withoutVerifying()->get('https://www.bcv.org.ve/');
                $info = explode('id="dolar"', $response);
                $info2 = explode('strong>', $info[1]);                
                $info3 = str_replace(" ", '', $info2[1]);
                $info3 = str_replace("</", '', $info3);
                $info3 = str_replace(",", '.', $info3);
                $info3 = trim($info3);
                $validatedData['amount'] = $info3;
                break;

            case '2':
                // buscar tasa de dolar today 
                $response = Http::get('https://s3.amazonaws.com/dolartoday/data.json');
                $response_data = json_decode($response);
                $promedio = $response_data->USD->dolartoday;
                $promedio = str_replace(",", '.', $promedio);
                $validatedData['amount'] = $promedio;
                break;
            
            case '3':
                // buscar tasa de dolar today 
                $response = Http::get('https://pydolarvenezuela-api.vercel.app/api/v1/dollar/unit/enparalelovzla');
                $response_data = json_decode($response);
                $promedio = $response_data->price;
                //$promedio = str_replace(",", '.', $promedio);
                $validatedData['amount'] = $promedio;
                break;

            case '4':
                // Calculo de INPC
                $validatedData['amount'] = 1;
                break; 
            
            default:
                // tomar la tasa enviada
                $validatedData['amount'] = 0;
                break;
        }

        $historical_exchange = Historical_exchanges::create($validatedData);

        return response()->json([
            'message' => 'Successfully created Historical exchange!',
            'historical' => $historical_exchange
        ], 201);
    }


    public function updateExchangeRates()
    {

        $response = Http::withoutVerifying()->get('https://www.bcv.org.ve/');
        $info = explode('id="dolar"', $response);
        $info2 = explode('strong>', $info[1]);                
        $info3 = str_replace(" ", '', $info2[1]);
        $info3 = str_replace("</", '', $info3);
        $info3 = str_replace(",", '.', $info3);
        $info3 = trim($info3);
        $historical_exchange = Historical_exchanges::create([
            'amount' => $info3,
            'idexchange' => 1
        ]);
           
        
        // buscar tasa de dolar today 
        $response = Http::get('https://pydolarvenezuela-api.vercel.app/api/v1/dollar/unit/enparalelovzla');
        $response_data = json_decode($response);
        $historical_exchange = Historical_exchanges::create([
            'amount' => $response_data->price,
            'idexchange' => 3
        ]);

        return response()->json([
            'message' => 'Successfully created Historical exchange!'
        ], 201);
    }
}
