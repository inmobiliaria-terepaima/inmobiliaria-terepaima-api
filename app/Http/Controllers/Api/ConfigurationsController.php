<?php

namespace App\Http\Controllers\Api;

use App\Models\Configurations;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use App\Models\Upload;
use Illuminate\Support\Facades\Storage;

class ConfigurationsController extends Controller
{

    public function show()
    {
        $configuration = Configurations::with('logo')
                                        ->with('gallery')
                                        ->with('login')
                                        ->get();
                                          
        if (is_null($configuration)) {
            return response()->json([
                'message' => 'Configuration dont found'
            ], 404);
        }   
        return response()->json($configuration, 201);
    }


    public function gallery()
    {
        $configuration = Configurations::with('logo')
                                        ->with('gallery')
                                        ->with('login')
                                        ->get();
     
        return response()->json([
            'gallery' => $configuration['gallery']
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Configurations  $configurations
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Configurations $configurations)
    {
        //
    }


    public function delete_gallery(Request $request)
    {
        // Validar la solicitud
        $validatedData = $request->validate([
            'idupload' => 'required|integer',
        ]);

        // Obtener el ID del item de upload a eliminar
        $uploadId = $validatedData['idupload'];

        // Buscar el item de upload en la base de datos
        $upload = Upload::find($uploadId);

        // Verificar si se encontró el item
        if (!$upload) {
            return response()->json([
                'message' => 'Upload not found'
            ], 404);
        }

        // Eliminar el item de upload de la base de datos
        $upload->delete();

        return response()->json([
            'message' => 'Upload deleted successfully'
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Configurations  $configurations
     * @return \Illuminate\Http\Response
     */
    public function update_gallery(Request $request)
    {
        //
        /*$validatedData = $request->validate([
            'file' => 'sometimes|file|mimetypes:video/mp4,image/jpeg,image/png|max:95120', // Permitir MP4, JPG, PNG de hasta 95 MB
            'fileType' => [
                Rule::requiredIf($request->has('file')),
                'string',
                'in:video,image,file,voice,logo,cof,cover,small-logo,infographic,gallery,login',
            ],
            'fileResize' => 'sometimes|array',
            'fileResize.width' => [
                Rule::requiredIf($request->has('fileResize')),
                'integer',
            ],
            'fileResize.height' => [
                Rule::requiredIf($request->has('fileResize')),
                'integer',
            ],
        ]);*/


        $configuration = Configurations::find(1);

        if ($request->has('file')) {
            $configuration->addUpload(
                $request->file,
                $configuration->getExtraData($request)
            );
        }
        
        // Subir el archivo mp4 al almacenamiento
        /*$file = $request->file('file');
        $filename = $this->makeFileName($file);
        $path = $file->storeAs('', $filename, 'public');*/

         return response()->json([
            'message' => 'Successfully created Configuration!',
            'configuration' => $configuration,
        ], 201);

    }

    protected function makeFileName($file)
    {
        $name = sha1(time() . $file->getClientOriginalName());
        $extension = $file->getClientOriginalExtension();

        return "{$name}.{$extension}";
    }

}
