<?php

namespace App\Http\Controllers\Api;

use App\Models\Expenses_invoices;
use App\Models\Invoices;
use App\Models\Properties;
use App\Models\Concepts;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\InvoicesController;
use Illuminate\Pagination\LengthAwarePaginator;

class ExpensesInvoicesController extends Controller
{

    public function index(Request $request)
    {
        $request->validate([
            'order' => 'sometimes|string|in:name,created_at,updated_at',
            'by' => [Rule::requiredIf($request->has('order')), 'string', 'in:asc,desc'],
            'idinvoice' => 'sometimes|integer|exists:invoices,id',
        ]);

        $expenses_invoices = Expenses_invoices::filters($request)
                ->with('expense_fee.concept')
                ->with('expense_fee.expense')
                ->with('invoice.property')
                ->orderby('id', 'asc')
                ->paginate(10);
                
        return response()->json($expenses_invoices, 201);
    }




    public function update(Request $request)
    {

        $request->validate([
            'id' => 'sometimes|integer|exists:expenses_invoices,id',
            'idinvoice' => 'sometimes|integer|exists:invoices,id',
            'amount' => 'sometimes',
            'action' => 'required|integer',
            'type' => 'required|integer'
        ]);

        

        // TYPE
        // 1 para actualizar un solo recibo 
        if($request->type == 1){

            switch($request->action) {
                // case(1): //modificar 
                // ejecutar algoritmo para actualizar el recibo
                // $this->update_expenses_invoices($request);

                case(2): //eliminar
                $expense_invoice = Expenses_invoices::findOrFail($request->id);
                $expense_invoice->delete();

                case(3): //modificar la descripcion del gasto 
                $expense_invoice = Expenses_invoices::findOrFail($request->id);
                $expense_invoice->update([
                    'description' => $request->amount
                ]);
            }
            if($request->action != 3){
                // ejecutar algoritmo para actualizar el recibo
                $this->update_expenses_invoices($request);
            }


        // 2 para actualizar todos los gastos del mismo id en el lote de recibos
        }else if($request->type == 2){

            //buscar todos los recibos de ese lote 
            $invoices = Invoices::filters($request)
                ->with('expenses_invoices')
                ->orderby('id', 'asc')
                ->get()
                ->toArray();

            // buscar datos del gasto del recibo 
            $dataExpense = Expenses_invoices::where('id', $request->id)
                                        ->get()
                                        ->toArray();
            
            // recorrer todos los recibos                 
            foreach ($invoices as $invoice) {
                // recorrer los gastos
                foreach($invoice['expenses_invoices'] as $item){
                    // buscar los que tengan el mismo ID del gasto   
                 // dd($item);                 
                    if($item['expense_fee_id'] == $dataExpense[0]['expense_fee_id']){

                            switch($request->action) {
                                // case(1): //modificar 
                                // ejecutar algoritmo para actualizar el recibo
                                // $this->update_expenses_invoices($request);

                                case(2): //eliminar
                                $expense_invoice = Expenses_invoices::findOrFail($item['id']);
                                $expense_invoice->delete();

                                case(3): //modificar la descripcion del gasto 
                                $expense_invoice = Expenses_invoices::findOrFail($item['id']);
                                $expense_invoice->update([
                                    'description' => $request->amount
                                ]);
                            }
                            
                            //actualizar request 
                            $request->merge([
                                'idinvoice' => $item['idinvoice'] ,
                                'id' => $item['id'] 
                            ]);
                            if($request->action != 3){
                                // ejecutar algoritmo para actualizar el recibo
                                $this->update_expenses_invoices($request);
                            }
                    }
                }
            }

        }

        return response()->json([
            'message' => 'Successfully update Expense Payment'
        ], 201);

    }


    //
    /**
     * Algortimo para actualizar el monto del gasto del recibos asi como el monto total del recibo.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function update_expenses_invoices(Request $request)
    {
        //dividir monto por la alicuota
        $dataInvoice = Invoices::where('id', $request->idinvoice)
                                ->with(['property'])
                                ->get()
                                ->toArray();

        // actualizar monto del gasto
        $values1 = $request->amount * ($dataInvoice[0]['property']['aliquot'] / 100);
        $expense_invoice=Expenses_invoices::where('id', $request->id)
                         ->update([
                            'amount_fee' => $request->amount, 
                            'amount' => $values1
                        ]);        

        // Actualizar recibo 
        InvoicesController::update_invoice($request->idinvoice);

    }


        //
       /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'idinvoice' => 'required',
            'expense_fee_id' => 'required',
            'aliquot' => 'required',
            'description' => 'required',
            'amount' => 'required',
            'currency_code' => 'required'
        ]);

        $expense_invoice = Expenses_invoices::create($validatedData);

        return response()->json([
            'message' => 'Successfully created Expenses Invoice!',
            'category' => $expense_invoice
        ], 201);
    }

      
}
