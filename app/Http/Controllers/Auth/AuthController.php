<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Mail\EmailConfirmation;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    /**
     * Registro de usuario
     */
    public function signUp(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string',
            'role' => 'required',
            'phone' => 'required',
            'document_number' => 'required'
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
			'email_verified_at' => Carbon::now(),
            'password' => bcrypt($request->password),
            'password_decode' => $request->password,
            'role' => $request->role,
            'phone' => $request->phone,
            'local_phone' => $request->local_phone,
            'document_number' => $request->document_number
        ]);

        return response()->json([
            'message' => 'Successfully created user!',
            'data' => $user
        ], 201);
    }

    /**
     * Inicio de sesión y creación de token
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);        

        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');

        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        // funcion para agregar el detalle de la permisologia del perfil
        // (PAra veuxy version 8)
        /* $userData = User::where('id', $request->user()->id)
                            ->get()
                            ->map(function ($item) {
                              $item['ability'] = [array(
                                    "action" => "manage",
                                    "subject" => "all",
                                )];
                              return $item;
                            }); */

        // PAra veuxy version 7 hacia abajo
          switch ($request->user()->role) {
              case 1:
                  $textRole = 'admin';
                  break;
              case 2:
                  $textRole = 'residence';
                  break;
              case 3:
                  $textRole = 'property';
                  break;              
              default:
                  $textRole = 'property';
                  break;
          }
         $userData = User::where('id', $request->user()->id)
                            ->get()
                            ->map(function ($item) use ($textRole) {
                              $item['userRole'] = $textRole;
                              return $item;
                            });

        return response()->json([
            'accessToken' => $tokenResult->accessToken,
            'tokenType' => 'Bearer',
            'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString(),
            'data' => $userData
        ]);
    }

    /**
     * Cierre de sesión (anular el token)
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ], 201);
    }

    /**
     * Obtener el objeto User como json
     */
    public function user(Request $request)
    {
        $user = User::where('id', $request->user()->id)
                          ->with('users_residences', 'properties')
                          ->get();

        return response()->json($user[0], 201);
    }

    /**
     * Obtener el objeto User como json por ID
     */
    public function show(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required|integer|exists:users,id'
        ]);

        $user = User::where('id', $request->id)
                     ->with('users_residences', 'properties.residence')
                     ->withCount('properties')
                     ->get();

        return response()->json($user, 201);
    }

    public static function validate_user(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|email'
        ]);
        
        $user = User::where('email', $request->email)->get();

        return response()->json($user, 201);
    }

    public function refresh_token(Request $request)
    {
        // dd($request);
        // $content = $request->all();
        /* $user = User::where('id', $request->refreshToken)->with('users_residences')->get();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();  */

        return response()->json([
            $request->post()
        ]);
    }

    /**
     * Obtener lista de usuarios 
     */
    public function users(Request $request)
    {
        /* $request->validate([
            'role' => 'required|integer',
        ]); */
        $users = User::filters($request)
                        ->with(['properties'])
                        ->paginate($request->limit); 

        return response()->json($users, 201);

    }

    // function for change password in user by id for request get

    public function change_password_user(Request $request)
    {        
        $validatedData = $request->validate([
            'id' => 'required|integer|exists:users,id',
            'password' => 'min:4|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:4'
        ]);    
        
        User::where('id', $request->id)
            ->update([
                'password' => bcrypt($request->password_confirmation), 
                'password_decode' => $request->password_confirmation
            ]);

        return response()->json([
            'message' => 'Successfully update password'
        ], 201);
    }

    // function for edit user by id for request get

    public function edit_user(Request $request)
    {        
        $validatedData = $request->validate([
            'id' => 'required|integer|exists:users,id',
            'name' => 'required|string',
            // 'email' => 'required|string|email|unique:users',
            'local_phone' => 'required',
            'phone' => 'required',
            'document_number' => 'required'
        ]);    
        
        User::where('id', $request->id)
            ->update([
                'name' => $request->name, 
                // 'email' => $request->email,
                'phone' => $request->phone, 
                'local_phone' => $request->local_phone,
                'document_number' => $request->document_number
            ]);

        return response()->json([
            'message' => 'Successfully update user'
        ], 201);
    }
}