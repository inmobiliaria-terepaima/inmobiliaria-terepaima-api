<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Invoices_exchanges extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idinvoice',
        'idhistorical_exchange',
    ];

    /**
     * A Invoice Exchange may have one historical exchange.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function historical_exchange()
    {
        return $this->hasOne(Historical_exchanges::class, 'id');
    }

    public function invoice()
    {
        return $this->hasOne(Invoices::class, 'id');
    }
}
