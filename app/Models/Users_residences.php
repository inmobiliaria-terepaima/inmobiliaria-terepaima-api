<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Users_residences extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes;

        /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'iduser',
        'idresidence',
        'type',
        'enabled',
    ];

        /**
     * The type that belong to the residense.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function residence()
    {
        return $this->belongsTo(Residences::class);
    }
    
}
