<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Expenses_invoices extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idinvoice',
        'expense_fee_id',
        'aliquot',
        'amount_fee',
        'created_at',
        'description',
        'amount',
        'currency_code',
    ];

    /**
     * The type that belong to the payments in invoice.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function expense_fee()
    {
        return $this->belongsTo(Expenses_fees::class);
    }

    public function invoice()
    {
        return $this->belongsTo(Invoices::class, 'idinvoice');
    }

    public function concept()
    {
        return $this->belongsToMany(Concepts::class, 'expenses_fees', 'id', 'idconcept');
    }


        /**
     * Scope a query to filter modules.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilters($query, $request)
    {
        if ($request->has('order')) {
            $query->orderBy($request->order, $request->by);
        } else {
            $query->orderBy('created_at', 'asc');
        }

        if ($request->has('search')) {
            $query->where(function ($q) use ($request) {
                $q->where('created_at', 'LIKE', '%'.$request->search.'%');
            });
        }

        if ($request->has('idinvoice')) {
            $query->where(function ($q) use ($request) {
                $q->where('idinvoice', '=', $request->idinvoice);
            });
        }

        if ($request->has('month')) {
            $query->whereMonth('created_at', $request->month);
        }

        if ($request->has('year')) {
            $query->whereYear('created_at', $request->year);
        }

    }
}
