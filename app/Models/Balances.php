<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Balances extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idpayment',
        'idinvoice',
        'property_id',
        'expense',
        'income',
        'balance',
        'type',
        'idresidence',
        'currency_code',
        'idexpense',
        'description',
    ];

    /**
     * The type that belong to the company.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function properties()
    {
        return $this->belongsTo(Properties::class);
    }

    public function invoice()
    {
        return $this->belongsTo(Invoices::class, 'idinvoice');
    }

    public function expense_data()
    {
        return $this->belongsTo(Expenses::class, 'idexpense');
    }

    /**
     * Scope a query to filter modules.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilters($query, $request)
    {
        if ($request->has('order')) {
            $query->orderBy($request->order, $request->by);
        } else {
            $query->orderBy('created_at', 'asc');
        }

        if ($request->has('search')) {
            $query->where(function ($q) use ($request) {
                $q->where('created_at', 'LIKE', '%'.$request->search.'%');
            });
        }

        if ($request->has('property_id')) {
            $query->where(function ($q) use ($request) {
                $q->where('property_id', '=', $request->property_id);
            });
        }

        if ($request->has('idexpense')) {
            $query->where(function ($q) use ($request) {
                $q->where('idexpense', '=', $request->idexpense);
            });
        }
    }
}
