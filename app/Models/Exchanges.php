<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Exchanges extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'method',
    ];

    /**
     * A Exchanges may have many historics.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function historical_exchanges()
    {
        return $this->hasMany(Historical_exchanges::class, 'idexchange');
    }

    public function historical_exchanges_last() {
        return $this->hasOne(Historical_exchanges::class, 'idexchange')->latest();
    }

        /**
     * Scope a query to filter modules.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilters($query, $request)
    {
        if ($request->has('order')) {
            $query->orderBy($request->order, $request->by);
        } else {
            $query->orderBy('created_at', 'asc');
        }

        if ($request->has('search')) {
            $query->where(function ($q) use ($request) {
                $q->where('created_at', 'LIKE', '%'.$request->search.'%');
            });
        }

        if ($request->has('idexchange')) {
            $query->where(function ($q) use ($request) {
                $q->where('id', '=', $request->idexchange);
            });
        }

        if ($request->has('limit')) {
            $query->limit($request->limit);
        }
    }
}
