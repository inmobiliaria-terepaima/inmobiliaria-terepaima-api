<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Configurations_residences extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idresidence',
        'idexchange',
        'expiration_date',
        'indexing_method',
        'dollar',
        'minimum_rate',
        'emit_date',
    ];

    /**
     * The type that belong to the resience.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function residence()
    {
        return $this->belongsTo(Residences::class);
    }

    public function exchange()
    {
        return $this->belongsTo(Exchanges::class, 'idexchange');
    }


            /**
     * Scope a query to filter modules.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilters($query, $request)
    {
        if ($request->has('order')) {
            $query->orderBy($request->order, $request->by);
        } else {
            $query->orderBy('created_at', 'asc');
        }

        if ($request->has('search')) {
            $query->where(function ($q) use ($request) {
                $q->where('created_at', 'LIKE', '%'.$request->search.'%');
            });
        }

        if ($request->has('idresidence')) {
            $query->where(function ($q) use ($request) {
                $q->where('idresidence', '=', $request->idresidence);
            });
        }

        if ($request->has('limit')) {
            $query->limit($request->limit);
        }
    }
}
