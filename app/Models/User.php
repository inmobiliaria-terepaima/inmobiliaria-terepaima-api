<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, HasApiTokens, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'password_decode',
        'role',
        'document_number',
        'phone',
        'local_phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * A user may have many properties.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function properties()
    {
        return $this->hasMany(Properties::class, 'user_id');
    }

    /**
     * The type that belong to the user in residence.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function residences()
    {
        return $this->belongsToMany(Residences::class, 'properties', 'residence_id', 'user_id');
    }

    public function users_residences()
    {
        return $this->belongsToMany(Residences::class, 'users_residences', 'iduser', 'idresidence')->withPivot('type', 'enabled');;
    }

        /**
     * Scope a query to filter modules.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilters($query, $request)
    {
        if ($request->has('order')) {
            $query->orderBy($request->order, $request->by);
        } else {
            $query->orderBy('created_at', 'asc');
        }

        if ($request->has('role')) {
            $query->where(function ($q) use ($request) {
                $q->where('role', '=', $request->role);
            });
        }

        if ($request->has('search')) {
            $query->where(function ($q) use ($request) {
                $q->where('name', 'like', "%" . $request->search . "%")
                ->orWhere('email', 'like', "%" . $request->search . "%")
                ->orWhere('document_number', 'like', "%" . $request->search . "%");
            });
        }

        if ($request->has('limit')) {
            $query->limit($request->limit);
        }
    }
}
