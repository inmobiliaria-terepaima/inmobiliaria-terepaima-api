<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Expenses_fees extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'property_id',
        'concept_id',
        'expense_id',
        'amount',
        'currency_code',
        'date',
        'status'
    ];

    public function concept()
    {
        return $this->belongsTo(Concepts::class);
    }

    public function expense()
    {
        return $this->belongsTo(Expenses::class);
    }

    public function scopeFilters($query, $request)
    {
        if ($request->has('order')) {
            $query->orderBy($request->order, $request->by);
        } else {
            $query->orderBy('created_at', 'asc');
        }

        if ($request->has('search')) {
            $query->where(function ($q) use ($request) {
                $q->where('created_at', 'LIKE', '%'.$request->search.'%');
            });
        }

        if ($request->has('idexpense')) {
            $query->where(function ($q) use ($request) {
                $q->where('expense_id', '=', $request->idexpense);
            });
        }

        if ($request->has('limit')) {
            $query->limit($request->limit);
        }
    }
}
