<?php

namespace App\Models;

use App\Traits\Uploadable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Residences extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes, Uploadable;

    /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'direction',
        'rif',
        'enable'
    ];

    /**
     * A residence may have many accounts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accounts()
    {
        return $this->hasMany(Accounts_residences::class, 'idresidence');
    }

    public function expenses()
    {
        return $this->hasMany(Expenses::class, 'idresidence');
    }

    public function properties()
    {
        return $this->hasMany(Properties::class, 'residence_id');
    }

    public function configuration_residence()
    {
        return $this->hasOne(Configurations_residences::class, 'idresidence');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'properties', 'residence_id', 'user_id');
    }

    public function users_residences()
    {
        return $this->belongsToMany(User::class, 'users_residences', 'idresidence', 'iduser');
    }


        /**
     * Scope a query to filter modules.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilters($query, $request)
    {
        if ($request->has('order')) {
            $query->orderBy($request->order, $request->by);
        } else {
            $query->orderBy('created_at', 'asc');
        }

        if ($request->has('search')) {
            $query->where(function ($q) use ($request) {
                $q->where('name', 'like', "%" . $request->search . "%")
                ->orWhere('rif', 'like', "%" . $request->search . "%");
            });
        }

        if ($request->has('limit')) {
            $query->limit($request->limit);
        }

        if ($request->has('enable')) {
            $query->where(function ($q) use ($request) {
                $q->where('enable', '=', $request->enable);
            });
        }
    }
}
