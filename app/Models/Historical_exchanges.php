<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Historical_exchanges extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idexchange',
        'amount',
    ];

    /**
     * A Residence may have many property.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function exchange()
    {
        return $this->belongsTo(Exchanges::class, 'idexchange');
    }

        /**
     * Scope a query to filter modules.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilters($query, $request)
    {
        if ($request->has('order')) {
            $query->orderBy($request->order, $request->by);
        } else {
            $query->orderBy('created_at', 'asc');
        }

        if ($request->has('search')) {
            $query->where(function ($q) use ($request) {
                $q->where('created_at', 'LIKE', '%'.$request->search.'%');
            });
        }

        if ($request->has('idexchange')) {
            $query->where(function ($q) use ($request) {
                $q->where('idexchange', '=', $request->idexchange);
            });
        }

        if ($request->has('limit')) {
            $query->limit($request->limit);
        }
    }
}
