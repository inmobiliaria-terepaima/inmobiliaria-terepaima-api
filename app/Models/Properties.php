<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Properties extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'residence_id',
        'aliquot',
        'number_property',
        'enable',
    ];


    /**
     * The type that belong to the company.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The type that belong to the residense.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function residence()
    {
        return $this->belongsTo(Residences::class);
    }

    /**
     * A property may have many balances.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function balances()
    {
        return $this->hasMany(Balances::class, 'property_id');
    }


    public function balances_last() {
        return $this->hasOne(Balances::class, 'property_id')->latest();
    }

    /**
     * A property may have many balances.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoices()
    {
        return $this->hasMany(Invoices::class, 'property_id');
    }



    /**
     * Scope a query to filter modules.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilters($query, $request)
    {
        if ($request->has('order')) {
            $query->orderBy($request->order, $request->by);
        } else {
            $query->orderBy('properties.created_at', 'asc');
        }

        if ($request->has('enable')) {
            $query->where(function ($q) use ($request) {
                $q->where('enable', '=', $request->enable);
            });
        }

        if ($request->has('residence_id')) {
            $query->where(function ($q) use ($request) {
                $q->where('residence_id', '=', $request->residence_id);
            });
        }

        if ($request->has('search')) {
            $query->join('users', function($join)
             {
               $join->on('users.id', '=', 'properties.user_id');

             })
             ->select('properties.*') 
             ->where('users.name', 'like', "%" . $request->search . "%")
             ->orWhere('users.email', 'like', "%" . $request->search . "%")
             ->orWhere('properties.number_property', 'like', "%" . $request->search . "%");
        }

        if ($request->has('limit')) {
            $query->limit($request->limit);
        }
    }


}
