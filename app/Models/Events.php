<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Events extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
    ];

    /**
     * The type that belong to the residense.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function residence()
    {
        return $this->belongsTo(Residences::class, 'idresidence');
    }

     /**
     * Scope a query to filter modules.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     * 
     * 
     */
    public function scopeFilters($query, $request)
    {
        if ($request->has('order')) {
            $query->orderBy($request->order, $request->by);
        } else {
            $query->orderBy('created_at', 'asc');
        }

        if ($request->has('search')) {
            $query->where(function ($q) use ($request) {
                $q->where('created_at', 'LIKE', '%'.$request->search.'%');
            });
        }

        if ($request->has('residence_id')) {
            $query->where(function ($q) use ($request) {
                $q->where('residence_id', '=', $request->residence_id);
            });
        }

        if ($request->has('type')) {
            $query->where(function ($q) use ($request) {
                $q->where('type', '=', $request->type);
            });
        }
    }
}
