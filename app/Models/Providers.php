<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Providers extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes;
    
    /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'direction',
        'rif',
        'phone',
        'local_phone',
        'rif',
        'idresidence',
        'email',
        'website',
        'calification',
        'observation',
        'contact_name',
    ];

    /**
     * A Exchanges may have many historics.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function residence()
    {
        return $this->belongsTo(Residences::class, 'idresidence');
    }

    /**
     * Scope a query to filter modules.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilters($query, $request)
    {
        if ($request->has('order')) {
            $query->orderBy($request->order, $request->by);
        } else {
            $query->orderBy('created_at', 'asc');
        }

        if ($request->has('search')) {
            $query->where(function ($q) use ($request) {
                $q->where('number_property', 'LIKE', '%'.$request->search.'%');
            });
        }

        if ($request->has('limit')) {
            $query->limit($request->limit);
        }

        if ($request->has('idresidence')) {
            $query->where(function ($q) use ($request) {
                $q->where('idresidence', '=', $request->idresidence);
            });
        }

    }
}
