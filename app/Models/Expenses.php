<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Expenses extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idresidence',
        'description',
        'amount',
        'type',
        'currency_code',
        'idexpense',
        'status',
        'category',
        'nic',
        'name',
    ];

    /**
     * A Residence may have many properties.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */


    public function exchange()
    {
        return $this->belongsTo(Exchanges::class, 'idexchange');
    }

    public function found()
    {
        return $this->belongsTo(Expenses::class, 'idexpense');
    }

    public function expenses_fee()
    {
        return $this->hasMany(Expenses_fees::class, 'expense_id');
    }

    public function expenses_fee_last() 
    {
        return $this->hasMany(Expenses_fees::class, 'expense_id')->latest();
    }

    public function balances_last() 
    {
        return $this->hasOne(Balances::class, 'idexpense')->latest();
    }

        /**
     * Scope a query to filter modules.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilters($query, $request)
    {
        if ($request->has('order')) {
            $query->orderBy($request->order, $request->by);
        } else {
            $query->orderBy('created_at', 'asc');
        }

        if ($request->has('search')) {
            $query->where(function ($q) use ($request) {
                $q->where('name', 'like', "%" . $request->search . "%");
            });
        }

        if ($request->has('idresidence')) {
            $query->where(function ($q) use ($request) {
                $q->where('idresidence', '=', $request->idresidence);
            });
        }

        if ($request->has('type')) {
            $query->where(function ($q) use ($request) {
                $q->where('type', '=', $request->type);
            });
        }

        if ($request->has('exclude')) {
            if($request->exclude == 1)
            {
                $query->where(function ($q) use ($request) {
                    $q->whereNotIn('type', array(7, 8));
                });
            }
        }

        if ($request->has('idexpense')) {
            $query->where(function ($q) use ($request) {
                $q->where('id', '=', $request->idexpense);
            });
        }

        if ($request->has('limit')) {
            $query->limit($request->limit);
        }
    }
}
