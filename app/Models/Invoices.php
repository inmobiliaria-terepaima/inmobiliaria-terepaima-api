<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;
use Laravel\Passport\HasApiTokens;

class Invoices extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idproperty',
        'expiration_date',
        'amount',
        'currency_code',
        'status',
        'active',
    ];

    /**
     * A Residence may have many property.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function property()
    {
        return $this->belongsTo(Properties::class, 'idproperty');
    }

    /**
     * A Residence may have many property.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expenses_invoices()
    {
        return $this->hasMany(Expenses_invoices::class, 'idinvoice');
    }

    /**
     * A Invoice may have many exchanges.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoices_exchanges()
    {
        // return $this->hasMany(Invoices_exchanges::class, 'idinvoice');
        return $this->belongsToMany(Historical_exchanges::class, 'invoices_exchanges', 'idinvoice', 'idhistorical_exchange');
    }

    /**
     * The type that belong to the expenses in invoice.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function expenses()
    {
        return $this->belongsToMany(Expenses_fees::class, 'expenses_invoices', 'idinvoice', 'expense_fee_id');
    }

    /**
     * The type that belong to the payments in invoice.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function payments()
    {
        return $this->belongsToMany(Payments::class, 'payments_invoices', 'idinvoice', 'idpayment');
    }

    /**
     * Scope a query to filter modules.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilters($query, $request)
    {
        if ($request->has('order')) {
            $query->orderBy($request->order, $request->by);
        } else {
            $query->orderBy('invoices.created_at', 'asc');
        }

        if ($request->has('search')) {
            $query->join('properties', function($join)
             {
               $join->on('properties.id', '=', 'invoices.idproperty');
             })
             ->select('invoices.*') 
             ->where('properties.number_property', 'like', "%" . $request->search . "%")
             ->orWhere('invoices.id', 'like', "%" . $request->search . "%");
        }

        if ($request->has('idproperty')) {
            $query->where(function ($q) use ($request) {
                $q->where('idproperty', '=', $request->idproperty);
            });
        }

        if ($request->has('status')) {
            $query->where(function ($q) use ($request) {
                $q->where('status', '=', $request->status);
            });
        }

        if ($request->has('month')) {
            $query->whereMonth('invoices.created_at', $request->month);
        }

        if ($request->has('year')) {
            $query->whereYear('invoices.created_at', $request->year);
        }

        if ($request->has('start_date')) {
            $query->whereBetween('date', [$request->start_date, $request->end_date]);
        }

        if ($request->has('limit')) {
            $query->limit($request->limit);
        }

        if ($request->has('residence_id')) {
             $query->join('properties', function($join)
             {
               $join->on('properties.id', '=', 'invoices.idproperty');
             })
             ->select('invoices.*') 
             ->where('properties.residence_id', $request->residence_id);
        }

    }
}
