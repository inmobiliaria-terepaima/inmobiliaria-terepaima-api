<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class Payments extends Model
{
    use HasFactory, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are guarded.
     *
     * @var array
     */
    protected $guarded = ['id'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'iduser',
        'idaccount_residence',
        'amount',
        'currency_code',
        'reference',
        'date',
        'status',
        'email',
    ];

    /**
     * The type that belong to the company.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'iduser');
    }

    /**
     * The type that belong to the company.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account_residence()
    {
        return $this->belongsTo(Accounts_residences::class, 'idaccount_residence');
    }

    /**
     * The type that belong to the payments in invoice.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function residence()
    {
        return $this->belongsToMany(Residences::class, 'accounts_residences', 'id', 'idresidence');
    }

    /**
     * The type that belong to the payments in invoice.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property()
    {
        return $this->belongsToMany(Properties::class, 'users', 'id', 'user_id');
    }

    /**
     * The type that belong to the payments in invoice.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function invoices()
    {
        return $this->belongsToMany(Payments::class, 'payments_invoices', 'idpayment', 'idinvoice');
    }

    /**
     * Scope a query to filter modules.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \Illuminate\Http\Request              $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilters($query, $request)
    {
        if ($request->has('order')) {
            $query->orderBy($request->order, $request->by);
        } else {
            $query->orderBy('created_at', 'asc');
        }

        if ($request->has('search')) {
            $query->where(function ($q) use ($request) {
                $q->where('created_at', 'LIKE', '%'.$request->search.'%');
            });
        }

        if ($request->has('iduser')) {
            $query->where(function ($q) use ($request) {
                $q->where('iduser', '=', $request->iduser);
            });
        }

        if ($request->has('idaccount_residence')) {
            $query->where(function ($q) use ($request) {
                $q->where('idaccount_residence', '=', $request->idaccount_residence);
            });
        } 

        if ($request->has('status')) {
            $query->where(function ($q) use ($request) {
                $q->where('status', '=', $request->status);
            });
        }

        if ($request->has('limit')) {
            $query->limit($request->limit);
        }


        if ($request->has('currency_code')) {
            $query->where(function ($q) use ($request) {
                $q->where('currency_code', '=', $request->currency_code);
            });
        }

        if ($request->has('idresidence')) {
             $query->join('accounts_residences', function($join)
             {
               $join->on('accounts_residences.id', '=', 'payments.idaccount_residence');

             })
             ->select('payments.*') 
             ->where('accounts_residences.idresidence', $request->idresidence);
        }

        if ($request->has('start_date')) {
            $query->whereBetween('date', [$request->start_date, $request->end_date]);
        }

    }
}
