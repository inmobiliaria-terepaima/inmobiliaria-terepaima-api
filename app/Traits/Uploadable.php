<?php

namespace App\Traits;

use App\Models\Upload;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Log;

trait Uploadable
{
    /**
     * Define a polymorphic one-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function uploads()
    {
        return $this->morphMany(Upload::class, 'uploadable');
    }

    /**
     * Define a polymorphic one-to-one relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function lastestUpload()
    {
        return $this->morphOne(Upload::class, 'uploadable')->orderBy('updated_at', 'desc');
    }

    /**
     * Define a polymorphic one-to-one relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function logo()
    {
        return $this->morphOne(Upload::class, 'uploadable')->where('type', 'logo');
    }

    /**
     * Define a polymorphic one-to-one relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function login()
    {
        return $this->morphOne(Upload::class, 'uploadable')->where('type', 'login');
    }

    /**
     * Define a polymorphic one-to-one relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function cover()
    {
        return $this->morphOne(Upload::class, 'uploadable')->where('type', 'cover');
    }

    /**
     * Define a polymorphic one-to-one relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this->morphOne(Upload::class, 'uploadable')->where('type', 'image');
    }

    /**
     * Define a polymorphic one-to-one relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function avatar()
    {
        return $this->morphOne(Upload::class, 'uploadable')->where('type', 'avatar');
    }

    /**
     * Define a polymorphic one-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function documents()
    {
        return $this->morphMany(Upload::class, 'uploadable')->where('type', 'document');
    }

    /**
     * Define a polymorphic one-to-many relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function gallery()
    {
        return $this->morphMany(Upload::class, 'uploadable')->where('type', 'gallery');
    }

    /**
     * Adds a upload to a given model.
     *
     * @param UploadedFile $file
     * @param array $data
     *
     * @return bool
     */
    public function addUpload(UploadedFile $file, $data = [])
    {
        if (!Arr::has($data, ['type'])) {
            return false;
        }

        /* if ($file->getClientOriginalExtension() === 'mp4') {
            $filename = $this->makeFileName($file);
            $route = $this->imageFullRoute($filename);

            try {
                $filesystem = Storage::disk('s3');
                $filesystem->putFileAs('', $file, $filename);
            } catch (\Exception $e) {
                Log::error('Error al subir el archivo: ' . $e->getMessage());
                return false;
            }
        } else { */
            $route = (Arr::has($data, ['height', 'width']))
                ? $this->resizeImage($file, $data['width'], $data['height'])
                : $this->uploadFile($file);
        //}

        $this->uploads()->save(
            new Upload([
                'route' => $route,
                'type' => $data['type']
            ])
        );

        return true;
    }

    /**
     * Sync a uploaded file.
     *
     * @param UploadedFile $file
     * @param array $data
     *
     * @return bool
     */
    public function syncUpload(UploadedFile $file, $data = [])
    {
        if (!Arr::has($data, ['type'])) {
            return false;
        }

        $route = (Arr::has($data, ['height', 'width']))
            ? $this->resizeImage($file, $data['width'], $data['height'])
            : $this->uploadFile($file);

        $extraData = ['route' => $route, 'type' => $data['type']];

        if (Arr::has($data, ['upload_id'])) {
            $this->deleteUploadedFile($data['upload_id'])->update($extraData);
        } else {
            $this->uploads()->save(new Upload($extraData));
        }

        return true;
    }

    /**
     * Gets the extra data.
     *
     * @param Request $request
     *
     * @return array
     */
    public function getExtraData(Request $request)
    {
        $extraData = ['type' => $request->fileType];

        if ($request->has('fileResize')) {
            $extraData['width'] = $request->fileResize['width'];
            $extraData['height'] = $request->fileResize['height'];
        }

        if ($request->has('fileId')) {
            $extraData['upload_id'] = $request->fileId;
        }

        return $extraData;
    }

    /**
     * Make a file name, based on the uploaded file.
     *
     * @param UploadedFile $file
     *
     * @return string
     */
    public function makeFileName(UploadedFile $file)
    {
        $name = sha1(time() . $file->getClientOriginalName());
        $extension = $file->getClientOriginalExtension();

        return "{$name}.{$extension}";
    }

    /**
     * Resize the image with the given data.
     *
     * @param UploadedFile $file
     * @param int|string $width
     * @param int|string $height
     *
     * @return string
     */
    protected function resizeImage(UploadedFile $file, $width, $height)
    {
        $filename = $this->makeFileName($file);

        if ($file->getClientOriginalExtension() == 'gif') {
            $image = new \Imagick($file->getRealPath());
            $image = $image->coalesceImages();

            do {
                $image->resizeImage($width, $height, \Imagick::FILTER_BOX, 1);
            } while ($image->nextImage());

            $image = $image->deconstructImages();
            Storage::disk('s3')->put($filename, $image->getImagesBlob());
        } else {
            $image = Image::make($file->getRealPath())->resize($width, $height);
            $imageString = $image->stream()->__toString();

            Storage::disk('s3')->put($filename, $imageString);
        }

        return $this->imageFullRoute($filename);
    }

    /**
     * Uploads a file to the storage.
     *
     * @param UploadedFile $file
     *
     * @return string
     */
    protected function uploadFile(UploadedFile $file)
    {
        $filename = $this->makeFileName($file);
        Storage::disk('s3')->putFileAs('', $file, $filename);

        return $this->imageFullRoute($filename);
    }

    /**
     * Full route of the file.
     *
     * @param string $filename
     *
     * @return string
     */
    protected function imageFullRoute($filename)
    {
        return config('filesystems.disks.s3.url') . '/' . $filename;
    }

    /**
     * Delete a uploaded file.
     *
     * @param int $uploadID
     *
     * @return \App\Models\Upload
     */
    protected function deleteUploadedFile($uploadID)
    {
        $upload = Upload::find($uploadID);

        $filename = Str::of($upload->route)->after('.com/');

        if ($filename != 'user-icon-placeholder.png') {
            Storage::disk('s3')->delete($filename);
        }

        return $upload;
    }
}
